/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */
package com.google.zxing.client.android;

public final class R {
	public static final class array {
		public static final int country_codes = 0x7f0b0000;
	}
	public static final class color {
		public static final int contents_text = 0x7f08000b;
		public static final int encode_view = 0x7f08000c;
		public static final int green = 0x7f08000a;
		public static final int help_button_view = 0x7f08000d;
		public static final int help_view = 0x7f08000e;
		public static final int possible_result_points = 0x7f08000f;
		public static final int result_image_border = 0x7f080010;
		public static final int result_minor_text = 0x7f080011;
		public static final int result_points = 0x7f080012;
		public static final int result_text = 0x7f080013;
		public static final int result_view = 0x7f080014;
		public static final int sbc_header_text = 0x7f080015;
		public static final int sbc_header_view = 0x7f080016;
		public static final int sbc_layout_view = 0x7f080018;
		public static final int sbc_list_item = 0x7f080017;
		public static final int sbc_page_number_text = 0x7f080019;
		public static final int sbc_snippet_text = 0x7f08001a;
		public static final int share_text = 0x7f08001b;
		public static final int status_text = 0x7f08001c;
		public static final int transparent = 0x7f08001d;
		public static final int viewfinder_frame = 0x7f08001e;
		public static final int viewfinder_laser = 0x7f08001f;
		public static final int viewfinder_mask = 0x7f080020;
	}
	public static final class drawable {
		public static final int launcher_icon = 0x7f0200b1;
		public static final int share_via_barcode = 0x7f0200c1;
		public static final int shopper_icon = 0x7f0200c2;
	}
	public static final class id {
		public static final int auto_focus = 0x7f070007;
		public static final int back_button = 0x7f070079;
		public static final int barcode_image_view = 0x7f070058;
		public static final int bookmark_title = 0x7f070052;
		public static final int bookmark_url = 0x7f070053;
		public static final int contents_supplement_text_view = 0x7f070062;
		public static final int contents_text_view = 0x7f070061;
		public static final int decode = 0x7f070008;
		public static final int decode_failed = 0x7f070009;
		public static final int decode_succeeded = 0x7f07000a;
		public static final int done_button = 0x7f07007a;
		public static final int encode_view = 0x7f070069;
		public static final int format_text_view = 0x7f07005a;
		public static final int format_text_view_label = 0x7f070059;
		public static final int help_contents = 0x7f070078;
		public static final int history_detail = 0x7f07007c;
		public static final int history_title = 0x7f07007b;
		public static final int image_view = 0x7f07006a;
		public static final int launch_product_query = 0x7f07000b;
		public static final int meta_text_view = 0x7f070060;
		public static final int meta_text_view_label = 0x7f07005f;
		public static final int page_number_view = 0x7f0700bb;
		public static final int preview_view = 0x7f070055;
		public static final int query_button = 0x7f0700b9;
		public static final int query_text_view = 0x7f0700b8;
		public static final int quit = 0x7f07000c;
		public static final int restart_preview = 0x7f07000d;
		public static final int result_button_view = 0x7f070063;
		public static final int result_list_view = 0x7f0700ba;
		public static final int result_view = 0x7f070057;
		public static final int return_scan_result = 0x7f07000e;
		public static final int search_book_contents_failed = 0x7f07000f;
		public static final int search_book_contents_succeeded = 0x7f070010;
		public static final int share_app_button = 0x7f0700bd;
		public static final int share_bookmark_button = 0x7f0700be;
		public static final int share_clipboard_button = 0x7f0700c0;
		public static final int share_contact_button = 0x7f0700bf;
		public static final int share_text_view = 0x7f0700c1;
		public static final int shopper_button = 0x7f070064;
		public static final int snippet_view = 0x7f0700bc;
		public static final int time_text_view = 0x7f07005e;
		public static final int time_text_view_label = 0x7f07005d;
		public static final int type_text_view = 0x7f07005c;
		public static final int type_text_view_label = 0x7f07005b;
		public static final int viewfinder_view = 0x7f070056;
	}
	public static final class layout {
		public static final int bookmark_picker_list_item = 0x7f03001e;
		public static final int capture = 0x7f030020;
		public static final int encode = 0x7f030024;
		public static final int help = 0x7f03002c;
		public static final int history_list_item = 0x7f03002d;
		public static final int search_book_contents = 0x7f03004d;
		public static final int search_book_contents_header = 0x7f03004e;
		public static final int search_book_contents_list_item = 0x7f03004f;
		public static final int share = 0x7f030050;
	}
	public static final class raw {
		public static final int beep = 0x7f060000;
	}
	public static final class string {
		public static final int app_name = 0x7f09001b;
		public static final int app_picker_name = 0x7f09001c;
		public static final int bookmark_picker_name = 0x7f09001d;
		public static final int button_add_calendar = 0x7f09001e;
		public static final int button_add_contact = 0x7f09001f;
		public static final int button_back = 0x7f090020;
		public static final int button_book_search = 0x7f090021;
		public static final int button_cancel = 0x7f090022;
		public static final int button_clipboard_empty = 0x7f090023;
		public static final int button_custom_product_search = 0x7f090024;
		public static final int button_dial = 0x7f090025;
		public static final int button_done = 0x7f090026;
		public static final int button_email = 0x7f090027;
		public static final int button_get_directions = 0x7f090028;
		public static final int button_google_shopper = 0x7f090029;
		public static final int button_mms = 0x7f09002a;
		public static final int button_ok = 0x7f09002b;
		public static final int button_open_browser = 0x7f09002c;
		public static final int button_product_search = 0x7f09002d;
		public static final int button_search_book_contents = 0x7f09002e;
		public static final int button_share_app = 0x7f09002f;
		public static final int button_share_bookmark = 0x7f090030;
		public static final int button_share_by_email = 0x7f090031;
		public static final int button_share_by_sms = 0x7f090032;
		public static final int button_share_clipboard = 0x7f090033;
		public static final int button_share_contact = 0x7f090034;
		public static final int button_show_map = 0x7f090035;
		public static final int button_sms = 0x7f090036;
		public static final int button_web_search = 0x7f090037;
		public static final int button_wifi = 0x7f090038;
		public static final int contents_contact = 0x7f090039;
		public static final int contents_email = 0x7f09003a;
		public static final int contents_location = 0x7f09003b;
		public static final int contents_phone = 0x7f09003c;
		public static final int contents_sms = 0x7f09003d;
		public static final int contents_text = 0x7f09003e;
		public static final int history_clear_one_history_text = 0x7f090040;
		public static final int history_clear_text = 0x7f09003f;
		public static final int history_email_title = 0x7f090041;
		public static final int history_empty = 0x7f090042;
		public static final int history_empty_detail = 0x7f090043;
		public static final int history_send = 0x7f090044;
		public static final int history_title = 0x7f090045;
		public static final int menu_about = 0x7f090046;
		public static final int menu_encode_mecard = 0x7f090047;
		public static final int menu_encode_vcard = 0x7f090048;
		public static final int menu_help = 0x7f090049;
		public static final int menu_history = 0x7f09004a;
		public static final int menu_settings = 0x7f09004b;
		public static final int menu_share = 0x7f09004c;
		public static final int msg_about = 0x7f09004d;
		public static final int msg_bulk_mode_scanned = 0x7f09004e;
		public static final int msg_camera_framework_bug = 0x7f09004f;
		public static final int msg_default_format = 0x7f090050;
		public static final int msg_default_meta = 0x7f090051;
		public static final int msg_default_mms_subject = 0x7f090052;
		public static final int msg_default_status = 0x7f090053;
		public static final int msg_default_time = 0x7f090054;
		public static final int msg_default_type = 0x7f090055;
		public static final int msg_encode_contents_failed = 0x7f090056;
		public static final int msg_google_books = 0x7f090057;
		public static final int msg_google_product = 0x7f090058;
		public static final int msg_google_shopper_missing = 0x7f090059;
		public static final int msg_install_google_shopper = 0x7f09005a;
		public static final int msg_intent_failed = 0x7f09005b;
		public static final int msg_redirect = 0x7f09005c;
		public static final int msg_sbc_book_not_searchable = 0x7f09005d;
		public static final int msg_sbc_failed = 0x7f09005e;
		public static final int msg_sbc_no_page_returned = 0x7f09005f;
		public static final int msg_sbc_page = 0x7f090060;
		public static final int msg_sbc_searching_book = 0x7f090061;
		public static final int msg_sbc_snippet_unavailable = 0x7f090062;
		public static final int msg_sbc_unknown_page = 0x7f090063;
		public static final int msg_share_explanation = 0x7f090064;
		public static final int msg_share_subject_line = 0x7f090065;
		public static final int msg_share_text = 0x7f090066;
		public static final int msg_sure = 0x7f090067;
		public static final int msg_unmount_usb = 0x7f090068;
		public static final int preferences_actions_title = 0x7f090069;
		public static final int preferences_bulk_mode_summary = 0x7f09006a;
		public static final int preferences_bulk_mode_title = 0x7f09006b;
		public static final int preferences_copy_to_clipboard_title = 0x7f09006c;
		public static final int preferences_custom_product_search_summary = 0x7f09006d;
		public static final int preferences_custom_product_search_title = 0x7f09006e;
		public static final int preferences_decode_1D_title = 0x7f09006f;
		public static final int preferences_decode_Data_Matrix_title = 0x7f090070;
		public static final int preferences_decode_QR_title = 0x7f090071;
		public static final int preferences_front_light_summary = 0x7f090072;
		public static final int preferences_front_light_title = 0x7f090073;
		public static final int preferences_general_title = 0x7f090074;
		public static final int preferences_name = 0x7f090075;
		public static final int preferences_play_beep_title = 0x7f090076;
		public static final int preferences_remember_duplicates_summary = 0x7f090077;
		public static final int preferences_remember_duplicates_title = 0x7f090078;
		public static final int preferences_result_title = 0x7f09007b;
		public static final int preferences_reverse_image_summary = 0x7f090079;
		public static final int preferences_reverse_image_title = 0x7f09007a;
		public static final int preferences_scanning_title = 0x7f09007c;
		public static final int preferences_search_country = 0x7f09007d;
		public static final int preferences_supplemental_summary = 0x7f09007e;
		public static final int preferences_supplemental_title = 0x7f09007f;
		public static final int preferences_vibrate_title = 0x7f090080;
		public static final int result_address_book = 0x7f090081;
		public static final int result_calendar = 0x7f090082;
		public static final int result_email_address = 0x7f090083;
		public static final int result_geo = 0x7f090084;
		public static final int result_isbn = 0x7f090085;
		public static final int result_product = 0x7f090086;
		public static final int result_sms = 0x7f090087;
		public static final int result_tel = 0x7f090088;
		public static final int result_text = 0x7f090089;
		public static final int result_uri = 0x7f09008a;
		public static final int result_wifi = 0x7f09008b;
		public static final int sbc_name = 0x7f09008c;
		public static final int share_name = 0x7f09008d;
		public static final int title_about = 0x7f09008e;
		public static final int wifi_changing_network = 0x7f09008f;
		public static final int wifi_ssid_label = 0x7f090090;
		public static final int wifi_type_label = 0x7f090091;
		public static final int zxing_url = 0x7f090092;
	}
	public static final class xml {
		public static final int preferences = 0x7f050000;
	}
}
