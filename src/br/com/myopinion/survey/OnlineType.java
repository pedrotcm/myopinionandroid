package br.com.myopinion.survey;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import br.com.myopinion.MyOpinionsActivity;
import br.com.myopinion.adapter.QustomDialogBuilder;
import br.com.myopinion.dao.AvailableRepliesDAO;
import br.com.myopinion.dao.BasicDAO;
import br.com.myopinion.dao.MainSurveysDAO;
import br.com.myopinion.dao.ObtainedRepliesDAO;
import br.com.myopinion.dao.QuestionsDAO;
import br.com.myopinion.dao.SurveyRepliesDAO;
import br.com.myopinion.model.MainSurveys;
import br.com.myopinion.model.ObtainedReplies;
import br.com.myopinion.model.Questions;
import br.com.myopinion.model.SurveyReplies;
import br.com.myopinion.questions.MultipleQuestion;
import br.com.myopinion.questions.SelectionQuestion;
import br.com.myopinion.questions.TextQuestion;
import br.com.opala.myopinion.R;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;

public class OnlineType extends SherlockFragmentActivity{
	
	private int contador = 0;
	private SurveyReplies surveyReply;
	
	private Cursor cursor;
	private BasicDAO basicDAO;
	private MainSurveysDAO mainSurveyDao;
	private SurveyRepliesDAO surveyReplyDao;
	private QuestionsDAO questionsDao;
	private AvailableRepliesDAO availableRepliesDao;
	private ObtainedRepliesDAO obtainedRepliesDao;
	private FragmentTransaction ft;
	private FragmentManager fm;
	
	private ProgressDialog pDialog;
	private AlertDialog alert;
	private Handler handler;
	private Boolean flag;
	private LocationManager locationManager; 
	private LocationListener locationListener;	
	private boolean gpsRunning = false;
	private boolean gpsOpened = false;
	private AlertDialog alertDialog;

	private Questions question;
	private Bundle bundle;
	private List qtdQuestions;
	
	private ImageView actionRight;
	private ImageView actionLeft;
	private TextView title;
	private TextView count;
	private Fragment fragment;
	
	private String numberText;
	private int nQuestion;
	private Cursor cursor2;

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		basicDAO.close();
	}
	
	@Override
	public void onCreate(Bundle icicle) {
		super.onCreate(icicle);
		try {
			setContentView(R.layout.activity_fragment_runtime);
	
			basicDAO = new BasicDAO(getApplicationContext());
			basicDAO.open();
			mainSurveyDao = new MainSurveysDAO(getApplicationContext());
			surveyReplyDao = new SurveyRepliesDAO(getApplicationContext());
			questionsDao = new QuestionsDAO(getApplicationContext());
			availableRepliesDao = new AvailableRepliesDAO(getApplicationContext());
			obtainedRepliesDao = new ObtainedRepliesDAO(getApplicationContext());
			
		    fm = getSupportFragmentManager();
			
			View mActionBarView = getLayoutInflater().inflate(R.layout.actionbar_custom_view, null);
			
			ActionBar actionBar = getSupportActionBar();
	        actionBar.setCustomView(mActionBarView);
	        actionBar.setDisplayShowTitleEnabled(false);
	        actionBar.setDisplayShowCustomEnabled(true);
	        actionBar.setDisplayUseLogoEnabled(true);
	        actionBar.setDisplayShowHomeEnabled(true);
	        

			cursor = mainSurveyDao.consultAllMainSurveys();
			cursor.moveToLast();
			
			cursor2 = questionsDao.consultQuestionsByQuestionType("GROUPQUESTIONS",
					"GROUPCADASTRE", cursor.getLong(0));
			cursor2.moveToFirst();
			

			qtdQuestions = questionsDao
					.getListDependentQuestions(cursor2.getLong(0));

		    //qtdQuestions = questionsDao.getListQuestions(cursor.getLong(0));

	        title = (TextView) mActionBarView.findViewById(R.id.title);
	        count = (TextView) mActionBarView.findViewById(R.id.count);
	        	  
	        numberText = null;
	        if (qtdQuestions.size() < 10){
	        	numberText = "01/" + "0" + qtdQuestions.size();
	        } else {
	        	numberText = "01/" + qtdQuestions.size();
	        }
	        count.setText(numberText);
	        
	        actionRight = (ImageView) mActionBarView.findViewById(R.id.actionRight);
	        actionRight.setOnClickListener(new OnClickListener() {
				
	         	@Override
				public void onClick(View v) {
	         		boolean check = false;
	         		nQuestion = 0;
	         		numberText = null;
	         			         		
	         		fragment = fm.findFragmentByTag(String.valueOf(fm.getBackStackEntryCount() - 1));
	         		String className = fragment.getClass().getName().substring(27);
	         		
	         		if (className.equalsIgnoreCase("MultipleQuestion")){
	         			 fragment = (MultipleQuestion) fm.findFragmentByTag(String.valueOf(fm.getBackStackEntryCount() - 1));
	 				    check = ((MultipleQuestion) fragment).buttonSaveCreate();
	 				    nQuestion = ((MultipleQuestion) fragment).num;
	         		} else if (className.equalsIgnoreCase("TextQuestion")){
	         			 fragment = (TextQuestion) fm.findFragmentByTag(String.valueOf(fm.getBackStackEntryCount() - 1));
		 				 check = ((TextQuestion) fragment).buttonSaveCreate();
		 				 nQuestion = ((TextQuestion) fragment).num;
	         		} else if (className.equalsIgnoreCase("SelectionQuestion")){
	         			 fragment = (SelectionQuestion) fm.findFragmentByTag(String.valueOf(fm.getBackStackEntryCount() - 1));
		 				 check = ((SelectionQuestion) fragment).buttonSaveCreate();
		 				 nQuestion = ((SelectionQuestion) fragment).num;
	         		}
	         			         		
	         		if (check == true){
	         			if(fm.getBackStackEntryCount() == qtdQuestions.size()){
	         		        actionRight.setBackgroundResource(Color.TRANSPARENT);
	         				//actionRight.setVisibility(View.GONE);
		         			title.setTextColor(Color.TRANSPARENT);
		         			//title.setVisibility(View.GONE);
		         			count.setVisibility(View.GONE);
		         		}
	         			
	         			if (fm.getBackStackEntryCount() > 0){
	         				nQuestion = nQuestion + 1;
	         				if (qtdQuestions.size() < 10){
	         					if (nQuestion < 10)
	         						numberText = "0" + nQuestion + "/" + "0" + qtdQuestions.size();
	         					else 
	         						numberText = nQuestion + "/" + "0" + qtdQuestions.size();
	         		        } else {
	         		        	if (nQuestion < 10)
	         						numberText = "0" + nQuestion + "/" + qtdQuestions.size();
	         					else 
	         						numberText = "" + nQuestion + "/" + qtdQuestions.size();
	         		        }
	         		        count.setText(numberText);
	        				actionLeft.setBackgroundResource(R.drawable.button_left);
	         				actionLeft.setVisibility(View.VISIBLE);
	         				title.setTextColor(Color.TRANSPARENT);
		        			//title.setVisibility(View.GONE);
		        		}
	         		}
	    	       
				}
	        });
	        
	        actionLeft = (ImageView) mActionBarView.findViewById(R.id.actionLeft);
	        actionLeft.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
     				onBackPressed();
				}
	        });
	        
			if(fm.getBackStackEntryCount() == 0){
		        actionLeft.setBackgroundResource(Color.TRANSPARENT);
				//actionLeft.setVisibility(View.GONE);
			}
	        
		  //  Fragment        fragment = fm.findFragmentById(R.id.fragment_content);
 				    				    
			//List listQuestions = questionsDao.getListQuestions(cursor.getLong(0));	
		    
			
			List listSurveyReply = surveyReplyDao.getListSurveyReplies(cursor.getLong(0));
		    Iterator<SurveyReplies> it = listSurveyReply.iterator();
			while (it.hasNext()) {
				surveyReply = (SurveyReplies) it.next();		
			}		
			
			
			question = (Questions) qtdQuestions.get(contador);
			bundle = new Bundle();
			bundle.putSerializable("question", question);
			bundle.putInt("contador", contador);
			bundle.putSerializable("surveyReply", surveyReply);
			bundle.putParcelableArrayList("qtdQuestions", (ArrayList<? extends Parcelable>) qtdQuestions);
			
			if (cursor.getString(9) != null){
				dialogDescription();
			}
			
			if (cursor.getInt(10) == 1){
				startGPS(LocationManager.NETWORK_PROVIDER);
			} 	 
			
			createFirstQuestion(question, bundle);		
			
		} catch (Throwable t) {
			Log.e("MyOpinions", t.getMessage(), t);
		}
	//	db.close();
		//finish();
	}
	
	private void dialogDescription(){
		
		final String COLOR = "#A8Cf45";
		
		final QustomDialogBuilder qustomDialogBuilder = (QustomDialogBuilder) new QustomDialogBuilder(this).
				setTitleColor(COLOR).
				setDividerColor(COLOR).
				setTitle(cursor.getString(1)).
				setMessage(cursor.getString(9)).
				setCancelable(false).
				setPositiveButton("OK",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {
						alertDialog.dismiss();
					}
				  });
				//setCustomView(R.layout.example_ip_address_layout, this);
				//setIcon(getResources().getDrawable(R.drawable.ic_launcher));
		alertDialog = qustomDialogBuilder.create();
		alertDialog.show();
		
		/*AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
			// set title
			alertDialogBuilder.setTitle(cursor.getString(1));
			alertDialogBuilder.setMessage(cursor.getString(9));

			// set dialog message
			alertDialogBuilder
				//.setMessage("Deseja responder o questionário novamente?")
				.setCancelable(false)
				.setPositiveButton("OK",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {
						alertDialog.dismiss();
					}
				  });
				

				// create alert dialog
				alertDialog = alertDialogBuilder.create();
				// show it
				alertDialog.show();		*/
	}
	
	private void createFirstQuestion(Questions question, Bundle bundle){

		if (question.getQuestionType().equalsIgnoreCase("MULTIPLE_CHOISE")) {
 		   //set Fragmentclass Arguments
			MultipleQuestion multipleQuestion = new MultipleQuestion();
			multipleQuestion.setArguments(bundle);
			
			ft = fm.beginTransaction();
			ft.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right);
			ft.add(R.id.fragment_content, multipleQuestion, String.valueOf(fm.getBackStackEntryCount()));
			ft.addToBackStack(null);
			ft.commit();
			
			} else if (question.getQuestionType().equalsIgnoreCase("TEXT")) {  		    		
			//set Fragmentclass Arguments
			TextQuestion textQuestion = new TextQuestion();
			textQuestion.setArguments(bundle);
			
			ft = fm.beginTransaction();
			ft.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right);
			ft.add(R.id.fragment_content, textQuestion, String.valueOf(fm.getBackStackEntryCount()));
			ft.addToBackStack(null);
			ft.commit();
			
			} else if (question.getQuestionType().equalsIgnoreCase("BOXES")) {
			SelectionQuestion selectionQuestion = new SelectionQuestion();
			selectionQuestion.setArguments(bundle);
			
			ft = fm.beginTransaction();
			ft.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right);
			ft.add(R.id.fragment_content, selectionQuestion, String.valueOf(fm.getBackStackEntryCount()));
			ft.addToBackStack(null);
			ft.commit();

}
	}
	
	/* @Override
		protected void onResume() {
			 System.out.println("RESUME");
			 flag = displayGpsStatus(getApplicationContext());
			 if ((flag == false) && (cursor.getInt(10) == 1)){
				 actionGPS();
			 } else if ((flag == true) && (cursor.getInt(10) == 1)){
				 actionGPS();
				 createFirstQuestion(question, bundle);
			 }
			 super.onResume();
	 }*/
	
	 private void actionGPS(){
			flag = displayGpsStatus(getApplicationContext());
			if (flag) {
				/*pDialog = new ProgressDialog(this);
				pDialog.setMessage("Iniciando o questionário. Por favor aguarde...");
				pDialog.setIndeterminate(false);
				pDialog.setCancelable(false);
				pDialog.show();*/

				startGPS(LocationManager.GPS_PROVIDER);
			
			} else {
				alertbox("GPS Status!", "Seu GPS está desligado.", this);
			}
	 }
	 
	 private Boolean displayGpsStatus(Context ctx) {
			ContentResolver contentResolver = ctx.getContentResolver();
			boolean gpsStatus = Settings.Secure.isLocationProviderEnabled(
					contentResolver, LocationManager.GPS_PROVIDER);
			if (gpsStatus) {
				return true;

			} else {
				return false;
			}
		}
		
		protected void alertbox(String title, String mymessage, final Context ctx) {
			final String COLOR = "#A8Cf45";
			final QustomDialogBuilder qustomDialogBuilder = (QustomDialogBuilder) new QustomDialogBuilder(this).
					setTitleColor(COLOR).
					setDividerColor(COLOR);
			qustomDialogBuilder.setCancelable(false)
					.setTitle("GPS desativado")
					.setMessage("É necessário ativar o GPS para poder iniciar o questionário.")
					.setPositiveButton("Ativar GPS",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog, int id) {
									// finish the current activity
									// AlertBoxAdvance.this.finish();
									//Intent myIntent = new Intent(
										//	Settings.ACTION_SECURITY_SETTINGS);
									ctx.startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
									//startActivity(myIntent);
									dialog.cancel();
								}
							})
					.setNegativeButton("Cancelar",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog, int id) {
									Intent intent = new Intent (OnlineType.this, MyOpinionsActivity.class);
									intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
									startActivity(intent);		
									finish();
									dialog.cancel();
								}
							});
			AlertDialog alert = qustomDialogBuilder.create();
			alert.show();
		}
		 
	 private void startGPS(String provedor) {
			locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
				
			locationListener = new MyLocationListener();
			locationManager.requestLocationUpdates(provedor, 0, 0, locationListener);
		}

	 
	 private class MyLocationListener implements LocationListener {
						
				@Override
		        public void onLocationChanged (Location loc) {
						
						String latitude = String.valueOf(loc.getLatitude());
						String longitude = String.valueOf(loc.getLongitude());
						String latLong = latitude + " " + longitude;
						Questions questionLoc = new Questions();
						questionLoc.setMain_survey_id(cursor.getLong(0));
						questionLoc.setRequired(false);
						questionLoc.setTitle("Latitude e Longitude:");
						questionLoc.setQuestionType("Coordinates");
						questionLoc.setQuestion_id(cursor2.getLong(0));
						Long idQuestion = questionsDao.createQuestion(questionLoc);
						
						ObtainedReplies obtainedReplies = new ObtainedReplies();
						obtainedReplies.setQuestion_id(idQuestion);
						obtainedReplies.setSurvey_reply_id(surveyReply.getId());
						obtainedReplies.setObtReply(latLong);
						obtainedRepliesDao.createObtReply(obtainedReplies);
						
		         		if (locationListener != null){
		         			locationManager.removeUpdates(locationListener);		         					        						
		         		} else {
		         			locationManager.removeUpdates(this);
		         		}				     		    
		     		    
		        }

				@Override
				public void onProviderDisabled(String provider) {
					// TODO Auto-generated method stub
					
				}

				@Override
				public void onProviderEnabled(String provider) {
					// TODO Auto-generated method stub
					
				}

				@Override
				public void onStatusChanged(String provider, int status, Bundle extras) {
					// TODO Auto-generated method stub
					
				}				
			}
		
	@Override
	public void onBackPressed() {
				        
		if (fm.getBackStackEntryCount() == 2){
	        actionLeft.setBackgroundResource(Color.TRANSPARENT);
	        title.setTextColor(Color.BLACK);
			//actionLeft.setVisibility(View.GONE);
			//title.setVisibility(View.VISIBLE);
		}
		
		if (fm.getBackStackEntryCount() > 1) {
			nQuestion = nQuestion - 1;
			if (qtdQuestions.size() < 10) {
				if (nQuestion < 10)
					numberText = "0" + nQuestion + "/" + "0" + qtdQuestions.size();
				else
					numberText = nQuestion + "/" + "0" + qtdQuestions.size();
			} else {
				if (nQuestion < 10)
					numberText = "0" + nQuestion + "/" + qtdQuestions.size();
				else
					numberText = "" + nQuestion + "/" + qtdQuestions.size();
			}
			count.setText(numberText);
		}
		
		if(fm.getBackStackEntryCount() != qtdQuestions.size()){
	        actionRight.setBackgroundResource(R.drawable.button_right);
 			actionRight.setVisibility(View.VISIBLE);
 			count.setVisibility(View.VISIBLE);
 		}
		  
		if(fm.getBackStackEntryCount() == 1){
			
			 AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);	 
				
				alertDialogBuilder
					.setMessage("O questionário não foi respondido. Deseja realmente sair?")
					.setCancelable(true)
					.setPositiveButton("Sim",new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,int id) {
							// if this button is clicked, close
							// current activity
							
							MainSurveys mainSurvey = mainSurveyDao.getMainSurvey(cursor.getLong(0));
							
							mainSurveyDao.deleteMainSurvey(mainSurvey.getId());
						 
						    Intent intent = new Intent (OnlineType.this, MyOpinionsActivity.class);
							intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
							startActivity(intent);		
							finish();
						}
					  })
					.setNegativeButton("Não",new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,int id) {
							// if this button is clicked, just close
							// the dialog box and do nothing
							dialog.cancel();
						}
					});
	
					// create alert dialog
					AlertDialog alertDialog = alertDialogBuilder.create();
	
					// show it
					alertDialog.show();	
		} else {
		 super.onBackPressed();
		}
	}
	
	 @Override
		public boolean onCreateOptionsMenu(Menu menu) {
			MenuItem item = menu.add(0, 0, 0, "Sair do questionário");

			return super.onCreateOptionsMenu(menu);

		}
	 
	 @Override
	 public boolean onOptionsItemSelected(MenuItem item) {
	    if (item.getItemId() == 0){
	    	leaveDialog();
		}
		return true;
	 }
	 
	 public void leaveDialog(){
			final String COLOR = "#A8Cf45";
			final QustomDialogBuilder qustomDialogBuilder = (QustomDialogBuilder) new QustomDialogBuilder(this).
					setTitleColor(COLOR).
					setDividerColor(COLOR);
			// set title
			qustomDialogBuilder.setTitle("Sair");

			// set dialog message
			qustomDialogBuilder
				.setMessage("O questionário e as respostas serão perdidos. Tem certeza que deseja sair?")
				.setCancelable(true)
				.setPositiveButton("Sim",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {
						mainSurveyDao.deleteMainSurvey(cursor.getLong(0));

					    Intent intent = new Intent (OnlineType.this, MyOpinionsActivity.class);
						intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						startActivity(intent);		
						finish();
					}
				  })
				.setNegativeButton("Não",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {
						dialog.cancel();
					}
				});

				// create alert dialog
				AlertDialog alertDialog = qustomDialogBuilder.create();

				// show it
				alertDialog.show();
		}
}