package br.com.myopinion.survey;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.os.Vibrator;
import android.provider.MediaStore;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.SimpleCursorAdapter.ViewBinder;
import android.widget.TextView;
import android.widget.Toast;
import br.com.myopinion.adapter.AdapterPhotosListView;
import br.com.myopinion.adapter.MyEditText;
import br.com.myopinion.adapter.QustomDialogBuilder;
import br.com.myopinion.adapter.Utility;
import br.com.myopinion.dao.AvailableRepliesDAO;
import br.com.myopinion.dao.BasicDAO;
import br.com.myopinion.dao.CadastreRepliesDAO;
import br.com.myopinion.dao.ObtainedCadastreRepliesDAO;
import br.com.myopinion.dao.ObtainedRepliesDAO;
import br.com.myopinion.dao.OtherQuestionDAO;
import br.com.myopinion.dao.OthersRepliesDAO;
import br.com.myopinion.dao.QuestionsDAO;
import br.com.myopinion.dao.SurveyRepliesDAO;
import br.com.myopinion.model.CadastreReplies;
import br.com.myopinion.model.ObtainedCadastreReplies;
import br.com.myopinion.model.ObtainedReplies;
import br.com.myopinion.model.OtherQuestion;
import br.com.myopinion.model.Questions;
import br.com.myopinion.model.SurveyReplies;
import br.com.myopinion.questions.MapQuestion;
import br.com.opala.myopinion.R;

import com.actionbarsherlock.app.SherlockFragment;

public class InflateSurvey extends SherlockFragment {

	public static final String COLOR = "#A8Cf45";

	private List<EditText> editTextList = new ArrayList<EditText>();
	private List<ListView> listViewMultiple = new ArrayList<ListView>();
	private List<ListView> listViewSelection = new ArrayList<ListView>();
	private List<TextView> textViewList = new ArrayList<TextView>();
	private List<ListView> listViewMultipleOthers = new ArrayList<ListView>();
	private List<ListView> listViewSelectionOthers = new ArrayList<ListView>();
	private List<EditText> editTextListOthers = new ArrayList<EditText>();

	private static List<Long> listIdsCadastreReplies = new ArrayList<Long>();

	private LinearLayout ll;

	private static SurveyReplies surveyReply;
	private int newOrUpdate;
	private String groupType;

	private int qntBlankReplies = 0; // qnt questoes em branco
	private ArrayList<Double> checkRequired; // qnt questoes obrigatorias
	private static int checkModify = 0;
	private BasicDAO basicDAO;
	private AvailableRepliesDAO availableRepliesDao;
	private QuestionsDAO questionsDao;
	private ObtainedRepliesDAO obtainedRepliesDao;
	private SurveyRepliesDAO surveyReplyDao;
	private CadastreRepliesDAO cadastreRepliesDao;
	private ObtainedCadastreRepliesDAO obtCadastreRepliesDao;
	private OthersRepliesDAO othersRepliesDao;
	private OtherQuestionDAO otherQuestionDao;
	private List<Questions> listQuestions = new ArrayList<Questions>();
	private LayoutInflater mInflater;

	private View rootView;
	private ImageView ivPhotoView;
	private String path;
	private EditText etPhotoPath;

	private int checkBlankReplies = 0;
	private int calculated = 0;
	private int calculatedOthers = 0;
	private int qntListOthers;
	private int qntListView;
	private int qntQuestions;

	private int year;
	private int month;
	private int day;
	private DatePickerDialog.OnDateSetListener datePickerListener;
	private EditText setDate;

	private int angle;
	private ListView lvPhotos;
	private ArrayList<String> listPhotoPaths;
	private AdapterPhotosListView adapterPhotos;
	private ArrayAdapter<String> adapterOthers;

	private TextView textView;
	private ListView listCadastre;
	private Long idGroupQuestion;
	private Cursor cursor;
	private long idCadastreReply;
	private static int qntCadastreReplies;
	private int required;

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		setReplies();
	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		// basicDAO.close();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		listIdsCadastreReplies = new ArrayList<Long>();
		checkModify = 0;
		calculated = 0;
		rootView = inflater.inflate(R.layout.inflate_survey, container, false);
		getActivity().getApplicationContext().setTheme(R.style.Theme_Styled);

		mInflater = LayoutInflater.from(getActivity().getApplicationContext());

		openDataBase();

		Bundle extras = getArguments();
		surveyReply = (SurveyReplies) extras.getSerializable("surveyReply");
		newOrUpdate = (Integer) extras.get("newOrUpdate");
		groupType = (String) extras.get("groupType");
		idGroupQuestion = (Long) extras.get("idGroupQuestion");
		required = extras.getInt("required");
		listQuestions = (List<Questions>) extras.getSerializable("questions");

		qntQuestions = 0;
		// List<Questions> listQuestions =
		// questionsDao.getListQuestions(surveyReply.getMain_survey_id());

		qntListView = countListView(listQuestions);
		qntListOthers = countListOthers(listQuestions);

		ll = (LinearLayout) rootView.findViewById(R.id.linearLayout);

		if (groupType.equalsIgnoreCase("GROUPQUESTIONS")) {

			createQuestions((ArrayList<Questions>) listQuestions, ll, mInflater);

		} else if (groupType.equalsIgnoreCase("GROUPCADASTRE")) {

			View view = mInflater.inflate(R.layout.group_cadastre, null);
			Button bt = (Button) view.findViewById(R.id.bt_group_cadastre);
			
			textView = (TextView) view.findViewById(R.id.label_group_replies);
			
			listCadastre = (ListView) view.findViewById(R.id.list_cadastre);
			cursor = cadastreRepliesDao
					.consultCadastreRepliesByCadastreQuestionAndSurveyReply(
							idGroupQuestion, surveyReply.getId());
			qntCadastreReplies = cursor.getCount();

			if (!cursor.isAfterLast())
				textView.setVisibility(View.VISIBLE);

			String nameColumn = cadastreRepliesDao.COLUNA_ID;
			String[] from = { nameColumn };
			int[] to = { R.id.text };

			SimpleCursorAdapter adapter = new SimpleCursorAdapter(
					getActivity(), R.layout.row_list, cursor, from, to);

			adapter.setViewBinder(new ViewBinder() {

				public boolean setViewValue(View aView, Cursor aCursor,
						int aColumnIndex) {
					if (aColumnIndex == 0) {
						try {
							String idCadastre = aCursor.getString(aColumnIndex);
							Cursor cursor = obtCadastreRepliesDao
									.consultarObtCadastreReplyByQuestionAndCadastreReply(
											listQuestions.get(0).getId(),
											Long.valueOf(idCadastre));
							TextView textView = (TextView) aView;
							ObtainedCadastreReplies obtCadastreReplies = new ObtainedCadastreReplies();
							obtCadastreReplies = obtCadastreRepliesDao
									.getObtCadastreReply(cursor.getLong(0));

							String label = obtCadastreReplies.getObtReply();
							if (label.length() > 20)
								label = obtCadastreReplies.getObtReply()
										.substring(0, 20) + "...";
							textView.setText(label);
						} catch (Exception e) {
							Log.e("MyOpinions", e.getMessage());
						}
						return true;
					}

					return false;
				}
			});

			listCadastre.setAdapter(adapter);
			Utility.setListViewHeightBasedOnChildren(listCadastre);

			listCadastre.setOnItemClickListener(new OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1,
						int arg2, long arg3) {

					cursor.moveToPosition(arg2);
					idCadastreReply = cursor.getLong(0);

					//MyAlertDialogFragment newFragment = new MyAlertDialogFragment();
					MyAlertDialogFragment dialog = (new MyAlertDialogFragment()).newInstance((ArrayList<Questions>) listQuestions,
									idCadastreReply, listCadastre, textView);
					dialog.show(getFragmentManager(), "dialog");
				}
			});

			listCadastre
					.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
						@Override
						public boolean onItemLongClick(AdapterView<?> arg0,
								View arg1, int pos, long arg3) {
							deleteDialog(pos);
							return true;
						}
					});

			bt.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					//MyAlertDialogFragment newFragment = new MyAlertDialogFragment();
							MyAlertDialogFragment dialog = (new MyAlertDialogFragment()).newInstance((ArrayList<Questions>) listQuestions,
									Long.parseLong("0"), listCadastre, textView);
					dialog.show(getFragmentManager(), "dialog");
				}
			});

			ll.addView(view);

		}
		// createButton();

		// Fragment fragmentButton =
		// getFragmentManager().findFragmentById(R.id.button_fragment_content);
		// Button buttonFragment = (Button)
		// fragmentButton.getView().findViewById(R.id.button_fragment);
		// buttonFragment.setText("Salvar informa����es");
		return rootView;
	}

	public void deletePhotoDialog(int position) {
		final int pos = position;
		final QustomDialogBuilder qustomDialogBuilder = (QustomDialogBuilder) new QustomDialogBuilder(
				getActivity()).setTitleColor(COLOR).setDividerColor(COLOR);
		// set title
		qustomDialogBuilder.setTitle("Excluir");

		// set dialog message
		qustomDialogBuilder
				.setMessage("Tem certeza que deseja excluir?")
				.setCancelable(true)
				.setPositiveButton("Sim",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {

								adapterPhotos.remove(adapterPhotos.getItem(pos));
								adapterPhotos.notifyDataSetChanged();
								Utility.setListViewHeightBasedOnChildren(lvPhotos);

								etPhotoPath.setText(null);
								for (String path : listPhotoPaths) {
									etPhotoPath.append(path + ";");
								}

							}

						})
				.setNegativeButton("Não",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.cancel();
							}
						});

		// create alert dialog
		AlertDialog alertDialog = qustomDialogBuilder.create();

		// show it
		alertDialog.show();
	}

	public void deleteDialog(int position) {
		cursor.moveToPosition(position);


		final QustomDialogBuilder qustomDialogBuilder = (QustomDialogBuilder) new QustomDialogBuilder(
				getActivity()).setTitleColor(COLOR).setDividerColor(COLOR);
		// set title
		qustomDialogBuilder.setTitle("Excluir");

		// set dialog message
		qustomDialogBuilder
				.setMessage("Tem certeza que deseja excluir?")
				.setCancelable(true)
				.setPositiveButton("Sim",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								// if this button is clicked, close
								// current activity
								// MyOpinionsActivity.this.finish();

								cadastreRepliesDao.deleteCadastreReply(cursor
										.getLong(0));
								cursor.requery();

								if (cursor.getCount() == 0)
									textView.setVisibility(View.GONE);
							}
						})
				.setNegativeButton("Não",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								// if this button is clicked, just close
								// the dialog box and do nothing
								dialog.cancel();
							}
						});

		// create alert dialog
		AlertDialog alertDialog = qustomDialogBuilder.create();

		// show it
		alertDialog.show();
	}

	public void openDataBase() {
		// basicDAO = new BasicDAO(getActivity());
		// basicDAO.open();
		surveyReplyDao = new SurveyRepliesDAO(getActivity()
				.getApplicationContext());
		questionsDao = new QuestionsDAO(getActivity().getApplicationContext());
		availableRepliesDao = new AvailableRepliesDAO(getActivity()
				.getApplicationContext());
		obtainedRepliesDao = new ObtainedRepliesDAO(getActivity()
				.getApplicationContext());
		cadastreRepliesDao = new CadastreRepliesDAO(getActivity()
				.getApplicationContext());
		obtCadastreRepliesDao = new ObtainedCadastreRepliesDAO(getActivity()
				.getApplicationContext());
		othersRepliesDao = new OthersRepliesDAO(getActivity()
				.getApplicationContext());
		otherQuestionDao = new OtherQuestionDAO(getActivity()
				.getApplicationContext());
	}

	public void createQuestions(ArrayList<Questions> listQuestions,
			LinearLayout ll, LayoutInflater mInflater) {
		for (int i = 0; i < listQuestions.size(); i++) {
			Questions question = listQuestions.get(i);

			if (!(question.getQuestionType().equalsIgnoreCase("LATITUDE")
					|| question.getQuestionType().equalsIgnoreCase("LONGITUDE") || question
					.getQuestionType().equalsIgnoreCase("COORDINATES"))) {
				int num = i + 1;
				String pergunta = num + ") " + question.getTitle();
				TextView tvQuestion = createTextView(pergunta, ll, mInflater);
				List<String> respostas = availableRepliesDao
						.getListAvailableReplies(question.getId());
				if (question.getQuestionType().equalsIgnoreCase("TEXT")) {
					createEditText(tvQuestion, i, question.getId(), ll,
							mInflater);
				} else if (question.getQuestionType().equalsIgnoreCase(
						"MULTIPLE_CHOISE")) {
					createListView(tvQuestion, i, question.getId(), respostas,
							android.R.layout.simple_list_item_single_choice,
							ListView.CHOICE_MODE_SINGLE, ll, mInflater);
				} else if (question.getQuestionType().equalsIgnoreCase("BOXES")) {
					createListView(tvQuestion, i, question.getId(), respostas,
							R.layout.simple_list_item_multiple_choice,
							ListView.CHOICE_MODE_MULTIPLE, ll, mInflater);
				} else if (question.getQuestionType().equalsIgnoreCase(
						"DATEPICKER")) {
					createDatePicker(tvQuestion, i, question.getId(), ll,
							mInflater);
				} else if (question.getQuestionType().equalsIgnoreCase("PHOTO")) {
					createPhotoQuestion(tvQuestion, i, question.getId(), ll,
							mInflater);
				}

				qntQuestions++;
			}
		}
	}

	public int countListView(List<Questions> listQuestions) {
		int count = 0;
		for (int i = 0; i < listQuestions.size(); i++) {
			Questions question = listQuestions.get(i);
			if (question.getQuestionType().equalsIgnoreCase("MULTIPLE_CHOISE")) {
				count++;
			} else if (question.getQuestionType().equalsIgnoreCase("BOXES")) {
				count++;
			}
		}
		return count;
	}

	public int countListOthers(List<Questions> listQuestions) {
		int count = 0;
		for (int i = 0; i < listQuestions.size(); i++) {
			Questions question = listQuestions.get(i);

			Cursor cAvailable = availableRepliesDao
					.consultAvaRepliesByQuestion(question.getId());
			while (!cAvailable.isAfterLast()) {
				OtherQuestion otherQuestion = otherQuestionDao
						.getOtherQuestion(cAvailable.getLong(0));
				if (otherQuestion != null) {
					count++;
				}
				cAvailable.moveToNext();
			}

		}
		return count;
	}

	public void createDatePicker(TextView tvQuestion, int numberQuestion,
			Long questionId, LinearLayout ll, LayoutInflater mInflater) {
		TAG tag = new TAG();
		tag.numberQuestion = numberQuestion;
		tag.questionId = questionId;
		tag.tvQuestion = tvQuestion;

		View view = mInflater.inflate(R.layout.question_datepicker_offline,
				null);
		EditText editDate = (EditText) view
				.findViewById(R.id.resposta_datepicker);
		editDate.setTag(tag);

		// Button datePicker = (Button)
		// view.findViewById(R.id.button_datepicker);

		editDate.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				setDate = (EditText) v;
				if (setDate.getEditableText().toString().equalsIgnoreCase("")) {
					Calendar calendar = Calendar.getInstance();

					year = calendar.get(Calendar.YEAR);
					month = calendar.get(Calendar.MONTH);
					day = calendar.get(Calendar.DAY_OF_MONTH);
				} else {
					String date = setDate.getEditableText().toString();
					year = Integer.valueOf(date.substring(6, 10));
					month = Integer.valueOf(date.substring(3, 5));
					day = Integer.valueOf(date.substring(0, 2));
					month = month - 1;
				}

				// TODO Auto-generated method stub
				DatePickerDialog datePickerDialog = new DatePickerDialog(
						getActivity(), datePickerListener, year, month, day);
				if (!datePickerDialog.isShowing()) {
					datePickerDialog.show();
				}
			}
		});

		datePickerListener = new DatePickerDialog.OnDateSetListener() {

			@Override
			public void onDateSet(DatePicker view, int syear, int monthOfYear,
					int dayOfMonth) {
				// TODO Auto-generated method stub

				year = syear;
				month = monthOfYear;
				day = dayOfMonth;

				String dia = null;
				int mes = month + 1;
				String mesok = null;
				if (day < 10) {
					dia = "0" + day;
				} else
					dia = String.valueOf(day);
				if (mes < 10) {
					mesok = "0" + mes;
				} else
					mesok = String.valueOf(mes);

				// set selected date into Text View
				setDate.setText(new StringBuilder().append(dia).append("/")
						.append(mesok).append("/").append(year).append(" "));

				TAG tag = (TAG) setDate.getTag();
				if (tag.tvQuestion.getError() != null)
					tag.tvQuestion.setError(null);
				// set selected date into Date Picker
				// date_picker.init(year, month, day, null);
			}
		};

		editTextList.add(editDate);
		ll.addView(view);
	}

	public void createEditText(TextView tvQuestion, int numberQuestion,
			Long questionId, LinearLayout ll, LayoutInflater mInflater) {

		final TAG tag = new TAG();
		tag.numberQuestion = numberQuestion;
		tag.questionId = questionId;
		tag.tvQuestion = tvQuestion;

		Cursor question = questionsDao.consultQuestion(tag.questionId);
		View view = mInflater.inflate(R.layout.edittext, null);

		final MyEditText et = (MyEditText) view.findViewById(R.id.replyEditText);

		et.setOnFocusChangeListener(new View.OnFocusChangeListener() {
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if (!hasFocus) {
					checkModify = 1;
				}
			}
		});
		
		et.setTag(tag);
		et.addTextChangedListener(new TextWatcher() {
			@Override
			public void afterTextChanged(Editable s) {
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				if (s != null && s.length() > 0
						&& tag.tvQuestion.getError() != null) {
					tag.tvQuestion.setError(null);
				}
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}
		});

		if (question.getString(5) != null) {
			if (question.getString(5).equalsIgnoreCase("NUMERIC")) {
				et.setInputType(InputType.TYPE_CLASS_PHONE);
			} else if (question.getString(5).equalsIgnoreCase("EMAIL")) {
				et.setInputType(InputType.TYPE_CLASS_TEXT
						| InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
			}
		}

		editTextList.add(et);
		ll.addView(view);
	}

	private static final int SELECT_PICTURE = 1;
	private static final int TAKE_PHOTO = 0;

	public void createPhotoQuestion(TextView tvQuestion, int numberQuestion,
			Long questionId, LinearLayout ll, LayoutInflater mInflater) {
		TAG tag = new TAG();
		tag.numberQuestion = numberQuestion;
		tag.questionId = questionId;
		tag.tvQuestion = tvQuestion;
		// Cursor question = questionsDao.consultQuestion(tag.questionId);
		View view = mInflater.inflate(R.layout.photo_question, null);
		etPhotoPath = (EditText) view.findViewById(R.id.photoPath);
		etPhotoPath.setTag(tag);
		Button bt = (Button) view.findViewById(R.id.bt_content);

		listPhotoPaths = new ArrayList<String>();
		lvPhotos = (ListView) view.findViewById(android.R.id.list);
		lvPhotos.setDivider(null);
		lvPhotos.setOnItemLongClickListener(new OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> arg0, View view,
					int pos, long arg3) {
				// TODO Auto-generated method stub
				deletePhotoDialog(pos);
				return true;
			}
		});

		bt.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				photoOptions();
			}
		});

		editTextList.add(etPhotoPath);
		ll.addView(view);
	}

	private void photoOptions() {

		AlertDialog.Builder mensagem = new AlertDialog.Builder(getActivity());
		final String[] lista = { "Galeria", "Câmera" };
		mensagem.setTitle("Escolha uma opção");

		mensagem.setItems(lista, new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int item) {
				path = null;
				switch (item) {
				case 0:
					try {
						Intent intent = new Intent();
						intent.setType("image/*");
						intent.setAction(Intent.ACTION_GET_CONTENT);
						startActivityForResult(
								Intent.createChooser(intent, "Localize a foto"),
								SELECT_PICTURE);

					} catch (Exception e) {
						e.printStackTrace();
					}

					break;

				case 1:
					try {
						File dir = new File(
								Environment
										.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
								"MyOpinion");

						if (!dir.exists()) {
							dir.mkdirs();
						}

						String timeStamp = new SimpleDateFormat(
								"yyyyMMdd_HHmmss").format(new Date());

						Intent i = new Intent(
								"android.media.action.IMAGE_CAPTURE");
						path = dir.getPath() + "/" + timeStamp + ".jpg";

						Uri fileUri = Uri.fromFile(new File(path));

						i.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

						startActivityForResult(i, TAKE_PHOTO);
					} catch (Exception e) {
						e.printStackTrace();
					}
					break;
				}
			}
		});

		mensagem.show();

	}

	public void onActivityResult(int request, int result, Intent data) {
		super.onActivityResult(request, result, data);
		if (request == TAKE_PHOTO) {
			if (result == getActivity().RESULT_OK) {
				try {
					if (path != null) {
						listPhotoPaths.add(path);

						adapterPhotos = new AdapterPhotosListView(
								getActivity(), R.layout.row_image,
								listPhotoPaths);
						lvPhotos.setAdapter(adapterPhotos);
						Utility.setListViewHeightBasedOnChildren(lvPhotos);

						TAG tag = (TAG) etPhotoPath.getTag();
						if (tag.tvQuestion.getError() != null)
							tag.tvQuestion.setError(null);
						etPhotoPath.setText(null);
						for (String path : listPhotoPaths) {
							etPhotoPath.append(path + ";");
						}
					} else {
						Toast toast = Toast
								.makeText(
										getActivity(),
										"Foto não pode ser carregada, tente novamente.",
										Toast.LENGTH_LONG);
						toast.setGravity(Gravity.CENTER, 0, 0);
						toast.show();
					}
				} catch (Exception e) {
					Log.w("Exception", e.getStackTrace().toString());
				}

			}
		} else if (request == SELECT_PICTURE) {
			if (result == getActivity().RESULT_OK) {

				Uri uri = data.getData();
				String path = getPath(uri);
				try {
					if (path != null) {
						listPhotoPaths.add(path);
						adapterPhotos = new AdapterPhotosListView(
								getActivity(), R.layout.row_image,
								listPhotoPaths);
						lvPhotos.setAdapter(adapterPhotos);
						Utility.setListViewHeightBasedOnChildren(lvPhotos);

						TAG tag = (TAG) etPhotoPath.getTag();
						if (tag.tvQuestion.getError() != null)
							tag.tvQuestion.setError(null);
						etPhotoPath.setText(null);
						for (String pathPhoto : listPhotoPaths) {
							etPhotoPath.append(pathPhoto + ";");
						}
					} else {
						Toast toast = Toast
								.makeText(
										getActivity(),
										"Foto não pode ser carregada, tente novamente.",
										Toast.LENGTH_LONG);
						toast.setGravity(Gravity.CENTER, 0, 0);
						toast.show();
					}
				} catch (Exception e) {
					Log.w("Exception", e.getStackTrace().toString());
				}
			}
		}

	}

	private String getPath(Uri uri) {

		String[] filePathColumn = { MediaStore.Images.Media.DATA };
		Cursor cursor = getActivity().getContentResolver().query(uri,
				filePathColumn, null, null, null);
		cursor.moveToFirst();
		int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
		String filePath = cursor.getString(columnIndex);
		cursor.close();
		return filePath;

	}

	private Bitmap ajustaFoto(Uri fileUri, String path) {

		getActivity().getContentResolver().notifyChange(fileUri, null);
		ContentResolver cr = getActivity().getContentResolver();

		Bitmap bitmap = null;
		int w = 0;
		int h = 0;
		Matrix mtx = new Matrix();

		float angle = 0;
		try {

			bitmap = android.provider.MediaStore.Images.Media.getBitmap(cr,
					fileUri);
			// captura as dimens���es da imagem

			w = bitmap.getWidth();
			h = bitmap.getHeight();

			// pega o caminho onda a imagem est��� salva
			ExifInterface exif = new ExifInterface(path);
			// pega a orienta������o real da imagem

			int orientation = exif.getAttributeInt(
					ExifInterface.TAG_ORIENTATION,
					ExifInterface.ORIENTATION_NORMAL);
			// gira a imagem de acordo com a orienta������o

			switch (orientation) {

			case 3: // ORIENTATION_ROTATE_180
				angle = 180;
				break;
			case 6: // ORIENTATION_ROTATE_90
				angle = 90;
				break;
			case 8: // ORIENTATION_ROTATE_270
				angle = 270;
				break;
			default: // ORIENTATION_ROTATE_0
				angle = 0;
				break;
			}

		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		FileOutputStream out;
		Bitmap rotatedBitmap = null;
		try {
			out = new FileOutputStream(path);
			// define um indice = 1 pois se der erro vai manter a imagem como
			// est���.
			// Integer idx = 1;
			// reupera as dimens���es da imagem
			w = bitmap.getWidth();
			h = bitmap.getHeight();
			// verifica qual a maior dimens���o e divide pela lateral final para
			// definir qual o indice de redu������o

			if (w > h) {
				if (w > 800) {
					w = 800;
				}

				if (h > 600) {
					h = 600;
				}

			} else {
				if (h > 800) {
					h = 800;
				}

				if (w > 600) {
					w = 600;
				}
			}

			// scale it to fit the screen, x and y swapped because my image is
			// wider than it is tall
			Bitmap scaledBitmap = Bitmap.createScaledBitmap(bitmap, w, h, true);

			// create a matrix object
			Matrix matrix = new Matrix();
			matrix.postRotate(angle); // rotate by angle

			// create a new bitmap from the original using the matrix to
			// transform the result
			rotatedBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0,
					scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix,
					true);

			// salva a imagem reduzida no disco
			rotatedBitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// uma nova instancia do bitmap rotacionado
		return rotatedBitmap;
	}

	public void createListView(final TextView tvQuestion, int numberQuestion,
			Long questionId, List<String> replies, int adapterLayout,
			int choiceMode, LinearLayout ll, LayoutInflater mInflater) {
		final TAG tag = new TAG();
		tag.numberQuestion = numberQuestion;
		tag.questionId = questionId;
		tag.tvQuestion = tvQuestion;
		final ListView lv = new ListView(getActivity().getApplicationContext()) {
			@Override
			protected void onLayout(boolean changed, int l, int t, int r, int b) {
				super.onLayout(changed, l, t, r, b);
				if (calculated < qntListView) {
					int count = getCount();
					int height = getHeight();
					setLayoutParams(new LinearLayout.LayoutParams(
							LinearLayout.LayoutParams.MATCH_PARENT, count
									* height));
					calculated++;
				}
			}
		};

		lv.setTag(tag);

		final Cursor cAvailable = availableRepliesDao
				.consultAvaRepliesByQuestion(questionId);
		String nameColumn = availableRepliesDao.COLUNA_AVAREPLY;
		String[] from = { nameColumn };
		int[] to = { android.R.id.text1 };
		SimpleCursorAdapter adapter = new SimpleCursorAdapter(getActivity(),
				adapterLayout, cAvailable, from, to);

		// ArrayAdapter<String> adapter =
		// new ArrayAdapter<String>(getActivity(), adapterLayout, replies);
		lv.setChoiceMode(choiceMode);
		lv.setDivider(null);
		lv.setAdapter(adapter);
		lv.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View view,
					int position, long id) {

				if (tag.tvQuestion.getError() != null)
					tag.tvQuestion.setError(null);

				checkModify = 1;

				cAvailable.moveToPosition(position);
				OtherQuestion otherQuestion = otherQuestionDao
						.getOtherQuestion(cAvailable.getLong(0));

				ListView lvOthers = null;
				TAGOther tagOther = null;
				TextView tvOthers = null;
				EditText etOthers = null;

				try {
					TAG tag = (TAG) lv.getTag();
					tagOther = tag.tagOther;
					lvOthers = tagOther.lvOthers;
					tvOthers = tagOther.tvOthers;
					etOthers = tagOther.etOthers;
				} catch (NullPointerException e) {
				}

				int choiceMode = 0;
				if (otherQuestion != null) {
					if (otherQuestion.getQuestion_type().equalsIgnoreCase(
							"MULTIPLE_CHOISE")) {
						choiceMode = 1;
						tvOthers.setVisibility(View.VISIBLE);
						lvOthers.setVisibility(View.VISIBLE);
						if (lvOthers.getChoiceMode() == 0)
							lvOthers.setChoiceMode(choiceMode);
					} else if (otherQuestion.getQuestion_type()
							.equalsIgnoreCase("BOXES")) {
						choiceMode = 2;
						tvOthers.setVisibility(View.VISIBLE);
						lvOthers.setVisibility(View.VISIBLE);
						if (lvOthers.getChoiceMode() == 0)
							lvOthers.setChoiceMode(choiceMode);
					} else if (otherQuestion.getQuestion_type()
							.equalsIgnoreCase("TEXT")) {
						tvOthers.setVisibility(View.VISIBLE);
						etOthers.setVisibility(View.VISIBLE);
					}
				} else {
					if (lvOthers != null && lvOthers.getVisibility() == 0) {
						lvOthers.clearChoices();
						lvOthers.setVisibility(View.GONE);
						tvOthers.setVisibility(View.GONE);
					} else if (etOthers != null
							&& etOthers.getVisibility() == 0) {
						etOthers.setText(null);
						etOthers.setVisibility(View.GONE);
						tvOthers.setVisibility(View.GONE);
					}
				}

			}
		});

		if (adapterLayout == android.R.layout.simple_list_item_single_choice)
			listViewMultiple.add(lv);
		else if (adapterLayout == R.layout.simple_list_item_multiple_choice)
			listViewSelection.add(lv);

		ll.addView(lv);

		OtherQuestion otherQuestion;

		while (!cAvailable.isAfterLast()) {
			otherQuestion = otherQuestionDao.getOtherQuestion(cAvailable
					.getLong(0));
			if (otherQuestion != null) {

				if (otherQuestion.getQuestion_type().equalsIgnoreCase(
						"MULTIPLE_CHOICE")
						|| otherQuestion.getQuestion_type().equalsIgnoreCase(
								"BOXES")) {

					List<String> listOthersReplies = othersRepliesDao
							.getListOthersReplies(otherQuestion.getId());
					if (!listOthersReplies.isEmpty()) {
						double numQuestion = Double.valueOf(numberQuestion
								+ ".1");
						String textQuestion = String.valueOf(numQuestion + 1)
								+ ") " + otherQuestion.getTitle();
						TextView tvOthers = createTextView(textQuestion, ll,
								mInflater);
						// tvOthers.setVisibility(View.GONE);

						final TAGOther tagOthers = new TAGOther();

						ListView lvOthers = new ListView(getActivity()
								.getApplicationContext()) {
							@Override
							protected void onLayout(boolean changed, int l,
									int t, int r, int b) {
								super.onLayout(changed, l, t, r, b);
								if (calculatedOthers < qntListOthers) {
									int count = getCount();
									int height = getHeight();
									setLayoutParams(new LinearLayout.LayoutParams(
											LinearLayout.LayoutParams.MATCH_PARENT,
											count * height));
									calculatedOthers++;
								}
							}
						};

						lvOthers.setOnItemClickListener(new OnItemClickListener() {

							@Override
							public void onItemClick(AdapterView<?> arg0,
									View arg1, int arg2, long arg3) {
								// TODO Auto-generated method stub

								if (tagOthers.tvOthers.getError() != null)
									tagOthers.tvOthers.setError(null);

								checkModify = 1;

								SparseBooleanArray checkedItems = lv
										.getCheckedItemPositions();
								if (checkedItems != null)
									if (checkedItems.size() == 0) {
										Toast toast = Toast.makeText(
												getActivity(),
												"Primeiro responda a questão: "
														+ "\n"
														+ tvQuestion.getText()
																.toString(),
												Toast.LENGTH_LONG);
										toast.setGravity(
												Gravity.CENTER_VERTICAL
														| Gravity.CENTER_HORIZONTAL,
												0, 0);
										toast.show();
									} else {
										String avaReplyWithOtherReplies = availableRepliesDao
												.getAvaReply(tagOthers.avaReplyId);
										ListView lv = tagOthers.lvQuestion;
										boolean checkOther = false;
										if (checkedItems != null) {
											for (int i = 0; i < checkedItems
													.size(); i++) {
												if (checkedItems.valueAt(i)) {
													Cursor c = (Cursor) lv
															.getAdapter()
															.getItem(
																	checkedItems
																			.keyAt(i));
													if (c.getString(2)
															.equalsIgnoreCase(
																	avaReplyWithOtherReplies)) {
														checkOther = true;
													}
												}
											}

											if (checkOther == false) {
												Toast toast = Toast
														.makeText(
																getActivity(),
																"Alternativas não disponíveis para a resposta da questão: "
																		+ "\n"
																		+ tvQuestion
																				.getText()
																				.toString(),
																Toast.LENGTH_LONG);
												toast.setGravity(
														Gravity.CENTER_VERTICAL
																| Gravity.CENTER_HORIZONTAL,
														0, 0);
												toast.show();
											}
										}
									}
							}
						});

						if (otherQuestion.getQuestion_type().equalsIgnoreCase(
								"MULTIPLE_CHOISE")) {
							adapterOthers = new ArrayAdapter<String>(
									getActivity(),
									android.R.layout.simple_list_item_single_choice,
									listOthersReplies);
							listViewMultipleOthers.add(lvOthers);
						} else if (otherQuestion.getQuestion_type()
								.equalsIgnoreCase("BOXES")) {
							adapterOthers = new ArrayAdapter<String>(
									getActivity(),
									R.layout.simple_list_item_multiple_choice,
									listOthersReplies);
							listViewSelectionOthers.add(lvOthers);
						}
						lvOthers.setDivider(null);
						lvOthers.setAdapter(adapterOthers);
						// lvOthers.setChoiceMode(ListView.CHOICE_MODE_NONE);

						tagOthers.questionId = questionId;
						tagOthers.avaReplyId = cAvailable.getLong(0);
						tagOthers.tvOthers = tvOthers;
						tagOthers.lvOthers = lvOthers;
						tagOthers.lvQuestion = lv;
						tagOthers.numberQuestion = numQuestion;
						lvOthers.setTag(tagOthers);

						tag.tagOther = tagOthers;
						lv.setTag(tag);

						ll.addView(lvOthers);
					}

				} else if (otherQuestion.getQuestion_type().equalsIgnoreCase(
						"TEXT")) {
					double numQuestion = Double.valueOf(numberQuestion + ".1");
					String textQuestion = String.valueOf(numQuestion + 1)
							+ ") " + otherQuestion.getTitle();
					TextView tvOthers = createTextView(textQuestion, ll,
							mInflater);
					tvOthers.setVisibility(View.GONE);

					View view = mInflater.inflate(R.layout.edittext, null);

					final MyEditText etOthers = (MyEditText) view
							.findViewById(R.id.replyEditText);

					etOthers.setOnFocusChangeListener(new View.OnFocusChangeListener() {
						@Override
						public void onFocusChange(View v, boolean hasFocus) {
							if (!hasFocus) {
								checkModify = 1;
							}
						}
					});
		
					etOthers.setTag(tag);
					etOthers.addTextChangedListener(new TextWatcher() {
						@Override
						public void afterTextChanged(Editable s) {
						}

						@Override
						public void onTextChanged(CharSequence s, int start,
								int before, int count) {
							if (s != null && s.length() > 0
									&& tag.tvQuestion.getError() != null) {
								tag.tvQuestion.setError(null);
							}
						}

						@Override
						public void beforeTextChanged(CharSequence s,
								int start, int count, int after) {
						}
					});

					if (otherQuestion.getTextType() != null) {
						if (otherQuestion.getTextType().equalsIgnoreCase(
								"NUMERIC")) {
							etOthers.setInputType(InputType.TYPE_CLASS_PHONE);
						} else if (otherQuestion.getTextType()
								.equalsIgnoreCase("EMAIL")) {
							etOthers.setInputType(InputType.TYPE_CLASS_TEXT
									| InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
						}
					}

					TAGOther tagOthers = new TAGOther();

					tagOthers.questionId = questionId;
					tagOthers.avaReplyId = cAvailable.getLong(0);
					tagOthers.tvOthers = tvOthers;
					tagOthers.etOthers = etOthers;
					tagOthers.lvQuestion = lv;
					tagOthers.numberQuestion = numQuestion;
					etOthers.setTag(tagOthers);

					tag.tagOther = tagOthers;
					lv.setTag(tag);

					etOthers.setVisibility(View.GONE);

					editTextListOthers.add(etOthers);
					ll.addView(view);
				}
			}
			cAvailable.moveToNext();
		}
	}

	public TextView createTextView(String pergunta, LinearLayout ll,
			LayoutInflater mInflater) {
		View view = mInflater.inflate(R.layout.textview, null);
		TextView tv = (TextView) view.findViewById(R.id.questionTextView);
		tv.setText(pergunta);
		textViewList.add(tv);
		ll.addView(view);

		return tv;
	}

	public void createButton() {
		Button button = new Button(getActivity().getApplicationContext());
		button.setText("Salvar informações");
		button.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				createOrUpdateReplies();

				checkRequired = checkRequired();
				if (checkRequired.isEmpty()) {
					if (newOrUpdate == 0) {
						dialogCreate();
					} else {
						dialogUpdate();
					}
				} else {
					Double pos = checkRequired.get(0) + 1;
					Toast toast = Toast.makeText(getActivity(), "Questão: "
							+ pos + " é obrigatória!", Toast.LENGTH_LONG);
					toast.setGravity(Gravity.CENTER, 0, 0);
					toast.show();
				}
			}
		});

		ll.addView(button);
	}

	public void dialogCreate() {

		final QustomDialogBuilder qustomDialogBuilder = (QustomDialogBuilder) new QustomDialogBuilder(
				getActivity()).setTitleColor(COLOR).setDividerColor(COLOR);
		// set title
		qustomDialogBuilder.setTitle("Informações salvas com sucesso!");

		// set dialog message
		qustomDialogBuilder
				.setMessage("Deseja responder o questionário novamente?")
				.setCancelable(false)
				.setPositiveButton("Sim",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {

								destroyMap();

								final Vibrator vibrator = (Vibrator) getActivity()
										.getSystemService(
												Context.VIBRATOR_SERVICE);
								vibrator.vibrate(50);
								Intent intent = new Intent(getActivity(),
										VerifyLocation.class);
								intent.putExtra("id",
										surveyReply.getMain_survey_id());
								startActivity(intent);

								getActivity().getFragmentManager()
										.popBackStack();
								getActivity().finish();

							}
						})
				.setNegativeButton("Não",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								// if this button is clicked, just close
								// the dialog box and do nothing
								// dialog.cancel();

								destroyMap();

								Intent intent = new Intent(getActivity(),
										ListQuestionnaries.class);
								intent.putExtra("id",
										surveyReply.getMain_survey_id());
								intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
								startActivity(intent);
								getActivity().finish();
							}
						});

		// create alert dialog
		AlertDialog alertDialog = qustomDialogBuilder.create();

		// show it
		alertDialog.show();
	}

	public void dialogUpdate() {

		destroyMap();

		final QustomDialogBuilder qustomDialogBuilder = (QustomDialogBuilder) new QustomDialogBuilder(
				getActivity()).setTitleColor(COLOR).setDividerColor(COLOR);
		// set title
		qustomDialogBuilder.setTitle("Atualizar").setMessage(
				"Informações atualizadas com sucesso!");

		// set dialog message
		qustomDialogBuilder
		// .setMessage("Deseja responder o question��rio novamente?")
				.setCancelable(true).setPositiveButton("OK",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								// if this button is clicked, close
								// current activity
								final Vibrator vibrator = (Vibrator) getActivity()
										.getSystemService(
												Context.VIBRATOR_SERVICE);
								vibrator.vibrate(50);
								getActivity().finish();
							}
						});

		// create alert dialog
		AlertDialog alertDialog = qustomDialogBuilder.create();

		// show it
		alertDialog.show();
	}

	public void createOrUpdateReplies() {

		for (EditText editText : editTextList) {
			TAG tag = (TAG) editText.getTag();

			List<ObtainedReplies> getObtainedReplies = obtainedRepliesDao
					.getListObtainedReplies(0, tag.questionId,
							surveyReply.getId());
			if (getObtainedReplies.size() == 0) {
				ObtainedReplies obtainedReplies = new ObtainedReplies();
				obtainedReplies.setQuestion_id(tag.questionId);
				obtainedReplies.setSurvey_reply_id(surveyReply.getId());
				obtainedReplies.setObtReply(editText.getText().toString());
				obtainedRepliesDao.createObtReply(obtainedReplies);
			} else {
				ObtainedReplies obtainedReplies = (ObtainedReplies) getObtainedReplies
						.get(0);
				obtainedReplies.setQuestion_id(tag.questionId);
				obtainedReplies.setSurvey_reply_id(surveyReply.getId());
				obtainedReplies.setObtReply(editText.getText().toString());
				obtainedRepliesDao.updateObtReply(obtainedReplies);
			}
		}

		for (ListView listView : listViewMultiple) {
			SparseBooleanArray checkedItems = listView
					.getCheckedItemPositions();
			TAG tag = (TAG) listView.getTag();

			if (checkedItems != null) {
				for (int j = 0; j < checkedItems.size(); j++) {
					if (checkedItems.valueAt(j)) {
						Cursor c = (Cursor) listView.getAdapter().getItem(
								checkedItems.keyAt(j));
						String item = c.getString(2);
						List<ObtainedReplies> getObtainedReplies = obtainedRepliesDao
								.getListObtainedReplies(0, tag.questionId,
										surveyReply.getId());
						if (getObtainedReplies.size() == 0) {
							ObtainedReplies obtainedReplies = new ObtainedReplies();
							obtainedReplies.setQuestion_id(tag.questionId);
							obtainedReplies.setSurvey_reply_id(surveyReply
									.getId());
							obtainedReplies.setObtReply(item);
							obtainedRepliesDao.createObtReply(obtainedReplies);
						} else {
							ObtainedReplies obtainedReplies = (ObtainedReplies) getObtainedReplies
									.get(0);
							obtainedReplies.setQuestion_id(tag.questionId);
							obtainedReplies.setSurvey_reply_id(surveyReply
									.getId());
							obtainedReplies.setObtReply(item);
							obtainedRepliesDao.updateObtReply(obtainedReplies);
						}
					}
				}
			}
		}

		for (ListView listView : listViewSelection) {
			SparseBooleanArray checkedItems = listView
					.getCheckedItemPositions();
			TAG tag = (TAG) listView.getTag();

			List<ObtainedReplies> getObtainedReplies = obtainedRepliesDao
					.getListObtainedReplies(0, tag.questionId,
							surveyReply.getId());
			for (int j = 0; j < getObtainedReplies.size(); j++) {
				obtainedRepliesDao.deleteObtReply(getObtainedReplies.get(j)
						.getId());
			}

			if (checkedItems != null) {
				for (int i = 0; i < checkedItems.size(); i++) {
					if (checkedItems.valueAt(i)) {
						Cursor c = (Cursor) listView.getAdapter().getItem(
								checkedItems.keyAt(i));
						String item = c.getString(2);
						ObtainedReplies obtainedReplies = new ObtainedReplies();
						obtainedReplies.setQuestion_id(tag.questionId);
						obtainedReplies.setSurvey_reply_id(surveyReply.getId());
						obtainedReplies.setObtReply(item);
						obtainedRepliesDao.createObtReply(obtainedReplies);
					}
				}
			}
		}

		for (ListView listView : listViewMultipleOthers) {
			SparseBooleanArray checkedItems = listView
					.getCheckedItemPositions();
			TAGOther tag = (TAGOther) listView.getTag();

			if (checkedItems != null) {
				for (int j = 0; j < checkedItems.size(); j++) {
					if (checkedItems.valueAt(j)) {
						String answer = (String) listView.getAdapter().getItem(
								checkedItems.keyAt(j));
						List<ObtainedReplies> getObtainedReplies = obtainedRepliesDao
								.getListObtainedReplies(1, tag.questionId,
										surveyReply.getId());
						if (getObtainedReplies.size() == 0) {
							ObtainedReplies obtainedReplies = new ObtainedReplies();
							obtainedReplies.setQuestion_id(tag.questionId);
							obtainedReplies.setSurvey_reply_id(surveyReply
									.getId());
							obtainedReplies.setObtReply(answer);
							obtainedReplies.setOther_answer(true);
							obtainedRepliesDao.createObtReply(obtainedReplies);
						} else {
							ObtainedReplies obtainedReplies = (ObtainedReplies) getObtainedReplies
									.get(0);
							obtainedReplies.setQuestion_id(tag.questionId);
							obtainedReplies.setSurvey_reply_id(surveyReply
									.getId());
							obtainedReplies.setObtReply(answer);
							obtainedReplies.setOther_answer(true);
							obtainedRepliesDao.updateObtReply(obtainedReplies);
						}
					}
				}
			}
		}

		for (ListView listView : listViewSelectionOthers) {
			SparseBooleanArray checkedItems = listView
					.getCheckedItemPositions();
			TAGOther tag = (TAGOther) listView.getTag();

			List<ObtainedReplies> getObtainedReplies = obtainedRepliesDao
					.getListObtainedReplies(1, tag.questionId,
							surveyReply.getId());
			for (int j = 0; j < getObtainedReplies.size(); j++) {
				obtainedRepliesDao.deleteObtReply(getObtainedReplies.get(j)
						.getId());
			}

			if (checkedItems != null) {
				for (int i = 0; i < checkedItems.size(); i++) {
					if (checkedItems.valueAt(i)) {
						String answer = (String) listView.getAdapter().getItem(
								checkedItems.keyAt(i));
						ObtainedReplies obtainedReplies = new ObtainedReplies();
						obtainedReplies.setQuestion_id(tag.questionId);
						obtainedReplies.setSurvey_reply_id(surveyReply.getId());
						obtainedReplies.setObtReply(answer);
						obtainedReplies.setOther_answer(true);
						obtainedRepliesDao.createObtReply(obtainedReplies);
					}
				}
			}
		}

		for (EditText editText : editTextListOthers) {
			TAGOther tag = (TAGOther) editText.getTag();

			List<ObtainedReplies> getObtainedReplies = obtainedRepliesDao
					.getListObtainedReplies(1, tag.questionId,
							surveyReply.getId());
			if (getObtainedReplies.size() == 0) {
				ObtainedReplies obtainedReplies = new ObtainedReplies();
				obtainedReplies.setQuestion_id(tag.questionId);
				obtainedReplies.setSurvey_reply_id(surveyReply.getId());
				obtainedReplies.setObtReply(editText.getText().toString());
				obtainedReplies.setOther_answer(true);
				obtainedRepliesDao.createObtReply(obtainedReplies);
			} else {
				ObtainedReplies obtainedReplies = (ObtainedReplies) getObtainedReplies
						.get(0);
				obtainedReplies.setQuestion_id(tag.questionId);
				obtainedReplies.setSurvey_reply_id(surveyReply.getId());
				obtainedReplies.setObtReply(editText.getText().toString());
				obtainedReplies.setOther_answer(true);
				obtainedRepliesDao.updateObtReply(obtainedReplies);
			}
		}

	}

	public void setReplies() {

		for (EditText editText : editTextList) {
			TAG tag = (TAG) editText.getTag();

			Questions questions = questionsDao.getQuestion(tag.questionId);
			List<ObtainedReplies> getObtainedReplies = obtainedRepliesDao
					.getListObtainedReplies(0, tag.questionId,
							surveyReply.getId());
			if (getObtainedReplies.size() != 0) {
				editText.setText(getObtainedReplies.get(0).getObtReply(),
						TextView.BufferType.EDITABLE);
			}

			if (questions.getQuestionType().equalsIgnoreCase("PHOTO")) {
				if (getObtainedReplies.size() != 0) {
					if (!getObtainedReplies.get(0).getObtReply()
							.equalsIgnoreCase("")) {
						String obtPaths = getObtainedReplies.get(0)
								.getObtReply();
						String[] paths = obtPaths.split(";");
						for (String p : paths) {
							listPhotoPaths.add(p);
						}

						adapterPhotos = new AdapterPhotosListView(
								getActivity(), R.layout.row_image,
								listPhotoPaths);
						lvPhotos.setAdapter(adapterPhotos);
						Utility.setListViewHeightBasedOnChildren(lvPhotos);
					}
				}
			}

		}

		for (ListView listView : listViewMultiple) {
			TAG tag = (TAG) listView.getTag();
			List<ObtainedReplies> getObtainedReplies = obtainedRepliesDao
					.getListObtainedReplies(0, tag.questionId,
							surveyReply.getId());
			if (getObtainedReplies.size() != 0) {
				for (int i = 0; i < listView.getCount(); i++) {
					Cursor c = (Cursor) listView.getAdapter().getItem(i);
					String item = c.getString(2);
					for (int j = 0; j < getObtainedReplies.size(); j++) {
						if (item.equalsIgnoreCase((getObtainedReplies.get(j))
								.getObtReply())) {
							listView.setItemChecked(i, true);
						}
					}
				}
			}
		}

		for (ListView listView : listViewSelection) {
			TAG tag = (TAG) listView.getTag();
			List<ObtainedReplies> getObtainedReplies = obtainedRepliesDao
					.getListObtainedReplies(0, tag.questionId,
							surveyReply.getId());
			if (getObtainedReplies.size() != 0) {
				for (int i = 0; i < listView.getCount(); i++) {
					Cursor c = (Cursor) listView.getAdapter().getItem(i);
					String item = c.getString(2);
					for (int j = 0; j < getObtainedReplies.size(); j++) {
						if (item.equalsIgnoreCase((getObtainedReplies.get(j))
								.getObtReply())) {
							listView.setItemChecked(i, true);
						}
					}
				}
			}
		}

		for (ListView listView : listViewMultipleOthers) {
			TAG tag = (TAG) listView.getTag();
			List<ObtainedReplies> getObtainedReplies = obtainedRepliesDao
					.getListObtainedReplies(1, tag.questionId,
							surveyReply.getId());
			if (getObtainedReplies.size() != 0) {
				listView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
				for (int i = 0; i < listView.getCount(); i++) {
					String answer = (String) listView.getAdapter().getItem(i);
					for (int j = 0; j < getObtainedReplies.size(); j++) {
						if (answer.equalsIgnoreCase((getObtainedReplies.get(j))
								.getObtReply())) {
							listView.setItemChecked(i, true);
						}
					}
				}
			}
		}

		for (ListView listView : listViewSelectionOthers) {
			TAGOther tag = (TAGOther) listView.getTag();
			List<ObtainedReplies> getObtainedReplies = obtainedRepliesDao
					.getListObtainedReplies(1, tag.questionId,
							surveyReply.getId());
			if (getObtainedReplies.size() != 0) {
				listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
				for (int i = 0; i < listView.getCount(); i++) {
					String answer = (String) listView.getAdapter().getItem(i);
					for (int j = 0; j < getObtainedReplies.size(); j++) {
						if (answer.equalsIgnoreCase((getObtainedReplies.get(j))
								.getObtReply())) {
							listView.setItemChecked(i, true);
						}
					}
				}
			}
		}

		for (EditText editText : editTextListOthers) {
			TAGOther tag = (TAGOther) editText.getTag();

			Questions questions = questionsDao.getQuestion(tag.questionId);
			List<ObtainedReplies> getObtainedReplies = obtainedRepliesDao
					.getListObtainedReplies(1, tag.questionId,
							surveyReply.getId());
			if (getObtainedReplies.size() != 0) {
				if (!getObtainedReplies.get(0).getObtReply()
						.equalsIgnoreCase("")) {
					editText.setText(getObtainedReplies.get(0).getObtReply(),
							TextView.BufferType.EDITABLE);
					tag.tvOthers.setVisibility(View.VISIBLE);
					tag.etOthers.setVisibility(View.VISIBLE);
				}
			}
		}

	}

	public ArrayList<Double> checkRequired() {
		checkRequired = new ArrayList<Double>();

		if (groupType.equalsIgnoreCase("GROUPCADASTRE")) {
			if (required == 1) {
				if (cursor.getCount() == 0)
					checkRequired.add((double) 0);
			}
		}

		for (EditText editText : editTextList) {
			TAG tag = (TAG) editText.getTag();
			Cursor question = questionsDao.consultQuestion(tag.questionId);
			question.moveToFirst();
			if (question.getInt(2) == 1) {
				if (editText.getText().toString().equalsIgnoreCase("")) {
					tag.tvQuestion.setError("Obrigatória");
					checkRequired.add(tag.numberQuestion);
				}
			}
		}

		for (ListView listView : listViewMultiple) {
			TAG tag = (TAG) listView.getTag();
			Cursor question = questionsDao.consultQuestion(tag.questionId);
			question.moveToFirst();
			SparseBooleanArray checkedItems = listView
					.getCheckedItemPositions();
			if (checkedItems != null) {
				if (question.getInt(2) == 1) {
					if (checkedItems.size() == 0) {
						tag.tvQuestion.setError("Obrigatória");
						checkRequired.add(tag.numberQuestion);
					}
				}
			}
		}
		for (ListView listView : listViewSelection) {
			TAG tag = (TAG) listView.getTag();
			Cursor question = questionsDao.consultQuestion(tag.questionId);
			question.moveToFirst();
			if (question.getInt(2) == 1) {
				if (getCheckedItemCount(listView) == 0) {
					tag.tvQuestion.setError("Obrigat��ria");
					checkRequired.add(tag.numberQuestion);
				}
			}
		}

		for (ListView listView : listViewSelectionOthers) {
			TAGOther tag = (TAGOther) listView.getTag();

			// Se no lv a avaReply tiver selecionadada, tvOther e lvOther ��
			// obrigatorio
			String avaReplyWithOtherReplies = availableRepliesDao
					.getAvaReply(tag.avaReplyId);

			ListView lv = tag.lvQuestion;
			ListView lvOthers = tag.lvOthers;
			SparseBooleanArray checkedItems = lv.getCheckedItemPositions();

			if (checkedItems != null) {
				for (int i = 0; i < checkedItems.size(); i++) {
					if (checkedItems.valueAt(i)) {
						Cursor c = (Cursor) lv.getAdapter().getItem(
								checkedItems.keyAt(i));
						if (c.getString(2).equalsIgnoreCase(
								avaReplyWithOtherReplies)
								&& getCheckedItemCount(lvOthers) == 0) {
							tag.tvOthers.setError("Obrigatória");
							checkRequired.add(tag.numberQuestion);
						}
					}
				}
			}
		}

		for (EditText editText : editTextListOthers) {
			TAGOther tag = (TAGOther) editText.getTag();

			ListView lv = tag.lvQuestion;
			EditText etOthers = tag.etOthers;
			String avaReplyWithOtherReplies = availableRepliesDao
					.getAvaReply(tag.avaReplyId);

			SparseBooleanArray checkedItems = lv.getCheckedItemPositions();

			if (checkedItems != null) {
				for (int i = 0; i < checkedItems.size(); i++) {
					if (checkedItems.valueAt(i)) {
						Cursor c = (Cursor) lv.getAdapter().getItem(
								checkedItems.keyAt(i));
						if (c.getString(2).equalsIgnoreCase(
								avaReplyWithOtherReplies)
								&& etOthers.getText().toString()
										.equalsIgnoreCase("")) {
							tag.tvOthers.setError("Obrigatória");
							checkRequired.add(tag.numberQuestion);
						}
					}
				}
			}
		}

		return checkRequired;
	}

	private static int getCheckedItemCount(ListView listView) {
		if (Build.VERSION.SDK_INT >= 11)
			return listView.getCheckedItemCount();
		else {
			int count = 0;
			for (int i = listView.getCount() - 1; i >= 0; i--)
				if (listView.isItemChecked(i))
					count++;
			return count;
		}
	}

	public int checkBlankReplies() {
		qntBlankReplies = 0;
		for (EditText editText : editTextList) {
			if (editText.getText().toString().equalsIgnoreCase("")) {
				qntBlankReplies++;
			}
		}
		for (ListView listView : listViewMultiple) {
			SparseBooleanArray checkedItems = listView
					.getCheckedItemPositions();
			if (checkedItems != null) {
				if (checkedItems.size() == 0) {
					qntBlankReplies++;
				}
			}
		}
		for (ListView listView : listViewSelection) {
			if (getCheckedItemCount(listView) == 0) {
				qntBlankReplies++;
			}
		}
		return qntBlankReplies;
	}

	public class TAG {
		private Long questionId;
		private double numberQuestion;
		private TextView tvQuestion;
		private TAGOther tagOther;
	}

	public class TAGOther {
		private Long questionId;
		private Long avaReplyId;
		private TextView tvOthers;
		private ListView lvOthers;
		private EditText etOthers;
		private ListView lvQuestion;
		private double numberQuestion;
	}

	public int verifyBlankReplies() {
		if (groupType.equalsIgnoreCase("GROUPCADASTRE")) {
			Cursor c = cadastreRepliesDao
					.consultCadastreRepliesByCadastreQuestionAndSurveyReply(
							idGroupQuestion, surveyReply.getId());
			if (qntCadastreReplies != c.getCount())
				checkBlankReplies = 1;
			else
				checkBlankReplies = 0;
		} else {

			qntBlankReplies = checkBlankReplies();
			if (qntBlankReplies == qntQuestions) {
				checkBlankReplies = 0;
			} else
				checkBlankReplies = 1;

		}
		return checkBlankReplies;
	}

	public void destroyMap() {
		if (MainOffline.isCoordinates) {
			Fragment fragMap = (MapQuestion) getActivity()
					.getSupportFragmentManager().findFragmentByTag(
							"android:switcher:" + R.id.pager + ":"
									+ MainOffline.pagePos);
			fragMap.onDestroyView();
		}
	}

	public void backPressedBlankReplies() {

		destroyMap();

		SurveyReplies surveyReplyDel = surveyReplyDao
				.getSurveyReply(surveyReply.getId());
		surveyReplyDao.deleteSurveyReply(surveyReplyDel.getId());
		Intent intent = new Intent(getActivity(), ListQuestionnaries.class);
		intent.putExtra("id", surveyReply.getMain_survey_id());
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(intent);
		getActivity().finish();
	}

	public void backPressedCreate() {
		final QustomDialogBuilder qustomDialogBuilder = (QustomDialogBuilder) new QustomDialogBuilder(
				getActivity()).setTitleColor(COLOR).setDividerColor(COLOR);
		qustomDialogBuilder
				.setTitle("Sair")
				.setMessage(
						"As informações inseridas não foram salvas. Deseja realmente sair?")
				.setCancelable(true)
				.setPositiveButton("Sim",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {

								destroyMap();

								SurveyReplies surveyReplyDel = surveyReplyDao
										.getSurveyReply(surveyReply.getId());
								surveyReplyDao.deleteSurveyReply(surveyReplyDel
										.getId());

								Intent intent = new Intent(getActivity(),
										ListQuestionnaries.class);
								intent.putExtra("id",
										surveyReply.getMain_survey_id());
								intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
								startActivity(intent);
								getActivity().finish();
							}
						})
				.setNegativeButton("Não",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.cancel();
							}
						});

		// create alert dialog
		AlertDialog alertDialog = qustomDialogBuilder.create();

		// show it
		alertDialog.show();
	}

	public int checkModify() {
		return checkModify;
	}

	public void backPressedUpdateUnmodified() {
		destroyMap();

		getActivity().finish();
	}

	public void backPressedUpdateModified() {

		final QustomDialogBuilder qustomDialogBuilder = (QustomDialogBuilder) new QustomDialogBuilder(
				getActivity()).setTitleColor(COLOR).setDividerColor(COLOR);
		qustomDialogBuilder
				.setTitle("Sair")
				.setMessage(
						"As informações inseridas não foram salvas. Deseja realmente sair?")
				.setCancelable(true)
				.setPositiveButton("Sim",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {

								destroyMap();

								Iterator<Long> it = listIdsCadastreReplies
										.iterator();
								while (it.hasNext()) {
									Long idCadastre;
									idCadastre = (Long) it.next();
									cadastreRepliesDao
											.deleteCadastreReply(idCadastre);
								}
								getActivity().finish();
							}
						})
				.setNegativeButton("Não",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								// if this button is clicked, just close
								// the dialog box and do nothing
								dialog.cancel();
							}
						});

		// create alert dialog
		AlertDialog alertDialog = qustomDialogBuilder.create();

		// show it
		alertDialog.show();
	}

	// public void onBackPressed() {
	//
	// qntBlankReplies = checkBlankReplies();
	// List<Questions> listQuestions =
	// questionsDao.getListQuestions(surveyReply.getMain_survey_id());
	//
	// if (qntBlankReplies == listQuestions.size()){
	// SurveyReplies surveyReplyDel =
	// surveyReplyDao.getSurveyReply(surveyReply.getId());
	//
	// surveyReplyDao.deleteSurveyReply(surveyReplyDel.getId());
	//
	// Intent intent = new Intent (getActivity(), ListQuestionnaries.class);
	// intent.putExtra("id", surveyReply.getMain_survey_id());
	// intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
	// startActivity(intent);
	// getActivity().finish();
	// } else {
	//
	// if (newOrUpdate == 0){
	//
	// final QustomDialogBuilder qustomDialogBuilder = (QustomDialogBuilder) new
	// QustomDialogBuilder(getActivity()).
	// setTitleColor(COLOR).
	// setDividerColor(COLOR);
	// qustomDialogBuilder
	// .setTitle("Sair")
	// .setMessage("As informa����es inseridas n��o foram salvas. Deseja realmente sair?")
	// .setCancelable(true)
	// .setPositiveButton("Sim",new DialogInterface.OnClickListener() {
	// public void onClick(DialogInterface dialog,int id) {
	// SurveyReplies surveyReplyDel =
	// surveyReplyDao.getSurveyReply(surveyReply.getId());
	// surveyReplyDao.deleteSurveyReply(surveyReplyDel.getId());
	//
	// Intent intent = new Intent (getActivity(), ListQuestionnaries.class);
	// intent.putExtra("id", surveyReply.getMain_survey_id());
	// intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
	// startActivity(intent);
	// getActivity().finish();
	// }
	// })
	// .setNegativeButton("N��o",new DialogInterface.OnClickListener() {
	// public void onClick(DialogInterface dialog,int id) {
	// dialog.cancel();
	// }
	// });
	//
	// // create alert dialog
	// AlertDialog alertDialog = qustomDialogBuilder.create();
	//
	// // show it
	// alertDialog.show();
	// } else if (newOrUpdate == 1){
	// if (checkModify == 1){
	// final QustomDialogBuilder qustomDialogBuilder = (QustomDialogBuilder) new
	// QustomDialogBuilder(getActivity()).
	// setTitleColor(COLOR).
	// setDividerColor(COLOR);
	// qustomDialogBuilder
	// .setTitle("Sair")
	// .setMessage("As informa����es inseridas n��o foram salvas. Deseja realmente sair?")
	// .setCancelable(true)
	// .setPositiveButton("Sim",new DialogInterface.OnClickListener() {
	// public void onClick(DialogInterface dialog,int id) {
	// getActivity().finish();
	// }
	// })
	// .setNegativeButton("N��o",new DialogInterface.OnClickListener() {
	// public void onClick(DialogInterface dialog,int id) {
	// // if this button is clicked, just close
	// // the dialog box and do nothing
	// dialog.cancel();
	// }
	// });
	//
	// // create alert dialog
	// AlertDialog alertDialog = qustomDialogBuilder.create();
	//
	// // show it
	// alertDialog.show();
	// } else{
	// getActivity().finish();
	// }
	// }
	// }
	// }

	public class MyAlertDialogFragment extends DialogFragment {
		private List<EditText> editTextList = new ArrayList<EditText>();
		private List<ListView> listViewMultiple = new ArrayList<ListView>();
		private List<ListView> listViewSelection = new ArrayList<ListView>();
		private List<TextView> textViewList = new ArrayList<TextView>();
		private LinearLayout ll;

		private int newOrUpdate;
		private String groupType;

		private int qntBlankReplies = 0; // qnt questoes em branco
		private ArrayList<Integer> checkRequired; // qnt questoes obrigatorias
		private BasicDAO basicDAO;
		private AvailableRepliesDAO availableRepliesDao;
		private QuestionsDAO questionsDao;
		private ObtainedRepliesDAO obtainedRepliesDao;
		private SurveyRepliesDAO surveyReplyDao;
		private ObtainedCadastreRepliesDAO obtainedCadastreRepliesDao;
		private CadastreRepliesDAO cadastreRepliesDao;

		private List<Questions> listQuestions = new ArrayList<Questions>();
		private LayoutInflater mInflater;

		private View rootView;

		private int checkBlankReplies = 0;
		private int calculated = 0;
		private int qntListView;

		private long idCadastreReply;
		private ListView listCadastre;
		private TextView textViewCadastre;
		//private Cursor cursor;
		
		public MyAlertDialogFragment (){
			
		}
		
		public MyAlertDialogFragment (ListView listCadastre, TextView textView){
			this.listCadastre = listCadastre;
			this.textViewCadastre = textView;
		}

		public MyAlertDialogFragment newInstance(
				ArrayList<Questions> listQuestions, Long idCadastreReply, ListView listCadastre, TextView textView) {
			MyAlertDialogFragment frag = new MyAlertDialogFragment(listCadastre, textView);
			Bundle args = new Bundle();
			args.putParcelableArrayList("listQuestions",
					(ArrayList<? extends Parcelable>) listQuestions);
			args.putLong("idCadastreReply", idCadastreReply);
			frag.setArguments(args);
			return frag;
		}

		@Override
		public void onDestroy() {
			// TODO Auto-generated method stub
			super.onDestroy();
			// basicDAO.close();
		}

		@Override
		public void onActivityCreated(Bundle savedInstanceState) {
			super.onActivityCreated(savedInstanceState);
			setReplies();
		}

		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			openDataBase();

			listQuestions = (List<Questions>) getArguments().getSerializable(
					"listQuestions");
			idCadastreReply = getArguments().getLong("idCadastreReply");

			qntListView = countListView(listQuestions);

			LayoutInflater inflater = getActivity().getLayoutInflater();
			View rootView = inflater.inflate(R.layout.inflate_group_cadastre,
					null);
			LinearLayout ll = (LinearLayout) rootView
					.findViewById(R.id.llGroupCadastre);

			
			createQuestions(listQuestions, ll, inflater);

			AlertDialog.Builder myDialog = new AlertDialog.Builder(
					getActivity());
			myDialog.setView(rootView);
			// Set icon and title
			// myDialog.setIcon(R.drawable.alert_dialog_dart_icon);
			// myDialog.setTitle("TESTANDO");

			// set up buttons

			myDialog.setPositiveButton("Salvar",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,
								int whichButton) {

							createOrUpdateReplies();
			
							cursor.requery();
							
//							cursor = cadastreRepliesDao
//									.consultCadastreRepliesByCadastreQuestionAndSurveyReply(
//											idGroupQuestion,
//											surveyReply.getId());

							String nameColumn = cadastreRepliesDao.COLUNA_ID;
							String[] from = { nameColumn };
							int[] to = { R.id.text };

							// SimpleCursorAdapter adapter = new
							// SimpleCursorAdapter(this, R.layout.row_list,
							// cursor, from, to);
							// setListAdapter(adapter);
							
							SimpleCursorAdapter adapter = new SimpleCursorAdapter(
									getActivity(), R.layout.row_list, cursor,
									from, to);

							adapter.setViewBinder(new ViewBinder() {

								public boolean setViewValue(View aView,
										Cursor aCursor, int aColumnIndex) {
									if (aColumnIndex == 0) {
										try {
											String idCadastre = aCursor
													.getString(aColumnIndex);
											Cursor cursor = obtainedCadastreRepliesDao
													.consultarObtCadastreReplyByQuestionAndCadastreReply(
															listQuestions
																	.get(0)
																	.getId(),
															Long.valueOf(idCadastre));
											TextView textView = (TextView) aView;
											ObtainedCadastreReplies obtCadastreReplies = new ObtainedCadastreReplies();
											obtCadastreReplies = obtainedCadastreRepliesDao
													.getObtCadastreReply(cursor
															.getLong(0));

											String label = obtCadastreReplies
													.getObtReply();
											if (label.length() > 20)
												label = obtCadastreReplies
														.getObtReply()
														.substring(0, 20)
														+ "...";
											textView.setText(label);
										} catch (Exception e) {
											Log.e("MyOpinions", e.getMessage());
										}
										return true;
									}

									return false;
								}
							});
							
							listCadastre.setAdapter(adapter);
							Utility.setListViewHeightBasedOnChildren(listCadastre);
							// Button buttonFragment = (Button)
							// fragmentButton.getView().findViewById(R.id.button_fragment);
							// buttonFragment.setText("Salvar informa����es");

							textViewCadastre.setVisibility(View.VISIBLE);
							checkModify = 1;
							
						}
					});
			myDialog.setNegativeButton("Cancelar",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,
								int whichButton) {

						}
					});

			// return the created Dialog
			return myDialog.create();

		}

		public void createQuestions(List<Questions> listQuestions2,
				LinearLayout ll, LayoutInflater mInflater) {
			editTextList = new ArrayList<EditText>();
			listViewMultiple = new ArrayList<ListView>();
			listViewSelection = new ArrayList<ListView>();
			textViewList = new ArrayList<TextView>();

			createTextView(questionsDao.getTitle(idGroupQuestion), ll, mInflater);
			
			for (int i = 0; i < listQuestions2.size(); i++) {
				Questions question = listQuestions2.get(i);
				int num = i + 1;
				String pergunta = num + ") " + question.getTitle();
				createTextView(pergunta, ll, mInflater);
				List<String> respostas = availableRepliesDao
						.getListAvailableReplies(question.getId());
				if (question.getQuestionType().equalsIgnoreCase("TEXT")) {
					createEditText(i, question.getId(), ll, mInflater);
				} else if (question.getQuestionType().equalsIgnoreCase(
						"MULTIPLE_CHOISE")) {
					createListView(i, question.getId(), respostas,
							android.R.layout.simple_list_item_single_choice,
							ListView.CHOICE_MODE_SINGLE, ll, mInflater);
				} else if (question.getQuestionType().equalsIgnoreCase("BOXES")) {
					createListView(i, question.getId(), respostas,
							R.layout.simple_list_item_multiple_choice,
							ListView.CHOICE_MODE_MULTIPLE, ll, mInflater);
				} else if (question.getQuestionType().equalsIgnoreCase(
						"DATEPICKER")) {
					createDatePicker(i, question.getId(), ll, mInflater);
				}
			}
		}

		public int countListView(List<Questions> listQuestions) {
			int count = 0;
			for (int i = 0; i < listQuestions.size(); i++) {
				Questions question = listQuestions.get(i);
				if (question.getQuestionType().equalsIgnoreCase(
						"MULTIPLE_CHOISE")) {
					count++;
				} else if (question.getQuestionType().equalsIgnoreCase("BOXES")) {
					count++;
				}
			}
			return count;
		}

		private int year;
		private int month;
		private int day;
		private DatePickerDialog.OnDateSetListener datePickerListener;
		private EditText setDate;

		public void createDatePicker(int numberQuestion, Long questionId,
				LinearLayout ll, LayoutInflater mInflater) {
			TAG tag = new TAG();
			tag.numberQuestion = numberQuestion;
			tag.questionId = questionId;
			View view = mInflater.inflate(R.layout.question_datepicker_offline,
					null);
			EditText editDate = (EditText) view
					.findViewById(R.id.resposta_datepicker);
			editDate.setTag(tag);

			// Button datePicker = (Button)
			// view.findViewById(R.id.button_datepicker);

			editDate.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {

					setDate = (EditText) v;
					if (setDate.getEditableText().toString()
							.equalsIgnoreCase("")) {
						Calendar calendar = Calendar.getInstance();

						year = calendar.get(Calendar.YEAR);
						month = calendar.get(Calendar.MONTH);
						day = calendar.get(Calendar.DAY_OF_MONTH);
					} else {
						String date = setDate.getEditableText().toString();
						year = Integer.valueOf(date.substring(6, 10));
						month = Integer.valueOf(date.substring(3, 5));
						day = Integer.valueOf(date.substring(0, 2));
						month = month - 1;
					}

					// TODO Auto-generated method stub
					DatePickerDialog datePickerDialog = new DatePickerDialog(
							getActivity(), datePickerListener, year, month, day);
					if (!datePickerDialog.isShowing()) {
						datePickerDialog.show();
					}
				}
			});

			datePickerListener = new DatePickerDialog.OnDateSetListener() {

				@Override
				public void onDateSet(DatePicker view, int syear,
						int monthOfYear, int dayOfMonth) {
					// TODO Auto-generated method stub

					year = syear;
					month = monthOfYear;
					day = dayOfMonth;

					String dia = null;
					int mes = month + 1;
					String mesok = null;
					if (day < 10) {
						dia = "0" + day;
					} else
						dia = String.valueOf(day);
					if (mes < 10) {
						mesok = "0" + mes;
					} else
						mesok = String.valueOf(mes);

					// set selected date into Text View
					setDate.setText(new StringBuilder().append(dia).append("/")
							.append(mesok).append("/").append(year).append(" "));
					// set selected date into Date Picker
					// date_picker.init(year, month, day, null);
				}
			};

			editTextList.add(editDate);
			ll.addView(view);
		}

		public void createEditText(int numberQuestion, Long questionId,
				LinearLayout ll, LayoutInflater mInflater) {
			TAG tag = new TAG();
			tag.numberQuestion = numberQuestion;
			tag.questionId = questionId;
			Cursor question = questionsDao.consultQuestion(tag.questionId);
			View view = mInflater.inflate(R.layout.edittext, null);
			EditText et = (EditText) view.findViewById(R.id.replyEditText);
			et.setTag(tag);
			if (question.getString(5) != null) {
				if (question.getString(5).equalsIgnoreCase("NUMERIC")) {
					et.setInputType(InputType.TYPE_CLASS_PHONE);
				} else if (question.getString(5).equalsIgnoreCase("EMAIL")) {
					et.setInputType(InputType.TYPE_CLASS_TEXT
							| InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
				}
			}

			et.setOnFocusChangeListener(new View.OnFocusChangeListener() {
				@Override
				public void onFocusChange(View v, boolean hasFocus) {
					if (!hasFocus) {
						checkModify = 1;
					}
				}
			});

			editTextList.add(et);
			ll.addView(view);
		}

		public void createListView(int numberQuestion, Long questionId,
				List<String> replies, int adapterLayout, int choiceMode,
				LinearLayout ll, LayoutInflater mInflater) {
			TAG tag = new TAG();
			tag.numberQuestion = numberQuestion;
			tag.questionId = questionId;
			ListView lv = new ListView(getActivity().getApplicationContext()) {
				@Override
				protected void onLayout(boolean changed, int l, int t, int r,
						int b) {
					super.onLayout(changed, l, t, r, b);
					if (calculated < qntListView) {
						int count = getCount();
						int height = getHeight();
						setLayoutParams(new LinearLayout.LayoutParams(
								LinearLayout.LayoutParams.MATCH_PARENT, count
										* height));
						calculated++;
					}
				}
			};
			lv.setTag(tag);
			ArrayAdapter<String> adapter = new ArrayAdapter<String>(
					getActivity(), adapterLayout, replies);
			lv.setChoiceMode(choiceMode);
			lv.setDivider(null);
			lv.setAdapter(adapter);
			lv.setOnItemClickListener(new OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> arg0, View view,
						int position, long id) {
					checkModify = 1;
				}
			});

			if (adapterLayout == android.R.layout.simple_list_item_single_choice)
				listViewMultiple.add(lv);
			else if (adapterLayout == R.layout.simple_list_item_multiple_choice)
				listViewSelection.add(lv);

			ll.addView(lv);
		}

		public void createTextView(String identify, LinearLayout ll,
				LayoutInflater mInflater) {
			View view = mInflater.inflate(R.layout.textview, null);
			TextView tv = (TextView) view.findViewById(R.id.questionTextView);
			tv.setText(identify);
			textViewList.add(tv);
			ll.addView(view);
		}

		public void openDataBase() {
			// basicDAO = new BasicDAO(getActivity().getApplicationContext());
			// basicDAO.open();
			surveyReplyDao = new SurveyRepliesDAO(getActivity()
					.getApplicationContext());
			questionsDao = new QuestionsDAO(getActivity()
					.getApplicationContext());
			availableRepliesDao = new AvailableRepliesDAO(getActivity()
					.getApplicationContext());
			obtainedRepliesDao = new ObtainedRepliesDAO(getActivity()
					.getApplicationContext());
			obtainedCadastreRepliesDao = new ObtainedCadastreRepliesDAO(
					getActivity().getApplicationContext());
			cadastreRepliesDao = new CadastreRepliesDAO(getActivity()
					.getApplicationContext());
		}

		public class TAG {
			private Long questionId;
			private int numberQuestion;
		}

		public void createOrUpdateReplies() {

			if (idCadastreReply == 0) {
				CadastreReplies cadastreReply = new CadastreReplies();
				cadastreReply.setCadastre_question_id(idGroupQuestion);
				cadastreReply.setSurvey_reply_id(surveyReply.getId());
				idCadastreReply = cadastreRepliesDao
						.createCadastreReply(cadastreReply);
				listIdsCadastreReplies.add(idCadastreReply);
			}
			for (EditText editText : editTextList) {
				TAG tag = (TAG) editText.getTag();

				List<ObtainedCadastreReplies> getObtainedCadastreReplies = obtainedCadastreRepliesDao
						.getListObtainedCadastreReplies(tag.questionId,
								idCadastreReply);
				if (getObtainedCadastreReplies.size() == 0) {
					ObtainedCadastreReplies obtainedCadastreReplies = new ObtainedCadastreReplies();
					obtainedCadastreReplies.setQuestion_id(tag.questionId);
					obtainedCadastreReplies.setObtReply(editText.getText()
							.toString());
					obtainedCadastreReplies
							.setCadastre_reply_id(idCadastreReply);
					obtainedCadastreRepliesDao
							.createObtCadastreReply(obtainedCadastreReplies);
				} else {
					ObtainedCadastreReplies obtainedCadastreReplies = (ObtainedCadastreReplies) getObtainedCadastreReplies
							.get(0);
					obtainedCadastreReplies.setQuestion_id(tag.questionId);
					obtainedCadastreReplies.setObtReply(editText.getText()
							.toString());
					obtainedCadastreReplies
							.setCadastre_reply_id(idCadastreReply);
					obtainedCadastreRepliesDao
							.updateObtCadastreReply(obtainedCadastreReplies);
				}
			}

			for (ListView listView : listViewMultiple) {
				SparseBooleanArray checkedItems = listView
						.getCheckedItemPositions();
				TAG tag = (TAG) listView.getTag();

				if (checkedItems != null) {
					for (int j = 0; j < checkedItems.size(); j++) {
						if (checkedItems.valueAt(j)) {
							String item = listView.getAdapter()
									.getItem(checkedItems.keyAt(j)).toString();

							List<ObtainedCadastreReplies> getObtainedCadastreReplies = obtainedCadastreRepliesDao
									.getListObtainedCadastreReplies(
											tag.questionId, idCadastreReply);
							if (getObtainedCadastreReplies.size() == 0) {
								ObtainedCadastreReplies obtainedCadastreReplies = new ObtainedCadastreReplies();
								obtainedCadastreReplies
										.setQuestion_id(tag.questionId);
								obtainedCadastreReplies.setObtReply(item);
								obtainedCadastreReplies
										.setCadastre_reply_id(idCadastreReply);
								obtainedCadastreRepliesDao
										.createObtCadastreReply(obtainedCadastreReplies);
							} else {
								ObtainedCadastreReplies obtainedCadastreReplies = (ObtainedCadastreReplies) getObtainedCadastreReplies
										.get(0);
								obtainedCadastreReplies
										.setQuestion_id(tag.questionId);
								obtainedCadastreReplies.setObtReply(item);
								obtainedCadastreReplies
										.setCadastre_reply_id(idCadastreReply);
								obtainedCadastreRepliesDao
										.updateObtCadastreReply(obtainedCadastreReplies);
							}
						}
					}
				}
			}

			for (ListView listView : listViewSelection) {
				SparseBooleanArray checkedItems = listView
						.getCheckedItemPositions();
				TAG tag = (TAG) listView.getTag();

				List<ObtainedCadastreReplies> getObtainedCadastreReplies = obtainedCadastreRepliesDao
						.getListObtainedCadastreReplies(tag.questionId,
								idCadastreReply);
				for (int j = 0; j < getObtainedCadastreReplies.size(); j++) {
					obtainedCadastreRepliesDao
							.deleteObtCadastreReply(getObtainedCadastreReplies
									.get(j).getId());
				}

				if (checkedItems != null) {
					for (int i = 0; i < checkedItems.size(); i++) {
						if (checkedItems.valueAt(i)) {
							String item = listView.getAdapter()
									.getItem(checkedItems.keyAt(i)).toString();

							ObtainedCadastreReplies obtainedCadastreReplies = new ObtainedCadastreReplies();
							obtainedCadastreReplies
									.setQuestion_id(tag.questionId);
							obtainedCadastreReplies.setObtReply(item);
							obtainedCadastreReplies
									.setCadastre_reply_id(idCadastreReply);
							obtainedCadastreRepliesDao
									.createObtCadastreReply(obtainedCadastreReplies);
						}
					}
				}
			}

		}

		public void setReplies() {
			for (EditText editText : editTextList) {
				TAG tag = (TAG) editText.getTag();
				List<ObtainedCadastreReplies> getObtainedReplies = obtainedCadastreRepliesDao
						.getListObtainedCadastreReplies(tag.questionId,
								idCadastreReply);
				if (getObtainedReplies.size() != 0) {
					editText.setText(getObtainedReplies.get(0).getObtReply(),
							TextView.BufferType.EDITABLE);
				}
			}

			for (ListView listView : listViewMultiple) {
				TAG tag = (TAG) listView.getTag();
				List<ObtainedCadastreReplies> getObtainedCadastreReplies = obtainedCadastreRepliesDao
						.getListObtainedCadastreReplies(tag.questionId,
								idCadastreReply);
				if (getObtainedCadastreReplies.size() != 0) {
					for (int i = 0; i < listView.getCount(); i++) {
						String item = listView.getAdapter().getItem(i)
								.toString();
						for (int j = 0; j < getObtainedCadastreReplies.size(); j++) {
							if (item.equalsIgnoreCase((getObtainedCadastreReplies
									.get(j)).getObtReply())) {
								listView.setItemChecked(i, true);
							}
						}
					}
				}
			}

			for (ListView listView : listViewSelection) {
				TAG tag = (TAG) listView.getTag();
				List<ObtainedCadastreReplies> getObtainedCadastreReplies = obtainedCadastreRepliesDao
						.getListObtainedCadastreReplies(tag.questionId,
								idCadastreReply);
				if (getObtainedCadastreReplies.size() != 0) {
					for (int i = 0; i < listView.getCount(); i++) {
						String item = listView.getAdapter().getItem(i)
								.toString();
						for (int j = 0; j < getObtainedCadastreReplies.size(); j++) {
							if (item.equalsIgnoreCase((getObtainedCadastreReplies
									.get(j)).getObtReply())) {
								listView.setItemChecked(i, true);
							}
						}
					}
				}
			}
		}
	}

}
