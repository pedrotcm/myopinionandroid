package br.com.myopinion.survey;

import java.text.SimpleDateFormat;
import java.util.Date;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.provider.Settings;
import android.util.Log;
import br.com.myopinion.MyOpinionsActivity;
import br.com.myopinion.dao.BasicDAO;
import br.com.myopinion.dao.MainSurveysDAO;
import br.com.myopinion.dao.QuestionsDAO;
import br.com.myopinion.dao.SurveyRepliesDAO;
import br.com.myopinion.model.SurveyReplies;
import br.com.opala.myopinion.R;

import com.actionbarsherlock.app.SherlockActivity;

public class VerifyLocation extends SherlockActivity implements Runnable{

	private Cursor cursor;
	private BasicDAO basicDAO;
	private MainSurveysDAO mainSurveyDao;
	private QuestionsDAO questionDao;
	private SurveyRepliesDAO surveyReplyDao;
	private LocationManager locationManager; 
	private  LocationListener locationListener;	
	private Vibrator vibrator;
	private ProgressDialog pDialog;
	private AlertDialog alert;
	private long idMain;
	private Handler handler;
	private boolean checkRun = false;
	private boolean running = false;
	private Boolean flag;
	
	private double latitude;
	private double longitude;

	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		basicDAO.close();
	}

	@Override
	public void onCreate(Bundle icicle) {
		super.onCreate(icicle);
		
		try {
			basicDAO = new BasicDAO(getApplicationContext());
			basicDAO.open();
	        mainSurveyDao = new MainSurveysDAO(getApplicationContext());
			questionDao = new QuestionsDAO(getApplicationContext());
			surveyReplyDao = new SurveyRepliesDAO(getApplicationContext());

			Bundle extras = getIntent().getExtras();
			idMain = extras.getLong("id");
			
			cursor = mainSurveyDao.consultMainSurvey(idMain);
			cursor.moveToFirst();
			
			vibrator = (Vibrator) getSystemService( Context.VIBRATOR_SERVICE );
					
		
		} catch (Throwable t) {
			Log.e("MyOpinions", t.getMessage(), t);
		}
	}
	
	private Boolean displayGpsStatus(Context ctx) {
		ContentResolver contentResolver = ctx.getContentResolver();
		boolean gpsStatus = Settings.Secure.isLocationProviderEnabled(
				contentResolver, LocationManager.GPS_PROVIDER);
		if (gpsStatus) {
			return true;
		} else {
			return false;
		}
	}
	
	protected void alertbox(String title, String mymessage, final Context ctx) {
		AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
		builder.setCancelable(false)
				.setTitle("GPS desativado")
				.setMessage("Ative o GPS para iniciar o questionário.")
				.setPositiveButton("Ativar GPS",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								// finish the current activity
								// AlertBoxAdvance.this.finish();
								//Intent myIntent = new Intent(
									//	Settings.ACTION_SECURITY_SETTINGS);
								ctx.startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
								//startActivity(myIntent);
								dialog.cancel();
							}
						})
				.setNegativeButton("Cancelar",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								Intent intent = new Intent (VerifyLocation.this, ListQuestionnaries.class);
								intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
								intent.putExtra("id", idMain);
								startActivity(intent);		
								finish();
								dialog.cancel();
							}
						});
		AlertDialog alert = builder.create();
		alert.show();
	}
	 
	
	 @Override
	protected void onResume() {
		
		 if (running == false ){
			 if (cursor.getDouble(6) != 0 && cursor.getDouble(7) !=0 && cursor.getDouble(8) != 0 && pDialog == null){
				 actionGetLocation();			 
			 } else if (cursor.getDouble(6) == 0 && cursor.getDouble(7) ==0 && cursor.getDouble(8) == 0) {
				 running = true;
				 try {
					 	SurveyReplies surveyReply = new SurveyReplies();
						Date data = new Date();
						SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
						String validate = dateFormat1.format(data);
						SimpleDateFormat dateFormat2 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
						Date validate2 = (Date) dateFormat2.parse(validate);
						surveyReply.setCreated_at(validate2);
						surveyReply.setMain_survey_id(idMain);
						long idSurveyReply = surveyReplyDao.createSurveyReply(surveyReply);
						surveyReply = surveyReplyDao.getSurveyReply(idSurveyReply);
						
						if (cursor.getString(4).equalsIgnoreCase("OFF_LINE")){
							Intent intent = new Intent(VerifyLocation.this, MainOffline.class );
							//intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
							intent.putExtra("surveyReply", surveyReply);
							intent.putExtra("newOrUpdate", 0);		
							startActivity(intent);
							finish();
						} else if (cursor.getString(4).equalsIgnoreCase("ON_LINE")){
							Intent intent = new Intent(VerifyLocation.this, OnlineType.class );
							intent.putExtra("contador", 0);
							intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
							startActivity(intent);
							finish();
						}
				 } catch (Throwable t) {
						Log.e("MyOpinions", t.getMessage(), t);
				 }
			 }
		 }
		 
		 super.onResume();
	}

	/*@Override
		public void onBackPressed() {
		 AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);	 
			
			alertDialogBuilder
				.setMessage("O questionário não foi respondido. Deseja realmente sair?")
				.setCancelable(true)
				.setPositiveButton("Sim",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {
						// if this button is clicked, close
						// current activity
						
						MainSurveys mainSurvey = mainSurveyDao.getMainSurvey(cursor.getLong(0));
						
						mainSurveyDao.deleteMainSurvey(mainSurvey.getId());
					 
					    Intent intent = new Intent (MainOnline.this, MyOpinionsActivity.class);
						intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						startActivity(intent);		
					//	db.close();
						finish();
					}
				  })
				.setNegativeButton("Não",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {
						// if this button is clicked, just close
						// the dialog box and do nothing
						dialog.cancel();
					}
				});

				// create alert dialog
				AlertDialog alertDialog = alertDialogBuilder.create();

				// show it
				alertDialog.show();
		}
		*/

	 private void actionGetLocation(){
		
			running = true;

			pDialog = new ProgressDialog(this);
			pDialog.setMessage("Iniciando o questionário. Por favor aguarde...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();

			startGPS(LocationManager.NETWORK_PROVIDER);
			
			handler = new Handler();
			handler.postDelayed(this, 30 * 1000); //45 seconds

	 }
	 
	 private void startGPS(String provedor) {
		locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
			
		locationListener = new MyLocationListener();
		locationManager.requestLocationUpdates(provedor, 0, 0, locationListener);
	}

	 
	 private class MyLocationListener implements LocationListener {
						
				@Override
		        public void onLocationChanged (Location loc) {
						System.out.println("LOCATION");
						checkRun = true;
						
						Location limitLocation = new Location("");
	        			limitLocation.setLatitude(cursor.getDouble(6));
	        			limitLocation.setLongitude(cursor.getDouble(7));
	        			
		         		if (locationListener != null){
		         			locationManager.removeUpdates(locationListener);
		         					        			        			
		        			verifyLocation(loc, limitLocation);
		         			
		         		} else {
		         			locationManager.removeUpdates(this);

		        			verifyLocation(loc, limitLocation);
		         		}
		         		
								     		    
		     		    
		        }

				@Override
				public void onProviderDisabled(String provider) {
					// TODO Auto-generated method stub
					
				}

				@Override
				public void onProviderEnabled(String provider) {
					// TODO Auto-generated method stub
					
				}

				@Override
				public void onStatusChanged(String provider, int status, Bundle extras) {
					// TODO Auto-generated method stub
					
				}				
			}
	 
	 private void verifyLocation(Location currentLocation, Location limitLocation){
		 if (currentLocation.distanceTo(limitLocation) < cursor.getDouble(8)) {
				vibrator.vibrate( 50 );
				try {
 				SurveyReplies surveyReply = new SurveyReplies();
					Date data = new Date();
					SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
					String validate = dateFormat1.format(data);
					SimpleDateFormat dateFormat2 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
					Date validate2 = (Date) dateFormat2.parse(validate);
					surveyReply.setCreated_at(validate2);
					surveyReply.setMain_survey_id(idMain);
					long idSurveyReply = surveyReplyDao.createSurveyReply(surveyReply);
					surveyReply = surveyReplyDao.getSurveyReply(idSurveyReply);
					
					if (cursor.getString(4).equalsIgnoreCase("OFF_LINE")){
					Intent intent = new Intent(VerifyLocation.this, MainOffline.class );
					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					intent.putExtra("surveyReply", surveyReply);
					intent.putExtra("newOrUpdate", 0);		
					startActivity(intent);
	     		    pDialog.cancel();
					finish();
					} else if (cursor.getString(4).equalsIgnoreCase("ON_LINE")){
						Intent intent = new Intent(VerifyLocation.this, OnlineType.class );
						intent.putExtra("contador", 0);
						intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						startActivity(intent);
						pDialog.cancel();
						finish();
					}
					
				} catch (Throwable t) {
					Log.e("MyOpinions", t.getMessage(), t);
				}
			} else {
				AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(VerifyLocation.this);
			 
				// set title
				alertDialogBuilder.setTitle("Acesso Negado");
				alertDialogBuilder.setMessage("A sua situação atual não permite acesso ao questionário.");
	 
				// set dialog message
				alertDialogBuilder
					//.setMessage("Deseja responder o questionário novamente?")
					.setCancelable(false)
					.setPositiveButton("OK",new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,int id) {
							// if this button is clicked, close
							// current activity
							//final Vibrator vibrator = (Vibrator) getSystemService( Context.VIBRATOR_SERVICE );
							//vibrator.vibrate( 50 );
							if (cursor.getString(4).equalsIgnoreCase("OFF_LINE")){
								Intent intent = new Intent(VerifyLocation.this, ListQuestionnaries.class );
								intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
								intent.putExtra("id", idMain);		
								startActivity(intent);
				     		    pDialog.cancel();
								finish();
								} else if (cursor.getString(4).equalsIgnoreCase("ON_LINE")){
									Intent intent = new Intent(VerifyLocation.this, MyOpinionsActivity.class );
									intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
									startActivity(intent);
									pDialog.cancel();
									finish();
								}
						}
					  });
					
	 
					// create alert dialog
					AlertDialog alertDialog = alertDialogBuilder.create();
	 
					// show it
					alertDialog.show();	
			}
			
	 }

	@Override
	public void run() {
		if (checkRun == false){
			locationManager.removeUpdates(locationListener);
			//startGPS(LocationManager.NETWORK_PROVIDER);
			LocationManager LM = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
			String bestProvider = LM.getBestProvider(new Criteria(), true);
			if (!bestProvider.equalsIgnoreCase("gps")){
				/*Location currentLocation = LM.getLastKnownLocation(bestProvider);
				Location limitLocation = new Location("");
    			limitLocation.setLatitude(cursor.getDouble(6));
    			limitLocation.setLongitude(cursor.getDouble(7));
    			verifyLocation(currentLocation, limitLocation);*/
				startGPS(LocationManager.NETWORK_PROVIDER);
			} else { 
				flag = displayGpsStatus(getApplicationContext());
				if (flag) {
					startGPS(LocationManager.GPS_PROVIDER);
				} else {
					alertbox("GPS Status!", "Seu GPS está desligado.", this);
				}
			}
		}
	}
	
}
	
