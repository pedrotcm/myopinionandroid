package br.com.myopinion.survey;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import br.com.myopinion.CaptureQRCode;
import br.com.myopinion.MyOpinionsActivity;
import br.com.myopinion.adapter.QustomDialogBuilder;
import br.com.myopinion.dao.AvailableRepliesDAO;
import br.com.myopinion.dao.BasicDAO;
import br.com.myopinion.dao.MainSurveysDAO;
import br.com.myopinion.dao.ObtainedRepliesDAO;
import br.com.myopinion.dao.QuestionsDAO;
import br.com.myopinion.dao.SurveyRepliesDAO;
import br.com.myopinion.http.RegisterActivity;
import br.com.myopinion.model.MainSurveys;
import br.com.opala.myopinion.R;

public class ListSurveys extends SherlockActivity{

	private Cursor cursor;
	private BasicDAO basicDAO;
	private MainSurveysDAO mainSurveyDao;
	private SurveyRepliesDAO surveyReplyDao;
	private QuestionsDAO questionsDao;
	private AvailableRepliesDAO availableRepliesDao;
	private ObtainedRepliesDAO obtainedRepliesDao;
	private Vibrator vibrator;
	private AlertDialog alert;
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		basicDAO.close();
	}

	@Override
	public void onCreate(Bundle icicle) {
		super.onCreate(icicle);
		setContentView(R.layout.list_surveys);
		//getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		try {
			vibrator = (Vibrator) getSystemService( Context.VIBRATOR_SERVICE );

			basicDAO = new BasicDAO(getApplicationContext());
			basicDAO.open();
	        mainSurveyDao = new MainSurveysDAO(getApplicationContext());
			surveyReplyDao = new SurveyRepliesDAO(getApplicationContext());
			questionsDao = new QuestionsDAO(getApplicationContext());
			availableRepliesDao = new AvailableRepliesDAO(getApplicationContext());
			obtainedRepliesDao = new ObtainedRepliesDAO(getApplicationContext());
	        
			//String query = "SELECT * FROM MAIN_SURVEYS WHERE MAIN_SURVEYS.SURVEY_TYPE = \"OFF_LINE\" ORDER BY DESCRIPTION" ;
		
			cursor = mainSurveyDao.consultMainSurveyBySurveyType("OFF_LINE");

	       String nameColumn = MainSurveysDAO.COLUNA_DESCRIPTION;
	       String[] from = { nameColumn };
	       int[] to = { R.id.text };
	         
	       SimpleCursorAdapter adapter = new SimpleCursorAdapter(this, R.layout.row_list, cursor, from, to);
		       //setListAdapter(adapter);
		       
	       ListView listView = (ListView) findViewById(android.R.id.list);
	       listView.setAdapter(adapter);
				
	       //registerForContextMenu(listView);
	
		   listView.setOnItemClickListener(new OnItemClickListener() {
					@Override
					public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
						cursor.moveToPosition(arg2);
						Intent in = new Intent (ListSurveys.this, ListQuestionnaries.class);
						in.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						in.putExtra("id", cursor.getLong(0));
						startActivity(in);
						finish();
				        }	
					});
		   
		   listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
               @Override
               public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
                       int pos, long arg3) {
            	   createDialog();
            	   return true;   
               }
           });
		       
		} catch (Throwable t) {
			Log.e("MyOpinions", t.getMessage(), t);
		}
	}
	
	
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
		 super.onCreateContextMenu(menu, v, menuInfo);
		 if (v.getId()==android.R.id.list) {
			vibrator.vibrate( 50 );
			final String COLOR = "#A8Cf45";
			QustomDialogBuilder qustomDialogBuilder = new QustomDialogBuilder(this).
						setTitle(cursor.getString(1)).
						setTitleColor(COLOR).
						setDividerColor(COLOR);
						View customView = qustomDialogBuilder.setCustomViewListView(R.layout.listview, this, cursor);
			 
						final ListView lv = (ListView) customView.findViewById(android.R.id.list);
					    lv.setOnItemClickListener(new OnItemClickListener() {
							@Override
							public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
									long arg3) {
								String item = lv.getItemAtPosition(arg2).toString();
								dialogItemSelected(item);
							//	alert.dismiss();
							}
						
				         });
				qustomDialogBuilder.create();
				qustomDialogBuilder.show();
				
		/*vibrator.vibrate( 50 );
		AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
		cursor.moveToPosition(info.position);
	    menu.setHeaderTitle("Questionário: " + cursor.getString(1));
	    String[] menuItems = getResources().getStringArray(R.array.menu_enviar);
	    for (int i = 0; i<menuItems.length; i++) {
	      menu.add(Menu.NONE, i, i, menuItems[i]);
	    }*/
	  }
	}
	
	public void createDialog(){
		vibrator.vibrate( 50 );
		final String COLOR = "#A8Cf45";
		QustomDialogBuilder qustomDialogBuilder = new QustomDialogBuilder(this).
					setTitle(cursor.getString(1)).
					setTitleColor(COLOR).
					setDividerColor(COLOR);
					View customView = qustomDialogBuilder.setCustomViewListView(R.layout.listview, this, cursor);
					
					final ListView lv = (ListView) customView.findViewById(android.R.id.list);
				    lv.setOnItemClickListener(new OnItemClickListener() {
						@Override
						public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
								long arg3) {
							String item = lv.getItemAtPosition(arg2).toString();
							dialogItemSelected(item);
							alert.dismiss();
						}
					
			         });
			alert = qustomDialogBuilder.create();
			alert.show();
	}
	
	public void dialogItemSelected(String menuItemName){
		final String COLOR = "#A8Cf45";
		if (menuItemName.equalsIgnoreCase("Excluir")){
			final QustomDialogBuilder qustomDialogBuilder = (QustomDialogBuilder) new QustomDialogBuilder(this).
					setTitleColor(COLOR).
					setDividerColor(COLOR);
			// set title
			qustomDialogBuilder.setTitle("Excluir Questionário");
 
			// set dialog message
			qustomDialogBuilder
				.setMessage("Tem certeza que deseja excluir o questionário?")
				.setCancelable(true)
				.setPositiveButton("Sim",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {
						// if this button is clicked, close
						// current activity
					//	MyOpinionsActivity.this.finish();
						//cursor.moveToPosition(info.position);
						
						MainSurveys mainSurvey = mainSurveyDao.getMainSurvey(cursor.getLong(0));
						System.out.println("IdExcluir: " + mainSurvey.getId());
						mainSurveyDao.deleteMainSurvey(mainSurvey.getId());
					
						cursor.requery();
						
						if(cursor.getCount() == 0){
							Intent in = new Intent (ListSurveys.this, MyOpinionsActivity.class);
							startActivity(in);
							finish();
						}
							
					}
				  })
				.setNegativeButton("Não",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {
						// if this button is clicked, just close
						// the dialog box and do nothing
						dialog.cancel();
					}
				});
 
				// create alert dialog
				AlertDialog alertDialog = qustomDialogBuilder.create();
 
				// show it
				alertDialog.show();

		} else if (menuItemName.equalsIgnoreCase("Sincronizar Respostas")){

			if (surveyReplyDao.getSyncSurveyReplies(cursor.getLong(0), 0).getCount() == 0){
				AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
 				// set title
 				alertDialogBuilder.setMessage("Todas as repostas estão sincronizadas.");
 	 
 				// set dialog message
 				alertDialogBuilder
 					//.setMessage("Deseja responder o questionário novamente?")
 					.setCancelable(false)
 					.setPositiveButton("OK",new DialogInterface.OnClickListener() {
 						public void onClick(DialogInterface dialog,int id) {
 							dialog.dismiss();
 						}
 					  });
 					

				// create alert dialog
				AlertDialog alertDialog = alertDialogBuilder.create();

				// show it
				alertDialog.show();	
			} else {
				final QustomDialogBuilder qustomDialogBuilder = (QustomDialogBuilder) new QustomDialogBuilder(this).
						setTitleColor(COLOR).
						setDividerColor(COLOR);
				// set title
				qustomDialogBuilder.setTitle("Sincronizar Respostas");
	 
				// set dialog message
				qustomDialogBuilder
					.setMessage("Tem certeza que deseja sincronizar as respostas?")
					.setCancelable(true)
					.setPositiveButton("Sim",new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,int id) {
							Intent in = new Intent (ListSurveys.this, RegisterActivity.class);
							in.putExtra("id", cursor.getLong(0));
							startActivity(in);
						//	db.close();
							finish();
						}
					  })
					.setNegativeButton("Não",new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,int id) {
							// if this button is clicked, just close
							// the dialog box and do nothing
							dialog.cancel();
						}
					});
	 
					// create alert dialog
					AlertDialog alertDialog = qustomDialogBuilder.create();
	 
					// show it
					alertDialog.show();
			}
		}
	}
	
	 @Override
	    public boolean onContextItemSelected(android.view.MenuItem item) {
			final String COLOR = "#A8Cf45";
		    final AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)item.getMenuInfo();
		    int menuItemIndex = item.getItemId();
			String[] menuItems = getResources().getStringArray(R.array.menu_enviar);
			String menuItemName = menuItems[menuItemIndex];
			if (menuItemName.equalsIgnoreCase("Excluir")){
				final QustomDialogBuilder qustomDialogBuilder = (QustomDialogBuilder) new QustomDialogBuilder(this).
						setTitleColor(COLOR).
						setDividerColor(COLOR);
				// set title
				qustomDialogBuilder.setTitle("Excluir Questionário");
	 
				// set dialog message
				qustomDialogBuilder
					.setMessage("Tem certeza que deseja excluir o questionário?")
					.setCancelable(true)
					.setPositiveButton("Sim",new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,int id) {
							// if this button is clicked, close
							// current activity
						//	MyOpinionsActivity.this.finish();
							cursor.moveToPosition(info.position);
							
							MainSurveys mainSurvey = mainSurveyDao.getMainSurvey(cursor.getLong(0));
							System.out.println("IdExcluir: " + mainSurvey.getId());
							mainSurveyDao.deleteMainSurvey(mainSurvey.getId());
						
						    System.out.println("Del MainSurvey");
							cursor.requery();
						}
					  })
					.setNegativeButton("Não",new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,int id) {
							// if this button is clicked, just close
							// the dialog box and do nothing
							dialog.cancel();
						}
					});
	 
					// create alert dialog
					AlertDialog alertDialog = qustomDialogBuilder.create();
	 
					// show it
					alertDialog.show();
					return true;

			} else if (menuItemName.equalsIgnoreCase("Enviar Respostas")){

				final QustomDialogBuilder qustomDialogBuilder = (QustomDialogBuilder) new QustomDialogBuilder(this).
						setTitleColor(COLOR).
						setDividerColor(COLOR);
				// set title
				qustomDialogBuilder.setTitle("Enviar Respostas");
	 
				// set dialog message
				qustomDialogBuilder
					.setMessage("Tem certeza que deseja enviar as respostas?")
					.setCancelable(true)
					.setPositiveButton("Sim",new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,int id) {
							Intent in = new Intent (ListSurveys.this, RegisterActivity.class);
							in.putExtra("id", cursor.getLong(0));
							startActivity(in);
						//	db.close();
							finish();
						}
					  })
					.setNegativeButton("Não",new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,int id) {
							// if this button is clicked, just close
							// the dialog box and do nothing
							dialog.cancel();
						}
					});
	 
					// create alert dialog
					AlertDialog alertDialog = qustomDialogBuilder.create();
	 
					// show it
					alertDialog.show();
			}
			
	    	return true;
	    }
	 
	 @Override
		public void onBackPressed() {
				Intent intent = new Intent (ListSurveys.this, MyOpinionsActivity.class);
				startActivity(intent);
				//db.close();
				finish();
			super.onBackPressed();
		}
	 
	 @Override
	 public boolean onOptionsItemSelected(MenuItem item) {
	    switch (item.getItemId()) {
	       case android.R.id.home:
	          NavUtils.navigateUpTo(this,
	                new Intent(this, MyOpinionsActivity.class));
	          return true;
	    }
	    return super.onOptionsItemSelected(item);
	 }
	/* @Override
		public boolean onCreateOptionsMenu(Menu menu) {
		    // Inflate the menu items for use in the action bar
		    MenuInflater inflater = getSupportMenuInflater();
		    inflater.inflate(R.layout.action_items, menu);
		    return super.onCreateOptionsMenu(menu);
		}
	 
	 @Override
	 public boolean onOptionsItemSelected(MenuItem item) {
	     // Handle presses on the action bar items
	     switch (item.getItemId()) {
	         case R.id.action_surveys:
	        	 vibrator.vibrate( 50 );
	        	 startActivity( new Intent( ListSurveys.this, ListSurveys.class ) );
	        	 finish();
	        	 return true;
	         case R.id.action_qrcode:
	        	 vibrator.vibrate( 50 );
	        	 startActivity( new Intent( ListSurveys.this, CaptureQRCode.class ) );
	        	 finish();
	        	 return true;
	         default:
	             return super.onOptionsItemSelected(item);
	     }
	 }*/
}
	
