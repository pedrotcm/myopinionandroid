//package br.com.myopinion.survey;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import android.app.AlertDialog;
//import android.content.Context;
//import android.content.DialogInterface;
//import android.content.Intent;
//import android.os.Bundle;
//import android.os.Parcelable;
//import android.os.Vibrator;
//import android.support.v4.app.Fragment;
//import android.support.v4.app.FragmentManager;
//import android.support.v4.app.FragmentManager.OnBackStackChangedListener;
//import android.util.SparseBooleanArray;
//import android.view.Gravity;
//import android.view.KeyEvent;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.View.OnClickListener;
//import android.view.View.OnKeyListener;
//import android.view.ViewGroup;
//import android.widget.Button;
//import android.widget.EditText;
//import android.widget.ExpandableListView;
//import android.widget.ListAdapter;
//import android.widget.ListView;
//import android.widget.Toast;
//import br.com.myopinion.adapter.AdapterListView;
//import br.com.myopinion.dao.AvailableRepliesDAO;
//import br.com.myopinion.dao.MainSurveysDAO;
//import br.com.myopinion.dao.ObtainedRepliesDAO;
//import br.com.myopinion.dao.QuestionsDAO;
//import br.com.myopinion.dao.SurveyRepliesDAO;
//import br.com.myopinion.model.ObtainedReplies;
//import br.com.myopinion.model.Questions;
//import br.com.myopinion.model.SurveyReplies;
//import br.com.opala.myopinion.R;
//
//import com.actionbarsherlock.app.SherlockListFragment;
//
//public class OfflineType extends SherlockListFragment {
//
//	private ListView listView;
//	private AdapterListView adapterListView;
//	private SurveyReplies surveyReply;
//	private int contador;
//	
//	private MainSurveysDAO mainSurveyDao;
//	private SurveyRepliesDAO surveyReplyDao;
//	private QuestionsDAO questionsDao;
//	private AvailableRepliesDAO availableRepliesDao;
//	private ObtainedRepliesDAO obtainedRepliesDao;
//	
//	private int qntNoReplies = 0; //qnt questoes em branco
//	private ArrayList<Integer> checkRequired; //qnt questoes obrigatorias
//	private int checkIfUpdate = 0; 
//	private Parcelable state;
//	private int newOrUpdate;
//	
//    private ExpandableListView ExpandList;
//   // private ExpandListAdapter ExpAdapter;
//
//    
//    
//    
//    @Override
//	public View onCreateView(LayoutInflater inflater, ViewGroup container,
//			Bundle savedInstanceState) {
//		try {
//			System.out.println("onCreateView");
//		View rootView = inflater.inflate(R.layout.main_offline, container, false);
//				           
//        mainSurveyDao = new MainSurveysDAO(getActivity().getApplicationContext());
//		mainSurveyDao.open();
//		surveyReplyDao = new SurveyRepliesDAO(getActivity().getApplicationContext());
//		surveyReplyDao.open();
//		questionsDao = new QuestionsDAO(getActivity().getApplicationContext());
//		questionsDao.open();
//		availableRepliesDao = new AvailableRepliesDAO(getActivity().getApplicationContext());
//		availableRepliesDao.open();
//		obtainedRepliesDao = new ObtainedRepliesDAO(getActivity().getApplicationContext());
//		obtainedRepliesDao.open();
//
//		Bundle extras = getArguments();
//	    surveyReply = (SurveyReplies) extras.getSerializable("surveyReply");
//		newOrUpdate = (Integer) extras.get("newOrUpdate");
//		
//	    List listQuestions = questionsDao.getListQuestions(surveyReply.getMain_survey_id());
//	    
//	    
//	    listView = (ListView) rootView.findViewById(android.R.id.list);
//		// Define o Listener quando alguem clicar no item.
//		//listView.setOnItemClickListener(this);       
//		adapterListView = new AdapterListView(getActivity(), surveyReply.getId(), (ArrayList<Questions>) listQuestions);
//		// Define o Adapter
//		listView.setAdapter(adapterListView);
//		//listView.setDescendantFocusability(ListView.FOCUS_BEFORE_DESCENDANTS);
//	    state = listView.onSaveInstanceState();
//	     //testes
//	   /* ExpandList = (ExpandableListView) findViewById(android.R.id.list);
//	    ExpAdapter = new ExpandListAdapter(OfflineType.this, (ArrayList<Questions>) listQuestions);
//	    ExpandList.setAdapter(ExpAdapter);
//	    state = ExpandList.onSaveInstanceState();
//*/
//	     //     
//		
//		//Button buttonFragment = (Button) rootView.findViewById(R.id.button_concluir);
//		Fragment fragmentButton = getFragmentManager().findFragmentById(R.id.button_fragment_content);
//		Button buttonFragment = (Button) fragmentButton.getView().findViewById(R.id.button_fragment);
//		buttonFragment.setText("Salvar informações");
//			buttonFragment.setOnClickListener(new OnClickListener() {
//				
//				@Override
//				public void onClick(View v) {
//					
//					checkRequired = CheckRequired();
//					
//					if (checkRequired.isEmpty()){
//						ListAdapter adapter = listView.getAdapter();
//					    for (int i = 0; i < listView.getAdapter().getCount(); i++) {
//						    //View view = listView.getChildAt(i);
//						    View view = listView.getAdapter().getView(i, null, null);
//						    Questions question = (Questions) listView.getAdapter().getItem(i);
//						  
//							List<ObtainedReplies> getObtainedReplies = obtainedRepliesDao.getListObtainedReplies(question.getId(), surveyReply.getId());
//							if (getObtainedReplies.size() == 0){
//							   if (question.getQuestionType().equalsIgnoreCase("GPS")) {
//								 	EditText latitude = (EditText) view.findViewById(R.id.latitude);
//								 	EditText longitude = (EditText) view.findViewById(R.id.longitude);
//								 	
//								 	ObtainedReplies obtLatitude = new ObtainedReplies();
//									obtLatitude.setQuestion_id(question.getId());
//									obtLatitude.setSurvey_reply_id(surveyReply.getId());
//									
//									String latitude_obtida = latitude.getEditableText().toString();
//									
//									obtLatitude.setObtReply(latitude_obtida);
//									obtainedRepliesDao.createObtReply(obtLatitude);
//
//									ObtainedReplies obtLongitude = new ObtainedReplies();
//									obtLongitude.setQuestion_id(question.getId());
//									obtLongitude.setSurvey_reply_id(surveyReply.getId());
//									
//									String longitude_obtida = longitude.getEditableText().toString();
//									
//									obtLongitude.setObtReply(longitude_obtida);
//									obtainedRepliesDao.createObtReply(obtLongitude);
//									
//							   } else  if (question.getQuestionType().equalsIgnoreCase("DATEPICKER")) {
//								 	EditText resposta = (EditText) view.findViewById(R.id.resposta_datepicker);
//								 	ObtainedReplies obtainedReplies = new ObtainedReplies();
//									obtainedReplies.setQuestion_id(question.getId());
//									obtainedReplies.setSurvey_reply_id(surveyReply.getId());
//									
//									String resposta_obtida = resposta.getEditableText().toString();
//				
//									obtainedReplies.setObtReply(resposta_obtida);
//									obtainedRepliesDao.createObtReply(obtainedReplies);
//							    } else if (question.getQuestionType().equalsIgnoreCase("TEXT")) {
//								 	EditText resposta = (EditText) view.findViewById(R.id.resposta_texto);
//								 	ObtainedReplies obtainedReplies = new ObtainedReplies();
//									obtainedReplies.setQuestion_id(question.getId());
//									obtainedReplies.setSurvey_reply_id(surveyReply.getId());
//									
//									String resposta_obtida = resposta.getEditableText().toString();
//				
//									obtainedReplies.setObtReply(resposta_obtida);
//									obtainedRepliesDao.createObtReply(obtainedReplies);
//						 		} else if (question.getQuestionType().equalsIgnoreCase("MULTIPLE_CHOISE")) {
//						    		 
//						 			ListView listViewMultiple = (ListView) view.findViewById(android.R.id.list);
//		
//						 			SparseBooleanArray checkedItems = listViewMultiple.getCheckedItemPositions();
//			    					if (checkedItems != null) {
//			    					    for (int j=0; j<checkedItems.size(); j++) {
//			    					        if (checkedItems.valueAt(j)) {
//			    					            String item = listViewMultiple.getAdapter().getItem(
//			    					                                  checkedItems.keyAt(j)).toString();
//			    					            //Log.i("MyOpinions",item + " was selected");
//		
//			    		    					ObtainedReplies obtainedReplies = new ObtainedReplies();
//			    		    					obtainedReplies.setQuestion_id(question.getId());
//			    		    					obtainedReplies.setSurvey_reply_id(surveyReply.getId());
//			    		    					obtainedReplies.setObtReply(item);
//			    		    					obtainedRepliesDao.createObtReply(obtainedReplies);
//		
//			    					        }
//			    					    }
//			    					}
//						 		} else if (question.getQuestionType().equalsIgnoreCase("BOXES")) {
//						 			
//						 			ListView listViewSelection = (ListView) view.findViewById(R.id.lista_selecao);
//						 			SparseBooleanArray checkedItems = listViewSelection.getCheckedItemPositions();
//			    					if (checkedItems != null) {
//			    					    for (int k=0; k<checkedItems.size(); k++) {
//			    					        if (checkedItems.valueAt(k)) {
//			    					            String item = listViewSelection.getAdapter().getItem(
//			    					                                  checkedItems.keyAt(k)).toString();
//			    					           // Log.i("MyOpinions",item + " was selected");
//		
//			    		    					ObtainedReplies obtainedReplies = new ObtainedReplies();
//			    		    					obtainedReplies.setQuestion_id(question.getId());
//			    		    					obtainedReplies.setSurvey_reply_id(surveyReply.getId());
//			    		    					obtainedReplies.setObtReply(item);
//			    		    					obtainedRepliesDao.createObtReply(obtainedReplies);
//		
//			    					        }
//			    					    }
//			    					}		
//						 		}
//							    
//					 		} else { //atualizar respostas
//					 			checkIfUpdate = 1;
//					 			if (question.getQuestionType().equalsIgnoreCase("GPS")) {
//					 				
//								 	EditText latitude = (EditText) view.findViewById(R.id.latitude);
//								 	//ObtainedReplies obtainedReplies = new ObtainedReplies();
//								 	ObtainedReplies obtLatitude = (ObtainedReplies) getObtainedReplies.get(0);
//								 	obtLatitude.setQuestion_id(question.getId());
//									obtLatitude.setSurvey_reply_id(surveyReply.getId());
//									
//									String latitude_obtida = latitude.getEditableText().toString();
//				
//									obtLatitude.setObtReply(latitude_obtida);
//									obtainedRepliesDao.updateObtReply(obtLatitude);
//									
//									EditText longitude = (EditText) view.findViewById(R.id.longitude);
//								 	//ObtainedReplies obtainedReplies = new ObtainedReplies();
//								 	ObtainedReplies obtLongitude = (ObtainedReplies) getObtainedReplies.get(1);
//								 	obtLongitude.setQuestion_id(question.getId());
//									obtLongitude.setSurvey_reply_id(surveyReply.getId());
//									
//									String longitude_obtida = longitude.getEditableText().toString();
//				
//									obtLongitude.setObtReply(longitude_obtida);
//									obtainedRepliesDao.updateObtReply(obtLongitude);
//									
//					 			} else if (question.getQuestionType().equalsIgnoreCase("DATEPICKER")) {
//									 	EditText resposta = (EditText) view.findViewById(R.id.resposta_datepicker);
//									 	//ObtainedReplies obtainedReplies = new ObtainedReplies();
//									 	ObtainedReplies obtainedReplies = (ObtainedReplies) getObtainedReplies.get(0);
//									 	obtainedReplies.setQuestion_id(question.getId());
//										obtainedReplies.setSurvey_reply_id(surveyReply.getId());
//										
//										String resposta_obtida = resposta.getEditableText().toString();
//					
//										obtainedReplies.setObtReply(resposta_obtida);
//										obtainedRepliesDao.updateObtReply(obtainedReplies);
//					 			 } else if (question.getQuestionType().equalsIgnoreCase("TEXT")) {
//								 	EditText resposta = (EditText) view.findViewById(R.id.resposta_texto);
//								 	ObtainedReplies obtainedReplies = (ObtainedReplies) getObtainedReplies.get(0);
//								 	//ObtainedReplies obtainedReplies = new ObtainedReplies();
//									obtainedReplies.setQuestion_id(question.getId());
//									obtainedReplies.setSurvey_reply_id(surveyReply.getId());
//									
//									String resposta_obtida = resposta.getEditableText().toString();
//				
//									obtainedReplies.setObtReply(resposta_obtida);
//									obtainedRepliesDao.updateObtReply(obtainedReplies);
//						 		} else if (question.getQuestionType().equalsIgnoreCase("MULTIPLE_CHOISE")) {
//						    		 
//						 			ListView listViewMultiple = (ListView) view.findViewById(android.R.id.list);
//		
//						 			SparseBooleanArray checkedItems = listViewMultiple.getCheckedItemPositions();
//			    					if (checkedItems != null) {
//			    					    for (int j=0; j<checkedItems.size(); j++) {
//			    					        if (checkedItems.valueAt(j)) {
//			    					            String item = listViewMultiple.getAdapter().getItem(
//			    					                                  checkedItems.keyAt(j)).toString();
//			    					          //  Log.i("Multiple",item + " was selected");
//		
//			    		    					//ObtainedReplies obtainedReplies = new ObtainedReplies();
//			    					            ObtainedReplies obtainedReplies = (ObtainedReplies) getObtainedReplies.get(0);
//			    					            obtainedReplies.setQuestion_id(question.getId());
//			    		    					obtainedReplies.setSurvey_reply_id(surveyReply.getId());
//			    		    					obtainedReplies.setObtReply(item);
//			    		    					obtainedRepliesDao.updateObtReply(obtainedReplies);
//		
//			    					        }
//			    					    }
//			    					}
//						 		} else if (question.getQuestionType().equalsIgnoreCase("BOXES")) {
//						 			
//						 			ListView listViewSelection = (ListView) view.findViewById(R.id.lista_selecao);
//						 			SparseBooleanArray checkedItems = listViewSelection.getCheckedItemPositions();
//			    					if (checkedItems != null) {
//			    					    for (int k=0; k<checkedItems.size(); k++) {
//			    					        if (checkedItems.valueAt(k)) {
//			    					            String item = listViewSelection.getAdapter().getItem(
//			    					                                  checkedItems.keyAt(k)).toString();
//			    					            //Log.i("Selection",item + " was selected");
//		
//			    					            for (int j=0; j < getObtainedReplies.size(); j++){
//				     					        	   obtainedRepliesDao.deleteObtReply(getObtainedReplies.get(j).getId());
//				     					           }
//			    					            
//			    		    					ObtainedReplies obtainedReplies = new ObtainedReplies();
//			    		    					obtainedReplies.setQuestion_id(question.getId());
//			    		    					obtainedReplies.setSurvey_reply_id(surveyReply.getId());
//			    		    					obtainedReplies.setObtReply(item);
//			    		    					obtainedRepliesDao.createObtReply(obtainedReplies);
//		
//			    					        }
//			    					    }
//			    					}		
//						 		}
//					 			
//					 		}
//						    
//						    //TextView tipoEditText = (TextView) view.findViewById(R.id.questao_texto);
//						    //EditText telefoneEditText = (EditText) viewTelefone.findViewById(R.id.telefone_form_telefone);
//						    //Log.d(TAG, tipoEditText.getText().toString() + ":" + telefoneEditText.getText().toString());
//						
//						}
//						
//					    if (checkIfUpdate == 0){
//							AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
//							 
//							// set title
//							alertDialogBuilder.setTitle("Informações salvas com sucesso!");
//				 
//							// set dialog message
//							alertDialogBuilder
//								.setMessage("Deseja responder o questionário novamente?")
//								.setCancelable(false)
//								.setPositiveButton("Sim",new DialogInterface.OnClickListener() {
//									public void onClick(DialogInterface dialog,int id) {
//										// if this button is clicked, close
//										// current activity
//										final Vibrator vibrator = (Vibrator) getActivity().getSystemService( Context.VIBRATOR_SERVICE );
//										vibrator.vibrate( 50 );
//										Intent intent = new Intent (getActivity(), VerifyLocation.class);
//										intent.putExtra("id", surveyReply.getMain_survey_id());
//										intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//										startActivity(intent);
//									//	db.close();
//										getActivity().finish();
//										
//									}
//								  })
//								.setNegativeButton("Não",new DialogInterface.OnClickListener() {
//									public void onClick(DialogInterface dialog,int id) {
//										// if this button is clicked, just close
//										// the dialog box and do nothing
//										//dialog.cancel();
//										Intent intent = new Intent (getActivity(), ListQuestionnaries.class);
//										intent.putExtra("id", surveyReply.getMain_survey_id());
//										intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//										startActivity(intent);
//								//		db.close();
//										getActivity().finish();
//									}
//								});
//				 
//								// create alert dialog
//								AlertDialog alertDialog = alertDialogBuilder.create();
//				 
//								// show it
//								alertDialog.show();				
//						} else {
//							AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
//							 
//							// set title
//							alertDialogBuilder.setTitle("Informações atualizadas com sucesso!");
//				 
//							// set dialog message
//							alertDialogBuilder
//								//.setMessage("Deseja responder o questionário novamente?")
//								.setCancelable(true)
//								.setPositiveButton("OK",new DialogInterface.OnClickListener() {
//									public void onClick(DialogInterface dialog,int id) {
//										// if this button is clicked, close
//										// current activity
//										final Vibrator vibrator = (Vibrator) getActivity().getSystemService( Context.VIBRATOR_SERVICE );
//										vibrator.vibrate( 50 );
//										//Intent intent = new Intent (OfflineType.this, ListQuestionnaries.class);
//										//intent.putExtra("id", surveyReply.getMain_survey_id());
//										//intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//										//startActivity(intent);
//										//db.close();
//										getActivity().finish();
//									}
//								  });
//				 
//								// create alert dialog
//								AlertDialog alertDialog = alertDialogBuilder.create();
//				 
//								// show it
//								alertDialog.show();			
//						}
//					} else {
//						listView.setSelection(checkRequired.get(0));
//						int pos = checkRequired.get(0) + 1;
//						Toast toast = Toast.makeText(getActivity(), "Questão: " + pos + " é obrigatória!", Toast.LENGTH_LONG );
//						toast.setGravity(Gravity.CENTER, 0, 0);
//						toast.show();
//					}
//				}
//			});
//			return rootView;
//		} catch (Exception e) {
//			System.out.println("Erro: " + e);
//			throw new RuntimeException(e);
//		}
//		
//	}
//
//    
//	/*@Override
//	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
//		Questions question = adapterListView.getItem(arg2);
//		 if (question.getQuestionType().equalsIgnoreCase("TEXT")) {
//    		 System.out.println("Entrou Text");
// 			Intent in = new Intent(this, TextQuestion.class);
// 			in.putExtra("contador", arg2);
// 			in.putExtra("question", question);
//			in.putExtra("surveyReply", surveyReply);
// 			startActivity(in);
// 			//db.close();
// 			//finish();
// 		} else if (question.getQuestionType().equalsIgnoreCase("MULTIPLE_CHOISE")) {
//    		 System.out.println("Entrou Mutiple");
// 			Intent in = new Intent(this, MultipleQuestion.class);
// 			in.putExtra("contador", arg2);
// 			in.putExtra("question", question);
//			in.putExtra("surveyReply", surveyReply);
// 			startActivity(in);
// 			//db.close();
// 			//finish();
// 		} else if (question.getQuestionType().equalsIgnoreCase("BOXES")) {
//    		 System.out.println("Entrou Select");
// 			Intent in = new Intent(this, SelectionQuestion.class);
// 			in.putExtra("contador", arg2);
// 			in.putExtra("question", question);
//			in.putExtra("surveyReply", surveyReply);
// 			startActivity(in);
// 			//db.close();
// 			//finish();
// 		}
//	}*/
//
//		public void onBackPressed() {
//			// set title
//			//alertDialogBuilder.setTitle("Alerta");
//			// set dialog message
//		 qntNoReplies = CheckQuestionsReplies();
//		 
//		 if (qntNoReplies == listView.getAdapter().getCount()){
//				SurveyReplies surveyReplyDel = surveyReplyDao.getSurveyReply(surveyReply.getId());
//				
//			    surveyReplyDao.deleteSurveyReply(surveyReplyDel.getId());
//		    
//			    Intent intent = new Intent (getActivity(), ListQuestionnaries.class);
//				intent.putExtra("id", surveyReply.getMain_survey_id());
//				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//				startActivity(intent);		
//			//	db.close();
//				getActivity().finish();
//		 } else {
//			 
//			  if (newOrUpdate == 0){
//	
//				 AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());	 
//		
//					alertDialogBuilder
//						.setMessage("As informações inseridas não foram salvas. Deseja realmente sair?")
//						.setCancelable(true)
//						.setPositiveButton("Sim",new DialogInterface.OnClickListener() {
//							public void onClick(DialogInterface dialog,int id) {
//								// if this button is clicked, close
//								// current activity
//	
//								    SurveyReplies surveyReplyDel = surveyReplyDao.getSurveyReply(surveyReply.getId());
//								    
//								    surveyReplyDao.deleteSurveyReply(surveyReplyDel.getId());
//								  
//								  
//							    Intent intent = new Intent (getActivity(), ListQuestionnaries.class);
//								intent.putExtra("id", surveyReply.getMain_survey_id());
//								intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//								startActivity(intent);		
//							//	db.close();
//								getActivity().finish();
//							}
//						  })
//						.setNegativeButton("Não",new DialogInterface.OnClickListener() {
//							public void onClick(DialogInterface dialog,int id) {
//								// if this button is clicked, just close
//								// the dialog box and do nothing
//								dialog.cancel();
//							}
//						});
//		
//						// create alert dialog
//						AlertDialog alertDialog = alertDialogBuilder.create();
//		
//						// show it
//						alertDialog.show();	
//				  } else if (newOrUpdate == 1){
//						if (adapterListView.checkAddReply == 1){
//							 AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());	 
//								
//								alertDialogBuilder
//									.setMessage("As informações inseridas não foram salvas. Deseja realmente sair?")
//									.setCancelable(true)
//									.setPositiveButton("Sim",new DialogInterface.OnClickListener() {
//										public void onClick(DialogInterface dialog,int id) {
//											// if this button is clicked, close
//											// current activity
//				
//																	  
//										   // Intent intent = new Intent (OfflineType.this, ListQuestionnaries.class);
//											//intent.putExtra("id", surveyReply.getMain_survey_id());
//											//intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//											//startActivity(intent);		
//											//db.close();
//											getActivity().finish();
//										}
//									  })
//									.setNegativeButton("Não",new DialogInterface.OnClickListener() {
//										public void onClick(DialogInterface dialog,int id) {
//											// if this button is clicked, just close
//											// the dialog box and do nothing
//											dialog.cancel();
//										}
//									});
//					
//									// create alert dialog
//									AlertDialog alertDialog = alertDialogBuilder.create();
//					
//									// show it
//									alertDialog.show();	
//						} else{
//							//Intent intent = new Intent (OfflineType.this, ListQuestionnaries.class);
//							//intent.putExtra("id", surveyReply.getMain_survey_id());
//							//intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//							//startActivity(intent);		
//							//db.close();
//							getActivity().finish();
//						}
//				  }
//		 	}
//		}
//
//    
//	 @Override
//	 public void onPause() {
//	     super.onPause();
//	 //    index = listView.getFirstVisiblePosition();
//	   //  View v = listView.getChildAt(0);
//	     //top = (v == null) ? 0 : v.getTop();
//	     state = listView.onSaveInstanceState();
//	    // state = ExpandList.onSaveInstanceState();
//
//	 }
//	  
//	 @Override
//	 public void onResume() {
//	     super.onResume();
//	     // return scroll positon to last viewed.
//	    // listView.setSelectionFromTop(index, top);
//	     listView.onRestoreInstanceState(state);
//	     //ExpandList.onRestoreInstanceState(state);
//
//	 }
//
//	 private ArrayList<Integer> CheckRequired(){
//		 checkRequired = new ArrayList<Integer>();
//			
//			for (int y = 0; y < listView.getAdapter().getCount(); y++){
//			    Questions question = (Questions) listView.getAdapter().getItem(y);
//					if (question.getRequired().booleanValue() == true){
//						View view = listView.getAdapter().getView(y, null, null);							    
//	
//						 	if (question.getQuestionType().equalsIgnoreCase("GPS")) {
//							 	EditText latitude = (EditText) view.findViewById(R.id.latitude);	
//							 	EditText longitude = (EditText) view.findViewById(R.id.longitude);	
//								String latitude_obtida = latitude.getEditableText().toString();
//								String longitude_obtidade = longitude.getEditableText().toString();
//								if (latitude_obtida.equalsIgnoreCase("") || longitude_obtidade.equalsIgnoreCase("")){
//									checkRequired.add(y);
//								} 		
//							}else if (question.getQuestionType().equalsIgnoreCase("DATEPICKER")) {
//							 	EditText resposta = (EditText) view.findViewById(R.id.resposta_datepicker);	
//								String resposta_obtida = resposta.getEditableText().toString();
//								if (resposta_obtida.equalsIgnoreCase("")){
//									checkRequired.add(y);
//								}
//					 		} else if (question.getQuestionType().equalsIgnoreCase("TEXT")) {
//							 	EditText resposta = (EditText) view.findViewById(R.id.resposta_texto);	
//								String resposta_obtida = resposta.getEditableText().toString();
//								if (resposta_obtida.equalsIgnoreCase("")){
//									checkRequired.add(y);
//								}
//					 		} else if (question.getQuestionType().equalsIgnoreCase("MULTIPLE_CHOISE")) {
//					 			ListView listViewMultiple = (ListView) view.findViewById(android.R.id.list);
//	
//					 			SparseBooleanArray checkedItems = listViewMultiple.getCheckedItemPositions();
//		    					if (checkedItems != null) {
//		    						if (checkedItems.size() == 0){
//										checkRequired.add(y);
//		    						}
//		    					}
//					 		} else if (question.getQuestionType().equalsIgnoreCase("BOXES")) {
//					 			
//					 			ListView listViewSelection = (ListView) view.findViewById(R.id.lista_selecao);
//					 			SparseBooleanArray checkedItems = listViewSelection.getCheckedItemPositions();
//		    					if (checkedItems != null) {
//		    						if (checkedItems.size() == 0){
//										checkRequired.add(y);
//		    						}		    					    
//		    					}		
//					 		}
//					}
//			}
//			return checkRequired;
//	 }
//	 
//	 private int CheckQuestionsReplies(){
//		 qntNoReplies = 0;
//			
//			for (int y = 0; y < listView.getAdapter().getCount(); y++){
//			    Questions question = (Questions) listView.getAdapter().getItem(y);
//						View view = listView.getAdapter().getView(y, null, null);							    
//					    
//						 	if (question.getQuestionType().equalsIgnoreCase("DATEPICKER")) {
//							 	EditText resposta = (EditText) view.findViewById(R.id.resposta_datepicker);	
//								String resposta_obtida = resposta.getEditableText().toString();
//								if (resposta_obtida.equalsIgnoreCase("")){
//									qntNoReplies++;
//								}
//							}	else if (question.getQuestionType().equalsIgnoreCase("TEXT")) {
//							 	EditText resposta = (EditText) view.findViewById(R.id.resposta_texto);	
//								String resposta_obtida = resposta.getEditableText().toString();
//								if (resposta_obtida.equalsIgnoreCase("")){
//									qntNoReplies++;
//								}
//					 		} else if (question.getQuestionType().equalsIgnoreCase("MULTIPLE_CHOISE")) {
//					 			ListView listViewMultiple = (ListView) view.findViewById(android.R.id.list);
//	
//					 			SparseBooleanArray checkedItems = listViewMultiple.getCheckedItemPositions();
//		    					if (checkedItems != null) {
//		    						if (checkedItems.size() == 0){
//										qntNoReplies++;
//		    						}
//		    					}
//					 		} else if (question.getQuestionType().equalsIgnoreCase("BOXES")) {
//					 			
//					 			ListView listViewSelection = (ListView) view.findViewById(R.id.lista_selecao);
//					 			SparseBooleanArray checkedItems = listViewSelection.getCheckedItemPositions();
//		    					if (checkedItems != null) {
//		    						if (checkedItems.size() == 0){
//										qntNoReplies++;
//		    						}		    					    
//		    					}		
//					 		} else if (question.getQuestionType().equalsIgnoreCase("GPS")) {
//							 	EditText latitude = (EditText) view.findViewById(R.id.latitude);	
//								String resposta_obtida = latitude.getEditableText().toString();
//								if (resposta_obtida.equalsIgnoreCase("")){
//									qntNoReplies++;
//								}
//					 		}
//			}
//			return qntNoReplies;
//	 }
//
//	 //action bar items
//	/* @Override 
//		public boolean onCreateOptionsMenu(Menu menu) {
//		    // Inflate the menu items for use in the action bar
//		    MenuInflater inflater = getSupportMenuInflater();
//		    inflater.inflate(R.layout.action_items, menu);
//		    return super.onCreateOptionsMenu(menu);
//		}
//	 
//	 @Override
//	 public boolean onOptionsItemSelected(MenuItem item) {
//	     // Handle presses on the action bar items
//	     switch (item.getItemId()) {
//	         case R.id.action_search:
//	        	 System.out.println("clicou");
//	        	 return true;
//	         default:
//	             return super.onOptionsItemSelected(item);
//	     }
//	 }*/
//}
