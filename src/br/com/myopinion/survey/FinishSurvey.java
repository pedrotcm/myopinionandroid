package br.com.myopinion.survey;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import br.com.myopinion.dao.BasicDAO;
import br.com.myopinion.dao.MainSurveysDAO;
import br.com.myopinion.http.RegisterActivity;
import br.com.opala.myopinion.R;

public class FinishSurvey extends Fragment{

	private int contador;
	private Cursor cursor;
	private BasicDAO basicDAO;
	private MainSurveysDAO mainSurveyDao;

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		basicDAO.close();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
 	
        View view = inflater.inflate(R.layout.finish_survey, container, false); 
             
        basicDAO = new BasicDAO(getActivity().getApplicationContext());
        basicDAO.open();
        mainSurveyDao = new MainSurveysDAO(getActivity().getApplicationContext());
		
        cursor = mainSurveyDao.consultAllMainSurveys();
		cursor.moveToLast();
				
		
		/*Fragment fragmentButton = getFragmentManager().findFragmentById(R.id.button_fragment_content);
		Button buttonFragment = (Button) fragmentButton.getView().findViewById(R.id.button_fragment);
		buttonFragment.setVisibility(View.GONE);*/
		
        Button button = (Button) view.findViewById(R.id.button_response);
        
    	contador = getArguments().getInt("contador");

		button.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent in = new Intent (getActivity(), RegisterActivity.class);
				in.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				in.putExtra("id", cursor.getLong(0));
				startActivity(in);
				getActivity().finish();
			}
		});
		return view;
	}
		 
}
