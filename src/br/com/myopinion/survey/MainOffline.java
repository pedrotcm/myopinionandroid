package br.com.myopinion.survey;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import android.database.Cursor;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.Gravity;
import android.widget.Toast;
import br.com.myopinion.dao.BasicDAO;
import br.com.myopinion.dao.MainSurveysDAO;
import br.com.myopinion.dao.ObtainedRepliesDAO;
import br.com.myopinion.dao.QuestionsDAO;
import br.com.myopinion.dao.SurveyRepliesDAO;
import br.com.myopinion.model.Questions;
import br.com.myopinion.model.SurveyReplies;
import br.com.myopinion.questions.MapQuestion;
import br.com.opala.myopinion.R;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;

public class MainOffline extends SherlockFragmentActivity {

	private ViewPager pager;
	private TabsAdapter mTabsAdapter;
	private BasicDAO basicDAO;
	private MainSurveysDAO mainSurveyDao;
	private SurveyReplies surveyReply;
	private QuestionsDAO questionsDao;
	private ObtainedRepliesDAO obtainedRepliesDao;
	private SurveyRepliesDAO surveyRepliesDao;
	private Cursor cursor2;
	private int newOrUpdate;
	private Cursor cursor;

	public static FragmentManager fragmentManager;
	public static boolean isCoordinates;
	public static int pagePos;

	private long positionCoordinates;
	private int count;
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		basicDAO.close();
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getApplicationContext().setTheme(R.style.Theme_Styled);
		setContentView(R.layout.offline_pager);
//
		basicDAO = new BasicDAO(getApplicationContext());
		basicDAO.open();
		mainSurveyDao = new MainSurveysDAO(getApplicationContext());
		questionsDao = new QuestionsDAO(getApplicationContext());
		obtainedRepliesDao = new ObtainedRepliesDAO(getApplicationContext());
		surveyRepliesDao = new SurveyRepliesDAO(getApplicationContext());
		
		fragmentManager = getSupportFragmentManager();

		Bundle extras = getIntent().getExtras();
		surveyReply = (SurveyReplies) extras.getSerializable("surveyReply");
		newOrUpdate = (Integer) extras.get("newOrUpdate");

		System.out.println("MainId: " + surveyReply.getMain_survey_id());
		
		cursor = mainSurveyDao.consultMainSurvey(surveyReply
				.getMain_survey_id());
		cursor.moveToFirst();

		cursor2 = questionsDao.consultQuestionsByQuestionType("GROUPQUESTIONS",
				"GROUPCADASTRE", surveyReply.getMain_survey_id());
		cursor2.moveToFirst();

		pager = (ViewPager) findViewById(R.id.pager);
		// pager = new ViewPager(this);
		// pager.setId(R.id.pager);

		final ActionBar bar = getSupportActionBar();
		bar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

		while (!cursor2.isAfterLast()) {
			isCoordinates = false;
			List<Questions> listQuestion = questionsDao
					.getListDependentQuestions(cursor2.getLong(0));
			for (Questions question : listQuestion) {
				if (question.getQuestionType().equalsIgnoreCase("COORDINATES")) {
					isCoordinates = true;
					positionCoordinates = cursor2.getLong(0);
				}
			}
			cursor2.moveToNext();
		}

		if (isCoordinates) // Verifica se tem tab para coordenadas
			pager.setOffscreenPageLimit(cursor2.getCount() + 1);
		else
			pager.setOffscreenPageLimit(cursor2.getCount());

		mTabsAdapter = new TabsAdapter(this, pager, bar);

		cursor2.moveToFirst();
		count = 0;
		while (!cursor2.isAfterLast()) {

			List<Questions> listQuestion = questionsDao
					.getListDependentQuestions(cursor2.getLong(0));
			Bundle newExtras = getIntent().getExtras();
			newExtras.putString("groupType", cursor2.getString(3));
			newExtras.putInt("required", cursor2.getInt(2));
			newExtras.putParcelableArrayList("questions",
					(ArrayList<? extends Parcelable>) listQuestion);
			newExtras.putLong("idGroupQuestion", cursor2.getLong(0));
			mTabsAdapter.addTab(bar.newTab().setText(cursor2.getString(1)),
					InflateSurvey.class, newExtras);

			if (isCoordinates && positionCoordinates == cursor2.getLong(0)) {
				Bundle extrasMap = getIntent().getExtras();
				extrasMap.putLong("id_mainSurvey", cursor.getLong(0));
				extrasMap.putLong("id_question", cursor2.getLong(0));
				extrasMap.putLong("id_surveyReply", surveyReply.getId());
				mTabsAdapter.addTab(bar.newTab().setText("Localização"),
						MapQuestion.class, extrasMap);
				pagePos = count + 1;
			}

			count++;
			cursor2.moveToNext();
		}
	}

	@Override
	public void onBackPressed() {
		int verifyBlankReplies = 0;
		int checkModify = 0;
		InflateSurvey fragment = null;

		for (int i = 0; i < cursor2.getCount(); i++) {
			fragment = (InflateSurvey) getSupportFragmentManager()
					.findFragmentByTag(
							"android:switcher:" + R.id.pager + ":" + i);
			if (fragment != null) {
				verifyBlankReplies = fragment.verifyBlankReplies()
						+ verifyBlankReplies;
				checkModify = fragment.checkModify() + checkModify;
			}
		}

		// fragment inicial que salva
		fragment = (InflateSurvey) getSupportFragmentManager()
				.findFragmentByTag("android:switcher:" + R.id.pager + ":0");
		if (verifyBlankReplies == 0) {
			fragment.backPressedBlankReplies();
		} else {
			if (newOrUpdate == 0) {
				fragment.backPressedCreate();
			} else if (newOrUpdate == 1) {
				if (checkModify == 0)
					fragment.backPressedUpdateUnmodified();
				else
					fragment.backPressedUpdateModified();
			}
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu items for use in the action bar
		MenuInflater inflater = getSupportMenuInflater();
		inflater.inflate(R.layout.actionbar_itemsave, menu);
		return super.onCreateOptionsMenu(menu);
	}

	public void showQuestionRequired(ArrayList<Double> checkRequired,
			String questionName) {
		Collections.sort(checkRequired);
		Double pos = checkRequired.get(0) + 1;
		String position = String.valueOf(pos);
		if (pos % 1 == 0)
			position = String.valueOf(pos.intValue());
		
		Toast toast = Toast.makeText(this, questionName + "\n" + "Questão "
				+ position + " obrigatória!", Toast.LENGTH_LONG);
		toast.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL,
				0, 0);
		toast.show();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle presses on the action bar items
		switch (item.getItemId()) {
		case R.id.action_save:

			boolean checkRequired = false;
			InflateSurvey fragment = null;
			ArrayList<Double> questionsRequired = new ArrayList<Double>();

			if (isCoordinates) {
				MapQuestion mapFrag = (MapQuestion) getSupportFragmentManager()
						.findFragmentByTag(
								"android:switcher:" + R.id.pager + ":"
										+ pagePos);
				mapFrag.saveCoordinates();
			}

			for (int i = 0; i < cursor2.getCount(); i++) {
				fragment = (InflateSurvey) getSupportFragmentManager()
						.findFragmentByTag(
								"android:switcher:" + R.id.pager + ":" + i);

				questionsRequired = fragment.checkRequired();

				if (questionsRequired.isEmpty())
					fragment.createOrUpdateReplies();
				else {
					mTabsAdapter.changeTab(i);
					cursor2.moveToPosition(i);
					showQuestionRequired(questionsRequired,
							cursor2.getString(1));
					checkRequired = true;
					break;
				}
			}

			if (checkRequired == false) {
				fragment = (InflateSurvey) getSupportFragmentManager()
						.findFragmentByTag(
								"android:switcher:" + R.id.pager + ":0");

				if (newOrUpdate == 0) {
					fragment.dialogCreate();
				} else {
					if (surveyReply.getSync() == true){
						surveyReply.setSync(false);
						surveyRepliesDao.updateSurveyReply(surveyReply);
					}
					fragment.dialogUpdate();
				}
			}
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

}
