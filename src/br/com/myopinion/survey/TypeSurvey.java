package br.com.myopinion.survey;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import br.com.myopinion.dao.AvailableRepliesDAO;
import br.com.myopinion.dao.BasicDAO;
import br.com.myopinion.dao.MainSurveysDAO;
import br.com.myopinion.dao.ObtainedRepliesDAO;
import br.com.myopinion.dao.QuestionsDAO;
import br.com.myopinion.dao.SurveyRepliesDAO;
import br.com.myopinion.model.MainSurveys;

import com.actionbarsherlock.app.SherlockActivity;


public class TypeSurvey extends SherlockActivity{
	private MainSurveys mainSurvey;
	
	private Cursor cursor;
	private BasicDAO basicDAO;
	private MainSurveysDAO mainSurveyDao;
	private SurveyRepliesDAO surveyReplyDao;
	private QuestionsDAO questionsDao;
	private AvailableRepliesDAO availableRepliesDao;
	private ObtainedRepliesDAO obtainedRepliesDao;
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		basicDAO.close();
	}

	@Override
	public void onCreate(Bundle icicle) {
		super.onCreate(icicle);
		try {
			
			basicDAO = new BasicDAO(getApplicationContext());
			basicDAO.open();
	        mainSurveyDao = new MainSurveysDAO(getApplicationContext());
			surveyReplyDao = new SurveyRepliesDAO(getApplicationContext());
			questionsDao = new QuestionsDAO(getApplicationContext());
			availableRepliesDao = new AvailableRepliesDAO(getApplicationContext());
			obtainedRepliesDao = new ObtainedRepliesDAO(getApplicationContext());
			
			Bundle extras = getIntent().getExtras();
			long idMain = extras.getLong("id");
						 
			cursor = mainSurveyDao.consultMainSurvey(idMain);
			cursor.moveToFirst();
		    	
			if (cursor.getString(4).equalsIgnoreCase("ON_LINE")){				
				Intent intent = new Intent(TypeSurvey.this, VerifyLocation.class );
				intent.putExtra("id", cursor.getLong(0));
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(intent);
				finish();
			} else if (cursor.getString(4).equalsIgnoreCase("OFF_LINE")){
				Intent intent = new Intent(TypeSurvey.this, ListQuestionnaries.class );
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				intent.putExtra("id", cursor.getLong(0));
				startActivity(intent);
				finish();
			}			

		} catch (Throwable t) {
			Log.e("MyOpinions", t.getMessage(), t);
		}
	}

}
