package br.com.myopinion.survey;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Parcelable;
import android.os.Vibrator;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.SimpleCursorAdapter.ViewBinder;
import android.widget.TextView;
import br.com.myopinion.adapter.AdapterListQuestionnaries;
import br.com.myopinion.adapter.QustomDialogBuilder;
import br.com.myopinion.dao.AvailableRepliesDAO;
import br.com.myopinion.dao.BasicDAO;
import br.com.myopinion.dao.MainSurveysDAO;
import br.com.myopinion.dao.ObtainedRepliesDAO;
import br.com.myopinion.dao.QuestionsDAO;
import br.com.myopinion.dao.SurveyRepliesDAO;
import br.com.myopinion.model.SurveyReplies;
import br.com.opala.myopinion.R;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;

public class ListQuestionnaries extends SherlockActivity {

	private Cursor cursor;
	private BasicDAO basicDAO;
	private MainSurveysDAO mainSurveyDao;
	private SurveyRepliesDAO surveyReplyDao;
	private long idMain;
	private Vibrator vibrator;
	
	private Parcelable state;
	private ListView listView;
	private TextView tvRespostas;
	private Cursor cursorSync;
	private BaseAdapter adapter;
	private List<SurveyReplies> lisSurveyReplies;
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		basicDAO.close();
	}

	@Override
	public void onCreate(Bundle icicle) {
		super.onCreate(icicle);
		setContentView(R.layout.list_questionnaries);
		//getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		try {
		
			vibrator = (Vibrator) getSystemService( Context.VIBRATOR_SERVICE );

			basicDAO = new BasicDAO(getApplicationContext());
			basicDAO.open();
	        mainSurveyDao = new MainSurveysDAO(getApplicationContext());
			surveyReplyDao = new SurveyRepliesDAO(getApplicationContext());
			new QuestionsDAO(getApplicationContext());
			new AvailableRepliesDAO(getApplicationContext());
			new ObtainedRepliesDAO(getApplicationContext());
			
			Bundle extras = getIntent().getExtras();
			idMain = extras.getLong("id");
			
				
			String nameColumn = SurveyRepliesDAO.COLUNA_CREATED_AT;
	        cursor = surveyReplyDao.consultSurveyRepliesByMainSurvey(idMain);
	       	cursorSync = surveyReplyDao.getSyncSurveyReplies(idMain, 0);
	       	
	       	String[] from = {nameColumn};
	       	int[] to = { R.id.text };
	      		
       		tvRespostas = (TextView) findViewById(R.id.list_questionnaries);

	       	if (!cursor.isAfterLast()){
	       		tvRespostas.setText("Respostas: " + cursor.getCount() + "\n" + "Não sincronizadas: " + cursorSync.getCount());
	       		tvRespostas.setVisibility(View.VISIBLE);
	       	}
	       	
	       	lisSurveyReplies = surveyReplyDao.getListSurveyReplies(idMain);
	       	
	       	listView = (ListView) findViewById(android.R.id.list);

//	       	SimpleCursorAdapter adapter = new SimpleCursorAdapter(this, R.layout.row_list, cursor, from, to);
//	      
//	       	adapter.setViewBinder(new ViewBinder() {
//
//	       	    public boolean setViewValue(View aView, Cursor aCursor, int aColumnIndex) {
//
//	       	        if (aColumnIndex == 1) {
//	       	                String createDate = aCursor.getString(aColumnIndex);
//	       	                TextView textView = (TextView) aView;
//	       	                long dateSec = Long.valueOf(createDate).longValue();
//	       	                Date date = new Date(dateSec);
//	       	         	    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
//	       	                String dateFormated = dateFormat.format(date);
//	       	        		String label = String.valueOf(dateFormated);
//	       	                textView.setText("Resposta: " + label + "h");
//	       	                return true;
//	       	         }
//
//	       	         return false;
//	       	    }
//	       	});
	       
	       	adapter = new AdapterListQuestionnaries(this, R.layout.row_list_questionnaries, lisSurveyReplies);
	       	listView.setAdapter(adapter);
		    state = listView.onSaveInstanceState();

	       	registerForContextMenu(listView);
	       	
	       	//String query2 = "SELECT * FROM MAIN_SURVEYS WHERE MAIN_SURVEYS._id = " + idMain;
	       	Cursor cursor2 = mainSurveyDao.consultMainSurvey(idMain);
	       	cursor2.moveToFirst();
	       	TextView survey = (TextView) findViewById(R.id.label_questionnaries);
			survey.setText(cursor2.getString(1));
			
	       	Button buttonQuestionnaries = (Button) findViewById(R.id.button_questionnaries);
	   		buttonQuestionnaries.setOnClickListener(new OnClickListener() {
			
				@Override
				public void onClick(View v) {
					try{
											
						Intent intent = new Intent (ListQuestionnaries.this, VerifyLocation.class);
						intent.putExtra("id", idMain);
						startActivity(intent);
						finish();
					} catch (Throwable t) {
						Log.e("MyOpinions", t.getMessage(), t);
					}
				}
				
	   		});
		

	       
	       
	       listView.setOnItemClickListener(new OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
					cursor.moveToPosition(arg2);
					
				    SurveyReplies surveyReply = surveyReplyDao.getSurveyReply(cursor.getLong(0));
				  
					Intent in = new Intent (ListQuestionnaries.this, MainOffline.class);
					in.putExtra("surveyReply", surveyReply);
					in.putExtra("newOrUpdate", 1);
					startActivity(in);
					//db.close();
					//finish();
			        }	
				});
			
	       
		} catch (Throwable t) {
			Log.e("MyOpinions", t.getMessage(), t);
		}
	}
	
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
		 super.onCreateContextMenu(menu, v, menuInfo);
	 if (v.getId()==android.R.id.list) {
		AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
		cursor.moveToPosition(info.position);
	    String[] menuItems = getResources().getStringArray(R.array.menu);
	    for (int i = 0; i<menuItems.length; i++) {
	      menu.add(Menu.NONE, i, i, menuItems[i]);
	    }
	  }
	}
	
	 @Override
	    public boolean onContextItemSelected(android.view.MenuItem item) {
		    AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)item.getMenuInfo();
		    int menuItemIndex = item.getItemId();
			String[] menuItems = getResources().getStringArray(R.array.menu);
			String menuItemName = menuItems[menuItemIndex];
			if (menuItemName.equalsIgnoreCase("Excluir")){
				
				cursor.moveToPosition(info.position);
			    SurveyReplies surveyReply = surveyReplyDao.getSurveyReply(cursor.getLong(0));
				
			    surveyReplyDao.deleteSurveyReply(surveyReply.getId());
			    

		       	lisSurveyReplies = surveyReplyDao.getListSurveyReplies(idMain);
		       	adapter = new AdapterListQuestionnaries(this, R.layout.row_list_questionnaries, lisSurveyReplies);
		       	listView.setAdapter(adapter);
		       	   
				cursor.requery();
				cursorSync.requery();
	       		tvRespostas.setText("Respostas: " + cursor.getCount() + "\n" + "Não sincronizadas: " + cursorSync.getCount());
				if(cursor.getCount() == 0){
		       		tvRespostas.setVisibility(View.GONE);
		          	
				}
				return true;
			}
			
	    	return true;
	    }
	 
	 @Override
		public void onBackPressed() {
				Intent intent = new Intent (ListQuestionnaries.this, ListSurveys.class);
				startActivity(intent);
			//	db.close();
				finish();
			super.onBackPressed();
		}
	 
	 @Override
	 public void onPause() {
	     super.onPause();
	 //    index = listView.getFirstVisiblePosition();
	   //  View v = listView.getChildAt(0);
	     //top = (v == null) ? 0 : v.getTop();
	     state = listView.onSaveInstanceState();
	    // state = ExpandList.onSaveInstanceState();

	 }
	  
	 @Override
	 public void onResume() {
	     super.onResume();
	     // return scroll positon to last viewed.
	    // listView.setSelectionFromTop(index, top);
	    lisSurveyReplies = surveyReplyDao.getListSurveyReplies(idMain);
	 	adapter = new AdapterListQuestionnaries(this, R.layout.row_list_questionnaries, lisSurveyReplies);
	 	listView.setAdapter(adapter);
       	
		cursor.requery();
	    cursorSync.requery();
	    tvRespostas.setText("Respostas: " + cursor.getCount() + "\n" + "Não sincronizadas: " + cursorSync.getCount());

	    listView.onRestoreInstanceState(state);
	     //ExpandList.onRestoreInstanceState(state);

	 }
	 
	 @Override
		public boolean onCreateOptionsMenu(Menu menu) {
			MenuItem item = menu.add(0, 0, 0, "Apagar todas as respostas");

			return super.onCreateOptionsMenu(menu);

		}
	 
	 @Override
	 public boolean onOptionsItemSelected(MenuItem item) {
	   /* switch (item.getItemId()) {
	       case android.R.id.home:
	          NavUtils.navigateUpTo(this,
	                new Intent(this, MyOpinionsActivity.class));
	          return true;    	   
	    }*/
	    if (item.getItemId() == 0){
	    	deleteDialog();
		}
	    
	    return super.onOptionsItemSelected(item);
	 }
	
	public void deleteDialog(){
		final String COLOR = "#A8Cf45";

		final QustomDialogBuilder qustomDialogBuilder = (QustomDialogBuilder) new QustomDialogBuilder(this).
				setTitleColor(COLOR).
				setDividerColor(COLOR);
		// set title
		qustomDialogBuilder.setTitle("Excluir Respostas");

		// set dialog message
		qustomDialogBuilder
			.setMessage("Tem certeza que deseja excluir todas as respostas?")
			.setCancelable(true)
			.setPositiveButton("Sim",new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {
					// if this button is clicked, close
					// current activity
				//	MyOpinionsActivity.this.finish();
					
					 List listSurveyReply = surveyReplyDao.getListSurveyReplies(idMain);
					    Iterator<SurveyReplies> it = listSurveyReply.iterator();
						while (it.hasNext()) {
						    SurveyReplies surveyReply = new SurveyReplies();
							surveyReply = (SurveyReplies) it.next();	
							surveyReplyDao.deleteSurveyReply(surveyReply.getId());
						}
						cursor.requery();
						tvRespostas.setVisibility(View.GONE);
				}
			  })
			.setNegativeButton("Não",new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {
					// if this button is clicked, just close
					// the dialog box and do nothing
					dialog.cancel();
				}
			});

			// create alert dialog
			AlertDialog alertDialog = qustomDialogBuilder.create();

			// show it
			alertDialog.show();
	}
}
	

