package br.com.myopinion.result;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import br.com.opala.myopinion.R;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

public class ResultActivity extends SherlockFragmentActivity{

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_fragment_result);
		
		List<Result> listResults = new ArrayList<Result>();
		
		String json_str = "[" +
				"{ \"title\":\"Sexo:\", \"replies\": [ \"Masculino\", \"Feminino\" ], \"results\": [ \"123\", \"155\" ] , \"type\":\"bar\" }, " +
				"{ \"title\":\"Que time você torce?\", \"replies\": [ \"Flamengo\", \"Vasco\", \"Fluminense\", \"Botafogo\" ], \"results\": [ \"254\", \"186\", \"112\", \"89\" ], \"type\":\"pie\" }," +
				"{ \"title\":\"Quais esportes você pratica?\", \"replies\": [ \"Futebol\", \"Vôlei\", \"Basquete\", \"Handebol\" ], \"results\": [ \"175\", \"125\", \"105\", \"95\" ], \"type\":\"pie\" }," +
				"{ \"title\":\"O que você acha do aplicativo?\", \"replies\": [ \"Bom\", \"Muito bom\", \"Excelente\", \"Ruim\", \"Deveria melhorar\", \"Nem ruim nem bom\" ], \"results\": [ \"23\", \"13\", \"15\", \"05\", \"09\", \"01\" ], \"type\":\"list\" }" +
				"]";

		JsonElement json = new JsonParser().parse(json_str);
		JsonArray array = json.getAsJsonArray();
		Iterator iterator = array.iterator();
		while (iterator.hasNext()){
			JsonElement jsonElement = (JsonElement) iterator.next();			
			Gson gson = new Gson();
		    Result result = gson.fromJson(jsonElement, Result.class);
		    listResults.add(result);
		}
		
		Bundle extras = new Bundle();
		extras.putSerializable("listResults", (Serializable) listResults);
		ResultFragment resultFragment =  new ResultFragment();
		resultFragment.setArguments(extras);
		
		FragmentManager fm = getSupportFragmentManager();
		FragmentTransaction ft = fm.beginTransaction();
		ft.add(R.id.fragment_result, resultFragment).commit();
	}

	public static class Result implements Serializable{
		/**
		 * 
		 */
		private static final long serialVersionUID = 1238144650554082015L;
		private String title;
		private List<String> replies;
		private List<Float> results;
		private String type;
				
		public String getTitle() {
			return title;
		}
		public void setTitle(String title) {
			this.title = title;
		}
		public List<String> getReplies() {
			return replies;
		}
		public void setReplies(List<String> replies) {
			this.replies = replies;
		}
		public List<Float> getResults() {
			return results;
		}
		public void setResults(List<Float> results) {
			this.results = results;
		}
		public String getType() {
			return type;
		}
		public void setType(String type) {
			this.type = type;
		}
		@Override
		public String toString() {
			return "Result [title=" + title + ", replies=" + replies
					+ ", results=" + results + ", type=" + type + "]";
		}
				
	}
	
}
