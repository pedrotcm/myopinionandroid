/*
 * 	   Created by Daniel Nadeau
 * 	   daniel.nadeau01@gmail.com
 * 	   danielnadeau.blogspot.com
 * 
 * 	   Licensed to the Apache Software Foundation (ASF) under one
       or more contributor license agreements.  See the NOTICE file
       distributed with this work for additional information
       regarding copyright ownership.  The ASF licenses this file
       to you under the Apache License, Version 2.0 (the
       "License"); you may not use this file except in compliance
       with the License.  You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

       Unless required by applicable law or agreed to in writing,
       software distributed under the License is distributed on an
       "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
       KIND, either express or implied.  See the License for the
       specific language governing permissions and limitations
       under the License.
 */

package br.com.myopinion.result;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;
import br.com.myopinion.adapter.Utility;
import br.com.myopinion.result.ResultActivity.Result;
import br.com.opala.myopinion.R;

import com.echo.holographlibrary.Bar;
import com.echo.holographlibrary.BarGraph;
import com.echo.holographlibrary.BarGraph.OnBarClickedListener;

import java.util.ArrayList;

public class BarFragment extends Fragment {

    private ArrayList<Bar> aBars;

	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.fragment_bargraph, container, false);
        final Resources resources = getResources();
        
        Utility utility = new Utility();
        
        Result result = (Result) getArguments().getSerializable("result");
        int nTitle = getArguments().getInt("nTitle");
        
        TextView titleResult = (TextView) v.findViewById(R.id.titleResult);
        titleResult.setText(nTitle + ") " + result.getTitle());
        
        aBars = new ArrayList<Bar>();
        for (int i=0; i < result.getReplies().size() ; i++){
        	Bar bar = new Bar();
            bar.setColor(resources.getColor(utility.getColor(i)));
            bar.setName(result.getReplies().get(i));
            bar.setValue(calcPercentage(result, result.getResults().get(i)));
            bar.setValueString(String.valueOf(result.getResults().get(i)));
            aBars.add(bar);
        }        
        
        BarGraph barGraph = (BarGraph) v.findViewById(R.id.bargraph);
        barGraph.setBars(aBars);

        barGraph.setOnBarClickedListener(new OnBarClickedListener() {

            @Override
            public void onClick(int index) {
            	Toast toast = Toast.makeText(getActivity(),
                 		String.format("%.2f", aBars.get(index).getValue()) + "%",
                 		Toast.LENGTH_SHORT );
 				//toast.setGravity(Gravity.CENTER, 0, 0);
 				toast.show();
            }
        });

        return v;
    }
    
	public float calcPercentage(Result result, float value){
		float total = 0;
		for (float v : result.getResults()){
			total = v + total;
		}
		
		float percentage = (value*100) / total;
		
		return percentage;
	}
    
}
