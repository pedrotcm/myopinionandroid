/*
 * 	   Created by Daniel Nadeau
 * 	   daniel.nadeau01@gmail.com
 * 	   danielnadeau.blogspot.com
 * 
 * 	   Licensed to the Apache Software Foundation (ASF) under one
       or more contributor license agreements.  See the NOTICE file
       distributed with this work for additional information
       regarding copyright ownership.  The ASF licenses this file
       to you under the Apache License, Version 2.0 (the
       "License"); you may not use this file except in compliance
       with the License.  You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

       Unless required by applicable law or agreed to in writing,
       software distributed under the License is distributed on an
       "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
       KIND, either express or implied.  See the License for the
       specific language governing permissions and limitations
       under the License.
 */

package br.com.myopinion.result;

import java.util.ArrayList;
import java.util.List;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import br.com.myopinion.adapter.AdapterLegend;
import br.com.myopinion.adapter.Utility;
import br.com.myopinion.result.ResultActivity.Result;
import br.com.opala.myopinion.R;

import com.echo.holographlibrary.PieGraph;
import com.echo.holographlibrary.PieGraph.OnSliceClickedListener;
import com.echo.holographlibrary.PieSlice;

public class PieFragment extends Fragment {

    private PieGraph pg;
    
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_piegraph, container, false);
        Resources resources = getResources();
        
        Utility utility = new Utility();
        
        Result result = (Result) getArguments().getSerializable("result");
        int nTitle = getArguments().getInt("nTitle");
        
        TextView titleResult = (TextView) v.findViewById(R.id.titleResult);
        titleResult.setText(nTitle + ") " + result.getTitle());
        
        ListView legend = (ListView) v.findViewById(android.R.id.list);
        BaseAdapter adapter = new AdapterLegend(getActivity(), R.layout.row_legend, result.getReplies());
        legend.setAdapter(adapter);
        
        pg = (PieGraph) v.findViewById(R.id.piegraph);
        for (int i=0; i < result.getReplies().size() ; i++){
            PieSlice slice = new PieSlice();
            slice.setColor(resources.getColor(utility.getColor(i)));
            slice.setValue(result.getResults().get(i));
            slice.setTitle(calcPercentage(result, result.getResults().get(i)));
            pg.addSlice(slice);
        }
    
        pg.setOnSliceClickedListener(new OnSliceClickedListener() {

            @Override
            public void onClick(int index) {
                Toast toast = Toast.makeText(getActivity(),
                		pg.getSlice(index).getTitle() + "%",
                		Toast.LENGTH_SHORT );
				//toast.setGravity(Gravity.CENTER, 0, 0);
				toast.show();
            }
        });
        

        return v;
    }
	
	public String calcPercentage(Result result, float value){
		float total = 0;
		for (float v : result.getResults()){
			total = v + total;
		}
		
		float percentage = (value*100) / total;
		
		return String.format("%.2f", percentage);
	}
	
}
