package br.com.myopinion.result;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import br.com.myopinion.result.ResultActivity.Result;
import br.com.opala.myopinion.R;

import com.actionbarsherlock.app.SherlockFragment;

public class ResultFragment extends SherlockFragment {

	
	
	private LinearLayout ll;

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onViewCreated(view, savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.inflate_survey, container, false);
		ll = (LinearLayout) rootView.findViewById(R.id.linearLayout);
	
		List<Result> listResults = (List<Result>) getArguments().getSerializable("listResults");
		
		for(int i = 0; i < listResults.size(); i++){
			if(listResults.get(i).getType().equalsIgnoreCase("pie")){
				createPieGraph(i, listResults.get(i));
			} else if (listResults.get(i).getType().equalsIgnoreCase("bar")){
				createBarGraph(i, listResults.get(i));
			} else if (listResults.get(i).getType().equalsIgnoreCase("list")){
				createListGraph(i, listResults.get(i));
			}
		}
				
		return rootView;
	}
	
	private void createPieGraph(int id, Result result){
		LinearLayout viewGraph = new LinearLayout(getActivity());
		viewGraph.setId(id+1);

		Bundle extras = new Bundle();
		extras.putInt("nTitle", id+1);
		extras.putSerializable("result", result);
		
		PieFragment pieGraph = new PieFragment();
		pieGraph.setArguments(extras);
		getFragmentManager().beginTransaction().add(viewGraph.getId(), pieGraph).commit();		
		ll.addView(viewGraph);
	}
	
	private void createBarGraph(int id, Result result){
		LinearLayout viewGraph = new LinearLayout(getActivity());
		viewGraph.setId(id+1);

		Bundle extras = new Bundle();
		extras.putInt("nTitle", id+1);
		extras.putSerializable("result", result);

		BarFragment barGraph = new BarFragment();
		barGraph.setArguments(extras);
		getFragmentManager().beginTransaction().add(viewGraph.getId(), barGraph).commit();		
		ll.addView(viewGraph);
	}
	
	private void createListGraph(int id, Result result){
		LinearLayout viewGraph = new LinearLayout(getActivity());
		viewGraph.setId(id+1);

		Bundle extras = new Bundle();
		extras.putInt("nTitle", id+1);
		extras.putSerializable("result", result);

		ListFragment listGraph = new ListFragment();
		listGraph.setArguments(extras);
		getFragmentManager().beginTransaction().add(viewGraph.getId(), listGraph).commit();		
		ll.addView(viewGraph);
	}	
}
