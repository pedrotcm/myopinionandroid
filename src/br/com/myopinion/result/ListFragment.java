/*
 * 	   Created by Daniel Nadeau
 * 	   daniel.nadeau01@gmail.com
 * 	   danielnadeau.blogspot.com
 * 
 * 	   Licensed to the Apache Software Foundation (ASF) under one
       or more contributor license agreements.  See the NOTICE file
       distributed with this work for additional information
       regarding copyright ownership.  The ASF licenses this file
       to you under the Apache License, Version 2.0 (the
       "License"); you may not use this file except in compliance
       with the License.  You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

       Unless required by applicable law or agreed to in writing,
       software distributed under the License is distributed on an
       "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
       KIND, either express or implied.  See the License for the
       specific language governing permissions and limitations
       under the License.
 */

package br.com.myopinion.result;

import java.util.LinkedHashMap;
import java.util.Map;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import br.com.myopinion.adapter.AdapterListGraph;
import br.com.myopinion.adapter.Utility;
import br.com.myopinion.result.ResultActivity.Result;
import br.com.opala.myopinion.R;

public class ListFragment extends Fragment {


	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.fragment_listgraph, container, false);
                
        Result result = (Result) getArguments().getSerializable("result");
        int nTitle = getArguments().getInt("nTitle");
        
        TextView titleResult = (TextView) v.findViewById(R.id.titleResult);
        titleResult.setText(nTitle + ") " + result.getTitle());
        
        
        Map<String, Integer> replies = new LinkedHashMap<String, Integer>();
        
        for (int i = 0; i< result.getReplies().size(); i++){
        	replies.put(result.getReplies().get(i), Math.round((result.getResults().get(i))));
        }
        
        System.out.println(replies.toString());

        ListView listResults = (ListView) v.findViewById(android.R.id.list);
        //AdapterListGraph adapter = new AdapterListGraph(getActivity(), R.layout.row_listgraph, replies);
        BaseAdapter adapter = new AdapterListGraph(getActivity(), R.layout.row_listgraph, result.getReplies(), result.getResults());
        listResults.setAdapter(adapter);
        
        //ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1,result.getReplies());
        //listResults.setAdapter(adapter2);
       // Utility.setListViewHeightBasedOnChildren(listResults);
        
        return v;
    }
    
	public float calcPercentage(Result result, float value){
		float total = 0;
		for (float v : result.getResults()){
			total = v + total;
		}
		
		double percentage = (value*100) / total;
		
		return Float.valueOf(String.format("%.2f", percentage));
	}
    
}
