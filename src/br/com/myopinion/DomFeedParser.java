package br.com.myopinion;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import br.com.myopinion.dao.AvailableRepliesDAO;
import br.com.myopinion.dao.BasicDAO;
import br.com.myopinion.dao.MainSurveysDAO;
import br.com.myopinion.dao.ObtainedRepliesDAO;
import br.com.myopinion.dao.OtherQuestionDAO;
import br.com.myopinion.dao.OthersRepliesDAO;
import br.com.myopinion.dao.QuestionsDAO;
import br.com.myopinion.dao.SurveyRepliesDAO;
import br.com.myopinion.model.AvailableReplies;
import br.com.myopinion.model.MainSurveys;
import br.com.myopinion.model.OtherQuestion;
import br.com.myopinion.model.OthersReplies;
import br.com.myopinion.model.Questions;
import br.com.myopinion.survey.ListQuestionnaries;
import br.com.myopinion.survey.TypeSurvey;

import com.actionbarsherlock.app.SherlockActivity;

	
public class DomFeedParser extends SherlockActivity{
	
	private MainSurveysDAO mainSurveyDao;
	private SurveyRepliesDAO surveyReplyDao;
	private QuestionsDAO questionsDao;
	private AvailableRepliesDAO availableRepliesDao;
	private ObtainedRepliesDAO obtainedRepliesDao;
	private OthersRepliesDAO othersRepliesDao;
	private OtherQuestionDAO otherQuestionDao;
	private BasicDAO basicDao;
	private Cursor cursor;
	private int checkExist = 0;
	private int position = 0;
	private DocumentBuilder builder;
	private Document dom;
	private byte[] decoded = null;
	private String link;
	private Boolean isLink = false;
	private Boolean linkOpened = false;
	private String arquivo;
	private boolean groupQuestions = false;
	/*protected DomFeedParser(String feedUrl) {
		super(feedUrl);
	}*/
	private Date validate3;
	private boolean checkDate = false;
	
	public static String convertStreamToString(InputStream is) throws Exception {
	    BufferedReader reader = new BufferedReader(new InputStreamReader(is));
	    StringBuilder sb = new StringBuilder();
	    String line = null;
	    while ((line = reader.readLine()) != null) {
	      sb.append(line).append("\n");
	    }
	    return sb.toString();
	}
	
	
	@Override
	protected void onResume() {
		if (isLink.booleanValue() == true && linkOpened.booleanValue() == false){
			invalidQRCode();
		} else if (linkOpened == true) {
			Intent intent = new Intent (DomFeedParser.this, MyOpinionsActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			finish();
		}
			
		super.onResume();
	}

	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		basicDao.close();
	}


	@Override
	protected void onCreate( Bundle savedInstanceState ) {
		super.onCreate( savedInstanceState );
		try{

		basicDao = new BasicDAO(getApplicationContext());
	    basicDao.open();
		mainSurveyDao = new MainSurveysDAO(getApplicationContext());
		surveyReplyDao = new SurveyRepliesDAO(getApplicationContext());
		questionsDao = new QuestionsDAO(getApplicationContext());
		availableRepliesDao = new AvailableRepliesDAO(getApplicationContext());
		obtainedRepliesDao = new ObtainedRepliesDAO(getApplicationContext());
		othersRepliesDao = new OthersRepliesDAO(getApplicationContext());
		otherQuestionDao = new OtherQuestionDAO(getApplicationContext());
				
			cursor = mainSurveyDao.consultMainSurveyBySurveyType("OFF_LINE");
	
			MainSurveys mainSurvey = new MainSurveys();

			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

			Bundle extras = getIntent().getExtras();
			arquivo = extras.getString("filexml");
			link = extras.getString("link");
			
			File file = getBaseContext().getFileStreamPath(arquivo);
                     
			FileInputStream fin = new FileInputStream(file);
		    String ret = convertStreamToString(fin);
		   
			// Set up secret key spec for 128-bit AES encryption and decryption
			SecretKeySpec sks = null;
			try {
				SecureRandom sr = SecureRandom.getInstance("SHA1PRNG");
				sr.setSeed("myopinion0p@l@".getBytes());
				KeyGenerator kg = KeyGenerator.getInstance("AES");
				kg.init(128, sr);
				sks = new SecretKeySpec((kg.generateKey()).getEncoded(), "AES");
			} catch (Exception e) {
				Log.e("Erro", "AES secret key spec error");
			}

			//Criptografando
//			String theTestText = ret;
//			// Encode the original data with AES
//			byte[] encodedBytes = null;
//			try {
//				Cipher c = Cipher.getInstance("AES");
//				c.init(Cipher.ENCRYPT_MODE, sks);
//				encodedBytes = c.doFinal(theTestText.getBytes());
//			} catch (Exception e) {
//				Log.e("Erro", "AES encryption error");
//			}		
//			//Log.e("Encoded",
//				//Base64.encodeToString(encodedBytes, Base64.DEFAULT));
//
//			 File myFile = new File("/sdcard/mysdfile.xml");
//				myFile.createNewFile();
//				FileOutputStream fOut = new FileOutputStream(myFile);
//				OutputStreamWriter myOutWriter = 
//										new OutputStreamWriter(fOut);
//				myOutWriter.append(Base64.encodeToString(encodedBytes, Base64.DEFAULT));
//				myOutWriter.close();
//				fOut.close();		    
//			    //Make sure you close all streams.
//			    fin.close(); 
			    
			
			try {
				String crypt = ret;
				decoded = Base64.decode(crypt.getBytes(), Base64.DEFAULT);
			} catch (IllegalArgumentException e) {
				
			}
			// Decode the encoded data with AES
			byte[] decodedBytes = null;
			try {
				Cipher c = Cipher.getInstance("AES");
				c.init(Cipher.DECRYPT_MODE, sks);
				decodedBytes = c.doFinal(decoded);
			} catch (Exception e) {
				Log.e("Erro", "AES decryption error");
			}		
			
			DocumentBuilder builder = factory.newDocumentBuilder();
			  
		    if (decodedBytes == null){
		    	dom = builder.parse(file);
		    } else {
			    			
				FileOutputStream output = openFileOutput("file.xml", MODE_WORLD_READABLE); 
				OutputStreamWriter osw = new OutputStreamWriter(output); 
				osw.write(new String(decodedBytes));
				osw.flush();
				osw.close();	
				
				File fileXML = getBaseContext().getFileStreamPath("file.xml");
				
				dom = builder.parse(fileXML);
		    }
		    
			dom.getDocumentElement().normalize();

			Element root = dom.getDocumentElement();
			//NodeList questions = root.getElementsByTagName("questions");
			NodeList tags = root.getChildNodes();
			for (int i = 0; i < tags.getLength(); i++) {
				Node tag = tags.item(i);
				if (tag.getNodeName().equalsIgnoreCase("validate")) {
					String validateDate = tag.getFirstChild().getNodeValue();
					String data = validateDate.substring(0,19);
					SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					Date validate1 = (Date) dateFormat1.parse(data);
					SimpleDateFormat dateFormat2 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
					String validate2 = dateFormat2.format(validate1);
					SimpleDateFormat dateFormat3 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
					validate3 = (Date) dateFormat3.parse(validate2);
					
					mainSurvey.setValidate(validate3);
					
				} else if (tag.getNodeName().equalsIgnoreCase("validateEnd")) {
					String validateDate = tag.getFirstChild().getNodeValue();
					String data = validateDate.substring(0,19);
					SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					Date validate1 = (Date) dateFormat1.parse(data);
					SimpleDateFormat dateFormat2 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
					String validate2 = dateFormat2.format(validate1);
					SimpleDateFormat dateFormat3 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
					Date validateEnd = (Date) dateFormat3.parse(validate2);
					  
					Date date = new Date();

					if (date.before(validate3) || date.after(validateEnd)){
						checkDate = true;		
					}
										
				} else if (tag.getNodeName().equalsIgnoreCase("description")) {
					mainSurvey.setDescription(tag.getFirstChild().getNodeValue());
				} else if (tag.getNodeName().equalsIgnoreCase("creationDate")) {
					String creationDate = tag.getFirstChild().getNodeValue();
					String data = creationDate.substring(0,19);
					SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					Date validate1 = (Date) dateFormat1.parse(data);
					SimpleDateFormat dateFormat2 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
					String validate2 = dateFormat2.format(validate1);
					SimpleDateFormat dateFormat3 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
					Date validate3 = (Date) dateFormat3.parse(validate2);
					mainSurvey.setCreated_at(validate3);
				} else if (tag.getNodeName().equalsIgnoreCase("interval")) {
					mainSurvey.setInterval(Integer.parseInt(tag.getFirstChild().getNodeValue()));
				} else if (tag.getNodeName().equalsIgnoreCase("surveyType")) {
					mainSurvey.setSurveyType(tag.getFirstChild().getNodeValue());
				} else if (tag.getNodeName().equalsIgnoreCase("descriptionComplete")) {
					mainSurvey.setDescriptionComplete(tag.getFirstChild().getNodeValue());
				} else if (tag.getNodeName().equalsIgnoreCase("getCoordinates")) {
					mainSurvey.setGetCoordinates(Boolean.valueOf(tag.getFirstChild().getNodeValue()));
				} else if (tag.getNodeName().equalsIgnoreCase("location")){
					NodeList itemsLocation = tag.getChildNodes();
					for (int k = 0; k < itemsLocation.getLength(); k++) {
						Node value_item = itemsLocation.item(k);
						if (value_item.getNodeName().equalsIgnoreCase("latitude")) {
							mainSurvey.setLatitude(Double.valueOf(value_item.getFirstChild().getNodeValue()));
						} else if (value_item.getNodeName().equalsIgnoreCase("longitude")) {
							mainSurvey.setLongitude(Double.valueOf(value_item.getFirstChild().getNodeValue()));
						} else if (value_item.getNodeName().equalsIgnoreCase("radius")) {
							mainSurvey.setRadius(Double.valueOf(value_item.getFirstChild().getNodeValue()));
						} 
					}
				}
			}
			
			while (!cursor.isAfterLast()) {
			    if ((mainSurvey.getCreated_at().compareTo(new Date(cursor.getLong(2)))) == 0 && mainSurvey.getSurveyType().equalsIgnoreCase("OFF_LINE")){
			    	checkExist = 1;
			    	position = cursor.getPosition();
			    }
			    cursor.moveToNext();
			}
			
			if (checkDate == true) {
				AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(DomFeedParser.this);
				 
				// set title
				alertDialogBuilder.setTitle("Acesso Negado");
				alertDialogBuilder.setMessage("A sua situação atual não permite acesso ao questionário.");

	 
				// set dialog message
				alertDialogBuilder
					//.setMessage("Deseja responder o questionário novamente?")
					.setCancelable(false)
					.setPositiveButton("OK",new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,int id) {
							// if this button is clicked, close
							// current activity
							cursor.moveToPosition(position);
							Intent intent = new Intent (DomFeedParser.this, MyOpinionsActivity.class);
							intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
							startActivity(intent);
							//db.close();
							finish();
						}
					  });
						 
					// create alert dialog
					AlertDialog alertDialog = alertDialogBuilder.create();
	 
					// show it
					alertDialog.show();	
			} else if (checkExist == 1 ){
				AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(DomFeedParser.this);
				 
				// set title
				alertDialogBuilder.setTitle("O questionário já existe.");
	 
				// set dialog message
				alertDialogBuilder
					//.setMessage("Deseja responder o questionário novamente?")
					.setCancelable(false)
					.setPositiveButton("OK",new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,int id) {
							// if this button is clicked, close
							// current activity
							cursor.moveToPosition(position);
							Intent intent = new Intent (DomFeedParser.this, ListQuestionnaries.class);
							intent.putExtra("id", cursor.getLong(0));
							intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
							startActivity(intent);
							//db.close();
							finish();
						}
					  });
						 
					// create alert dialog
					AlertDialog alertDialog = alertDialogBuilder.create();
	 
					// show it
					alertDialog.show();		
			} else {
				
			long idMainSurvey = 0;
			long idQuestion = 0;
			long idQuestionDep = 0;
			long idAvailableReplies = 0;
			long idOtherQuestion = 0;
			
			idMainSurvey = mainSurveyDao.createMainSurvey(mainSurvey);
			NodeList tagQuestions = root.getElementsByTagName("questions");
			
			for (int y = 0; y < tagQuestions.getLength(); y++) {
				Node question = tagQuestions.item(y);
				NodeList tagQuestion = question.getChildNodes();
				for (int w = 0; w < tagQuestion.getLength(); w++) {
					Questions questions = new Questions();
					Node property = tagQuestion.item(w);
					NodeList properties = property.getChildNodes();
					for (int j = 0; j < properties.getLength(); j++) {
						Node value = properties.item(j);
						if (value.getNodeName().equalsIgnoreCase("title")) {
							questions.setTitle(value.getFirstChild().getNodeValue());
						} else if (value.getNodeName().equalsIgnoreCase("required")) {
							questions.setRequired(Boolean.valueOf(value.getFirstChild().getNodeValue()));
						} else if (value.getNodeName().equalsIgnoreCase("textType")) {
							questions.setTextType(value.getFirstChild().getNodeValue());
						} else if (value.getNodeName().equalsIgnoreCase("questionType")) {
								questions.setQuestionType(value.getFirstChild().getNodeValue());
								questions.setMain_survey_id(idMainSurvey);
								if (value.getFirstChild().getNodeValue().equalsIgnoreCase("GROUPQUESTIONS")){
									idQuestionDep = questionsDao.createQuestion(questions);
								} else if(value.getFirstChild().getNodeValue().equalsIgnoreCase("GROUPCADASTRE")){
									idQuestionDep = questionsDao.createQuestion(questions);
								} else {
									idQuestion = questionsDao.createQuestion(questions);	
								}
						} else if (value.getNodeName().equalsIgnoreCase("items")) {
							NodeList items = value.getChildNodes();
							for (int k = 0; k < items.getLength(); k++) {
								NodeList item = items.item(k).getChildNodes();
								for(int l = 0; l <item.getLength(); l++){
									Node value_item = item.item(l);
									AvailableReplies availableReplies = new AvailableReplies();
									if (value_item.getNodeName().equalsIgnoreCase("itemLabel")) {
										availableReplies.setAvaReply(value_item.getFirstChild().getNodeValue());
										availableReplies.setQuestion_id(idQuestion);
										idAvailableReplies = availableRepliesDao.createAvaReply(availableReplies);
									} 
								}
							}
						} else if (value.getNodeName().equalsIgnoreCase("dependentQuestions")) {
							NodeList items = value.getChildNodes();
							for (int k = 0; k < items.getLength(); k++) {
								NodeList item = items.item(k).getChildNodes();
								Questions questionDependent = new Questions();
								for(int l = 0; l <item.getLength(); l++){
									Node value_item = item.item(l);
									if (value_item.getNodeName().equalsIgnoreCase("title")) {
										questionDependent.setTitle(value_item.getFirstChild().getNodeValue());
									} else if (value_item.getNodeName().equalsIgnoreCase("required")) {
										questionDependent.setRequired(Boolean.valueOf(value_item.getFirstChild().getNodeValue()));
									} else if (value_item.getNodeName().equalsIgnoreCase("textType")) {
										questionDependent.setTextType(value_item.getFirstChild().getNodeValue());
									} else if (value_item.getNodeName().equalsIgnoreCase("questionType")) {
											questionDependent.setQuestionType(value_item.getFirstChild().getNodeValue());
											questionDependent.setQuestion_id(idQuestionDep);
											idQuestion = questionsDao.createQuestionDependent(questionDependent);		
									} else if (value_item.getNodeName().equalsIgnoreCase("items")) {
										NodeList itemsDependent = value_item.getChildNodes();
										for (int m = 0; m < itemsDependent.getLength(); m++) {
											NodeList itemDependent = itemsDependent.item(m).getChildNodes();
											for(int n = 0; n <itemDependent.getLength(); n++){
												Node value_itemDependent = itemDependent.item(n);
												AvailableReplies availableReplies = new AvailableReplies();
												if (value_itemDependent.getNodeName().equalsIgnoreCase("itemLabel")) {
													availableReplies.setAvaReply(value_itemDependent.getFirstChild().getNodeValue());
													availableReplies.setQuestion_id(idQuestion);
													idAvailableReplies = availableRepliesDao.createAvaReply(availableReplies);
												} else if (value_itemDependent.getNodeName().equalsIgnoreCase("otherQuestion")) {
													NodeList nodesOtherQuestion = value_itemDependent.getChildNodes();
													OtherQuestion otherQuestion = new OtherQuestion();
													for (int o = 0; o < nodesOtherQuestion.getLength(); o++) {
														Node valueOtherQuestion = nodesOtherQuestion.item(o);	
														if (valueOtherQuestion.getNodeName().equalsIgnoreCase("otherTitle")) {
															otherQuestion.setTitle(valueOtherQuestion.getFirstChild().getNodeValue());
														} else if (valueOtherQuestion.getNodeName().equalsIgnoreCase("otherTextType")) {
															otherQuestion.setTextType(valueOtherQuestion.getFirstChild().getNodeValue());
														} else if (valueOtherQuestion.getNodeName().equalsIgnoreCase("otherQuestionType")) {
															otherQuestion.setQuestion_type(valueOtherQuestion.getFirstChild().getNodeValue());
															otherQuestion.setAvailable_reply_id(idAvailableReplies);
															idOtherQuestion = otherQuestionDao.createOtherQuestion(otherQuestion);
														} else if (valueOtherQuestion.getNodeName().equalsIgnoreCase("otherReply")) {
															OthersReplies otherReply = new OthersReplies();
															otherReply.setOtherReply(valueOtherQuestion.getFirstChild().getNodeValue());
															otherReply.setOther_question_id(idOtherQuestion);
															othersRepliesDao.createOtherReply(otherReply);
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
			
			deleteFile(arquivo);
			deleteFile("file.xml");
			
			Intent in = new Intent(DomFeedParser.this, TypeSurvey.class );
			in.putExtra("id", idMainSurvey);
			startActivity(in);

			//db.close();
			finish();
		}
	
		} catch (Throwable t) {
			deleteFile(arquivo);

			Log.e("MyOpinions", t.getMessage(), t);
			isLink = true;
			
		}
	}
	
	private void invalidQRCode(){
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
		 
		// set title
		alertDialogBuilder.setTitle("QRCode inválido");

		// set dialog message
		alertDialogBuilder
			//.setMessage("Deseja responder o questionário novamente?")
			.setCancelable(false)
			.setPositiveButton("OK",new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {
					// if this button is clicked, close
					// current activity
					//final Vibrator vibrator = (Vibrator) getSystemService( Context.VIBRATOR_SERVICE );
					//vibrator.vibrate( 50 );
					if (!link.startsWith("http://") && !link.startsWith("https://"))
						   link = "http://" + link;
					Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(link));
					startActivity(browserIntent);
					linkOpened = true;
				}
			  });
			

			// create alert dialog
			AlertDialog alertDialog = alertDialogBuilder.create();

			// show it
			alertDialog.show();		
	}
}

