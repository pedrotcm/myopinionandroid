package br.com.myopinion;

import java.util.Iterator;
import java.util.List;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Vibrator;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import br.com.myopinion.dao.BasicDAO;
import br.com.myopinion.dao.MainSurveysDAO;
import br.com.myopinion.http.DownloadSurvey;
import br.com.myopinion.model.SurveyReplies;
import br.com.myopinion.survey.ListSurveys;
import br.com.opala.myopinion.R;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;


public class MyOpinionsActivity extends SherlockActivity {

	private Vibrator vibrator;
	private Cursor cursor;
	private BasicDAO basicDAO;
	private MainSurveysDAO mainSurveyDao;
	private AlertDialog alertDialog;
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		basicDAO.close();
	}

	@Override
	public void onCreate( Bundle savedInstanceState ) {
		super.onCreate( savedInstanceState );
		setContentView( R.layout.my_opinions );
		vibrator = (Vibrator) getSystemService( Context.VIBRATOR_SERVICE );

		basicDAO = new BasicDAO(getApplicationContext());
		basicDAO.open();
	    mainSurveyDao = new MainSurveysDAO(getApplicationContext());
		
	}

	@Override
	public void onBackPressed() {
	
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
 
			// set title
			//alertDialogBuilder.setTitle("Alerta");
 
			// set dialog message
			alertDialogBuilder
				.setMessage("Tem certeza que deseja sair?")
				.setCancelable(true)
				.setPositiveButton("Sim",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {
						// if this button is clicked, close
						// current activity
						MyOpinionsActivity.this.finish();
					}
				  })
				.setNegativeButton("Não",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {
						// if this button is clicked, just close
						// the dialog box and do nothing
						dialog.cancel();
					}
				});
 
				// create alert dialog
				AlertDialog alertDialog = alertDialogBuilder.create();
 
				// show it
				alertDialog.show();
	}	


	 @Override
		public boolean onCreateOptionsMenu(Menu menu) {
		    // Inflate the menu items for use in the action bar
			menu.add(0, 0, 0, "Carregar questionário");

		    MenuInflater inflater = getSupportMenuInflater();
		    inflater.inflate(R.layout.action_items, menu);
		    return super.onCreateOptionsMenu(menu);
		}
	 
	 @Override
	 public boolean onOptionsItemSelected(MenuItem item) {
	     // Handle presses on the action bar items
	     switch (item.getItemId()) {
	         case R.id.action_surveys:
	        	 vibrator.vibrate( 50 );
	        	 
	        	cursor = mainSurveyDao.consultMainSurveyBySurveyType("OFF_LINE");
	 			if(cursor.isAfterLast()) {
	 				AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
	 				// set title
	 				alertDialogBuilder.setMessage("Não há questionários para serem respondidos.");
	 	 
	 				// set dialog message
	 				alertDialogBuilder
	 					//.setMessage("Deseja responder o questionário novamente?")
	 					.setCancelable(false)
	 					.setPositiveButton("OK",new DialogInterface.OnClickListener() {
	 						public void onClick(DialogInterface dialog,int id) {
	 							alertDialog.dismiss();
	 						}
	 					  });
	 					
	 	 
	 					// create alert dialog
	 					alertDialog = alertDialogBuilder.create();
	 					// show it
	 					alertDialog.show();		
	 			} else {
	        	 
	        	 startActivity( new Intent( MyOpinionsActivity.this, ListSurveys.class ) );
	        	 finish();
	 			}
	        	 return true;
	         case R.id.action_qrcode:
	        	 vibrator.vibrate( 50 );
	        	 startActivity( new Intent( MyOpinionsActivity.this, CaptureQRCode.class ) );
	        	 finish();
	        	 return true;
	         case 0:
	     	    	dialogLoadSurvey();
	     	    	return true;
	         default:
	             return super.onOptionsItemSelected(item);
	     }
	 }
	 
	public void dialogLoadSurvey() {

		LayoutInflater li = getLayoutInflater();

		View view = li.inflate(R.layout.alert_dialog, null);
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setCancelable(true);
		builder.setMessage("Digite a URL do questionário:");
		builder.setView(view);

		final EditText url_survey = (EditText) view
				.findViewById(R.id.url_survey);

		Button buttonLoadSurvey = (Button) view
				.findViewById(R.id.bt_load_survey);

		buttonLoadSurvey.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				InputMethodManager imm = (InputMethodManager) MyOpinionsActivity.this.getSystemService(
					      Context.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(url_survey.getWindowToken(), 0);
					
				String url = url_survey.getText().toString();
				Intent intent = new Intent(MyOpinionsActivity.this,
						DownloadSurvey.class);
				intent.putExtra("qrcode", url);
				startActivity(intent);
				finish();
			}
		});

		AlertDialog dialog = builder.create();
		dialog.show();
	}
}
