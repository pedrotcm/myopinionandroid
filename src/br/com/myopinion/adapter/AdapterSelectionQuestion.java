package br.com.myopinion.adapter;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.TextView;
import br.com.myopinion.dao.ObtainedRepliesDAO;
import br.com.myopinion.model.ObtainedReplies;
import br.com.opala.myopinion.R;

public class AdapterSelectionQuestion extends ArrayAdapter<String> {

	private final Activity context;
	private final List<String> replies;
	private int resource;
    private long idQuestion;
    private long idSurveyReply;
	private ObtainedRepliesDAO obtainedRepliesDao;
	private ObtainedReplies obtReply;
	private boolean loading;
	private Set<Integer> mCheckedItems;
	private List<ObtainedReplies> getObtainedReplies;
	
	public AdapterSelectionQuestion(Activity context, int resource, List<String> replies, long idQuestion, long idSurveyReply) {
		super(context, resource, replies);
		this.context = context;
		this.replies = replies;
		this.resource = resource;
		this.mCheckedItems = new LinkedHashSet<Integer>();
		this.idQuestion = idQuestion;
		this.idSurveyReply = idSurveyReply;
		this.loading = false;
		obtainedRepliesDao = new ObtainedRepliesDAO(context);
		
		getObtainedReplies = obtainedRepliesDao.getListObtainedReplies(0, idQuestion, idSurveyReply);

        if (loading == false && !getObtainedReplies.isEmpty()) {
			for (ObtainedReplies obt : getObtainedReplies){
				for (int i =0 ; i < replies.size() ; i++){
					if (replies.get(i).equalsIgnoreCase(obt.getObtReply()))
						mCheckedItems.add(i);
				}
			}
		loading = true;
		}
	}
	
	public List<String> getItemsChecked() {
		List<String> listReplies = new ArrayList<String>();
		if (mCheckedItems.iterator().hasNext()){
			for (Integer i : mCheckedItems){
				listReplies.add(replies.get(i));
			}
			return listReplies;
		} else
			return null;
	}
	
	public String getString(int position){
		return replies.get(position);
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		String reply = getItem(position);

		final int pos = position;
		
        LayoutInflater inflater = context.getLayoutInflater();
		
		Holder holder;
        if (convertView == null) {
            convertView = inflater.inflate(resource, null);
            holder = new Holder();
            holder.replySelection = (TextView) convertView.findViewById(R.id.replySelection);
            holder.checkBox = (CheckBox) convertView.findViewById(R.id.cbReplySelection);                
            holder.replySelection.setTag(holder.checkBox);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }
                        
        holder.checkBox.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				CheckBox cb = (CheckBox) v;
				if (cb.isChecked()){
					mCheckedItems.add(pos);
				} else {
					mCheckedItems.remove(pos);
				}
				
			}
		});
        
        holder.replySelection.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if (( (CheckBox) v.getTag() ).isChecked()){
					mCheckedItems.remove(pos);
        		} else {
        			mCheckedItems.add(pos);
        		}
				notifyDataSetChanged();
			}
		});
        
        if(!mCheckedItems.contains(pos)){
            holder.checkBox.setChecked(false);
        } else if (mCheckedItems.contains(pos)){
            holder.checkBox.setChecked(true);
        }
        
        holder.replySelection.setText(reply);    
       
        return convertView;
	}
	
	private static class Holder {
		public TextView replySelection;
		public CheckBox checkBox;
	}
	
}