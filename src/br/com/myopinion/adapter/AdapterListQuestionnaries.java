package br.com.myopinion.adapter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.List;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import br.com.myopinion.model.SurveyReplies;
import br.com.opala.myopinion.R;

public class AdapterListQuestionnaries extends ArrayAdapter<SurveyReplies> {

	private final Activity context;
	private final List<SurveyReplies> surveyReplies;
	private int resource;
	
	public AdapterListQuestionnaries(Activity context, int resource, List<SurveyReplies> surveyReplies) {
		super(context, resource, surveyReplies);
		this.context = context;
		this.surveyReplies = surveyReplies;
		this.resource = resource;
	}
	
	public SurveyReplies getSurveyReply(int position){
		return surveyReplies.get(position);
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		SurveyReplies surveyReply = getItem(position);
	
		View view = convertView;
		Holder holder = new Holder();
		
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
           
            view = inflater.inflate(resource, null);
            TextView tvSurveyReply = (TextView) view.findViewById(R.id.data);
            ImageView iconSync = (ImageView) view.findViewById(R.id.icon_sync);

            holder.tvSurveyReply = tvSurveyReply;
    		holder.iconSync = iconSync;
            
            view.setTag(holder);
        } else
            holder = (Holder) view.getTag(); 
        
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        String dateFormated = dateFormat.format(surveyReply.getCreated_at());
           
        holder.tvSurveyReply.setText("Resposta: " + dateFormated);
        
        if (surveyReply.getSync() == true)
            holder.iconSync.setBackgroundResource(R.drawable.ic_action_tick);
        else 
            holder.iconSync.setBackgroundResource(R.drawable.ic_action_reload);
        
		return view;
	}
	
	private static class Holder {
	    public TextView tvSurveyReply;
	    public ImageView iconSync;
	}
	
}