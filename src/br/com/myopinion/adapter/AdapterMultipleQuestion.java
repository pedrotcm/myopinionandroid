package br.com.myopinion.adapter;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RadioButton;
import android.widget.TextView;
import br.com.myopinion.dao.ObtainedRepliesDAO;
import br.com.myopinion.model.ObtainedReplies;
import br.com.opala.myopinion.R;

public class AdapterMultipleQuestion extends ArrayAdapter<String> {

	private final Activity context;
	private final List<String> replies;
	private int resource;
    private RadioButton mSelectedRB;
    private Set<Integer> mSelectedPosition;
    private long idQuestion;
    private long idSurveyReply;
	private ObtainedRepliesDAO obtainedRepliesDao;
	private ObtainedReplies obtReply;
	private boolean loading;
	
	public AdapterMultipleQuestion(Activity context, int resource, List<String> replies, long idQuestion, long idSurveyReply) {
		super(context, resource, replies);
		this.context = context;
		this.replies = replies;
		this.resource = resource;
		this.mSelectedPosition = new LinkedHashSet<Integer>();
		this.idQuestion = idQuestion;
		this.idSurveyReply = idSurveyReply;
		this.loading = false;
		obtainedRepliesDao = new ObtainedRepliesDAO(context);
		
		List<ObtainedReplies> getObtainedReplies = obtainedRepliesDao.getListObtainedReplies(0, idQuestion, idSurveyReply);
		obtReply = null;
		if (!getObtainedReplies.isEmpty()){
			obtReply = (ObtainedReplies) getObtainedReplies.get(0);
		}

	}
	
	public String getItemChecked() {
		if (mSelectedPosition.iterator().hasNext())
			return replies.get(mSelectedPosition.iterator().next());
		else
			return null;
	}
	
	public String getString(int position){
		return replies.get(position);
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		String reply = getItem(position);

		final Set<Integer> pos = new LinkedHashSet<Integer>();
		pos.add(position);

        LayoutInflater inflater = context.getLayoutInflater();
		
		Holder holder;
        if (convertView == null) {
            convertView = inflater.inflate(resource, null);
            holder = new Holder();
            holder.replyMultiple = (TextView) convertView.findViewById(R.id.replyMultiple);
            holder.radioBtn = (RadioButton) convertView.findViewById(R.id.rbReplyMultiple);    
             
            if (loading == false){
            	if (obtReply != null && obtReply.getObtReply().equalsIgnoreCase(reply)){
            	  if ((!pos.equals(mSelectedPosition)) && mSelectedRB != null) {
                        mSelectedRB.setChecked(false);
                    }
                	
                	holder.radioBtn.setChecked(true);
                    mSelectedPosition = new LinkedHashSet<Integer>(pos);
                    mSelectedRB = holder.radioBtn;
                    
                	loading = true;
                }
            }
            
            holder.replyMultiple.setTag(holder.radioBtn);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }
                  
        holder.radioBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if ((!pos.equals(mSelectedPosition)) && mSelectedRB != null) {
                    mSelectedRB.setChecked(false);
                }

                mSelectedPosition = new LinkedHashSet<Integer>(pos);
                mSelectedRB = (RadioButton) view;
               // notifyDataSetChanged();
            }
        });

        if(!pos.equals(mSelectedPosition)){
            holder.radioBtn.setChecked(false);
        } else{
            holder.radioBtn.setChecked(true);
            if(mSelectedRB != null && holder.radioBtn != mSelectedRB){
                mSelectedRB = holder.radioBtn;
            }
        }

        holder.replyMultiple.setText(reply);
        
        holder.replyMultiple.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				   if ((!pos.equals(mSelectedPosition)) && mSelectedRB != null) {
	                    mSelectedRB.setChecked(false);
	                }

	                mSelectedPosition = new LinkedHashSet<Integer>(pos);
	                mSelectedRB = (RadioButton) v.getTag();
	                notifyDataSetChanged();
			}
		});

        return convertView;
	}
	
	private static class Holder {
		public TextView replyMultiple;
		public RadioButton radioBtn;
	}
	
}