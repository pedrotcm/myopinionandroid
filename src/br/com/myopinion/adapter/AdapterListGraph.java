package br.com.myopinion.adapter;

import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;
import br.com.opala.myopinion.R;

public class AdapterListGraph extends ArrayAdapter<String> {

	private final Activity context;
	private final List<String> replies;
	private final List<Float> results;
	private int resource;
    private Utility utility;

	
	public AdapterListGraph(Activity context, int resource, List<String> replies, List<Float> results) {
		super(context,resource, replies);
		this.context = context;
		this.replies = replies;
		this.results = results;
		this.resource = resource;
		System.out.println("adapter");
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		//Map<String, Integer> reply = getItem(position);
		String reply = getItem(position);
		
		View view = convertView;
		Holder holder = new Holder();
		
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
           
            view = inflater.inflate(resource, null);
            TextView labelReply = (TextView) view.findViewById(R.id.labelReply);
            TextView qntReply = (TextView) view.findViewById(R.id.qntReply);
            RelativeLayout layoutGraph = (RelativeLayout) view.findViewById(R.id.layout_listgraph);

            holder.labelReply = labelReply;
    		holder.qntReply = qntReply;
    		holder.layoutGraph = layoutGraph;
            
            view.setTag(holder);
        } else
            holder = (Holder) view.getTag(); 
                             
        holder.labelReply.setText(reply);
        holder.qntReply.setText(String.format("%.0f", results.get(position)));
        
        if (position % 2 == 0){
            holder.layoutGraph.setBackgroundResource(utility.getColor(9));
        } else {
            holder.layoutGraph.setBackgroundResource(utility.getColor(8));
        }
       
		return view;
	}
	
	private static class Holder {
	    public TextView labelReply;
	    public TextView qntReply;
	    public RelativeLayout layoutGraph;
	}
	
}