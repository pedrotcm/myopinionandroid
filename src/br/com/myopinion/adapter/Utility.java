package br.com.myopinion.adapter;

import java.util.ArrayList;
import java.util.List;

import br.com.opala.myopinion.R;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ListAdapter;
import android.widget.ListView;

public class Utility {
	
    private static List<Integer> colors;
	
    public Utility() {
    	this.colors = new ArrayList<Integer>();
   	
    	colors.add(R.color.red);
    	colors.add(R.color.blue);
    	colors.add(R.color.transparent_blue);
    	colors.add(R.color.green);
    	colors.add(R.color.green_light);
    	colors.add(R.color.orange);
    	colors.add(R.color.transparent_orange);
    	colors.add(R.color.purple);
    	colors.add(R.color.gray91);
    	colors.add(R.color.gray71);
    	colors.add(R.color.gray51);

    }
    
	public static int getColor(int i){
		return colors.get(i);
	}
	
    public static void setListViewHeightBasedOnChildren(ListView listView) {
          ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
        // pre-condition
              return;
        }

        int totalHeight = listView.getPaddingTop() + listView.getPaddingBottom();
        for (int i = 0; i < listAdapter.getCount(); i++) {
             View listItem = listAdapter.getView(i, null, listView);
             if (listItem instanceof ViewGroup) {
                listItem.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
             }
             listItem.measure(0, 0);
             totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
                  listView.setLayoutParams(params);
    }
 }