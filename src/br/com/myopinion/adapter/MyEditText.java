package br.com.myopinion.adapter;

import android.content.Context;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.widget.EditText;

public class MyEditText extends EditText{

	public MyEditText(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
	}

	 @Override
	  public boolean onKeyPreIme(int keyCode, KeyEvent event)
	  {
	    if(keyCode == KeyEvent.KEYCODE_BACK)
	    {
	      clearFocus();
	    }
	    return super.onKeyPreIme(keyCode, event);
	  }
}
