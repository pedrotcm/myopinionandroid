///*
// * To change this template, choose Tools | Templates
// * and open the template in the editor.
// */
//package br.com.myopinion.adapter;
//
//import java.util.ArrayList;
//import java.util.Calendar;
//import java.util.HashMap;
//import java.util.List;
//
//import android.app.Activity;
//import android.app.AlertDialog;
//import android.app.DatePickerDialog;
//import android.content.ContentResolver;
//import android.content.Context;
//import android.content.DialogInterface;
//import android.content.Intent;
//import android.database.Cursor;
//import android.database.sqlite.SQLiteDatabase;
//import android.location.Location;
//import android.location.LocationListener;
//import android.location.LocationManager;
//import android.os.Bundle;
//import android.provider.Settings;
//import android.text.InputType;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.View.MeasureSpec;
//import android.view.View.OnClickListener;
//import android.view.ViewGroup;
//import android.widget.AdapterView;
//import android.widget.AdapterView.OnItemClickListener;
//import android.widget.ArrayAdapter;
//import android.widget.BaseAdapter;
//import android.widget.Button;
//import android.widget.Checkable;
//import android.widget.DatePicker;
//import android.widget.EditText;
//import android.widget.LinearLayout;
//import android.widget.LinearLayout.LayoutParams;
//import android.widget.ListAdapter;
//import android.widget.ListView;
//import android.widget.TextView;
//import br.com.myopinion.dao.AvailableRepliesDAO;
//import br.com.myopinion.dao.MainSurveysDAO;
//import br.com.myopinion.dao.ObtainedRepliesDAO;
//import br.com.myopinion.dao.QuestionsDAO;
//import br.com.myopinion.dao.SurveyRepliesDAO;
//import br.com.myopinion.model.ObtainedReplies;
//import br.com.myopinion.model.Questions;
//import br.com.myopinion.questions.MapQuestion;
//import br.com.opala.myopinion.R;
//
//public class AdapterListView extends BaseAdapter{
//	
//	public static final String NUMERIC = "NUMERIC";
//	public static final String EMAIL = "EMAIL";
//	public static final String TEXT = "TEXT";
//	public static final String MULTIPLE_CHOISE = "MULTIPLE_CHOISE";
//	public static final String BOXES = "BOXES";
//	public static final String GPS = "GPS";
//	public static final String DATEPICKER = "DATEPICKER";
//	
//    private LayoutInflater mInflater;
//    private ArrayList<Questions> questions;
//    private Context context;
//    private Long idSurveyReply;
//    
//	private Cursor cursor;
//	private SQLiteDatabase db;
//	private MainSurveysDAO mainSurveyDao;
//	private SurveyRepliesDAO surveyReplyDao;
//	private QuestionsDAO questionsDao;
//	private AvailableRepliesDAO availableRepliesDao;
//	private ObtainedRepliesDAO obtainedRepliesDao;
//    
//	private Calendar calendar;
//	
//	private int year;
//	private int month;
//	private int day;
//
//	private int posDatePicker;
//	private EditText respostaDatePicker;
//    private String[] values;
//    private HashMap<Integer,ArrayList<String>> values_hash = new HashMap<Integer,ArrayList<String>>();
//    private ArrayList<String> itemsChecked = new ArrayList<String>();
//    
//    public int checkAddReply = 0;
//	
//	private AlertDialog alerta;
//	private LocationManager locationManager;
//	private  LocationListener locationListener;	
//	private Boolean flag;
//	private View viewLat;
//	private View viewLong;
//	private Button findInMap;
//	private EditText latitude;
//	private EditText longitude;
//	
//	private MyEditText resposta;
//		
//    public AdapterListView(Context context, Long idSurveyReply, ArrayList<Questions> questions) {
//        //Itens que preencheram o listview
//    	System.out.println("idSurveyReply: " + idSurveyReply);
//        this.questions = questions;
//        this.context = context;
//        this.idSurveyReply = idSurveyReply;
//        
//        
//        //responsavel por pegar o Layout do item.
//        mInflater = LayoutInflater.from(context);
//        
//        values = new String[this.questions.size()];
//       
//    	mainSurveyDao = new MainSurveysDAO(context);
//		mainSurveyDao.open();
//		surveyReplyDao = new SurveyRepliesDAO(context);
//		surveyReplyDao.open();
//		questionsDao = new QuestionsDAO(context);
//		questionsDao.open();
//		availableRepliesDao = new AvailableRepliesDAO(context);
//		availableRepliesDao.open();
//		obtainedRepliesDao = new ObtainedRepliesDAO(context);
//		obtainedRepliesDao.open();
//		
//		notifyDataSetChanged();
//		
//		calendar = Calendar.getInstance();
//		
//		year = calendar.get(Calendar.YEAR);
//		month = calendar.get(Calendar.MONTH);
//		day = calendar.get(Calendar.DAY_OF_MONTH);
//		
//		for(int i=0;i<this.questions.size();i++)
//		{
//
//			List<ObtainedReplies> getObtainedReplies = obtainedRepliesDao.getListObtainedReplies(questions.get(i).getId(), idSurveyReply);
//
//			if(getObtainedReplies.size() != 0){
//				if((getObtainedReplies.size() == 1) && (!questions.get(i).getQuestionType().equalsIgnoreCase(BOXES))){
//					ObtainedReplies obtReplies = getObtainedReplies.get(0);
//					values[i] = obtReplies.getObtReply();
//				} else {
//					itemsChecked = new ArrayList<String>();
//					for (int j = 0; j< getObtainedReplies.size(); j++){
//						ObtainedReplies obtReplies = getObtainedReplies.get(j);
//						itemsChecked.add(obtReplies.getObtReply());
//						values_hash.put(i, itemsChecked);
//					}
//				}
//			} else {
//				values[i] = "";		
//			}
//		}
//    }
//
//    
//    
//    /**
//     * Retorna a quantidade de itens
//     *
//     * @return
//     */
//    public int getCount() {
//        return questions.size();
//    }
//
//    /**
//     * Retorna o item de acordo com a posicao dele na tela.
//     *
//     * @param position
//     * @return
//     */
//    public Questions getItem(int position) {
//        return questions.get(position);
//    }
//
//    /**
//     * Sem implementa??????o
//     *
//     * @param position
//     * @return
//     */
//    public long getItemId(int position) {
//        return position;
//    }
//    
//    public int getItemViewType(int position){
//    	int type = 0;
//    	if (questions.get(position).getQuestionType().equalsIgnoreCase(TEXT)){
//    		type = 1;
//    	} else if (questions.get(position).getQuestionType().equalsIgnoreCase(MULTIPLE_CHOISE)){
//    		type = 2;
//    	} else if (questions.get(position).getQuestionType().equalsIgnoreCase(BOXES)){
//    		type = 3;
//    	} else if (questions.get(position).getQuestionType().equalsIgnoreCase(DATEPICKER)){
//    		type = 4;
//    	}       		
//    		return type;
//    }
//
//    public View getView(int position, View view, ViewGroup parent) {
//
//        //Pega o item de acordo com a pos??????o.
//        Questions question = questions.get(position);
//        System.out.println("Questao: " + question.getTitle());
//        if (question.getQuestionType().equalsIgnoreCase(TEXT)) {
//    		 
//    			view = mInflater.inflate(R.layout.question_text_offline, null);
//	    		 
//    	        final int pos = position;
//
//	    		//holder = new ViewHouder();
//	            //holder.ref=position;
//	 			TextView tvquestao = (TextView) view.findViewById(R.id.questao_texto);
//	 			TextView tvpergunta = (TextView) view.findViewById(R.id.pergunta_texto);
//	 			resposta = (MyEditText) view.findViewById(R.id.resposta_texto);
//	 			
//	 			//resposta.requestFocusFromTouch();
//	 			if (question.getTextType() != null){
//	 				if(question.getTextType().equalsIgnoreCase(NUMERIC)){
//	 					resposta.setInputType(InputType.TYPE_CLASS_PHONE);
//	 				} else if (question.getTextType().equalsIgnoreCase(EMAIL)){
//	 					resposta.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
//	 				}
//	 			}
//	 			
//	 			resposta.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//	 				@Override
//	 				public void onFocusChange(View v, boolean hasFocus) {
//	 				    if (!hasFocus) {
//	 				        LinearLayout parent = (LinearLayout) v.getParent();
//	 				        EditText qtyTemp = (EditText) parent.findViewById(R.id.resposta_texto);
//	 				        values[pos] = qtyTemp.getText().toString();
//	 				        checkAddReply = 1;
//	 				        //System.out.println("Position: " + pos);
//	 				        //System.out.println("Resposta: " + values[pos]);
//	 				    } 
//	 				}
//	 			});
//	    		//view.setTag(holder);
//    	//	} else {
//    			//holder = (ViewHouder) view.getTag();
//    		//}
//    		
//    		
//	 			//holderText.tvQuestao = (TextView) view.findViewById(R.id.questao_texto);
//	 			//holderText.tvPergunta = (TextView) view.findViewById(R.id.pergunta_texto);
//
//	 		//	view.setTag(holderText);
//    	//	} else {
//    			//holderText = (ViewText) view.getTag();
//    		//}
//    		int num = position + 1;
//            String pergunta = num + ") ";
//     		tvquestao.setText(pergunta);
//     		tvpergunta.setText(question.getTitle());
//     		resposta.setText(values[pos]);
//         // ((TextView) view.findViewById(R.id.questao_texto)).setText(pergunta);       	
//          //((TextView) view.findViewById(R.id.pergunta_texto)).setText(question.getTitle());  
//             
// 		} else  if (question.getQuestionType().equalsIgnoreCase(DATEPICKER)) {
//   		//if( view == null )
//   			view = mInflater.inflate(R.layout.question_datepicker_offline, null);
//	    		 
//   	        final int pos = position;
//   	        
//   	        if (values[pos] != ""){   	        	
//   	        	year = Integer.valueOf(values[pos].substring(6, 10));
//   	        	month = Integer.valueOf(values[pos].substring(3, 5));
//   	        	day = Integer.valueOf(values[pos].substring(0, 2));
//   	        	month=month-1;
//   	        }
//   	        
//	 		//	TextView tvquestao = (TextView) view.findViewById(R.id.questao_datepicker);
//	 		//	TextView tvpergunta = (TextView) view.findViewById(R.id.pergunta_datepicker);
//	 			//date_picker = (DatePicker) view.findViewById(R.id.date_picker);
//	 			//date_picker.setDescendantFocusability(DatePicker.FOCUS_);
//	 			//date_picker.init(year, month, day, null);
//	 			respostaDatePicker = (EditText) view.findViewById(R.id.resposta_datepicker);
//	 			//Button datePicker = (Button) view.findViewById(R.id.button_datepicker);
//	 			
//	 			respostaDatePicker.setOnClickListener(new OnClickListener() {
//					
//					@Override
//					public void onClick(View v) {
//						// TODO Auto-generated method stub
//						posDatePicker = pos;
//						DatePickerDialog datePickerDialog = new DatePickerDialog(v.getContext(), datePickerListener, year, month ,day);
//						if (!datePickerDialog.isShowing()){
//							datePickerDialog.show();
//						}
//					}
//				});
//	 	
//	   		int num = position + 1;
//	           String pergunta = num + ") ";
//	    		//tvquestao.setText(pergunta);
//	    		//tvpergunta.setText(question.getTitle());
//	    		respostaDatePicker.setText(values[pos]);
// 		} else if (question.getQuestionType().equalsIgnoreCase(MULTIPLE_CHOISE)) {
// 			
// 	 		view = mInflater.inflate(R.layout.question_multiple_offline, null, true);
// 	 			
//    	    final int pos = position;
//
//    		List<String> respostas = availableRepliesDao.getListAvailableReplies(question.getId());
//			
//			 final ListView lv = (ListView) view.findViewById(android.R.id.list);
// 			 ArrayAdapter<String> adapter = 
//			   		new ArrayAdapter<String>(view.getContext(), android.R.layout.simple_list_item_single_choice, respostas);
//			 lv.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
//       		 lv.setAdapter(adapter);		 
//
//       		 lv.setOnItemClickListener(new OnItemClickListener() {
//				@Override
//				public void onItemClick(AdapterView<?> arg0, View view,
//						int position, long id) {
//	                        lv.setItemChecked(position, true);
//	                        values[pos] = lv.getItemAtPosition(position).toString();
//	 				        checkAddReply = 1;
//	                    // here show dialog
//				}
//			
//       		} );
//
//			for (int i=0; i<lv.getCount(); i++) {
//		    	String item = lv.getAdapter().getItem(i).toString();
//		    		 if (item.equalsIgnoreCase((values[pos]))) {
//		    			 lv.setItemChecked(i, true);
//		    		 }
//		    }
//			
//			  int num = position + 1;
//	          String pergunta = num + ") ";
//	          ((TextView) view.findViewById(R.id.questao_multipla)).setText(pergunta);       	
//	          ((TextView) view.findViewById(R.id.pergunta_multipla)).setText(question.getTitle());  
// 				
// 		}  else if (question.getQuestionType().equalsIgnoreCase(GPS)) {
// 			
//			view = mInflater.inflate(R.layout.question_gps_offline, null, true);
//
//	        final int pos = position;
//
//	    	locationManager=null;
//	    	locationListener=null;	
//	    	flag = false; 	    
//	    	  
//	        locationManager = (LocationManager) view.getContext().getSystemService(Context.LOCATION_SERVICE);
//			Button btBuscarCoordenadas = (Button) view.findViewById(R.id.button_buscar_coordenadas);
// 			findInMap = (Button) view.findViewById(R.id.button_visualizar_coordenadas);
// 			
//			  int num = position + 1;
//	          String pergunta = num + ") ";
//	          ((TextView) view.findViewById(R.id.questao_gps)).setText(pergunta);       	
//	          ((TextView) view.findViewById(R.id.pergunta_gps)).setText(question.getTitle());
//  		    viewLat = (View) view.findViewById(R.id.viewLat);
//  		    viewLong = (View) view.findViewById(R.id.viewLong);
//  		    latitude = (EditText) view.findViewById(R.id.latitude);
//  		    longitude = (EditText) view.findViewById(R.id.longitude);
//
//  		  itemsChecked = values_hash.get(pos);
//    	  if (itemsChecked == null){
//    	   	itemsChecked = new ArrayList<String>();
//    	  } else {
//			viewLat.setVisibility(View.VISIBLE);
//  		    latitude.setText(itemsChecked.get(0), TextView.BufferType.EDITABLE);
//  		    latitude.setFocusable(false);
//      		latitude.setClickable(false);
//  		      
//  		    viewLong.setVisibility(View.VISIBLE);
//      		longitude.setText(itemsChecked.get(1), TextView.BufferType.EDITABLE);
//       		longitude.setFocusable(false);
//       		longitude.setClickable(false);
//       		
// 		    findInMap.setVisibility(View.VISIBLE);
//
//    	  } 
//	   
//	    	  
//		       		
//  		    findInMap.setOnClickListener(new OnClickListener() {
//				
//
//				@Override
//				public void onClick(View v) {
//					Intent intent = new Intent (v.getContext(), MapQuestion.class);
//					intent.putExtra("latitude", latitude.getEditableText().toString());
//					intent.putExtra("longitude", longitude.getEditableText().toString());
//					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//					v.getContext().startActivity(intent);	
//				}
//			});
//  		    
//			btBuscarCoordenadas.setOnClickListener(new OnClickListener() {
//				
//
//				@Override
//				public void onClick(View v) {
//					flag = displayGpsStatus(v.getContext());
//					if (flag) {
//
//							LayoutInflater li = ((Activity) v.getContext()).getLayoutInflater();
//					     
//						    View view2 = li.inflate(R.layout.loading, null);
//						    AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());
//						    builder.setCancelable(false);
//						    builder.setView(view2);
//						    					
//						    view2.findViewById(R.id.bt_cancelar).setOnClickListener(new View.OnClickListener() {
//						        public void onClick(View arg0) {
//						        	if (locationListener != null)
//						        		locationManager.removeUpdates(locationListener);
//						            alerta.cancel();
//						        }
//						    });
//						    
//						    alerta = builder.create();
//						    alerta.show();
//						   
//						
//						
//						locationListener = new MyLocationListener(pos);
//						
//						locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0,
//				               locationListener);
//											
//						} else {
//						alertbox("GPS Status!", "Seu GPS está desligado.", v.getContext());
//						}
//				}
//			});
// 			
// 		}	else if (question.getQuestionType().equalsIgnoreCase(BOXES)) {
//
//    		view = mInflater.inflate(R.layout.question_selection_offline, null, true);
//    		
//    	        final int pos = position;
//
//    	    itemsChecked = values_hash.get(pos);
//    	    if (itemsChecked == null)
//    	    	itemsChecked = new ArrayList<String>();
//    	    
//    	    
//	        List<String> respostas = availableRepliesDao.getListAvailableReplies(question.getId());
//	       
//			
//			final ListView lv = (ListView) view.findViewById(R.id.lista_selecao);
// 			 ArrayAdapter<String> adapter = 
//			   		new ArrayAdapter<String>(view.getContext(), R.layout.simple_list_item_multiple_choice, respostas);
//			lv.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
//       		lv.setAdapter(adapter);
//
//       		if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
//		       		
//	       		lv.setOnItemClickListener(new OnItemClickListener() {
//					@Override
//					public void onItemClick(AdapterView<?> arg0, View view,
//							int position, long id) {
//						if(((Checkable) lv.getChildAt(position)).isChecked()){
//		                        lv.setItemChecked(position, true);
//			                    values[pos] = lv.getItemAtPosition(position).toString();
//		                        itemsChecked.add(values[pos]);
//								values_hash.put(pos, itemsChecked);
//		 				        checkAddReply = 1;
//		 				        			        
//		                    // here show dialog
//						} else {
//							 lv.setItemChecked(position, false);
//		                     values[pos] = lv.getItemAtPosition(position).toString();
//		                     itemsChecked.remove(values[pos]);
//							 values_hash.put(pos, itemsChecked);
//		 				        checkAddReply = 1;
//						}
//						
//					}
//				
//	       		} );
//       	} else { //versões menores que a jellybean
//       		lv.setOnItemClickListener(new OnItemClickListener() {
//				@Override
//				public void onItemClick(AdapterView<?> arg0, View view,
//						int position, long id) {
//					if(((Checkable) lv.getChildAt(position)).isChecked()){
//	                        lv.setItemChecked(position, false);
//		                    values[pos] = lv.getItemAtPosition(position).toString();
//	                        itemsChecked.remove(values[pos]);
//							values_hash.put(pos, itemsChecked);
//	 				        checkAddReply = 1;
//	                    // here show dialog
//					} else {
//						 lv.setItemChecked(position, true);
//	                     values[pos] = lv.getItemAtPosition(position).toString();
//	                     itemsChecked.add(values[pos]);
//						 values_hash.put(pos, itemsChecked);
//						 checkAddReply = 1;
//					}
//				}
//			
//       		} );
//       	}
//
//       			for (int i=0; i<lv.getCount(); i++) {
//		    	String item = lv.getAdapter().getItem(i).toString();
//	       		for (Integer j : values_hash.keySet())
//	       		   for (String s : values_hash.get(j)){
//	       			   if ((j == position) && (item.equalsIgnoreCase((s)))) {
//		    			 lv.setItemChecked(i, true);
//		    		 }
//	       		   }
//		    }
//
//			  int num = position + 1;
//	          String pergunta = num + ") ";
//	          ((TextView) view.findViewById(R.id.questao_selecao)).setText(pergunta);       	
//	          ((TextView) view.findViewById(R.id.pergunta_selecao)).setText(question.getTitle());
// 		}     	
//
//        //infla o layout para podermos preencher os dados
//       // view = mInflater.inflate(R.layout.pergunta_list, null);
//        //atravez do layout pego pelo LayoutInflater, pegamos cada id relacionado
//        //ao item e definimos as informa??????es.
//        //int num = position + 1;
//        //String pergunta = num + ") " + question.getTitle();
//        //((TextView) view.findViewById(R.id.text)).setText(pergunta);       	
//        return view;
//    }
//    
//    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {
//		 
//		// when dialog box is closed, below method will be called.
//		public void onDateSet(DatePicker view, int selectedYear,int selectedMonth, int selectedDay) {
//		    checkAddReply = 1;
//			year = selectedYear;
//			month = selectedMonth;
//			day = selectedDay;
//			
//			String dia = null;
//			int mes = month + 1;
//			String mesok = null;
//			if (day < 10){
//				dia = "0" + day;
//			} else dia = String.valueOf(day);
//			if (mes <10){
//				mesok = "0" + mes;
//			} else mesok = String.valueOf(mes);
//			
//			// set selected date into Text View
//			respostaDatePicker.setText(new StringBuilder().append(dia)
//			   .append("/").append(mesok).append("/").append(year).append(" "));
//            values[posDatePicker] = respostaDatePicker.getEditableText().toString();
//			// set selected date into Date Picker
//			//date_picker.init(year, month, day, null);
//			
//		}
//	};
//    
//	/*----------Method to Check GPS is enable or disable ------------- */
//	private Boolean displayGpsStatus(Context ctx) {
//		ContentResolver contentResolver = ctx.getContentResolver();
//		boolean gpsStatus = Settings.Secure.isLocationProviderEnabled(
//				contentResolver, LocationManager.GPS_PROVIDER);
//		if (gpsStatus) {
//			return true;
//
//		} else {
//			return false;
//		}
//	}
//
//	/*----------Method to create an AlertBox ------------- */
//	protected void alertbox(String title, String mymessage, final Context ctx) {
//		AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
//		builder.setCancelable(false)
//				.setTitle("O GPS está desativado.")
//				.setPositiveButton("Ativar GPS",
//						new DialogInterface.OnClickListener() {
//							public void onClick(DialogInterface dialog, int id) {
//								// finish the current activity
//								// AlertBoxAdvance.this.finish();
//								//Intent myIntent = new Intent(
//									//	Settings.ACTION_SECURITY_SETTINGS);
//								ctx.startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
//								//startActivity(myIntent);
//								dialog.cancel();
//							}
//						})
//				.setNegativeButton("Cancelar",
//						new DialogInterface.OnClickListener() {
//							public void onClick(DialogInterface dialog, int id) {
//								// cancel the dialog box
//								dialog.cancel();
//							}
//						});
//		AlertDialog alert = builder.create();
//		alert.show();
//	}
//	
//private class MyLocationListener implements LocationListener {
//	
//	private int position;
//	
//	public MyLocationListener(){
//		
//	}
//	
//    public MyLocationListener(int pos) {
//    	this.position = pos;
//    }
//	
//		@Override
//        public void onLocationChanged (Location loc) {
//          
//        		itemsChecked.remove(latitude.getText().toString());
//        		itemsChecked.remove(longitude.getText().toString());
//
//                String getLongitude = String.valueOf(loc.getLongitude());  
//    		    String getLatitude = String.valueOf(loc.getLatitude());
//    		    
//			    checkAddReply = 1;
//
//    		    viewLat.setVisibility(View.VISIBLE);
//    		    latitude.setText(getLatitude, TextView.BufferType.EDITABLE);
//    		    latitude.setFocusable(false);
//        		latitude.setClickable(false);
//    		      
//    		    viewLong.setVisibility(View.VISIBLE);
//        		longitude.setText(getLongitude, TextView.BufferType.EDITABLE);
//         		longitude.setFocusable(false);
//         		longitude.setClickable(false);
//     		    
//         		if (locationListener != null){
//         			locationManager.removeUpdates(locationListener);
//         		} else {
//         			locationManager.removeUpdates(this);
//         		}
//         		
//     		    itemsChecked.add(latitude.getEditableText().toString());
//     		    itemsChecked.add(longitude.getEditableText().toString());
//				values_hash.put(position, itemsChecked);
//				
//     		    findInMap.setVisibility(View.VISIBLE);
//     		    
//     		    alerta.cancel();
//     		    
//        }
//
//		@Override
//		public void onProviderDisabled(String provider) {
//			// TODO Auto-generated method stub
//			
//		}
//
//		@Override
//		public void onProviderEnabled(String provider) {
//			// TODO Auto-generated method stub
//			
//		}
//
//		@Override
//		public void onStatusChanged(String provider, int status, Bundle extras) {
//			// TODO Auto-generated method stub
//			
//		}
//		
//		
//		
//		
//	}
//
//
//    static class ViewHouder {
//    	private TextView pergunta;
//    	private TextView questao;
//    	private EditText resposta;
//    	int ref;
//    }
//    
//}
//
