package br.com.myopinion.adapter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import br.com.myopinion.model.SurveyReplies;
import br.com.opala.myopinion.R;

public class AdapterLegend extends ArrayAdapter<String> {

	private final Activity context;
	private final List<String> replies;
	private int resource;
    private List<Integer> colors;
    private Utility utility;

	
	public AdapterLegend(Activity context, int resource, List<String> replies) {
		super(context, resource, replies);
		this.context = context;
		this.replies = replies;
		this.resource = resource;
		this.utility = new Utility();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		String reply = getItem(position);
	
		View view = convertView;
		Holder holder = new Holder();
		
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
           
            view = inflater.inflate(resource, null);
            TextView labelLegend = (TextView) view.findViewById(R.id.label_legend);
            LinearLayout colorLegend = (LinearLayout) view.findViewById(R.id.color_legend);

            holder.labelLegend = labelLegend;
    		holder.colorLegend = colorLegend;
            
            view.setTag(holder);
        } else
            holder = (Holder) view.getTag(); 
                   
        holder.labelLegend.setText(reply);
        holder.colorLegend.setBackgroundResource(utility.getColor(position));
       
		return view;
	}
	
	private static class Holder {
	    public LinearLayout colorLegend;
	    public TextView labelLegend;
	}
	
}