package br.com.myopinion.adapter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import br.com.opala.myopinion.R;

public class AdapterPhotosListView extends ArrayAdapter<String> {

	private final Activity context;
	private final List<String> photoPaths;
	private int resource;
	
	public AdapterPhotosListView(Activity context, int resource, List<String> photoPaths) {
		super(context, resource, photoPaths);
		this.context = context;
		this.photoPaths = photoPaths;
		this.resource = resource;
	}
	
	public String getPhotoPath(int position){
		return photoPaths.get(position);
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		String path = getItem(position);
	
		View view = convertView;
		Holder holder = new Holder();
		
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(resource, null);
            ImageView photo = (ImageView) view.findViewById(R.id.photoView);
    		Uri fileUri = Uri.fromFile(new File(path));
    		Bitmap fotoBitmap = ajustaFoto(fileUri, path);

    		holder.bitmap = fotoBitmap;
            holder.ivPhoto = photo;
    		
            view.setTag(holder);
        } else
            holder = (Holder) view.getTag(); 
        
        holder.ivPhoto.setImageBitmap(holder.bitmap);
        
		return view;
	}
	
	private static class Holder {
	    public ImageView ivPhoto;
	    public Bitmap bitmap;
	}
	
	private Bitmap ajustaFoto(Uri fileUri, String path) {

		context.getContentResolver().notifyChange(fileUri, null);
		ContentResolver cr = context.getContentResolver();

		Bitmap bitmap = null;
		int w = 0;
		int h = 0;
		Matrix mtx = new Matrix();
		
		float angle = 0;
		try {

			bitmap = android.provider.MediaStore.Images.Media.getBitmap(cr,
					fileUri);
			// captura as dimens�es da imagem

			w = bitmap.getWidth();
			h = bitmap.getHeight();

			// pega o caminho onda a imagem est� salva
			ExifInterface exif = new ExifInterface(path);
			// pega a orienta��o real da imagem

			int orientation = exif.getAttributeInt(
					ExifInterface.TAG_ORIENTATION,
					ExifInterface.ORIENTATION_NORMAL);
			// gira a imagem de acordo com a orienta��o

			switch (orientation) {

			case 3: // ORIENTATION_ROTATE_180
				angle = 180; break;
			case 6: // ORIENTATION_ROTATE_90
				angle = 90; break;
			case 8: // ORIENTATION_ROTATE_270
				angle = 270; break;
			default: // ORIENTATION_ROTATE_0
				angle = 0; break;
			}

		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		FileOutputStream out;
		Bitmap rotatedBitmap = null;
		try {
			out = new FileOutputStream(path);
			// define um indice = 1 pois se der erro vai manter a imagem como
			// est�.
			// Integer idx = 1;
			// reupera as dimens�es da imagem
			w = bitmap.getWidth();
			h = bitmap.getHeight();
			// verifica qual a maior dimens�o e divide pela lateral final para
			// definir qual o indice de redu��o

			if (w>h) {
				if (w > 800) {
					w = 800;
				}

				if (h > 600) {
					h = 600;
				}

			} else {
				if (h > 800) {
					h = 800;
				}

				if (w > 600) {
					w = 600;
				}				
			}

			// scale it to fit the screen, x and y swapped because my image is wider than it is tall
			Bitmap scaledBitmap = Bitmap.createScaledBitmap(bitmap, w, h, true);
			 		
			// create a matrix object
			Matrix matrix = new Matrix();
			matrix.postRotate(angle); // rotate by angle
			 
			// create a new bitmap from the original using the matrix to transform the result
			rotatedBitmap = Bitmap.createBitmap(scaledBitmap , 0, 0, scaledBitmap .getWidth(), scaledBitmap .getHeight(), matrix, true);
			 
			// salva a imagem reduzida no disco
			rotatedBitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// uma nova instancia do bitmap rotacionado
		return rotatedBitmap;
	}
}