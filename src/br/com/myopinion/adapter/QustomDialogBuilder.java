package br.com.myopinion.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import br.com.myopinion.dao.MainSurveysDAO;
import br.com.myopinion.http.RegisterActivity;
import br.com.myopinion.model.MainSurveys;
import br.com.opala.myopinion.R;

public class QustomDialogBuilder extends AlertDialog.Builder{

	/** The custom_body layout */
	private View mDialogView;
	
	/** optional dialog title layout */
	private TextView mTitle;
	/** optional alert dialog image */
	private ImageView mIcon;
	/** optional message displayed below title if title exists*/
	private TextView mMessage;
	/** The colored holo divider. You can set its color with the setDividerColor method */
	private View mDivider;
	
	private Cursor cursor;
	private MainSurveysDAO mainSurveyDao;
	
    public QustomDialogBuilder(Context context) {
        super(context);

        mainSurveyDao = new MainSurveysDAO(context);
		
        mDialogView = View.inflate(context, R.layout.qustom_dialog_layout, null);
        setView(mDialogView);

        mTitle = (TextView) mDialogView.findViewById(R.id.alertTitle);
        mMessage = (TextView) mDialogView.findViewById(R.id.message);
        mIcon = (ImageView) mDialogView.findViewById(R.id.icon);
        mDivider = mDialogView.findViewById(R.id.titleDivider);
	}

    /** 
     * Use this method to color the divider between the title and content.
     * Will not display if no title is set.
     * 
     * @param colorString for passing "#ffffff"
     */
    public QustomDialogBuilder setDividerColor(String colorString) {
    	mDivider.setBackgroundColor(Color.parseColor(colorString));
    	return this;
    }
 
    @Override
    public QustomDialogBuilder setTitle(CharSequence text) {
        mTitle.setText(text);
        return this;
    }

    public QustomDialogBuilder setTitleColor(String colorString) {
    	mTitle.setTextColor(Color.parseColor(colorString));
    	return this;
    }

    @Override
    public QustomDialogBuilder setMessage(int textResId) {
        mMessage.setText(textResId);
        return this;
    }

    @Override
    public QustomDialogBuilder setMessage(CharSequence text) {
        mMessage.setText(text);
        return this;
    }

    @Override
    public QustomDialogBuilder setIcon(int drawableResId) {
        mIcon.setImageResource(drawableResId);
        return this;
    }

    @Override
    public QustomDialogBuilder setIcon(Drawable icon) {
        mIcon.setImageDrawable(icon);
        return this;
    }
    
    /**
     * This allows you to specify a custom layout for the area below the title divider bar
     * in the dialog. As an example you can look at example_ip_address_layout.xml and how
     * I added it in TestDialogActivity.java
     * 
     * @param resId  of the layout you would like to add
     * @param context
     */
    public QustomDialogBuilder setCustomView(int resId, Context context) {
    	View customView = View.inflate(context, resId, null);
    	((FrameLayout)mDialogView.findViewById(R.id.customPanel)).addView(customView);
    	return this;
    }
    
    public View setCustomViewListView(int resId, Context context, Cursor cursor) {
    	View customView = View.inflate(context, resId, null);
    	this.cursor = cursor;
    	final Context ctx = context;
    	final AlertDialog alert = this.create();
    	final ListView lv = (ListView) customView.findViewById(android.R.id.list);
    	 String[] items = { "Sincronizar Respostas", "Excluir" };        
         ArrayAdapter<String> adapter = new ArrayAdapter<String>(context,
                     android.R.layout.simple_list_item_1, items);
         lv.setAdapter(adapter);
         
         if (mMessage.getText().toString().equalsIgnoreCase(""))
     		mDialogView.findViewById(R.id.contentPanel).setVisibility(View.GONE);
         
    	((FrameLayout)mDialogView.findViewById(R.id.customPanel)).addView(customView);
    	return customView;
    }
    
    @Override
    public AlertDialog show() {
    	if (mTitle.getText().equals(""))
    		mDialogView.findViewById(R.id.topPanel).setVisibility(View.GONE);
    	
    	return super.show();
    }
    
    public void dialogItemSelected(String menuItemName, Context context){
		final String COLOR = "#A8Cf45";
		final Context ctx = context;
		if (menuItemName.equalsIgnoreCase("Excluir")){
			final QustomDialogBuilder qustomDialogBuilder = (QustomDialogBuilder) new QustomDialogBuilder(ctx).
					setTitleColor(COLOR).
					setDividerColor(COLOR);
			// set title
			qustomDialogBuilder.setTitle("Excluir Questionário");
 
			// set dialog message
			qustomDialogBuilder
				.setMessage("Tem certeza que deseja excluir o questionário?")
				.setCancelable(true)
				.setPositiveButton("Sim",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {
						// if this button is clicked, close
						// current activity
					//	MyOpinionsActivity.this.finish();
						//cursor.moveToPosition(info.position);
						
						MainSurveys mainSurvey = mainSurveyDao.getMainSurvey(cursor.getLong(0));
						System.out.println("IdExcluir: " + mainSurvey.getId());
						mainSurveyDao.deleteMainSurvey(mainSurvey.getId());
					
					    System.out.println("Del MainSurvey");
						cursor.requery();
					}
				  })
				.setNegativeButton("Não",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {
						// if this button is clicked, just close
						// the dialog box and do nothing
						dialog.cancel();
					}
				});
 
				// create alert dialog
				AlertDialog alertDialog = qustomDialogBuilder.create();
 
				// show it
				alertDialog.show();

		} else if (menuItemName.equalsIgnoreCase("Enviar Respostas")){

			final QustomDialogBuilder qustomDialogBuilder = (QustomDialogBuilder) new QustomDialogBuilder(ctx).
					setTitleColor(COLOR).
					setDividerColor(COLOR);
			// set title
			qustomDialogBuilder.setTitle("Enviar Respostas");
 
			// set dialog message
			qustomDialogBuilder
				.setMessage("Tem certeza que deseja enviar as respostas?")
				.setCancelable(true)
				.setPositiveButton("Sim",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {
						Intent in = new Intent (ctx, RegisterActivity.class);
						in.putExtra("id", cursor.getLong(0));
						ctx.startActivity(in);
					//	db.close();
						((Activity) ctx).finish();
					}
				  })
				.setNegativeButton("Não",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {
						// if this button is clicked, just close
						// the dialog box and do nothing
						dialog.cancel();
					}
				});
 
				// create alert dialog
				AlertDialog alertDialog = qustomDialogBuilder.create();
 
				// show it
				alertDialog.show();
		}
	}
	

}
