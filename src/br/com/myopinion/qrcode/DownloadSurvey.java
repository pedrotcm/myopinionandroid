package br.com.myopinion.qrcode;

import java.io.BufferedInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.http.conn.params.ConnManagerParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import com.actionbarsherlock.app.SherlockActivity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;
import br.com.myopinion.DomFeedParser;
import br.com.myopinion.MyOpinionsActivity;
import br.com.opala.myopinion.R;

public class DownloadSurvey extends SherlockActivity {
		
	// button to show progress dialog
		Button btnShowProgress;
		private static final int HTTP_TIMEOUT = 30*1000;

		// Progress Dialog
		private ProgressDialog pDialog;
		private String arquivo;
		private String qrcode;
		private String msg;
		private boolean linkOpened = false;
		
		// Progress dialog type (0 - for Horizontal progress bar)
		public static final int progress_bar_type = 0; 
		
		@Override
		public void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);
			setContentView(R.layout.background);
			
			final Bundle extras = getIntent().getExtras();
			qrcode = extras.getString("qrcode");
			
			new DownloadFileFromURL().execute(qrcode);
			
			/**
			 * Show Progress bar click event
			 * */
			/*btnShowProgress.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// starting new Async Task
					System.out.println("Download: " + qrcode);
					new DownloadFileFromURL().execute(qrcode);
				}
			});*/
		}

		/**
		 * Showing Dialog
		 * */
		@Override
		protected Dialog onCreateDialog(int id) {
			switch (id) {
			case progress_bar_type:
				pDialog = new ProgressDialog(this);
				pDialog.setMessage("Carregando o questionário. Por favor aguarde...");
				pDialog.setIndeterminate(false);
				pDialog.setCancelable(false);
				pDialog.show();
				return pDialog;
			default:
				return null;
			}
		}
		
		

		/**
		 * Background Async Task to download file
		 * */
		class DownloadFileFromURL extends AsyncTask<String, String, String> {
			/**
			 * Before starting background thread
			 * Show Progress Bar Dialog
			 * */
			@Override
			protected void onPreExecute() {
				super.onPreExecute();
				showDialog(progress_bar_type);
			}

			/**
			 * Downloading file in background thread
			 * */
			@Override
			protected String doInBackground(String... f_url) {
				int count;
		        try {

		            URL url = new URL(f_url[0]);
		            HttpURLConnection conection = (HttpURLConnection) url.openConnection();
		            
		            conection.setConnectTimeout(30*1000);
		            conection.setReadTimeout(30*1000);
		            
		            conection.connect();
		            
		            // getting file length
		            int lenghtOfFile = conection.getContentLength();

		            // input stream to read file - with 8k buffer
		            InputStream input = new BufferedInputStream(url.openStream(), 8192);
		            
		            // Output stream to write file
		            //OutputStream output = new FileOutputStream("/sdcard/pesquisa.xml");
                              
		            Date getDate = new Date();
					SimpleDateFormat dateFormat = new SimpleDateFormat("HHmmssddMMyyyy"); 
					String dataFormatada = dateFormat.format(getDate);
					arquivo = "survey_" + dataFormatada + ".xml";
					FileOutputStream output = openFileOutput(arquivo, MODE_WORLD_READABLE); 
					
		            byte data[] = new byte[1024];

		            long total = 0;

		            while ((count = input.read(data)) != -1) {
		                total += count;
		                // publishing the progress....
		                // After this onProgressUpdate will be called
		               // publishProgress(""+(int)((total*100)/lenghtOfFile));
		                
		                // writing data to file
		                output.write(data, 0, count);
		            }

		            // flushing output
		            output.flush();
		            
		            // closing streams
		            output.close();
		            input.close();
		            
		           // long start = System.currentTimeMillis();

					Intent in = new Intent(DownloadSurvey.this, DomFeedParser.class );
					in.putExtra("filexml", arquivo);
					in.putExtra("link", qrcode);
					startActivity(in);

					//long duration = System.currentTimeMillis() - start;
					//Log.i("MyOpinions", "Parser duration=" + duration);
	            
		        } catch (RuntimeException e) {
		        	Log.e("Erro Download: ", e.getMessage());
		        	msg = e.getMessage();
		        } catch (Exception e) {
		        	Log.e("Erro2 Download: ", e.toString());
		        	msg = e.toString();
				}
		        
		        return msg;
			}
			
			
			/**
			 * Updating progress bar
			 * */
			/*protected void onProgressUpdate(String... progress) {
				// setting progress percentage
	            pDialog.setProgress(Integer.parseInt(progress[0]));
	       }*/

			/**
			 * After completing background task
			 * Dismiss the progress dialog
			 * **/
			@Override
			protected void onPostExecute(String file_url) {
				// dismiss the dialog after the file was downloaded
				if (msg != null){
					dismissDialog(progress_bar_type);

					if (msg.startsWith("java.net.MalformedURL") || msg.startsWith("java.io.FileNotFoundException")){
						linkOpened = true;
						invalidQRCode();
					} else {
						Toast.makeText(DownloadSurvey.this, "Ocorreu um problema ao carregar o questionário. Verifique sua conexão e tente novamente.", Toast.LENGTH_LONG ).show();
						Intent intent = new Intent (DownloadSurvey.this, MyOpinionsActivity.class);
						startActivity(intent);
						finish();
					}
				} else {
					dismissDialog(progress_bar_type);
					finish();
				}
				// Displaying downloaded image into image view
				// Reading image path from sdcard
			//	String imagePath = Environment.getExternalStorageDirectory().toString() + "/downloadedfile.jpg";
				// setting downloaded into image view
			//	my_image.setImageDrawable(Drawable.createFromPath(imagePath));
			}

		}
				
		@Override
		protected void onResume() {
			System.out.println("resume");
			//if (isLink.booleanValue() == true && linkOpened.booleanValue() == false){
				//invalidQRCode();
			if (linkOpened == true) {
				Intent intent = new Intent (DownloadSurvey.this, MyOpinionsActivity.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(intent);
				finish();
			}
				
			super.onResume();
		}
		
		 @Override
			public void onBackPressed() {
					Intent intent = new Intent (DownloadSurvey.this, CaptureQRCode.class);
					startActivity(intent);
					finish();
				super.onBackPressed();
			}
		 
		 private void invalidQRCode(){
				AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
				 
				// set title
				alertDialogBuilder.setTitle("QRCode inválido");

				// set dialog message
				alertDialogBuilder
					//.setMessage("Deseja responder o questionário novamente?")
					.setCancelable(false)
					.setPositiveButton("OK",new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,int id) {
							// if this button is clicked, close
							// current activity
							//final Vibrator vibrator = (Vibrator) getSystemService( Context.VIBRATOR_SERVICE );
							//vibrator.vibrate( 50 );
							if (!qrcode.startsWith("http://") && !qrcode.startsWith("https://"))
								   qrcode = "http://" + qrcode;
							Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(qrcode));
							startActivity(browserIntent);
							linkOpened = true;
						}
					  });
					

					// create alert dialog
					AlertDialog alertDialog = alertDialogBuilder.create();

					// show it
					alertDialog.show();		
			}
		 
}
