package br.com.myopinion.questions;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import br.com.myopinion.adapter.AdapterMultipleQuestion;
import br.com.myopinion.dao.AvailableRepliesDAO;
import br.com.myopinion.dao.MainSurveysDAO;
import br.com.myopinion.dao.ObtainedRepliesDAO;
import br.com.myopinion.dao.QuestionsDAO;
import br.com.myopinion.dao.SurveyRepliesDAO;
import br.com.myopinion.model.ObtainedReplies;
import br.com.myopinion.model.Questions;
import br.com.myopinion.model.SurveyReplies;
import br.com.myopinion.survey.FinishSurvey;
import br.com.opala.myopinion.R;

public class MultipleQuestion extends Fragment {

	private ListView listView;
	public int num;

	private Cursor cursor;
	private Cursor cursor2;
	private MainSurveysDAO mainSurveyDao;
	private SurveyRepliesDAO surveyReplyDao;
	private QuestionsDAO questionsDao;
	private AvailableRepliesDAO availableRepliesDao;
	private ObtainedRepliesDAO obtainedRepliesDao;

	private int contador;
	private Questions question;
	private SurveyReplies surveyReply;
	private List qtdQuestions;
	private FragmentTransaction ft;
	private AdapterMultipleQuestion adapter;

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onViewCreated(view, savedInstanceState);

		InputMethodManager imm = (InputMethodManager) getActivity()
				.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(listView.getWindowToken(), 0);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		View view = inflater.inflate(R.layout.question_multiple_online,
				container, false);

		mainSurveyDao = new MainSurveysDAO(getActivity()
				.getApplicationContext());
		surveyReplyDao = new SurveyRepliesDAO(getActivity()
				.getApplicationContext());
		questionsDao = new QuestionsDAO(getActivity().getApplicationContext());
		availableRepliesDao = new AvailableRepliesDAO(getActivity()
				.getApplicationContext());
		obtainedRepliesDao = new ObtainedRepliesDAO(getActivity()
				.getApplicationContext());

		surveyReply = (SurveyReplies) getArguments().getSerializable(
				"surveyReply");
		question = (Questions) getArguments().getSerializable("question");
		contador = getArguments().getInt("contador");
		qtdQuestions = getArguments().getParcelableArrayList("qtdQuestions");

		TextView perguntaId = (TextView) view
				.findViewById(R.id.pergunta_multipla);
		TextView questaoId = (TextView) view
				.findViewById(R.id.questao_multipla);
		listView = (ListView) view.findViewById(android.R.id.list);

		num = contador + 1;

		cursor = mainSurveyDao.consultMainSurvey(surveyReply
				.getMain_survey_id());
		cursor.moveToFirst();

		// qtdQuestions = questionsDao.getListQuestions(cursor.getLong(0));

		List availableReplies = availableRepliesDao
				.getListAvailableReplies(question.getId());

		List<String> respostas = new ArrayList<String>();

		Iterator<String> it = availableReplies.iterator();
		while (it.hasNext()) {
			respostas.add(it.next());
		}

		cursor2 = obtainedRepliesDao.consultObtReplyByQuestionAndSurveyReply(0,
				question.getId(), surveyReply.getId());

		adapter = new AdapterMultipleQuestion(getActivity(),
				R.layout.adapter_multiple, respostas, question.getId(),
				surveyReply.getId());

		listView.setAdapter(adapter);

		questaoId.setText(num + ") ");
		perguntaId.setText(question.getTitle());

//		if (!cursor2.isAfterLast()) {
//			List<ObtainedReplies> getObtainedReplies = obtainedRepliesDao
//					.getListObtainedReplies(0, question.getId(),
//							surveyReply.getId());
//			for (int i = 0; i < listView.getCount(); i++) {
//				String item = listView.getAdapter().getItem(i).toString();
//				Log.i("MyOpinions", item + " existente");
//				for (int j = 0; j < getObtainedReplies.size(); j++) {
//					if (item.equalsIgnoreCase((getObtainedReplies.get(j))
//							.getObtReply())) {
//						System.out.println("igual");
//					}
//				}
//			}
//		}

		return view;
	}

	private boolean checkRequired(String checkedItem) {
		if (question.getRequired().booleanValue() == true) {
			if (checkedItem == null || checkedItem.equalsIgnoreCase("")) {
				Toast toast = Toast.makeText(getActivity(),
						"Questão obrigatória!", Toast.LENGTH_LONG);
				toast.setGravity(Gravity.CENTER, 0, 0);
				toast.show();
				return true;
			}
		}
		return false;
	}

	private void initQuestion(int contador) {

		Questions question = (Questions) qtdQuestions.get(contador);
		Bundle bundle = new Bundle();
		bundle.putSerializable("question", question);
		bundle.putInt("contador", contador);
		bundle.putSerializable("surveyReply", surveyReply);
		bundle.putParcelableArrayList("qtdQuestions",
				(ArrayList<? extends Parcelable>) qtdQuestions);

		if (question.getQuestionType().equalsIgnoreCase("MULTIPLE_CHOISE")) {
			// set Fragmentclass Arguments
			MultipleQuestion multipleQuestion = new MultipleQuestion();
			multipleQuestion.setArguments(bundle);

			ft = getFragmentManager().beginTransaction();
			ft.setCustomAnimations(R.anim.slide_in_right,
					R.anim.slide_out_left, R.anim.slide_in_left,
					R.anim.slide_out_right);
			ft.replace(R.id.fragment_content, multipleQuestion,
					String.valueOf(contador));
			ft.addToBackStack(null);
			ft.commit();
		} else if (question.getQuestionType().equalsIgnoreCase("BOXES")) {
			// set Fragmentclass Arguments
			SelectionQuestion selectionQuestion = new SelectionQuestion();
			selectionQuestion.setArguments(bundle);

			ft = getFragmentManager().beginTransaction();
			ft.setCustomAnimations(R.anim.slide_in_right,
					R.anim.slide_out_left, R.anim.slide_in_left,
					R.anim.slide_out_right);
			ft.replace(R.id.fragment_content, selectionQuestion,
					String.valueOf(contador));
			ft.addToBackStack(null);
			ft.commit();
		} else if (question.getQuestionType().equalsIgnoreCase("TEXT")) {
			// set Fragmentclass Arguments
			TextQuestion textQuestion = new TextQuestion();
			textQuestion.setArguments(bundle);

			ft = getFragmentManager().beginTransaction();
			ft.setCustomAnimations(R.anim.slide_in_right,
					R.anim.slide_out_left, R.anim.slide_in_left,
					R.anim.slide_out_right);
			ft.replace(R.id.fragment_content, textQuestion,
					String.valueOf(contador));
			ft.addToBackStack(null);
			ft.commit();
		}
	}

	private void finishSurvey(int contador) {
		Bundle bundle = new Bundle();
		bundle.putInt("contador", contador);
		FinishSurvey finishSurvey = new FinishSurvey();
		finishSurvey.setArguments(bundle);

		ft = getFragmentManager().beginTransaction();
		ft.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left,
				R.anim.slide_in_left, R.anim.slide_out_right);
		ft.replace(R.id.fragment_content, finishSurvey);
		ft.addToBackStack(null);
		ft.commit();
	}

	public boolean buttonSaveCreate() {
		if (cursor2.isAfterLast()) {

			String checkedItem = adapter.getItemChecked();

			if (checkRequired(checkedItem)) {
				return false;
			} else {

				contador = contador + 1;

				ObtainedReplies obtainedReplies = new ObtainedReplies();
				obtainedReplies.setQuestion_id(question.getId());
				obtainedReplies.setSurvey_reply_id(surveyReply.getId());
				obtainedReplies.setObtReply(checkedItem);
				obtainedRepliesDao.createObtReply(obtainedReplies);
			

				if (contador < qtdQuestions.size()) {
					initQuestion(contador);
				} else {
					finishSurvey(contador);
				}
				return true;
			}
		} else {

			List<ObtainedReplies> getObtainedReplies = obtainedRepliesDao
					.getListObtainedReplies(0, question.getId(),
							surveyReply.getId());
			final ObtainedReplies obtReplies = (ObtainedReplies) getObtainedReplies
					.get(0);

			String checkedItem = adapter.getItemChecked();

			if (checkRequired(checkedItem)) {
				return false;
			} else {
				contador = contador + 1;

				obtReplies.setQuestion_id(question.getId());
				obtReplies.setSurvey_reply_id(surveyReply.getId());
				obtReplies.setObtReply(checkedItem);
				obtainedRepliesDao.updateObtReply(obtReplies);

				if (contador < qtdQuestions.size()) {
					initQuestion(contador);
				} else {
					finishSurvey(contador);
				}
				return true;
			}

		}
	}
}
