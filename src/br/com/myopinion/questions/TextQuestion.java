package br.com.myopinion.questions;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.InputType;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import br.com.myopinion.dao.AvailableRepliesDAO;
import br.com.myopinion.dao.MainSurveysDAO;
import br.com.myopinion.dao.ObtainedRepliesDAO;
import br.com.myopinion.dao.QuestionsDAO;
import br.com.myopinion.dao.SurveyRepliesDAO;
import br.com.myopinion.model.ObtainedReplies;
import br.com.myopinion.model.Questions;
import br.com.myopinion.model.SurveyReplies;
import br.com.myopinion.survey.FinishSurvey;
import br.com.opala.myopinion.R;

public class TextQuestion extends Fragment{
	
		public int num;
		
		private Cursor cursor;
		private Cursor cursor2;
		private MainSurveysDAO mainSurveyDao;
		private SurveyRepliesDAO surveyReplyDao;
		private QuestionsDAO questionsDao;
		private AvailableRepliesDAO availableRepliesDao;
		private ObtainedRepliesDAO obtainedRepliesDao;
		
		private int contador;
		private Questions question;
		private SurveyReplies surveyReply;
		private List qtdQuestions;
		private FragmentTransaction ft;
		
		private EditText resposta;
		private View view;
	
		
		
		@Override
		public void onViewCreated(View view, Bundle savedInstanceState) {
			// TODO Auto-generated method stub
			super.onViewCreated(view, savedInstanceState);
			
		        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(
					      Context.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(resposta.getWindowToken(), 0);
		}


		@Override
    	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
	        view = inflater.inflate(R.layout.question_text_online, container, false); 
					        
	        mainSurveyDao = new MainSurveysDAO(getActivity().getApplicationContext());
	        mainSurveyDao.open();
	     	surveyReplyDao = new SurveyRepliesDAO(getActivity().getApplicationContext());
	     	surveyReplyDao.open();
	     	questionsDao = new QuestionsDAO(getActivity().getApplicationContext());
	     	questionsDao.open();
	     	availableRepliesDao = new AvailableRepliesDAO(getActivity().getApplicationContext());
	     	availableRepliesDao.open();
	     	obtainedRepliesDao = new ObtainedRepliesDAO(getActivity().getApplicationContext());
	     	obtainedRepliesDao.open();
					
	     	surveyReply = (SurveyReplies) getArguments().getSerializable("surveyReply");
	    	question = (Questions) getArguments().getSerializable("question");
	    	contador = getArguments().getInt("contador");
	    	qtdQuestions = getArguments().getParcelableArrayList("qtdQuestions");
	    		    	
	        num = contador + 1;
			cursor = mainSurveyDao.consultMainSurvey(surveyReply.getMain_survey_id());
			cursor.moveToFirst();
			
		   // qtdQuestions = questionsDao.getListQuestions(cursor.getLong(0));

			cursor2 = obtainedRepliesDao.consultObtReplyByQuestionAndSurveyReply(0, question.getId(), surveyReply.getId());
							
			resposta = (EditText) view.findViewById(R.id.resposta_texto);
	        TextView perguntaId = (TextView) view.findViewById(R.id.pergunta_texto);
	        TextView questaoId = (TextView) view.findViewById(R.id.questao_texto);
	        
	        questaoId.setText(num + ") ");
	        perguntaId.setText(question.getTitle());
	        
	    	if (question.getTextType() != null){
 				if(question.getTextType().equalsIgnoreCase("NUMERIC")){
 					resposta.setInputType(InputType.TYPE_CLASS_PHONE);
 				} else if (question.getTextType().equalsIgnoreCase("EMAIL")){
 					resposta.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
 				}
 			}

	    	if (!cursor2.isAfterLast()){
	    		cursor2.moveToFirst();
			 	resposta.setText(cursor2.getString(1), TextView.BufferType.EDITABLE);	
	    	}
	    		
			return view;
			 
	 }
	 

	private boolean checkRequired(String resposta){
		if (question.getRequired().booleanValue() == true){
			if(resposta.equalsIgnoreCase("")){
				Toast toast = Toast.makeText(getActivity(), "Questão obrigatória!", Toast.LENGTH_LONG );
				toast.setGravity(Gravity.CENTER, 0, 0);
				toast.show();
				return true;
			}
		}
		return false;
	}
	
	private void initQuestion(int contador){
    	System.out.println("Contador: " + contador);
    	
		Questions question = (Questions) qtdQuestions.get(contador);
		Bundle bundle = new Bundle();
	 	bundle.putSerializable("question", question);
		bundle.putInt("contador", contador);
		bundle.putSerializable("surveyReply", surveyReply);
		bundle.putParcelableArrayList("qtdQuestions", (ArrayList<? extends Parcelable>) qtdQuestions);
		
   	 	if (question.getQuestionType().equalsIgnoreCase("MULTIPLE_CHOISE")) {
   	 		//set Fragmentclass Arguments
			MultipleQuestion multipleQuestion = new MultipleQuestion();
			multipleQuestion.setArguments(bundle);
			
   		 	ft = getFragmentManager().beginTransaction();
	        ft.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right);
	        ft.replace(R.id.fragment_content, multipleQuestion, String.valueOf(contador));
	        ft.addToBackStack(null);
	        ft.commit();
		} else if (question.getQuestionType().equalsIgnoreCase("BOXES")) {
   	 	   //set Fragmentclass Arguments
			SelectionQuestion selectionQuestion = new SelectionQuestion();
			selectionQuestion.setArguments(bundle);
			
   		 	ft = getFragmentManager().beginTransaction();
	        ft.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right);
	        ft.replace(R.id.fragment_content, selectionQuestion, String.valueOf(contador));
	        ft.addToBackStack(null);
	        ft.commit();
		} else if (question.getQuestionType().equalsIgnoreCase("TEXT")) {
   	 	   //set Fragmentclass Arguments
			TextQuestion textQuestion = new TextQuestion();
			textQuestion.setArguments(bundle);
			
   		 	ft = getFragmentManager().beginTransaction();
	        ft.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right);
	        ft.replace(R.id.fragment_content, textQuestion, String.valueOf(contador));
	        ft.addToBackStack(null);
	        ft.commit();
		} 
	}
	
	 private void finishSurvey(int contador){
			Bundle bundle = new Bundle();
			bundle.putInt("contador", contador);
			FinishSurvey finishSurvey = new FinishSurvey();
			finishSurvey.setArguments(bundle);
			
		 	ft = getFragmentManager().beginTransaction();
	        ft.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right);
	        ft.replace(R.id.fragment_content, finishSurvey, String.valueOf(contador));
	        ft.addToBackStack(null);
	        ft.commit();
	 }
	 
	 public boolean buttonSaveCreate(){
		 if(cursor2.isAfterLast()){      
			 
					LinearLayout ll = (LinearLayout) view.findViewById(R.id.linearLayout1);
					ll.requestFocus();
					InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(
						      Context.INPUT_METHOD_SERVICE);
						imm.hideSoftInputFromWindow(resposta.getWindowToken(), 0);
						
					String resposta_obtida = resposta.getEditableText().toString();

					if(checkRequired(resposta_obtida)){
						return false;
					} else {
						contador = contador + 1;
						
						ObtainedReplies obtainedReplies = new ObtainedReplies();
						obtainedReplies.setQuestion_id(question.getId());
						obtainedReplies.setSurvey_reply_id(surveyReply.getId());
						
						obtainedReplies.setObtReply(resposta_obtida);
						obtainedRepliesDao.createObtReply(obtainedReplies);
						
						if (contador < qtdQuestions.size()){
							initQuestion(contador);
    					} else {
    						finishSurvey(contador);
    					}
						return true;
					}
				} else {
				 	cursor2.moveToFirst();
				 	//resposta.setText(cursor2.getString(1), TextView.BufferType.EDITABLE);			 
				
					List<ObtainedReplies> getObtainedReplies = obtainedRepliesDao.getListObtainedReplies(0, question.getId(), surveyReply.getId());
					final ObtainedReplies obtReplies = (ObtainedReplies) getObtainedReplies.get(0);
					
					LinearLayout ll = (LinearLayout) view.findViewById(R.id.linearLayout1);
					ll.requestFocus();
					InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(
						      Context.INPUT_METHOD_SERVICE);
						imm.hideSoftInputFromWindow(resposta.getWindowToken(), 0);
					
					String resposta_obtida = resposta.getEditableText().toString();

					if(checkRequired(resposta_obtida)){
						return false;
					} else {
						
						contador = contador + 1;
						
						obtReplies.setQuestion_id(question.getId());
						obtReplies.setSurvey_reply_id(surveyReply.getId());

						obtReplies.setObtReply(resposta_obtida);
						obtainedRepliesDao.updateObtReply(obtReplies);


						if (contador < qtdQuestions.size()){
							initQuestion(contador);
    					} else {
    						finishSurvey(contador);
    					}
						return true;
					}

				}
	 } 
}
