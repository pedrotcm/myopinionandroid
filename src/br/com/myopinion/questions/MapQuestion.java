package br.com.myopinion.questions;

import java.util.List;

import android.database.Cursor;
import android.location.Location;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import br.com.myopinion.dao.ObtainedRepliesDAO;
import br.com.myopinion.dao.QuestionsDAO;
import br.com.myopinion.model.ObtainedReplies;
import br.com.myopinion.model.Questions;
import br.com.myopinion.survey.MainOffline;
import br.com.opala.myopinion.R;

import com.NYXDigital.NiceSupportMapFragment;
import com.actionbarsherlock.app.SherlockFragment;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapQuestion extends SherlockFragment {

private View view;
/**
 * Note that this may be null if the Google Play services APK is not
 * available.
 */

private QuestionsDAO questionsDao;
private ObtainedRepliesDAO obtainedRepliesDao;
private GoogleMap mMap;
private Double latitude, longitude;
private Marker marker;

private long id_mainSurvey;
private long id_question;
private long id_surveyReply;
private Cursor cLatitude;
private List<ObtainedReplies> obtReplyLat;
private Cursor cLongitude;
private List<ObtainedReplies> obtReplyLng;
private TextView tvLatitude;
private TextView tvLongitude;




@Override
public View onCreateView(LayoutInflater inflater, ViewGroup container,
        Bundle savedInstanceState) {
	super.onCreateView(inflater, container, savedInstanceState);
    if (container == null) {
        return null;
    }
    
	questionsDao = new QuestionsDAO(getActivity().getApplicationContext());
	obtainedRepliesDao = new ObtainedRepliesDAO(getActivity().getApplicationContext());
    
    Bundle extras = getArguments();
	id_mainSurvey = extras.getLong("id_mainSurvey");
	id_question = extras.getLong("id_question");
	id_surveyReply = extras.getLong("id_surveyReply");
	
	cLatitude = questionsDao.consultCoordinatesQuestion("LATITUDE", id_question);
	if (cLatitude.getCount() != 0)
		obtReplyLat = obtainedRepliesDao.getListObtainedReplies(0, cLatitude.getLong(0), id_surveyReply);
	cLongitude = questionsDao.consultCoordinatesQuestion("LONGITUDE", id_question);
	if (cLongitude.getCount() != 0)
		obtReplyLng = obtainedRepliesDao.getListObtainedReplies(0, cLongitude.getLong(0), id_surveyReply);

    view = (RelativeLayout) inflater.inflate(R.layout.map, container, false);
    // Passing harcoded values for latitude & longitude. Please change as per your need. This is just used to drop a Marker on the Map
            
    tvLatitude = (TextView) view.findViewById(R.id.tvLatitude);
    tvLongitude = (TextView) view.findViewById(R.id.tvLongitude);
    
    setUpMapIfNeeded(); // For setting up the MapFragment
            
    return view;
}

/***** Sets up the map if it is possible to do so *****/
public void setUpMapIfNeeded() {
    // Do a null check to confirm that we have not already instantiated the map.
    if (mMap == null) {
        // Try to obtain the map from the SupportMapFragment.
    	NiceSupportMapFragment mapFragment= ((NiceSupportMapFragment) MainOffline.fragmentManager
                .findFragmentById(R.id.mapQuestion));
    	mapFragment.setPreventParentScrolling(false);
        mMap = mapFragment.getMap();
        
    	// Check if we were successful in obtaining the map.
        if (mMap != null){
            setUpMap();
        
        }
    }
}

/**
 * This is where we can add markers or lines, add listeners or move the
 * camera.
 * <p>
 * This should only be called once and when we are sure that {@link #mMap}
 * is not null.
 */
private void setUpMap() {
    // For showing a move to my loction button
    mMap.setMyLocationEnabled(true);
	mMap.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {
		
		@Override
		public void onMarkerDragStart(Marker arg0) {
			// TODO Auto-generated method stub
		}
		
		@Override
		public void onMarkerDragEnd(Marker arg0) {
			// TODO Auto-generated method stub
			
			marker = arg0;
			latitude = arg0.getPosition().latitude;
			longitude = arg0.getPosition().longitude;
//			chamarProcesso("" + arg0.getPosition().latitude,
//					"" + arg0.getPosition().longitude);
			tvLatitude.setText(String.format("%.7f", latitude));
			tvLongitude.setText(String.format("%.7f", longitude));
			
			mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(
					latitude, longitude), 15.0f));
			
			
		}
		
		@Override
		public void onMarkerDrag(Marker arg0) {
			// TODO Auto-generated method stub
			
		}
	});
	
	mMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
		
		@Override
		public void onMapLongClick(LatLng arg0) {
			// TODO Auto-generated method stub
			if(marker != null){
				marker.remove();
			}
			marker = mMap
					.addMarker(new MarkerOptions()
							.position(arg0)
							.title("Localização correta?")
							.draggable(true)
							.snippet("Senão, arraste para o local correto.")
					);
					
			latitude = arg0.latitude;
			longitude = arg0.longitude;
						

			tvLatitude.setText(String.format("%.7f", latitude));
			tvLongitude.setText(String.format("%.7f", longitude));
			
			marker.showInfoWindow();

			mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(arg0, 15.0f));
		}
	});
    // Obter coordenadas
    mMap.setOnMyLocationChangeListener(new GoogleMap.OnMyLocationChangeListener() {
		boolean findLatLng = true;
		@Override
		public void onMyLocationChange(Location arg0) {
			// TODO Auto-generated method stub
			System.out.println("changeLocation");
			if (findLatLng){
				latitude = arg0.getLatitude();
				longitude = arg0.getLongitude();
				
				mapCenter(latitude, longitude);
				
				findLatLng = false;
			}
		}
	});
    
    
}

@Override
public void onViewCreated(View view, Bundle savedInstanceState) {
    // TODO Auto-generated method stub
   // if (mMap != null)
     //   setUpMap();

    if (mMap == null) {
        // Try to obtain the map from the SupportMapFragment.
    	NiceSupportMapFragment mapFragment = ((NiceSupportMapFragment) MainOffline.fragmentManager
                .findFragmentById(R.id.mapQuestion));
    	mapFragment.setPreventParentScrolling(false);
    	mMap = mapFragment.getMap();
        // Check if we were successful in obtaining the map.
        if (mMap != null)
            setUpMap();
    }
}


/**** The mapfragment's id must be removed from the FragmentManager
 **** or else if the same it is passed on the next time then 
 **** app will crash ****/
//@Override
public void onDestroyView() {
    super.onDestroyView();
    if (mMap != null) {
        MainOffline.fragmentManager.beginTransaction()
            .remove(MainOffline.fragmentManager.findFragmentById(R.id.mapQuestion)).commit();
        mMap = null;
    }
}

public void mapCenter(double lat, double lng) {
	try {
		
		if(cLatitude.getCount() != 0 && obtReplyLat.size() != 0){
			lat = Double.valueOf(obtReplyLat.get(0).getObtReply());
		}
		if(cLongitude.getCount() != 0 && obtReplyLng.size() != 0){
			lng = Double.valueOf(obtReplyLng.get(0).getObtReply());
		}
//
//		mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(
//				lat, lng), 15.0f));

		tvLatitude.setText(String.format("%.7f", lat));
		tvLongitude.setText(String.format("%.7f", lng));
		
		mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(
				lat, lng), 15.0f));
		marker = mMap.addMarker(new MarkerOptions()
				.position(new LatLng(lat, lng))
				.title("Localização correta?")
				.draggable(true)
				.snippet("Senão, arraste para o local correto."));

		marker.showInfoWindow();
				
	} catch (Exception e) {
		// TODO: handle exception
	}
}

public void saveCoordinates(){
	Cursor cCoordinates = questionsDao.consultCoordinatesQuestion("COORDINATES", id_question);
		//System.out.println(cCoordinates.getCount());
		if (cCoordinates.getCount() == 1){
			if (cLatitude.getCount() == 0) {
				Questions lat = new Questions();
				lat.setRequired(false);
				lat.setTitle("Latitude:");
				lat.setQuestionType("LATITUDE");
				lat.setQuestion_id(id_question);
				Long idQuestionLat = questionsDao.createQuestionDependent(lat);

				//System.out.println("Criou latidude");
				ObtainedReplies obtLat = new ObtainedReplies();
				obtLat.setQuestion_id(idQuestionLat);
				obtLat.setSurvey_reply_id(id_surveyReply);
				obtLat.setObtReply(String.valueOf(latitude));
				obtainedRepliesDao.createObtReply(obtLat);
			} else {
				if (obtReplyLat.size() == 0) {
					//System.out.println("Criou latidude");
					ObtainedReplies obtLat = new ObtainedReplies();
					obtLat.setQuestion_id(cLatitude.getLong(0));
					obtLat.setSurvey_reply_id(id_surveyReply);
					obtLat.setObtReply(String.valueOf(latitude));
					obtainedRepliesDao.createObtReply(obtLat);
				} else {
					//System.out.println("Atualizou latidude");
					ObtainedReplies obtLat = obtReplyLat.get(0);
					obtLat.setQuestion_id(cLatitude.getLong(0));
					obtLat.setSurvey_reply_id(id_surveyReply);
					obtLat.setObtReply(String.valueOf(latitude));
					obtainedRepliesDao.updateObtReply(obtLat);
				}
			}
			
			if (cLongitude.getCount() == 0) {
				Questions lng = new Questions();
				lng.setRequired(false);
				lng.setTitle("Longitude:");
				lng.setQuestionType("LONGITUDE");
				lng.setQuestion_id(id_question);
				Long idQuestionLng = questionsDao.createQuestionDependent(lng);
				
				//System.out.println("Criou longitude");
				ObtainedReplies obtLng = new ObtainedReplies();
				obtLng.setQuestion_id(idQuestionLng);
				obtLng.setSurvey_reply_id(id_surveyReply);
				obtLng.setObtReply(String.valueOf(longitude));
				obtainedRepliesDao.createObtReply(obtLng);
			} else {
				if (obtReplyLng.size() == 0) {
					//System.out.println("Criou longitude");
					ObtainedReplies obtLng = new ObtainedReplies();
					obtLng.setQuestion_id(cLongitude.getLong(0));
					obtLng.setSurvey_reply_id(id_surveyReply);
					obtLng.setObtReply(String.valueOf(longitude));
					obtainedRepliesDao.createObtReply(obtLng);
				} else {
					//System.out.println("Atualizou Longitude");
					ObtainedReplies obtLng = obtReplyLng.get(0);
					obtLng.setQuestion_id(cLongitude.getLong(0));
					obtLng.setSurvey_reply_id(id_surveyReply);
					obtLng.setObtReply(String.valueOf(longitude));
					obtainedRepliesDao.updateObtReply(obtLng);
				}
			}	
		}
	}
}