package br.com.myopinion.questions;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import android.content.Context;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import br.com.myopinion.adapter.AdapterSelectionQuestion;
import br.com.myopinion.dao.AvailableRepliesDAO;
import br.com.myopinion.dao.MainSurveysDAO;
import br.com.myopinion.dao.ObtainedRepliesDAO;
import br.com.myopinion.dao.QuestionsDAO;
import br.com.myopinion.dao.SurveyRepliesDAO;
import br.com.myopinion.model.ObtainedReplies;
import br.com.myopinion.model.Questions;
import br.com.myopinion.model.SurveyReplies;
import br.com.myopinion.survey.FinishSurvey;
import br.com.opala.myopinion.R;

public class SelectionQuestion extends Fragment {
	
	private ListView listView;
	public int num;
	
	private Cursor cursor;
	private Cursor cursor2;
	private MainSurveysDAO mainSurveyDao;
	private SurveyRepliesDAO surveyReplyDao;
	private QuestionsDAO questionsDao;
	private AvailableRepliesDAO availableRepliesDao;
	private ObtainedRepliesDAO obtainedRepliesDao;
	private List<ObtainedReplies> getObtainedReplies;
	
	private int contador;
	private Questions question;
	private SurveyReplies surveyReply;
	private List qtdQuestions;
	private FragmentTransaction ft;
	private AdapterSelectionQuestion adapter;
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onViewCreated(view, savedInstanceState);
		
	        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(
				      Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(listView.getWindowToken(), 0);
	}
	
	@Override
    	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
	        View view = inflater.inflate(R.layout.question_selection_online, container, false); 
				
	        mainSurveyDao = new MainSurveysDAO(getActivity().getApplicationContext());
	     	surveyReplyDao = new SurveyRepliesDAO(getActivity().getApplicationContext());
	     	questionsDao = new QuestionsDAO(getActivity().getApplicationContext());
	     	availableRepliesDao = new AvailableRepliesDAO(getActivity().getApplicationContext());
	     	obtainedRepliesDao = new ObtainedRepliesDAO(getActivity().getApplicationContext());
					
	     	surveyReply = (SurveyReplies) getArguments().getSerializable("surveyReply");
	    	question = (Questions) getArguments().getSerializable("question");
	    	contador = getArguments().getInt("contador");
	    	qtdQuestions = getArguments().getParcelableArrayList("qtdQuestions");

	        TextView perguntaId = (TextView) view.findViewById(R.id.pergunta_selecao);
	        listView = (ListView) view.findViewById(android.R.id.list);
	        TextView questaoId = (TextView) view.findViewById(R.id.questao_selecao);
	        
		    num = contador + 1; 
		   
			cursor = mainSurveyDao.consultMainSurvey(surveyReply.getMain_survey_id());
			cursor.moveToFirst();
			
		    //qtdQuestions = questionsDao.getListQuestions(cursor.getLong(0));

		    List availableReplies = availableRepliesDao.getListAvailableReplies(question.getId());
		    
	        List<String> respostas = new ArrayList<String>();
	       
			Iterator<String> it = availableReplies.iterator();
			while (it.hasNext()) {
				respostas.add(it.next());
			}
			
			adapter = new AdapterSelectionQuestion(getActivity(),
					R.layout.adapter_selection, respostas, question.getId(),
					surveyReply.getId());

			listView.setAdapter(adapter);
		
			cursor2 = obtainedRepliesDao.consultObtReplyByQuestionAndSurveyReply(0, question.getId(), surveyReply.getId());
			
			while(cursor2.isAfterLast() == false){
				cursor2.moveToNext();
			}
			
 	        questaoId.setText(num + ") ");
 	        perguntaId.setText(question.getTitle());
 	        
// 	        if (cursor2.getCount() != 0){
// 	        	getObtainedReplies = obtainedRepliesDao.getListObtainedReplies(0, question.getId(), surveyReply.getId());
// 	        	for (int i=0; i<listView.getCount(); i++) {
// 	        		String item = listView.getAdapter().getItem(i).toString();
// 					Log.i("MyOpinions", item + " existente");
//				 	for (int j=0; j<getObtainedReplies.size(); j++){
//				 		if (item.equalsIgnoreCase((getObtainedReplies.get(j)).getObtReply())) {
//				 			System.out.println("igual");
//				 		}
//				 	}
//				}
//			}
	      
		return view;		
	 }
	 
	 private boolean checkRequired(List<String> listReplies){
			if (question.getRequired().booleanValue() == true){
				if (listReplies == null || listReplies.isEmpty()) {
						Toast toast = Toast.makeText(getActivity(), "Questão obrigatória!", Toast.LENGTH_LONG );
						toast.setGravity(Gravity.CENTER, 0, 0);
						toast.show();
						return true;
				}
			}
			return false;
		}
		
	 private static int getCheckedItemCount(ListView listView)
		{
		    if (Build.VERSION.SDK_INT >= 11) return listView.getCheckedItemCount();
		    else
		    {
		        int count = 0;
		        for (int i = listView.getCount() - 1; i >= 0; i--)
		            if (listView.isItemChecked(i)) count++;
		        return count;
		    }
		}
	 private void initQuestion(int contador){

			Questions question = (Questions) qtdQuestions.get(contador);
			Bundle bundle = new Bundle();
   	 		bundle.putSerializable("question", question);
			bundle.putInt("contador", contador);
			bundle.putSerializable("surveyReply", surveyReply);
			bundle.putParcelableArrayList("qtdQuestions", (ArrayList<? extends Parcelable>) qtdQuestions);

	   	 	if (question.getQuestionType().equalsIgnoreCase("MULTIPLE_CHOISE")) {
	   	 		//set Fragmentclass Arguments
				MultipleQuestion multipleQuestion = new MultipleQuestion();
				multipleQuestion.setArguments(bundle);
				
	   		 	ft = getFragmentManager().beginTransaction();
		        ft.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right);
		        ft.replace(R.id.fragment_content, multipleQuestion, String.valueOf(contador));
		        ft.addToBackStack(null);
		        ft.commit();
			} else if (question.getQuestionType().equalsIgnoreCase("BOXES")) {
	   	 	   //set Fragmentclass Arguments
				SelectionQuestion selectionQuestion = new SelectionQuestion();
				selectionQuestion.setArguments(bundle);
				
	   		 	ft = getFragmentManager().beginTransaction();
		        ft.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right);
		        ft.replace(R.id.fragment_content, selectionQuestion, String.valueOf(contador));
		        ft.addToBackStack(null);
		        ft.commit();
			} else if (question.getQuestionType().equalsIgnoreCase("TEXT")) {
	   	 	   //set Fragmentclass Arguments
				TextQuestion textQuestion = new TextQuestion();
				textQuestion.setArguments(bundle);
				
	   		 	ft = getFragmentManager().beginTransaction();
		        ft.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right);
		        ft.replace(R.id.fragment_content, textQuestion, String.valueOf(contador));
		        ft.addToBackStack(null);
		        ft.commit();
			} 
		}
	 
	 private void finishSurvey(int contador){
			Bundle bundle = new Bundle();
			bundle.putInt("contador", contador);
			FinishSurvey finishSurvey = new FinishSurvey();
			finishSurvey.setArguments(bundle);
			
   		 	ft = getFragmentManager().beginTransaction();
	        ft.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right);
	        ft.replace(R.id.fragment_content, finishSurvey);
	        ft.addToBackStack(null);
	        ft.commit();
	 }
	 
	 public boolean buttonSaveCreate(){
		 if(cursor2.getCount() == 0){
	        							
			List<String> listReplies = adapter.getItemsChecked();

			if (checkRequired(listReplies)) {
				return false;
			} else {
				contador = contador + 1;

				for (int i = 0; i < listReplies.size(); i++) {
					String item = listReplies.get(i);

					ObtainedReplies obtainedReplies = new ObtainedReplies();
					obtainedReplies.setQuestion_id(question.getId());
					obtainedReplies.setSurvey_reply_id(surveyReply.getId());
					obtainedReplies.setObtReply(item);
					obtainedRepliesDao.createObtReply(obtainedReplies);

				}

				if (contador < qtdQuestions.size()) {
					initQuestion(contador);
				} else {
					finishSurvey(contador);
				}
				return true;
			}

		} else {

			getObtainedReplies = obtainedRepliesDao.getListObtainedReplies(0,
					question.getId(), surveyReply.getId());

			List<String> listReplies = adapter.getItemsChecked();

			if (checkRequired(listReplies)) {
				return false;
			} else {
				for (int j = 0; j < getObtainedReplies.size(); j++) {
					obtainedRepliesDao.deleteObtReply(getObtainedReplies.get(j)
							.getId());
				}

				contador = contador + 1;

				for (int i = 0; i < listReplies.size(); i++) {
					String item = listReplies.get(i);

					ObtainedReplies obtainedReplies = new ObtainedReplies();
					obtainedReplies.setQuestion_id(question.getId());
					obtainedReplies.setSurvey_reply_id(surveyReply.getId());
					obtainedReplies.setObtReply(item);
					obtainedRepliesDao.createObtReply(obtainedReplies);

				}
				if (contador < qtdQuestions.size()) {
					initQuestion(contador);
				} else {
					finishSurvey(contador);
				}
				return true;
			}
		}
	}
}
