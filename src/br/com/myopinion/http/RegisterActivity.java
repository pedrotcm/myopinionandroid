package br.com.myopinion.http;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.apache.http.client.HttpResponseException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.params.ConnManagerParams;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;
import br.com.myopinion.MyOpinionsActivity;
import br.com.myopinion.adapter.QustomDialogBuilder;
import br.com.myopinion.dao.AvailableRepliesDAO;
import br.com.myopinion.dao.BasicDAO;
import br.com.myopinion.dao.CadastreRepliesDAO;
import br.com.myopinion.dao.MainSurveysDAO;
import br.com.myopinion.dao.ObtainedCadastreRepliesDAO;
import br.com.myopinion.dao.ObtainedRepliesDAO;
import br.com.myopinion.dao.QuestionsDAO;
import br.com.myopinion.dao.SurveyRepliesDAO;
import br.com.myopinion.model.MainSurveys;
import br.com.myopinion.model.ObtainedCadastreReplies;
import br.com.myopinion.model.ObtainedReplies;
import br.com.myopinion.model.Questions;
import br.com.myopinion.model.SurveyReplies;
import br.com.myopinion.survey.ListSurveys;

import com.actionbarsherlock.app.SherlockActivity;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

public class RegisterActivity extends SherlockActivity {

	//private final static String URL = "http://200.253.123.148:5001/";
	private final static String URL = "http://187.63.48.148:5002/";

	//private final static String URL = "http://192.168.43.174:3000/";
	private static final int HTTP_TIMEOUT = 45*1000;

	private BasicDAO basicDAO;
	private MainSurveysDAO mainSurveyDao;
	private SurveyRepliesDAO surveyReplyDao;
	private QuestionsDAO questionsDao;
	private AvailableRepliesDAO availableRepliesDao;
	private ObtainedRepliesDAO obtainedRepliesDao;
	private CadastreRepliesDAO cadastreRepliesDao;
	private ObtainedCadastreRepliesDAO obtainedCadastreRepliesDao;
	private long idMainWeb;
	private MainSurveys mainSurvey;
	private List<Questions> listQuestions = new ArrayList<Questions>();
	private List<Questions> listQuestionsDependents = new ArrayList<Questions>();
	private List<Questions> questions = new ArrayList<Questions>();
	private long idMain;
	private List<SurveyReplies> listSurveyReplies;

	
	private String msg;
	private ProgressDialog pDialog;
	// Progress dialog type (0 - for Horizontal progress bar)
	public static final int progress_bar_type = 0; 
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		basicDAO.close();
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case progress_bar_type:
			Cursor c = surveyReplyDao.getSyncSurveyReplies(idMain, 0);
			pDialog = new ProgressDialog(this);
			pDialog.setProgress(0);
			pDialog.setMax(c.getCount());
			pDialog.setMessage("Sincronizando as respostas. Por favor aguarde...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
			pDialog.show();
			return pDialog;
		default:
			return null;
		}
	}
	private ArrayList<MainSurveys> listMainSurveyWeb = new ArrayList<MainSurveys>();
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		basicDAO = new BasicDAO(getApplicationContext());
		basicDAO.open();
		mainSurveyDao = new MainSurveysDAO(getApplicationContext());
		surveyReplyDao = new SurveyRepliesDAO(getApplicationContext());
		questionsDao = new QuestionsDAO(getApplicationContext());
		availableRepliesDao = new AvailableRepliesDAO(getApplicationContext());
		obtainedRepliesDao = new ObtainedRepliesDAO(getApplicationContext());
		cadastreRepliesDao = new CadastreRepliesDAO(getApplicationContext());
		obtainedCadastreRepliesDao = new ObtainedCadastreRepliesDAO(getApplicationContext());
		
		msg = null;
		
		Bundle extras = getIntent().getExtras();
		idMainWeb = extras.getLong("id");
		idMain = extras.getLong("id");
		
		mainSurvey = mainSurveyDao.getMainSurvey(idMainWeb);
	    listQuestions = questionsDao.getListQuestions(idMainWeb);
		new SearchSurveys().execute(URL);
		
		/*RegisterTask registerTask = new RegisterTask(
				RegisterActivity.this);
		registerTask.setMessageLoading("Registering new account...");
		registerTask.execute(REGISTER_API_ENDPOINT_URL);*/
	

	}	

	private class RegisterSurvey extends AsyncTask<String, Void, String>  {
		
		@Override
		protected String doInBackground(String... urls) {
			DefaultHttpClient client = new DefaultHttpClient();
			HttpParams params = client.getParams();
			HttpConnectionParams.setConnectionTimeout(params, HTTP_TIMEOUT);
			HttpConnectionParams.setSoTimeout(params, HTTP_TIMEOUT);
			ConnManagerParams.setTimeout(params, HTTP_TIMEOUT);
			//client.getParams().setParameter(CoreConnectionPNames.CONNECTION_TIMEOUT, HTTP_TIMEOUT);
		    //client.getParams().setParameter(CoreConnectionPNames.SO_TIMEOUT, HTTP_TIMEOUT);
			
			HttpPost post = new HttpPost(urls[0] + "/main_surveys.json");
			JSONObject holder = new JSONObject();
			JSONObject mainSurveyObj = new JSONObject();
			String response = null;
			JSONObject json = new JSONObject();

			DefaultHttpClient client2 = new DefaultHttpClient();
			HttpParams params2 = client2.getParams();
			HttpConnectionParams.setConnectionTimeout(params2, HTTP_TIMEOUT);
			HttpConnectionParams.setSoTimeout(params2, HTTP_TIMEOUT);
			ConnManagerParams.setTimeout(params2, HTTP_TIMEOUT);

			HttpPost post2 = new HttpPost(urls[0] + "/questions.json");
			JSONObject holder2 = new JSONObject();
			JSONObject questionsObj = new JSONObject();
			String response2 = null;
			JSONObject json2 = new JSONObject();

			try {
				try {
					//json.put("success", false);
					//json.put("info", "Something went wrong. Retry!");
					mainSurveyObj.put("description", mainSurvey.getDescription());
					mainSurveyObj.put("interval", mainSurvey.getInterval());
					mainSurveyObj.put("surveyType", mainSurvey.getSurveyType());
					mainSurveyObj.put("created_at", mainSurvey.getCreated_at());
					mainSurveyObj.put("validate",	mainSurvey.getValidate());
				
					holder.put("main_survey", mainSurveyObj);
					StringEntity se = new StringEntity(holder.toString(), HTTP.UTF_8);
					post.setEntity(se);
					post.setHeader("Accept", "application/json");
					post.setHeader("Content-Type", "application/json");

					ResponseHandler<String> responseHandler = new BasicResponseHandler();
					response = client.execute(post, responseHandler);		
				/*	HttpResponse response= client.execute(post);
					
					BufferedReader bufferedReader = null;
					bufferedReader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
					StringBuffer stringBuffer = new StringBuffer("");
					String line = "";
					String ls = System.getProperty("line.separator");

					while((line = bufferedReader.readLine()) != null){
						stringBuffer.append(line + ls);
					}

					bufferedReader.close();
					String resultado = stringBuffer.toString();
					System.out.println("RESULTADO: " + resultado);
					
					Header[] s = response.getAllHeaders();
					response.getParams();

					System.out.println("THe header from the httpclient:");
					for(int i=0; i < s.length; i++){
					    Header hd = s[i];       
					    System.out.println("Header Name: "+hd.getName()
					        + "       " + " Header Value: " + hd.getValue());
					}

					CookieStore cookieStore = client.getCookieStore();
					List <Cookie> cookies =  cookieStore.getCookies();
					for (Cookie cookie: cookies) {
						System.out.println("Cookies");
					  //  if (cookie.getDomain().equals("192.168.0.12") && cookie.getName().equals("csrftoken")) {
					    	System.out.println(cookie.getDomain());
					    	System.out.println(cookie.getName());
					    	String CSRFTOKEN = cookie.getValue();
					    	System.out.println(CSRFTOKEN);
					//    }
					}
					*/
					
					json = new JSONObject(response);
					
		
					String idMainSurvey = json.getString("id");
					idMainWeb = Long.valueOf(idMainSurvey);
					for (int i=0 ; i < listQuestions.size() ; i++){
						questionsObj.put("title", listQuestions.get(i).getTitle());
						questionsObj.put("required", listQuestions.get(i).getRequired());
						questionsObj.put("questionType", listQuestions.get(i).getQuestionType());
						questionsObj.put("main_survey_id",	idMainSurvey);
					
						holder2.put("question", questionsObj);
						StringEntity se2 = new StringEntity(holder2.toString(), HTTP.UTF_8);
						post2.setEntity(se2);
						post2.setHeader("Accept", "application/json");
						post2.setHeader("Content-Type", "application/json");
	
						
						ResponseHandler<String> responseHandler2 = new BasicResponseHandler();
						response2 = client2.execute(post2, responseHandler2);
						
						
						if(listQuestions.get(i).getQuestionType().equalsIgnoreCase("GROUPQUESTIONS") ||
								listQuestions.get(i).getQuestionType().equalsIgnoreCase("GROUPCADASTRE")){
							listQuestionsDependents = questionsDao.getListDependentQuestions(listQuestions.get(i).getId());
							
							for (int j=0 ; j < listQuestionsDependents.size() ; j++){
								DefaultHttpClient client3 = new DefaultHttpClient();
								HttpParams params3 = client3.getParams();
								HttpConnectionParams.setConnectionTimeout(params3, HTTP_TIMEOUT);
								HttpConnectionParams.setSoTimeout(params3, HTTP_TIMEOUT);
								ConnManagerParams.setTimeout(params3, HTTP_TIMEOUT);

								HttpPost post3 = new HttpPost(urls[0] + "/questions.json");
								JSONObject holder3 = new JSONObject();
								JSONObject questionCadastreObj = new JSONObject();
								String response3 = null;
								JSONObject json3 = new JSONObject();
								
								questionCadastreObj.put("title", listQuestionsDependents.get(j).getTitle());
								questionCadastreObj.put("required", listQuestionsDependents.get(j).getRequired());
								questionCadastreObj.put("questionType", listQuestionsDependents.get(j).getQuestionType());
								questionCadastreObj.put("main_survey_id", idMainSurvey);
							
								holder3.put("question", questionCadastreObj);
								StringEntity se3 = new StringEntity(holder3.toString(), HTTP.UTF_8);
								post3.setEntity(se3);
								post3.setHeader("Accept", "application/json");
								post3.setHeader("Content-Type", "application/json");
			
								
								ResponseHandler<String> responseHandler3 = new BasicResponseHandler();
								response3 = client3.execute(post3, responseHandler3);
								
			
							}
						}
					}
					
				} catch (HttpResponseException e) {
					e.printStackTrace();
					Log.e("ClientProtocol", "" + e);
					msg = e.getMessage();
				} catch (IOException e) {
					e.printStackTrace();
					Log.e("IO", "" + e);
					msg = e.getMessage();
				}
			} catch (JSONException e) {
				e.printStackTrace();
				Log.e("JSON", "" + e);
				msg = e.getMessage();
			}

			return msg;
		}

		@Override
		protected void onPostExecute(String msg) {
			if ( msg == null){//if (json.getBoolean("success")) {		
	
				try {				
						new SendReplies().execute(URL);
					
			/*			Intent intent = new Intent(getApplicationContext(),
								MyOpinionsActivity.class);
						startActivity(intent);
						finish();*/
					//}
				//	Toast.makeText(context, json.getString("info"),
					//		Toast.LENGTH_LONG).show();
				} catch (Exception e) {
					e.printStackTrace();
				} 
			} else {
				dismissDialog(progress_bar_type);

				Toast.makeText(RegisterActivity.this, "Ocorreu um problema ao enviar as respostas. Verifique sua conexão e tente novamente.", Toast.LENGTH_LONG ).show();
				Intent intent = new Intent (RegisterActivity.this, MyOpinionsActivity.class);
				startActivity(intent);
				finish();
			}
		}
	}
	
	
	private class SearchSurveys extends AsyncTask<String, Void, String>  {
		
		private int opc = 0;
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			showDialog(progress_bar_type);
		}
		
		@Override
		protected String doInBackground(String... urls) {
			DefaultHttpClient client = new DefaultHttpClient();
			HttpParams params = client.getParams();
			HttpConnectionParams.setConnectionTimeout(params, HTTP_TIMEOUT);
			HttpConnectionParams.setSoTimeout(params, HTTP_TIMEOUT);
			ConnManagerParams.setTimeout(params, HTTP_TIMEOUT);

			HttpGet get = new HttpGet(urls[0] + "/main_surveys.json");
			String response = null;

			try {
				get.setHeader("Accept", "application/json");
				get.setHeader("Content-Type", "application/json");

				ResponseHandler<String> responseHandler = new BasicResponseHandler();
				response = client.execute(get, responseHandler);
				
				//System.out.println("GET: " + response);
				/*HttpResponse response2= client.execute(get);
				
				Header[] s = response2.getAllHeaders();
				response2.getParams();

				System.out.println("THe header from the httpclient:");
				for(int i=0; i < s.length; i++){
				    Header hd = s[i];       
				    System.out.println("Header Name: "+hd.getName()
				        + "       " + " Header Value: " + hd.getValue());
				}
				
				CookieStore cookieStore = client.getCookieStore();
				List <Cookie> cookies =  cookieStore.getCookies();
				for (Cookie cookie: cookies) {
					System.out.println("Cookies");
				  //  if (cookie.getDomain().equals("192.168.0.12") && cookie.getName().equals("csrftoken")) {
				    	System.out.println(cookie.getDomain());
				    	System.out.println(cookie.getName());
				    	String CSRFTOKEN = cookie.getValue();
				    	System.out.println(CSRFTOKEN);
				//    }
				}*/
				
				JsonElement json = new JsonParser().parse(response);

				JsonArray array = json.getAsJsonArray();

				Iterator iterator = array.iterator();
				while (iterator.hasNext()){
					JsonElement json1 = (JsonElement) iterator.next();
					Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss").create();
					MainSurveys mSurvey = gson.fromJson(json1, MainSurveys.class);
					listMainSurveyWeb.add(mSurvey);
				}
				
				
			} catch (HttpResponseException e) {
				e.printStackTrace();
				Log.e("ClientProtocol", "" + e);
				msg = e.getMessage();
			} catch (IOException e) {
				e.printStackTrace();
				Log.e("IO", "" + e);
				msg = e.getMessage();
			} catch (Exception e) {
				e.printStackTrace();
				Log.e("Exception", "" + e);
				msg = e.getMessage();
			}


			return msg;
		}

		@Override
		protected void onPostExecute(String msg) {
			if (msg == null){
				try {
					
					//System.out.println(mainSurvey.getCreated_at());
					final long hoursInMillis = 60L * 60L * 1000L;
				//	Date newDate = new Date(mainSurvey.getCreated_at().getTime() + 
					//                        (1L * hoursInMillis)); // Add 1 hour
					
					Date newDate = new Date(mainSurvey.getCreated_at().getTime()); 

					    Iterator<MainSurveys> it = listMainSurveyWeb.iterator();
					 //   System.out.println(newDate);
						while (it.hasNext()) {
							MainSurveys mainSurveyIt = (MainSurveys) it.next();
							//System.out.println(mainSurveyIt.getCreated_at());
							if (newDate.compareTo(mainSurveyIt.getCreated_at()) == 0){
								//System.out.println("iguais");
								opc = 1;
								idMainWeb = mainSurveyIt.getId();
								break;
							}
						}
					
						switch (opc){
							case 0 : { //O questionario nao existe no Servidor
								new	RegisterSurvey().execute(URL);
								break;
							}
							case 1 : { // O questionario existe no Servidor
								new SendReplies().execute(URL);
								break;
							}
						}
				
				}  catch (NullPointerException e) {
					e.printStackTrace();
				} catch (Exception e) {
					e.printStackTrace();
				} 
			} else {
				dismissDialog(progress_bar_type);

				Toast.makeText(RegisterActivity.this, "Ocorreu um problema ao enviar as respostas. Verifique sua conexão e tente novamente.", Toast.LENGTH_LONG ).show();
				Intent intent = new Intent (RegisterActivity.this, MyOpinionsActivity.class);
				startActivity(intent);
				finish();
			}
		}
	}
	
	private class SendReplies extends AsyncTask<String, Void, String>{
		
		@Override
		protected String doInBackground(String... urls) {
			
			DefaultHttpClient client = new DefaultHttpClient();
			HttpParams params = client.getParams();
			HttpConnectionParams.setConnectionTimeout(params, HTTP_TIMEOUT);
			HttpConnectionParams.setSoTimeout(params, HTTP_TIMEOUT);
			ConnManagerParams.setTimeout(params, HTTP_TIMEOUT);

			HttpGet get = new HttpGet(urls[0] + "/questions.json");
			String response = null;
		
			try {
					get.setHeader("Accept", "application/json");
					get.setHeader("Content-Type", "application/json");

					ResponseHandler<String> responseHandler = new BasicResponseHandler();
					response = client.execute(get, responseHandler);
					JsonElement json = new JsonParser().parse(response);

					
					
					JsonArray array = json.getAsJsonArray();

					Iterator iterator = array.iterator();
					while (iterator.hasNext()){
						JsonElement json1 = (JsonElement) iterator.next();
						Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss").create();
						Questions question = gson.fromJson(json1, Questions.class);
						if(question.getMain_survey_id() == idMainWeb ){
							questions.add(question);
							//System.out.println(question.getTitle());
						}
					}
					
				    listSurveyReplies = surveyReplyDao.getListSyncSurveyReplies(mainSurvey.getId(), 0);
					
					DefaultHttpClient client2 = new DefaultHttpClient();
					HttpParams params2 = client2.getParams();
					HttpConnectionParams.setConnectionTimeout(params2, HTTP_TIMEOUT);
					HttpConnectionParams.setSoTimeout(params2, HTTP_TIMEOUT);
					ConnManagerParams.setTimeout(params2, HTTP_TIMEOUT);

					HttpPost post2 = new HttpPost(urls[0] + "/survey_replies.json");
					JSONObject holder2 = new JSONObject();
					JSONObject surveyRepliesObj = new JSONObject();
					String response2 = null;
					JSONObject json2 = new JSONObject();
					
					DefaultHttpClient client3 = new DefaultHttpClient();
					HttpParams params3 = client3.getParams();
					HttpConnectionParams.setConnectionTimeout(params3, HTTP_TIMEOUT);
					HttpConnectionParams.setSoTimeout(params3, HTTP_TIMEOUT);
					ConnManagerParams.setTimeout(params3, HTTP_TIMEOUT);

					HttpPost post3 = new HttpPost(urls[0] + "/obtained_replies.json");
					JSONObject holder3 = new JSONObject();
					JSONObject obtainedRepliesObj = new JSONObject();
					String response3 = null;
					JSONObject json3 = new JSONObject();
					
					for (int i=0 ; i < listSurveyReplies.size() ; i++){
						
						surveyRepliesObj.put("created_at", listSurveyReplies.get(i).getCreated_at());
						surveyRepliesObj.put("main_survey_id", idMainWeb);
						
						holder2.put("survey_reply", surveyRepliesObj);
						StringEntity se2 = new StringEntity(holder2.toString(), HTTP.UTF_8);
						post2.setEntity(se2);
						post2.setHeader("Accept", "application/json");
						post2.setHeader("Content-Type", "application/json");
	
						ResponseHandler<String> responseHandler2 = new BasicResponseHandler();
						response2 = client2.execute(post2, responseHandler);				
						json2 = new JSONObject(response2);
						
						String idSurveyReply = json2.getString("id");
						
						for (int p=0 ; p <listQuestions.size() ; p++){
							//System.out.println("GroupQuestion: " + listQuestions.get(p).getTitle());
							listQuestionsDependents = questionsDao.getListDependentQuestions(listQuestions.get(p).getId());
							
							Cursor cursor = cadastreRepliesDao
									.consultCadastreRepliesByCadastreQuestionAndSurveyReply(
											listQuestions.get(p).getId(), listSurveyReplies.get(i).getId());
					    //	System.out.println("Quant Resp: " + cursor.getCount());	   
						
							Set<Long> questionsOK = new LinkedHashSet<Long>();
					    	if (cursor.getCount() == 0) {
								for (int j=0 ; j < questions.size() ; j++){
									 Iterator<Questions> it = listQuestionsDependents.iterator();	
									 long idQuestion = 0;
									 while (it.hasNext()) {
										    Questions q = new Questions();
											q = (Questions) it.next();
											if (q.getTitle().equalsIgnoreCase(questions.get(j).getTitle())){
												if (!questionsOK.contains(q.getId())){
													//System.out.println("Q: " + q.getTitle());
													idQuestion = q.getId();
													questionsOK.add(q.getId());
													break;
												}
											}
										}		
								//mecher aqui
							    List<ObtainedReplies> listObtainedReplies = obtainedRepliesDao.getListCompleteObtainedReplies(idQuestion, listSurveyReplies.get(i).getId());
		    
							    for (int k=0 ; k < listObtainedReplies.size() ; k++){
							    //	System.out.println("Reply: " + listObtainedReplies.get(k).getObtReply());
									obtainedRepliesObj.put("obtReplies", listObtainedReplies.get(k).getObtReply());
									obtainedRepliesObj.put("survey_reply_id", idSurveyReply);
									obtainedRepliesObj.put("question_id", questions.get(j).getId());
									
									//System.out.println(listObtainedReplies.get(k).getObtReply());
									
									holder3.put("obtained_reply", obtainedRepliesObj);
									StringEntity se3 = new StringEntity(holder3.toString(), HTTP.UTF_8);
									post3.setEntity(se3);
									post3.setHeader("Accept", "application/json");
									post3.setHeader("Content-Type", "application/json");
				
									ResponseHandler<String> responseHandler3 = new BasicResponseHandler();
									response3 = client3.execute(post3, responseHandler3);
																	
									if (questions.get(j).getQuestionType().equalsIgnoreCase("PHOTO")){
										if (listObtainedReplies.get(k).getObtReply()!=null && 
												!listObtainedReplies.get(k).getObtReply().equalsIgnoreCase("")){
		
											JSONObject obtReply_resposta = new JSONObject(response3);
		
											// recebi o id do obtReply criado
											String idObtReply = obtReply_resposta.getString("id");
											File imageObtReplyInFile;
										
											List<String> listPhotoPaths = new ArrayList<String>();
											String obtPaths = listObtainedReplies.get(k).getObtReply();
											String [] paths = obtPaths.split(";");
											for (String pth : paths){
												listPhotoPaths.add(pth);
											}
	
											for (String path : listPhotoPaths){
												Uri fileUri = Uri.fromFile(new File(path));
												Bitmap fotoBitmap = ajustaFoto(fileUri, path);
												
												imageObtReplyInFile = new File(getApplicationContext().getCacheDir(), "photoobtained.jpg");
												imageObtReplyInFile.createNewFile();
												Bitmap bitmap = fotoBitmap;
												ByteArrayOutputStream bos = new ByteArrayOutputStream();
												bitmap.compress(CompressFormat.PNG, 0 /*ignored for PNG*/, bos);
												byte[] bitmapdata = bos.toByteArray();
												FileOutputStream fos = new FileOutputStream(imageObtReplyInFile);
												fos.write(bitmapdata);
		
												postImage(urls[0] + "/obtained_replies/" + idObtReply + "/add_image.json",
														imageObtReplyInFile);
											}	
										}
									}
							   }
							
							}
						}
							while (!cursor.isAfterLast()){
								questionsOK = new LinkedHashSet<Long>();
								for (int j=0 ; j < questions.size() ; j++){
									 Iterator<Questions> it = listQuestionsDependents.iterator();	
									 long idQuestion = 0;
									 while (it.hasNext()) {
										    Questions q = new Questions();
											q = (Questions) it.next();
											if (q.getTitle().equalsIgnoreCase(questions.get(j).getTitle())){
												if (!questionsOK.contains(q.getId())){
													//System.out.println("Q: " + q.getTitle());
													idQuestion = q.getId();
													questionsOK.add(q.getId());
													
											    	List<ObtainedCadastreReplies> getObtainedReplies = obtainedCadastreRepliesDao
																.getListObtainedCadastreReplies(idQuestion,
																		cursor.getLong(0));
											    	for (ObtainedCadastreReplies obt : getObtainedReplies){
											    		//System.out.println("Resp: " + obt.getObtReply());
											    		obtainedRepliesObj.put("obtReplies", obt.getObtReply());
														obtainedRepliesObj.put("survey_reply_id", idSurveyReply);
														obtainedRepliesObj.put("question_id", questions.get(j).getId());
																							
														holder3.put("obtained_reply", obtainedRepliesObj);
														StringEntity se3 = new StringEntity(holder3.toString(), HTTP.UTF_8);
														post3.setEntity(se3);
														post3.setHeader("Accept", "application/json");
														post3.setHeader("Content-Type", "application/json");
									
														ResponseHandler<String> responseHandler3 = new BasicResponseHandler();
														response3 = client3.execute(post3, responseHandler3);
											    	}
													break;
												}
											}
										}
								}
							  	cursor.moveToNext();	
							}

					}
					listSurveyReplies.get(i).setSync(true);
					surveyReplyDao.updateSurveyReply(listSurveyReplies.get(i));
					pDialog.incrementProgressBy(1);
				}
					
				} catch (HttpResponseException e) {
					e.printStackTrace();
					Log.e("ClientProtocol", "" + e);
					msg = e.getMessage();
				} catch (IOException e) {
					e.printStackTrace();
					Log.e("IO", "" + e);
					msg = e.getMessage();
				} catch (JSONException e) {
					e.printStackTrace();
					Log.e("JSON", "" + e);
					msg = e.getMessage();
				}
			return msg;
			

		}

		@Override
		protected void onPostExecute(String msg) {
			if (msg == null){
				try {
					//if (json.getBoolean("success")) {		
					
					Cursor cursor = mainSurveyDao.consultMainSurvey(idMain);

					if (cursor.getString(4).equalsIgnoreCase("ON_LINE")){
						mainSurveyDao.deleteMainSurvey(idMain);
						
						Intent intent = new Intent(getApplicationContext(),
								MyOpinionsActivity.class);
						intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						startActivity(intent);
						finish();
						dismissDialog(progress_bar_type);
						Toast.makeText(RegisterActivity.this, "As respostas foram sincronizadas com sucesso!",
								Toast.LENGTH_LONG).show();
					} else {
						
						dismissDialog(progress_bar_type);
						deleteDialog();
					
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				
			} else {
				dismissDialog(progress_bar_type);

				Toast.makeText(RegisterActivity.this, "Ocorreu um problema ao sincronizar as respostas. Verifique sua conexão e tente novamente.", Toast.LENGTH_LONG ).show();
				Intent intent = new Intent (RegisterActivity.this, MyOpinionsActivity.class);
				startActivity(intent);
				finish();
			}
		}
	}
	
	private Bitmap ajustaFoto(Uri fileUri, String path) {
		float angle = 0;
		getApplicationContext().getContentResolver().notifyChange(fileUri, null);
		ContentResolver cr = getApplicationContext().getContentResolver();

		Bitmap bitmap = null;
		int w = 0;
		int h = 0;
		Matrix mtx = new Matrix();
		
		try {

			bitmap = android.provider.MediaStore.Images.Media.getBitmap(cr,
					fileUri);
			// captura as dimens�es da imagem

			w = bitmap.getWidth();
			h = bitmap.getHeight();

			// pega o caminho onda a imagem est� salva
			ExifInterface exif = new ExifInterface(path);
			// pega a orienta��o real da imagem

			int orientation = exif.getAttributeInt(
					ExifInterface.TAG_ORIENTATION,
					ExifInterface.ORIENTATION_NORMAL);
			// gira a imagem de acordo com a orienta��o

			angle = 0;
			switch (orientation) {

			case 3: // ORIENTATION_ROTATE_180
				angle = 180; break;
			case 6: // ORIENTATION_ROTATE_90
				angle = 90; break;
			case 8: // ORIENTATION_ROTATE_270
				angle = 270; break;
			default: // ORIENTATION_ROTATE_0
				angle = 0; break;
			}

		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		FileOutputStream out;
		Bitmap rotatedBitmap = null;
		try {
			out = new FileOutputStream(path);
			// define um indice = 1 pois se der erro vai manter a imagem como
			// est�.
			// Integer idx = 1;
			// reupera as dimens�es da imagem
			w = bitmap.getWidth();
			h = bitmap.getHeight();
			// verifica qual a maior dimens�o e divide pela lateral final para
			// definir qual o indice de redu��o

			if (w>h) {
				if (w > 800) {
					w = 800;
				}

				if (h > 600) {
					h = 600;
				}

			} else {
				if (h > 800) {
					h = 800;
				}

				if (w > 600) {
					w = 600;
				}				
			}

			// scale it to fit the screen, x and y swapped because my image is wider than it is tall
			Bitmap scaledBitmap = Bitmap.createScaledBitmap(bitmap, w, h, true);
			 		
			// create a matrix object
			Matrix matrix = new Matrix();
			matrix.postRotate(angle); // rotate by angle
			 
			// create a new bitmap from the original using the matrix to transform the result
			rotatedBitmap = Bitmap.createBitmap(scaledBitmap , 0, 0, scaledBitmap .getWidth(), scaledBitmap .getHeight(), matrix, true);
			 
			// salva a imagem reduzida no disco
			rotatedBitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// uma nova instancia do bitmap rotacionado
		return rotatedBitmap;
	}
	
	public static void postImage(String url, File foto) throws FileNotFoundException{

		RequestParams params = new RequestParams();
		params.put("image[image]", foto);
		AsyncHttpClient client = new AsyncHttpClient();	
		client.post(url, params, new JsonHttpResponseHandler() {
			@Override
			public void onSuccess(JSONObject response) {
				Integer codigo;
                try {
					codigo = response.getInt("codigo");
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
			}
			
			
		}); 

	}
	
	public void deleteDialog(){
		final String COLOR = "#A8Cf45";

		final QustomDialogBuilder qustomDialogBuilder = (QustomDialogBuilder) new QustomDialogBuilder(RegisterActivity.this).
				setTitleColor(COLOR).
				setDividerColor(COLOR);
		// set title
		qustomDialogBuilder.setTitle("Respostas Sincronizadas");

		// set dialog message
		qustomDialogBuilder
			.setMessage("As respostas foram sincronizadas com sucesso. Deseja apagar as respostas sincronizadas do dispositivo?")
			.setCancelable(true)
			.setPositiveButton("Sim",new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {
				
				deleteReplies();
				
				Intent intent = new Intent(RegisterActivity.this,
						ListSurveys.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(intent);
				finish();
				
				}
			  })
			.setNegativeButton("Não",new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {
					
				Intent intent = new Intent(RegisterActivity.this,
						ListSurveys.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(intent);
				finish();
				
				}
			});

			// create alert dialog
			AlertDialog alertDialog = qustomDialogBuilder.create();

			// show it
			alertDialog.show();
	}
	
	private void deleteReplies() {
		
		//Cursor cursor = mainSurveyDao.consultMainSurvey(idMain);
	    //List listSurveyReply = surveyReplyDao.getListSyncSurveyReplies(mainSurvey.getId(), 1);
	    Iterator<SurveyReplies> it = listSurveyReplies.iterator();
		while (it.hasNext()) {
		    SurveyReplies surveyReply = new SurveyReplies();
			surveyReply = (SurveyReplies) it.next();	
			surveyReplyDao.deleteSurveyReply(surveyReply.getId());
		}		
	}
}