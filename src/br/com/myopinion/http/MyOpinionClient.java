package br.com.myopinion.http;

import java.io.UnsupportedEncodingException;

import org.apache.http.entity.StringEntity;

import android.content.Context;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

public class MyOpinionClient {
	private static final String BASE_URL = "http://187.63.48.148:5002/";

    private static AsyncHttpClient client = new AsyncHttpClient();

    public static void get(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        client.get(getAbsoluteUrl(url), params, responseHandler);
    }
    
    public static void get(Context context, String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        client.get(context, getAbsoluteUrl(url), params, responseHandler);
    }

    public static void post(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        client.post(getAbsoluteUrl(url), params, responseHandler);
    }

    public static void post(Context context, String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        client.post(context, getAbsoluteUrl(url), params, responseHandler);
    }
//    public static void login(RequestParams params, AsyncHttpResponseHandler responseHandler) {
//        post("login.php", params, responseHandler);
//    }
//
//    public static void dadosProfessor(String chave, RequestParams params, AsyncHttpResponseHandler responseHandler) {
//        client.addHeader("chave", chave);
//        post("dados_professor.php", params, responseHandler);
//    }
//
//    public static void enviarFrequencia(Context context, String chave, String json, AsyncHttpResponseHandler responseHandler) {
//        client.addHeader("chave", chave);
//
//        try {
//            client.post(context, getAbsoluteUrl("sincronizar.php"), new StringEntity(json), "application/json", responseHandler);
//        } catch (UnsupportedEncodingException e) {
//            e.printStackTrace();
//        }
//    }

    private static String getAbsoluteUrl(String relativeUrl) {
        return BASE_URL + relativeUrl;
    }
}
