package br.com.myopinion.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.com.myopinion.model.ObtainedReplies;
import br.com.myopinion.model.Questions;
import br.com.myopinion.model.Sections;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.SQLException;

public class SectionsDAO extends BasicDAO {

	public SectionsDAO(Context ctx) {
		super(ctx);
	}

	public static final String TABELA_SECTIONS = "SECTIONS";


    private long main_survey_id;
    private String title;
    private Boolean required;
    private Boolean sectionType;
    
	public static final String COLUNA_ID = "_id";
	public static final String COLUNA_MAIN_SURVEY_ID = "main_survey_id";
	public static final String COLUNA_TITLE = "title";
	public static final String COLUNA_REQUIRED = "required";
	public static final String COLUNA_SECTION_TYPE = "section_type";
	
	public static final int REQUIRED_FALSE = 0;
	public static final int REQUIRED_TRUE = 1;

	public static final String SECTIONS_CREATE_TABLE = "CREATE TABLE "
			+ TABELA_SECTIONS + "  (" + COLUNA_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
			+ COLUNA_TITLE	+ " TEXT NOT NULL,"
			+ COLUNA_REQUIRED + " INTEGER NOT NULL,"
			+ COLUNA_MAIN_SURVEY_ID + " INTEGER,"
			+ " FOREIGN KEY ( " + COLUNA_MAIN_SURVEY_ID + " )  REFERENCES "
			+ MainSurveysDAO.TABELA_MAIN_SURVEYS + " (" + MainSurveysDAO.COLUNA_ID
			+ " ) ON DELETE CASCADE ON UPDATE CASCADE);";

	public long createSection(Sections section) {

		ContentValues values = ofSectionForContentValues(section);
		return mDb.insert(TABELA_SECTIONS, null, values);
	}
	
	public long createQuestionDependent(Sections question) {

		ContentValues values = ofQuestionDependentForContentValues(question);
		return mDb.insert(TABELA_SECTIONS, null, values);
	}

	public static ContentValues ofSectionForContentValues(Sections section) {
		ContentValues values = new ContentValues();

		values.put(COLUNA_TITLE, section.getTitle());
		values.put(COLUNA_REQUIRED, section.getRequired() ? REQUIRED_TRUE : REQUIRED_FALSE);
		values.put(COLUNA_SECTION_TYPE, section.getSectionType());
		values.put(COLUNA_MAIN_SURVEY_ID, section.getMain_survey_id());

		return values;
	}

	public static ContentValues ofQuestionDependentForContentValues(Sections section) {
		ContentValues values = new ContentValues();

		values.put(COLUNA_TITLE, section.getTitle());
		values.put(COLUNA_REQUIRED, section.getRequired() ? REQUIRED_TRUE : REQUIRED_FALSE);
		values.put(COLUNA_SECTION_TYPE, section.getSectionType());
		values.put(COLUNA_MAIN_SURVEY_ID, section.getMain_survey_id());

		return values;
	}

	public Sections getSection(long idSection) {

		Cursor c = consultSection(idSection);
		Sections section = ofCursorFromSection(c);
		c.close();

		return section;
	}

	public static Sections ofCursorFromSection(Cursor c) {
		if (c == null || c.getCount() < 1) {
			return null;
		}
		Sections section = new Sections();
		
		section.setId(c.getLong(c.getColumnIndex(COLUNA_ID)));
		section.setTitle(c.getString(c.getColumnIndex(COLUNA_TITLE)));
		section.setRequired((c.getInt(c.getColumnIndex(COLUNA_REQUIRED)) == REQUIRED_TRUE));
		//question.setTest((c.getInt(c.getColumnIndex(COLUNA_TEST)) == TEST_FALSE));
		section.setSectionType(c.getString(c.getColumnIndex(COLUNA_SECTION_TYPE)));
		section.setMain_survey_id(c.getLong(c.getColumnIndex(COLUNA_MAIN_SURVEY_ID)));
		
		return section;
	}

	public boolean updateSection(Sections section) {

		ContentValues values = ofSectionForContentValues(section);
		return mDb.update(TABELA_SECTIONS, values, COLUNA_ID + "=?",
				new String[] { String.valueOf(section.getId()) }) > 0;
	}

	public boolean deleteSection(long idSection) {

		return mDb.delete(TABELA_SECTIONS, COLUNA_ID + "=?",
				new String[] { String.valueOf(idSection) }) > 0;
	}

	public String getTitle(long idQuestion) {

		Cursor c = consultSection(idQuestion);

		String title = c.getString(c.getColumnIndex(COLUNA_TITLE));
		c.close();

		return title;
	}
	
	public String getSectionType(long idSection) {

		Cursor c = consultSection(idSection);

		String questionType = c.getString(c.getColumnIndex(COLUNA_SECTION_TYPE));
		c.close();

		return questionType;
	}

	// Retorna TUDO que estiver na tabela 
	public Cursor consultAllSections() {

		return mDb
				.query(TABELA_SECTIONS, null, null, null, null, null, null);
	}

	public Cursor consultSection(long idQuestion) throws SQLException {

		Cursor mCursor =

		mDb.query(true, TABELA_SECTIONS, null, COLUNA_ID + "=?",
				new String[] { String.valueOf(idQuestion) }, null, null,
				null, null);
		if (mCursor != null) {
			mCursor.moveToFirst();
		}
		return mCursor;

	}

	public List<Sections> getListSections(long idMainSurvey) {

		Cursor c = consultSectionsByMainSurvey(idMainSurvey);
		List<Sections> listSections = new ArrayList<Sections>();
		while(!c.isAfterLast()) {
			Sections section = ofCursorFromSection(c);
			listSections.add(section);
		c.moveToNext();
		}
			
		c.close();

		return listSections;
	}
	
	public Cursor consultSectionsByMainSurvey(long idMainSurvey)
			throws SQLException {

		Cursor mCursor =

		mDb.query(true, TABELA_SECTIONS, null, COLUNA_MAIN_SURVEY_ID + "=?",
				new String[] { String.valueOf(idMainSurvey) }, null, null, null,
				null);
		if (mCursor != null) {
			mCursor.moveToFirst();
		}
		return mCursor;

	}
//	
//	public Cursor consultQuestionsByQuestionType(String questionType1, String questionType2, long idMainSurvey)
//			throws SQLException {
//
//		Cursor mCursor =
//
//		mDb.query(true, TABELA_SECTIONS, null, "(" + COLUNA_QUESTION_TYPE + "=? or " + COLUNA_QUESTION_TYPE + "=?)" + " and "
//													+ COLUNA_MAIN_SURVEY_ID + "=?",
//				new String[] { questionType1, questionType2, String.valueOf(idMainSurvey) }, null, null, null,
//				null);
//		if (mCursor != null) {
//			mCursor.moveToFirst();
//		}
//		return mCursor;
//
//	}

	public long getNumberOfSections() {
		return DatabaseUtils.queryNumEntries(mDb, TABELA_SECTIONS);
	}

}
