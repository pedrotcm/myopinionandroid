package br.com.myopinion.dao;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.SQLException;
import br.com.myopinion.model.ObtainedReplies;

public class ObtainedRepliesDAO extends BasicDAO {

	public ObtainedRepliesDAO(Context ctx) {
		super(ctx);
	}

	public static final String TABELA_OBTAINED_REPLIES = "OBTAINED_REPLIES";

	public static final String COLUNA_ID = "_id";
	public static final String COLUNA_OBTREPLY = "obtReply";
	public static final String COLUNA_OTHER_ANSWER = "other_answer";
	public static final String COLUNA_QUESTION_ID = "question_id";
	public static final String COLUNA_SURVEY_REPLY_ID = "survey_reply_id";
	
	public static final int OTHER_ANSWER_FALSE = 0;
	public static final int OTHER_ANSWER_TRUE = 1;

	public static final String OBTAINED_REPLIES_CREATE_TABLE = "CREATE TABLE "
			+ TABELA_OBTAINED_REPLIES + "  (" + COLUNA_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
			+ COLUNA_OBTREPLY + " TEXT,"
			+ COLUNA_OTHER_ANSWER + " INTEGER,"
			+ COLUNA_QUESTION_ID + " INTEGER NOT NULL,"
			+ COLUNA_SURVEY_REPLY_ID + " INTEGER NOT NULL,"
			+ " FOREIGN KEY ( " + COLUNA_QUESTION_ID + " )  REFERENCES "
			+ QuestionsDAO.TABELA_QUESTIONS + " (" + QuestionsDAO.COLUNA_ID
			+ " ) ON DELETE CASCADE ON UPDATE CASCADE);"
			+ " FOREIGN KEY ( " + COLUNA_SURVEY_REPLY_ID + " )  REFERENCES "
			+ SurveyRepliesDAO.TABELA_SURVEY_REPLIES + " (" + SurveyRepliesDAO.COLUNA_ID
			+ " ) ON DELETE CASCADE ON UPDATE CASCADE);";

	public long createObtReply(ObtainedReplies obtReply) {

		ContentValues values = ofObtReplyForContentValues(obtReply);
		return mDb.insert(TABELA_OBTAINED_REPLIES, null, values);
	}

	public static ContentValues ofObtReplyForContentValues(ObtainedReplies obtReply) {
		ContentValues values = new ContentValues();

		values.put(COLUNA_OBTREPLY, obtReply.getObtReply());
		values.put(COLUNA_OTHER_ANSWER, obtReply.isOther_answer()? OTHER_ANSWER_TRUE : OTHER_ANSWER_FALSE);
		values.put(COLUNA_QUESTION_ID, obtReply.getQuestion_id());
		values.put(COLUNA_SURVEY_REPLY_ID, obtReply.getSurvey_reply_id());
		return values;
	}

	public ObtainedReplies getObtReply(long idObtReply){

		Cursor c = consultObtReply(idObtReply);
		ObtainedReplies emp = ofCursorForObtReply(c);
		c.close();

		return emp;
	}

	public static ObtainedReplies ofCursorForObtReply(Cursor c) {
		if (c == null || c.getCount() < 1) {
			return null;
		}
		ObtainedReplies obtReply = new ObtainedReplies();
		obtReply.setId(c.getLong(c.getColumnIndex(COLUNA_ID)));

		obtReply.setObtReply(c.getString(c.getColumnIndex(COLUNA_OBTREPLY)));
		obtReply.setOther_answer((c.getInt(c.getColumnIndex(COLUNA_OTHER_ANSWER)) == OTHER_ANSWER_TRUE));
		obtReply.setQuestion_id(c.getLong(c.getColumnIndex(COLUNA_QUESTION_ID)));
		obtReply.setSurvey_reply_id(c.getLong(c.getColumnIndex(COLUNA_SURVEY_REPLY_ID)));

		return obtReply;
	}

	public List<ObtainedReplies> getListCompleteObtainedReplies(long idQuestion, long idSurveyReply) {

		Cursor c = consultCompleteObtReplyByQuestionAndSurveyReply(idQuestion, idSurveyReply);
		List<ObtainedReplies> listObtReplies = new ArrayList<ObtainedReplies>();
		while(!c.isAfterLast()) {
			ObtainedReplies obtReply = ofCursorForObtReply(c);
			listObtReplies.add(obtReply);
		c.moveToNext();
		}
			
		c.close();

		return listObtReplies;
	}
	
	public List<ObtainedReplies> getListObtainedReplies(int isOtherAnswer, long idQuestion, long idSurveyReply) {

		Cursor c = consultObtReplyByQuestionAndSurveyReply(isOtherAnswer, idQuestion, idSurveyReply);
		List<ObtainedReplies> listObtReplies = new ArrayList<ObtainedReplies>();
		while(!c.isAfterLast()) {
			ObtainedReplies obtReply = ofCursorForObtReply(c);
			listObtReplies.add(obtReply);
		c.moveToNext();
		}
			
		c.close();

		return listObtReplies;
	}
	
	public List<ObtainedReplies> getListObtainedRepliesForCadastreQuestion(long idQuestion) {

		Cursor c = consultObtReplyByQuestion(idQuestion);
		List<ObtainedReplies> listObtReplies = new ArrayList<ObtainedReplies>();
		while(!c.isAfterLast()) {
			ObtainedReplies obtReply = ofCursorForObtReply(c);
			listObtReplies.add(obtReply);
		c.moveToNext();
		}
			
		c.close();

		return listObtReplies;
	}
	
	public boolean updateObtReply(ObtainedReplies obtReply) {

		ContentValues values = ofObtReplyForContentValues(obtReply);
		return mDb.update(TABELA_OBTAINED_REPLIES, values, COLUNA_ID + "=?",
				new String[] { String.valueOf(obtReply.getId()) }) > 0;
	}

	public boolean deleteObtReply(long idObtReply) {

		return mDb.delete(TABELA_OBTAINED_REPLIES, COLUNA_ID + "=?",
				new String[] { String.valueOf(idObtReply) }) > 0;
	}

	public String getReply(long idEmprestimo) {

		Cursor c = consultObtReply(idEmprestimo);

		String reply = c.getString(c.getColumnIndex(COLUNA_OBTREPLY));
		c.close();

		return reply;
	}

	// Retorna TUDO que estiver na tabela
	public Cursor consultAllObtainedReplies() {

		return mDb
				.query(TABELA_OBTAINED_REPLIES, null, null, null, null, null, null);
	}

	public Cursor consultObtReply(long idObtReply) throws SQLException {

		Cursor mCursor =

		mDb.query(true, TABELA_OBTAINED_REPLIES, null, COLUNA_ID + "=?",
				new String[] { String.valueOf(idObtReply) }, null, null,
				null, null);
		if (mCursor != null) {
			mCursor.moveToFirst();
		}
		return mCursor;

	}

	public Cursor consultCompleteObtReplyByQuestionAndSurveyReply(long idQuestion, long idSurveyReply)
			throws SQLException {

		Cursor mCursor =

		mDb.query(true, TABELA_OBTAINED_REPLIES, null, COLUNA_QUESTION_ID + "=?" + " and "
														+ COLUNA_SURVEY_REPLY_ID + "=?",
				new String[] { String.valueOf(idQuestion), String.valueOf(idSurveyReply) }, null, null, null,
				null);
		if (mCursor != null) {
			mCursor.moveToFirst();
		}
		return mCursor;
	}
	
	public Cursor consultObtReplyByQuestionAndSurveyReply(int isOtherAnswer, long idQuestion, long idSurveyReply)
			throws SQLException {

		Cursor mCursor =

		mDb.query(true, TABELA_OBTAINED_REPLIES, null, COLUNA_QUESTION_ID + "=?" + " and "
														+ COLUNA_SURVEY_REPLY_ID + "=? and " + COLUNA_OTHER_ANSWER + "=?",
				new String[] { String.valueOf(idQuestion), String.valueOf(idSurveyReply), String.valueOf(isOtherAnswer) }, null, null, null,
				null);
		if (mCursor != null) {
			mCursor.moveToFirst();
		}
		return mCursor;
	}
	
	public Cursor consultObtReplyByQuestion(long idQuestion)
			throws SQLException {

		Cursor mCursor =

		mDb.query(true, TABELA_OBTAINED_REPLIES, null, COLUNA_QUESTION_ID + "=?" + " and "
														+ COLUNA_SURVEY_REPLY_ID + "=?" ,
				new String[] { String.valueOf(idQuestion)}, null, null, null,
				null);
		if (mCursor != null) {
			mCursor.moveToFirst();
		}
		return mCursor;

	}

	public long getNumberOfObtReply() {
		return DatabaseUtils.queryNumEntries(mDb, TABELA_OBTAINED_REPLIES);
	}

}
