package br.com.myopinion.dao;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.SQLException;
import br.com.myopinion.model.ObtainedCadastreReplies;

public class ObtainedCadastreRepliesDAO extends BasicDAO {

	public ObtainedCadastreRepliesDAO(Context ctx) {
		super(ctx);
	}

	public static final String TABELA_OBTAINED_CADASTRE_REPLIES = "OBTAINED_CADASTRE_REPLIES";

	public static final String COLUNA_ID = "_id";
	public static final String COLUNA_OBTREPLY = "obtReply";
	public static final String COLUNA_QUESTION_ID = "question_id";
	public static final String COLUNA_CADASTRE_REPLY_ID = "cadastre_reply_id";

	public static final String OBTAINED_CADASTRE_REPLIES_CREATE_TABLE = "CREATE TABLE "
			+ TABELA_OBTAINED_CADASTRE_REPLIES + "  (" + COLUNA_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
			+ COLUNA_OBTREPLY + " TEXT,"
			+ COLUNA_QUESTION_ID + " INTEGER NOT NULL,"
			+ COLUNA_CADASTRE_REPLY_ID + " INTEGER NOT NULL,"
			+ " FOREIGN KEY ( " + COLUNA_QUESTION_ID + " )  REFERENCES "
			+ QuestionsDAO.TABELA_QUESTIONS + " (" + QuestionsDAO.COLUNA_ID
			+ " ) ON DELETE CASCADE ON UPDATE CASCADE);"
			+ " FOREIGN KEY ( " + COLUNA_CADASTRE_REPLY_ID + " )  REFERENCES "
			+ CadastreRepliesDAO.TABELA_CADASTRE_REPLIES + " (" + CadastreRepliesDAO.COLUNA_ID
			+ " ) ON DELETE CASCADE ON UPDATE CASCADE);";

	public long createObtCadastreReply(ObtainedCadastreReplies obtCadastreReply) {

		ContentValues values = ofObtCadastreReplyForContentValues(obtCadastreReply);
		return mDb.insert(TABELA_OBTAINED_CADASTRE_REPLIES, null, values);
	}

	public static ContentValues ofObtCadastreReplyForContentValues(ObtainedCadastreReplies obtCadastreReply) {
		ContentValues values = new ContentValues();

		values.put(COLUNA_OBTREPLY, obtCadastreReply.getObtReply());
		values.put(COLUNA_QUESTION_ID, obtCadastreReply.getQuestion_id());
		values.put(COLUNA_CADASTRE_REPLY_ID, obtCadastreReply.getCadastre_reply_id());
		return values;
	}

	public ObtainedCadastreReplies getObtCadastreReply(long idObtReply){

		Cursor c = consultObtCadastreReply(idObtReply);
		ObtainedCadastreReplies emp = ofCursorForObtCadastreReply(c);
		c.close();

		return emp;
	}

	public static ObtainedCadastreReplies ofCursorForObtCadastreReply(Cursor c) {
		if (c == null || c.getCount() < 1) {
			return null;
		}
		ObtainedCadastreReplies obtReply = new ObtainedCadastreReplies();
		obtReply.setId(c.getLong(c.getColumnIndex(COLUNA_ID)));

		obtReply.setObtReply(c.getString(c.getColumnIndex(COLUNA_OBTREPLY)));

		obtReply.setQuestion_id(c.getLong(c.getColumnIndex(COLUNA_QUESTION_ID)));
		obtReply.setCadastre_reply_id(c.getLong(c.getColumnIndex(COLUNA_CADASTRE_REPLY_ID)));

		return obtReply;
	}

	public List<ObtainedCadastreReplies> getListObtainedCadastreReplies(long idQuestion, long idCadastreReply) {

		Cursor c = consultarObtCadastreReplyByQuestionAndCadastreReply(idQuestion, idCadastreReply);
		List<ObtainedCadastreReplies> listObtCadastreReplies = new ArrayList<ObtainedCadastreReplies>();
		while(!c.isAfterLast()) {
			ObtainedCadastreReplies obtReply = ofCursorForObtCadastreReply(c);
			listObtCadastreReplies.add(obtReply);
		c.moveToNext();
		}
			
		c.close();

		return listObtCadastreReplies;
	}
	
	public List<ObtainedCadastreReplies> getListObtainedCadastreRepliesForCadastreQuestion(long idQuestion) {

		Cursor c = consultObtCadastreReplyByQuestion(idQuestion);
		List<ObtainedCadastreReplies> listObtCadastreReplies = new ArrayList<ObtainedCadastreReplies>();
		while(!c.isAfterLast()) {
			ObtainedCadastreReplies obtReply = ofCursorForObtCadastreReply(c);
			listObtCadastreReplies.add(obtReply);
		c.moveToNext();
		}
			
		c.close();

		return listObtCadastreReplies;
	}
	
	public boolean updateObtCadastreReply(ObtainedCadastreReplies obtCadastreReply) {

		ContentValues values = ofObtCadastreReplyForContentValues(obtCadastreReply);
		return mDb.update(TABELA_OBTAINED_CADASTRE_REPLIES, values, COLUNA_ID + "=?",
				new String[] { String.valueOf(obtCadastreReply.getId()) }) > 0;
	}

	public boolean deleteObtCadastreReply(long idObtCadastreReply) {

		return mDb.delete(TABELA_OBTAINED_CADASTRE_REPLIES, COLUNA_ID + "=?",
				new String[] { String.valueOf(idObtCadastreReply) }) > 0;
	}

	public String getObtainedCadastreReply(long idObtCadastreReply) {

		Cursor c = consultObtCadastreReply(idObtCadastreReply);

		String reply = c.getString(c.getColumnIndex(COLUNA_OBTREPLY));
		c.close();

		return reply;
	}

	// Retorna TUDO que estiver na tabela
	public Cursor consultAllObtainedCadastreReplies() {

		return mDb
				.query(TABELA_OBTAINED_CADASTRE_REPLIES, null, null, null, null, null, null);
	}

	public Cursor consultObtCadastreReply(long idObtCadastreReply) throws SQLException {

		Cursor mCursor =

		mDb.query(true, TABELA_OBTAINED_CADASTRE_REPLIES, null, COLUNA_ID + "=?",
				new String[] { String.valueOf(idObtCadastreReply) }, null, null,
				null, null);
		if (mCursor != null) {
			mCursor.moveToFirst();
		}
		return mCursor;

	}

	public Cursor consultarObtCadastreReplyByQuestionAndCadastreReply(long idQuestion, long idCadastreReply)
			throws SQLException {

		Cursor mCursor =

		mDb.query(true, TABELA_OBTAINED_CADASTRE_REPLIES, null, COLUNA_QUESTION_ID + "=?" + " and "
														+ COLUNA_CADASTRE_REPLY_ID + "=?" ,
				new String[] { String.valueOf(idQuestion), String.valueOf(idCadastreReply) }, null, null, null,
				null);
		if (mCursor != null) {
			mCursor.moveToFirst();
		}
		return mCursor;

	}
	
	public Cursor consultObtCadastreReplyByQuestion(long idQuestion)
			throws SQLException {

		Cursor mCursor =

		mDb.query(true, TABELA_OBTAINED_CADASTRE_REPLIES, null, COLUNA_QUESTION_ID + "=?",
				new String[] { String.valueOf(idQuestion)}, null, null, null,
				null);
		if (mCursor != null) {
			mCursor.moveToFirst();
		}
		return mCursor;

	}

	public long getNumberOfCadastreObtReply() {
		return DatabaseUtils.queryNumEntries(mDb, TABELA_OBTAINED_CADASTRE_REPLIES);
	}

}
