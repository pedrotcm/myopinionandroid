package br.com.myopinion.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.SQLException;
import br.com.myopinion.model.AvailableReplies;
import br.com.myopinion.model.OthersReplies;

public class OthersRepliesDAO extends BasicDAO {

	public OthersRepliesDAO(Context ctx) {
		super(ctx);
	}

	public static final String TABELA_OTHERS_REPLIES = "OTHERS_REPLIES";
	public static final String COLUNA_OTHER_REPLY = "otherReply";
	public static final String COLUNA_OTHER_QUESTION_ID = "other_question_id";

	public static final String OTHERS_REPLIES_CREATE_TABLE = "CREATE TABLE "
			+ TABELA_OTHERS_REPLIES + "  ("
			+ COLUNA_OTHER_REPLY + " TEXT,"
			+ COLUNA_OTHER_QUESTION_ID + " INTEGER NOT NULL,"
			+ " FOREIGN KEY ( " + COLUNA_OTHER_QUESTION_ID + " )  REFERENCES "
			+ OtherQuestionDAO.TABELA_OTHER_QUESTION + " (" + OtherQuestionDAO.COLUNA_ID
			+ " ) ON DELETE CASCADE ON UPDATE CASCADE);";

	public long createOtherReply(OthersReplies otherReply) {

		ContentValues values = ofOtherReplyForContentValues(otherReply);
		return mDb.insert(TABELA_OTHERS_REPLIES, null, values);
	}

	public static ContentValues ofOtherReplyForContentValues(OthersReplies otherReply) {
		ContentValues values = new ContentValues();

		values.put(COLUNA_OTHER_QUESTION_ID, otherReply.getOther_question_id());
		values.put(COLUNA_OTHER_REPLY, otherReply.getOtherReply());

		return values;
	}

	/*public AvailableReplies getAvaReply(long idAvaReply) {

		Cursor c = consultAvaReply(idAvaReply);
		AvailableReplies avaReply = ofCursorFromAvaReply(c);
		c.close();

		return avaReply;
	}
*/
	public static OthersReplies ofCursorFromOtherReply(Cursor c) {
		if (c == null || c.getCount() < 1) {
			return null;
		}
		OthersReplies otherReply = new OthersReplies();

		otherReply.setOther_question_id(c.getLong(c.getColumnIndex(COLUNA_OTHER_QUESTION_ID)));
		otherReply.setOtherReply(c.getString(c.getColumnIndex(COLUNA_OTHER_REPLY)));

		return otherReply;
	}

	/*public boolean updateAvaReply(AvailableReplies avaReply) {

		ContentValues values = ofAvaReplyForContentValues(avaReply);
		return mDb.update(TABELA_AVAILABLE_REPLIES, values, COLUNA_ID + "=?",
				new String[] { String.valueOf(avaReply.getId()) }) > 0;
	}

	public boolean removeAvaReply(long idAvaReply) {

		return mDb.delete(TABELA_AVAILABLE_REPLIES, COLUNA_ID + "=?",
				new String[] { String.valueOf(idAvaReply) }) > 0;
	}

	public String getReply(long idAvaReply) {

		Cursor c = consultAvaReply(idAvaReply);

		String reply = c.getString(c.getColumnIndex(COLUNA_AVAREPLY));
		c.close();

		return reply;
	}*/
	

	public List<String> getListOthersReplies(long idOtherQuestion) {

		Cursor c = consultOthersRepliesByOtherQuestion(idOtherQuestion);
		List<String> listOthersReplies = new ArrayList<String>();
		while(!c.isAfterLast()) {
			listOthersReplies.add(c.getString(c.getColumnIndex(COLUNA_OTHER_REPLY)));
		c.moveToNext();
		}
			
		c.close();

		return listOthersReplies;
	}



	// Retorna TUDO que estiver na tabela Emprestimos, assim como o método V1,
	// mas é bem mais simples.
	public Cursor consultAllOthersReplies() {

		return mDb
				.query(TABELA_OTHERS_REPLIES, null, null, null, null, null, null);
	}

	/*public Cursor consultAvaReply(long idAvaReply) throws SQLException {

		Cursor mCursor =

		mDb.query(true, TABELA_AVAILABLE_REPLIES, null, COLUNA_ID + "=?",
				new String[] { String.valueOf(idAvaReply) }, null, null,
				null, null);
		if (mCursor != null) {
			mCursor.moveToFirst();
		}
		return mCursor;

	}*/
	
	public Cursor consultOthersRepliesByOtherQuestion(long idOtherQuestion) throws SQLException {

		Cursor mCursor =

		mDb.query(true, TABELA_OTHERS_REPLIES, null, COLUNA_OTHER_QUESTION_ID + "=?",
				new String[] { String.valueOf(idOtherQuestion) }, null, null, null,
				null);
		if (mCursor != null) {
			mCursor.moveToFirst();
		}
		return mCursor;

	}

	/*public Cursor consultAvaReplyByQuestion(long idCategoria)
			throws SQLException {

		Cursor mCursor =

		mDb.query(true, TABELA_AVAILABLE_REPLIES, null, COLUNA_QUESTION_ID + "=?",
				new String[] { String.valueOf(idCategoria) }, null, null, null,
				null);
		if (mCursor != null) {
			mCursor.moveToFirst();
		}
		return mCursor;

	}*/

	public long getNumberOfOthersReplies() {
		return DatabaseUtils.queryNumEntries(mDb, TABELA_OTHERS_REPLIES);
	}

}
