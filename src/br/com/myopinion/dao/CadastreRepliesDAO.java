package br.com.myopinion.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.SQLException;
import br.com.myopinion.model.CadastreReplies;
import br.com.myopinion.model.Questions;
import br.com.myopinion.model.SurveyReplies;

public class CadastreRepliesDAO extends BasicDAO {

	public CadastreRepliesDAO(Context ctx) {
		super(ctx);
	}

	public static final String TABELA_CADASTRE_REPLIES = "CADASTRE_REPLIES";

	public static final String COLUNA_ID = "_id";
	public static final String COLUNA_CADASTRE_QUESTION_ID = "cadastre_question_id";
	public static final String COLUNA_SURVEY_REPLY_ID = "survey_reply_id";


	public static final String CADASTRE_REPLIES_CREATE_TABLE = "CREATE TABLE "
			+ TABELA_CADASTRE_REPLIES + "  (" + COLUNA_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
			+ COLUNA_CADASTRE_QUESTION_ID + " INTEGER NOT NULL,"
			+ COLUNA_SURVEY_REPLY_ID + " INTEGER NOT NULL,"
			+ " FOREIGN KEY ( " + COLUNA_CADASTRE_QUESTION_ID + " )  REFERENCES "
			+ QuestionsDAO.TABELA_QUESTIONS + " (" + QuestionsDAO.COLUNA_ID
			+ " ) ON DELETE CASCADE ON UPDATE CASCADE);"
			+ " FOREIGN KEY ( " + COLUNA_SURVEY_REPLY_ID + " )  REFERENCES "
			+ SurveyRepliesDAO.TABELA_SURVEY_REPLIES + " (" + SurveyRepliesDAO.COLUNA_ID
			+ " ) ON DELETE CASCADE ON UPDATE CASCADE);";
;

	public long createCadastreReply(CadastreReplies cadastreReply) {

		ContentValues values = ofCadastreRepliesForContentValues(cadastreReply);
		return mDb.insert(TABELA_CADASTRE_REPLIES, null, values);
	}

	public static ContentValues ofCadastreRepliesForContentValues(CadastreReplies cadastreReply) {
		ContentValues values = new ContentValues();

		values.put(COLUNA_CADASTRE_QUESTION_ID, cadastreReply.getCadastre_question_id());
		values.put(COLUNA_SURVEY_REPLY_ID, cadastreReply.getSurvey_reply_id());
		
		return values;
	}

	public CadastreReplies getCadastreReply(long idCadastreReply) {

		Cursor c = consultCadastreReply(idCadastreReply);
		CadastreReplies cadastreReply = ofCursorForCadastreReply(c);
		c.close();

		return cadastreReply;
	}

	public static CadastreReplies ofCursorForCadastreReply(Cursor c) {
		if (c == null || c.getCount() < 1) {
			return null;
		}
		CadastreReplies emp = new CadastreReplies();
		emp.setId(c.getLong(c.getColumnIndex(COLUNA_ID)));
		emp.setCadastre_question_id(c.getLong(c.getColumnIndex(COLUNA_CADASTRE_QUESTION_ID)));
		
		return emp;
	}

	public boolean updateCadastreReply(CadastreReplies cadastreReply) {

		ContentValues values = ofCadastreRepliesForContentValues(cadastreReply);
		return mDb.update(TABELA_CADASTRE_REPLIES, values, COLUNA_ID + "=?",
				new String[] { String.valueOf(cadastreReply.getId()) }) > 0;
	}

	public boolean deleteCadastreReply(long idCadastreReply) {

		return mDb.delete(TABELA_CADASTRE_REPLIES, COLUNA_ID + "=?",
				new String[] { String.valueOf(idCadastreReply) }) > 0;
	}
	
	// Retorna TUDO que estiver na tabela Emprestimos, assim como o método V1,
	// mas é bem mais simples.
	public Cursor consultAllCadastreReplies() {

		return mDb
				.query(TABELA_CADASTRE_REPLIES, null, null, null, null, null, null);
	}

	public Cursor consultCadastreReply(long idCadastreReply) throws SQLException {

		Cursor mCursor =

		mDb.query(true, TABELA_CADASTRE_REPLIES, null, COLUNA_ID + "=?",
				new String[] { String.valueOf(idCadastreReply) }, null, null,
				null, null);
		if (mCursor != null) {
			mCursor.moveToFirst();
		}
		return mCursor;

	}

	public List<CadastreReplies> getListCadastreReplies(long idCadastreQuestion, long idSurveyReply) {

		Cursor c = consultCadastreRepliesByCadastreQuestionAndSurveyReply(idCadastreQuestion, idSurveyReply);
		List<CadastreReplies> listCadastreReplies = new ArrayList<CadastreReplies>();
		while(!c.isAfterLast()) {
			CadastreReplies cadastreReply = ofCursorForCadastreReply(c);
			listCadastreReplies.add(cadastreReply);
		c.moveToNext();
		}
			
		c.close();

		return listCadastreReplies;
	}
	
	public Cursor consultCadastreRepliesByCadastreQuestionAndSurveyReply(long idCadastreQuestion, long idSurveyReply)
			throws SQLException {

		Cursor mCursor =

		mDb.query(true, TABELA_CADASTRE_REPLIES, null, COLUNA_CADASTRE_QUESTION_ID + "=?" + " and "
				+ COLUNA_SURVEY_REPLY_ID + "=?",
				new String[] { String.valueOf(idCadastreQuestion),  String.valueOf(idSurveyReply) }, null, null, COLUNA_ID + " DESC",
				null);
		if (mCursor != null) {
			mCursor.moveToFirst();
		}
		return mCursor;

	}

	public long getNumberOfCadastreReplies() {
		return DatabaseUtils.queryNumEntries(mDb, TABELA_CADASTRE_REPLIES);
	}

}
