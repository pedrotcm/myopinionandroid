package br.com.myopinion.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.com.myopinion.model.ObtainedReplies;
import br.com.myopinion.model.Questions;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.SQLException;

public class QuestionsDAO extends BasicDAO {

	public QuestionsDAO(Context ctx) {
		super(ctx);
	}

	public static final String TABELA_QUESTIONS = "QUESTIONS";

	public static final String COLUNA_ID = "_id";
	public static final String COLUNA_MAIN_SURVEY_ID = "main_survey_id";
	public static final String COLUNA_TITLE = "title";
	public static final String COLUNA_REQUIRED = "required";
	public static final String COLUNA_QUESTION_TYPE = "question_type";
	public static final String COLUNA_TEXT_TYPE = "text_type";
	public static final String COLUNA_QUESTION_ID = "question_id";
	
	public static final int REQUIRED_FALSE = 0;
	public static final int REQUIRED_TRUE = 1;

	public static final String QUESTIONS_CREATE_TABLE = "CREATE TABLE "
			+ TABELA_QUESTIONS + "  (" + COLUNA_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
			+ COLUNA_TITLE	+ " TEXT NOT NULL,"
			+ COLUNA_REQUIRED + " INTEGER NOT NULL,"
			+ COLUNA_QUESTION_TYPE + " TEXT NOT NULL,"
			+ COLUNA_MAIN_SURVEY_ID + " INTEGER,"
			+ COLUNA_TEXT_TYPE + " TEXT,"
			+ COLUNA_QUESTION_ID + " INTEGER,"
			+ " FOREIGN KEY ( " + COLUNA_QUESTION_ID + " )  REFERENCES "
			+ QuestionsDAO.TABELA_QUESTIONS + " (" + QuestionsDAO.COLUNA_ID
			+ " ) ON DELETE CASCADE ON UPDATE CASCADE);"
			+ " FOREIGN KEY ( " + COLUNA_MAIN_SURVEY_ID + " )  REFERENCES "
			+ MainSurveysDAO.TABELA_MAIN_SURVEYS + " (" + MainSurveysDAO.COLUNA_ID
			+ " ) ON DELETE CASCADE ON UPDATE CASCADE);";

	public long createQuestion(Questions question) {

		ContentValues values = ofQuestionForContentValues(question);
		return mDb.insert(TABELA_QUESTIONS, null, values);
	}
	
	public long createQuestionDependent(Questions question) {

		ContentValues values = ofQuestionDependentForContentValues(question);
		return mDb.insert(TABELA_QUESTIONS, null, values);
	}

	public static ContentValues ofQuestionForContentValues(Questions question) {
		ContentValues values = new ContentValues();

		values.put(COLUNA_TITLE, question.getTitle());
		values.put(COLUNA_REQUIRED, question.getRequired() ? REQUIRED_TRUE : REQUIRED_FALSE);
		//values.put(COLUNA_TEST, question.getTest() ? TEST_TRUE : TEST_FALSE);
		values.put(COLUNA_QUESTION_TYPE, question.getQuestionType());
		values.put(COLUNA_MAIN_SURVEY_ID, question.getMain_survey_id());
		values.put(COLUNA_TEXT_TYPE, question.getTextType());
		values.put(COLUNA_QUESTION_ID, question.getQuestion_id());

		return values;
	}

	public static ContentValues ofQuestionDependentForContentValues(Questions question) {
		ContentValues values = new ContentValues();

		values.put(COLUNA_TITLE, question.getTitle());
		values.put(COLUNA_REQUIRED, question.getRequired() ? REQUIRED_TRUE : REQUIRED_FALSE);
		//values.put(COLUNA_TEST, question.getTest() ? TEST_TRUE : TEST_FALSE);
		values.put(COLUNA_QUESTION_TYPE, question.getQuestionType());
		values.put(COLUNA_TEXT_TYPE, question.getTextType());
		values.put(COLUNA_QUESTION_ID, question.getQuestion_id());

		return values;
	}

	public Questions getQuestion(long idQuestion) {

		Cursor c = consultQuestion(idQuestion);
		Questions question = ofCursorFromQuestion(c);
		c.close();

		return question;
	}

	public static Questions ofCursorFromQuestion(Cursor c) {
		if (c == null || c.getCount() < 1) {
			return null;
		}
		Questions question = new Questions();
		
		question.setId(c.getLong(c.getColumnIndex(COLUNA_ID)));
		question.setTitle(c.getString(c.getColumnIndex(COLUNA_TITLE)));
		question.setRequired((c.getInt(c.getColumnIndex(COLUNA_REQUIRED)) == REQUIRED_TRUE));
		//question.setTest((c.getInt(c.getColumnIndex(COLUNA_TEST)) == TEST_FALSE));
		question.setQuestionType(c.getString(c.getColumnIndex(COLUNA_QUESTION_TYPE)));
		question.setMain_survey_id(c.getLong(c.getColumnIndex(COLUNA_MAIN_SURVEY_ID)));
		question.setTextType(c.getString(c.getColumnIndex(COLUNA_TEXT_TYPE)));
		question.setQuestion_id(c.getLong(c.getColumnIndex(COLUNA_QUESTION_ID)));
		
		return question;
	}

	public boolean updateQuestion(Questions question) {

		ContentValues values = ofQuestionForContentValues(question);
		return mDb.update(TABELA_QUESTIONS, values, COLUNA_ID + "=?",
				new String[] { String.valueOf(question.getId()) }) > 0;
	}

	public boolean deleteQuestion(long idQuestion) {

		return mDb.delete(TABELA_QUESTIONS, COLUNA_ID + "=?",
				new String[] { String.valueOf(idQuestion) }) > 0;
	}

	public String getTitle(long idQuestion) {

		Cursor c = consultQuestion(idQuestion);

		String title = c.getString(c.getColumnIndex(COLUNA_TITLE));
		c.close();

		return title;
	}
	
	public String getQuestionType(long idQuestion) {

		Cursor c = consultQuestion(idQuestion);

		String questionType = c.getString(c.getColumnIndex(COLUNA_QUESTION_TYPE));
		c.close();

		return questionType;
	}

	// Retorna TUDO que estiver na tabela 
	public Cursor consultAllQuestions() {

		return mDb
				.query(TABELA_QUESTIONS, null, null, null, null, null, null);
	}

	public Cursor consultQuestion(long idQuestion) throws SQLException {

		Cursor mCursor =

		mDb.query(true, TABELA_QUESTIONS, null, COLUNA_ID + "=?",
				new String[] { String.valueOf(idQuestion) }, null, null,
				null, null);
		if (mCursor != null) {
			mCursor.moveToFirst();
		}
		return mCursor;

	}

	public List<Questions> getListQuestions(long idMainSurvey) {

		Cursor c = consultQuestionsByMainSurvey(idMainSurvey);
		List<Questions> listQuestions = new ArrayList<Questions>();
		while(!c.isAfterLast()) {
			Questions question = ofCursorFromQuestion(c);
			listQuestions.add(question);
		c.moveToNext();
		}
			
		c.close();

		return listQuestions;
	}
	
	
	public Cursor consultQuestionsByMainSurvey(long idMainSurvey)
			throws SQLException {

		Cursor mCursor =

		mDb.query(true, TABELA_QUESTIONS, null, COLUNA_MAIN_SURVEY_ID + "=?",
				new String[] { String.valueOf(idMainSurvey) }, null, null, null,
				null);
		if (mCursor != null) {
			mCursor.moveToFirst();
		}
		return mCursor;

	}
	
	public List<Questions> getListDependentQuestions(long idQuestion) {

		Cursor c = consultQuestionsByQuestionAndMainSurvey(idQuestion);
		List<Questions> listQuestions = new ArrayList<Questions>();
		while(!c.isAfterLast()) {
			Questions question = ofCursorFromQuestion(c);
			listQuestions.add(question);
		c.moveToNext();
		}
			
		c.close();

		return listQuestions;
	}
	
	public Cursor consultQuestionsByQuestionAndMainSurvey(long idQuestion)
			throws SQLException {

		Cursor mCursor =

		mDb.query(true, TABELA_QUESTIONS, null, COLUNA_QUESTION_ID + "=?" ,
				new String[] { String.valueOf(idQuestion) }, null, null, null,
				null);
		if (mCursor != null) {
			mCursor.moveToFirst();
		}
		return mCursor;

	}
	
	public Cursor consultQuestionsByQuestionType(String questionType1, String questionType2, long idMainSurvey)
			throws SQLException {

		Cursor mCursor =

		mDb.query(true, TABELA_QUESTIONS, null, "(" + COLUNA_QUESTION_TYPE + "=? or " + COLUNA_QUESTION_TYPE + "=?)" + " and "
													+ COLUNA_MAIN_SURVEY_ID + "=?",
				new String[] { questionType1, questionType2, String.valueOf(idMainSurvey) }, null, null, null,
				null);
		if (mCursor != null) {
			mCursor.moveToFirst();
		}
		return mCursor;
	}


	public Cursor consultCoordinatesQuestion(String questionType, long idQuestion)
			throws SQLException {

		Cursor mCursor =

		mDb.query(true, TABELA_QUESTIONS, null, COLUNA_QUESTION_TYPE + "=?" + " and "
													+ COLUNA_QUESTION_ID + "=?",
				new String[] { questionType, String.valueOf(idQuestion) }, null, null, null,
				null);
		if (mCursor != null) {
			mCursor.moveToFirst();
		}
		return mCursor;
	}

	public long getNumberOfQuestions() {
		return DatabaseUtils.queryNumEntries(mDb, TABELA_QUESTIONS);
	}

}
