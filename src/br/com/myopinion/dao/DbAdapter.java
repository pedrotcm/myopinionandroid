package br.com.myopinion.dao;


import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DbAdapter {

	private static final String TAG = "DbAdapter";
	private DatabaseHelper mDbHelper;
	private SQLiteDatabase mDb;

	private static final String DB_NAME = "MYOPINION";
	private static final int DATABASE_VERSION = 9;

	private final Context mCtx;

	private static class DatabaseHelper extends SQLiteOpenHelper {
		@Override
		public void onOpen(SQLiteDatabase db) {
			super.onOpen(db);
			if (!db.isReadOnly()) {
				db.execSQL("PRAGMA foreign_keys=ON;");
			}
		}

		DatabaseHelper(Context context) {
			super(context, DB_NAME, null, DATABASE_VERSION);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {

			db.execSQL(MainSurveysDAO.MAIN_SURVEY_CREATE_TABLE);
			db.execSQL(QuestionsDAO.QUESTIONS_CREATE_TABLE);
			db.execSQL(AvailableRepliesDAO.AVAILABLE_REPLIES_CREATE_TABLE);
			db.execSQL(SurveyRepliesDAO.SURVEY_REPLIES_CREATE_TABLE);
			db.execSQL(ObtainedRepliesDAO.OBTAINED_REPLIES_CREATE_TABLE);
			db.execSQL(CadastreRepliesDAO.CADASTRE_REPLIES_CREATE_TABLE);
			db.execSQL(ObtainedCadastreRepliesDAO.OBTAINED_CADASTRE_REPLIES_CREATE_TABLE);
			db.execSQL(OthersRepliesDAO.OTHERS_REPLIES_CREATE_TABLE);
			//db.execSQL(SectionsDAO.SECTIONS_CREATE_TABLE);
			db.execSQL(OtherQuestionDAO.OTHER_QUESTION_CREATE_TABLE);
			
			Log.w("DbAdapter", "DB criado com sucesso!");
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			Log.w(TAG, "Atualizando o banco de dados da versão " + oldVersion
					+ " para " + newVersion
					+ ", todos os dados serão perdidos!");
			db.execSQL("DROP TABLE IF EXISTS " + MainSurveysDAO.TABELA_MAIN_SURVEYS);
			db.execSQL("DROP TABLE IF EXISTS " + QuestionsDAO.TABELA_QUESTIONS);
			db.execSQL("DROP TABLE IF EXISTS " + AvailableRepliesDAO.TABELA_AVAILABLE_REPLIES);
			db.execSQL("DROP TABLE IF EXISTS " + SurveyRepliesDAO.TABELA_SURVEY_REPLIES);
			db.execSQL("DROP TABLE IF EXISTS " + ObtainedRepliesDAO.TABELA_OBTAINED_REPLIES);
			db.execSQL("DROP TABLE IF EXISTS " + CadastreRepliesDAO.TABELA_CADASTRE_REPLIES);
			db.execSQL("DROP TABLE IF EXISTS " + ObtainedCadastreRepliesDAO.TABELA_OBTAINED_CADASTRE_REPLIES);
			db.execSQL("DROP TABLE IF EXISTS " + OthersRepliesDAO.TABELA_OTHERS_REPLIES);
			//db.execSQL("DROP TABLE IF EXISTS " + SectionsDAO.TABELA_SECTIONS);
			db.execSQL("DROP TABLE IF EXISTS " + OtherQuestionDAO.TABELA_OTHER_QUESTION);
			
			onCreate(db);
		}
	}

	public DbAdapter(Context ctx) {
		this.mCtx = ctx;
	}

	public  SQLiteDatabase open() throws SQLException {
		mDbHelper = new DatabaseHelper(mCtx);
		mDb = mDbHelper.getWritableDatabase();
		return mDb;
	}

	public void close() {
		mDbHelper.close();
		mDb.close();
	}

}
