package br.com.myopinion.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.SQLException;
import br.com.myopinion.model.AvailableReplies;

public class AvailableRepliesDAO extends BasicDAO {

	public AvailableRepliesDAO(Context ctx) {
		super(ctx);
	}

	public static final String TABELA_AVAILABLE_REPLIES = "AVAILABLE_REPLIES";
	public static final String COLUNA_ID = "_id";
	public static final String COLUNA_QUESTION_ID = "question_id";
	public static final String COLUNA_AVAREPLY = "avaReply";

	public static final String AVAILABLE_REPLIES_CREATE_TABLE = "CREATE TABLE "
			+ TABELA_AVAILABLE_REPLIES + "  (" + COLUNA_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
			+ COLUNA_QUESTION_ID + " INTEGER NOT NULL,"
			+ COLUNA_AVAREPLY + " TEXT,"
			+ " FOREIGN KEY ( " + COLUNA_QUESTION_ID + " )  REFERENCES "
			+ QuestionsDAO.TABELA_QUESTIONS + " (" + QuestionsDAO.COLUNA_ID
			+ " ) ON DELETE CASCADE ON UPDATE CASCADE);";

	public long createAvaReply(AvailableReplies avaReply) {

		ContentValues values = ofAvaReplyForContentValues(avaReply);
		return mDb.insert(TABELA_AVAILABLE_REPLIES, null, values);
	}

	public static ContentValues ofAvaReplyForContentValues(AvailableReplies avaReply) {
		ContentValues values = new ContentValues();

		values.put(COLUNA_ID, avaReply.getId());
		values.put(COLUNA_QUESTION_ID, avaReply.getQuestion_id());
		values.put(COLUNA_AVAREPLY, avaReply.getAvaReply());

		return values;
	}

	/*public AvailableReplies getAvaReply(long idAvaReply) {

		Cursor c = consultAvaReply(idAvaReply);
		AvailableReplies avaReply = ofCursorFromAvaReply(c);
		c.close();

		return avaReply;
	}
*/
	public static AvailableReplies ofCursorFromAvaReply(Cursor c) {
		if (c == null || c.getCount() < 1) {
			return null;
		}
		AvailableReplies avaReply = new AvailableReplies();

		avaReply.setId(c.getLong(c.getColumnIndex(COLUNA_ID)));
		avaReply.setQuestion_id(c.getLong(c.getColumnIndex(COLUNA_QUESTION_ID)));
		avaReply.setAvaReply(c.getString(c.getColumnIndex(COLUNA_AVAREPLY)));

		return avaReply;
	}

	/*public boolean updateAvaReply(AvailableReplies avaReply) {

		ContentValues values = ofAvaReplyForContentValues(avaReply);
		return mDb.update(TABELA_AVAILABLE_REPLIES, values, COLUNA_ID + "=?",
				new String[] { String.valueOf(avaReply.getId()) }) > 0;
	}

	public boolean removeAvaReply(long idAvaReply) {

		return mDb.delete(TABELA_AVAILABLE_REPLIES, COLUNA_ID + "=?",
				new String[] { String.valueOf(idAvaReply) }) > 0;
	}
 	*/
	public String getAvaReply(long idAvaReply) {

		Cursor c = consultAvaReply(idAvaReply);

		String avaReply = c.getString(c.getColumnIndex(COLUNA_AVAREPLY));
		c.close();

		return avaReply;
	}
	

	public List<String> getListAvailableReplies(long idQuestion) {

		Cursor c = consultAvaRepliesByQuestion(idQuestion);
		List<String> listReplies = new ArrayList<String>();
		while(!c.isAfterLast()) {
			listReplies.add(c.getString(c.getColumnIndex(COLUNA_AVAREPLY)));
		c.moveToNext();
		}
			
		c.close();

		return listReplies;
	}



	// Retorna TUDO que estiver na tabela Emprestimos, assim como o método V1,
	// mas é bem mais simples.
	public Cursor consultAllAvaReplies() {

		return mDb
				.query(TABELA_AVAILABLE_REPLIES, null, null, null, null, null, null);
	}

	public Cursor consultAvaReply(long idAvaReply) throws SQLException {

		Cursor mCursor =

		mDb.query(true, TABELA_AVAILABLE_REPLIES, null, COLUNA_ID + "=?",
				new String[] { String.valueOf(idAvaReply) }, null, null,
				null, null);
		if (mCursor != null) {
			mCursor.moveToFirst();
		}
		return mCursor;

	}
	
	public Cursor consultAvaRepliesByQuestion(long idQuestion) throws SQLException {

		Cursor mCursor =

		mDb.query(true, TABELA_AVAILABLE_REPLIES, null, COLUNA_QUESTION_ID + "=?",
				new String[] { String.valueOf(idQuestion) }, null, null, null,
				null);
		if (mCursor != null) {
			mCursor.moveToFirst();
		}
		return mCursor;

	}

	/*public Cursor consultAvaReplyByQuestion(long idCategoria)
			throws SQLException {

		Cursor mCursor =

		mDb.query(true, TABELA_AVAILABLE_REPLIES, null, COLUNA_QUESTION_ID + "=?",
				new String[] { String.valueOf(idCategoria) }, null, null, null,
				null);
		if (mCursor != null) {
			mCursor.moveToFirst();
		}
		return mCursor;

	}*/

	public long getNumberOfAvaReply() {
		return DatabaseUtils.queryNumEntries(mDb, TABELA_AVAILABLE_REPLIES);
	}

}
