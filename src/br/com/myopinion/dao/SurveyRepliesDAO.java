package br.com.myopinion.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.SQLException;
import br.com.myopinion.model.Questions;
import br.com.myopinion.model.SurveyReplies;

public class SurveyRepliesDAO extends BasicDAO {

	public SurveyRepliesDAO(Context ctx) {
		super(ctx);
	}

	public static final String TABELA_SURVEY_REPLIES = "SURVEY_REPLIES";

	public static final String COLUNA_ID = "_id";
	public static final String COLUNA_CREATED_AT = "created_at";
	public static final String COLUNA_MAIN_SURVEY_ID = "main_survey_id";
	public static final String COLUNA_SYNC = "sync";

	public static final int SYNC_FALSE = 0;
	public static final int SYNC_TRUE = 1;
	
	public static final String SURVEY_REPLIES_CREATE_TABLE = "CREATE TABLE "
			+ TABELA_SURVEY_REPLIES + "  (" + COLUNA_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
			+ COLUNA_CREATED_AT + " INTEGER NOT NULL,"
			+ COLUNA_MAIN_SURVEY_ID + " INTEGER NOT NULL,"
			+ COLUNA_SYNC + " INTEGER NOT NULL,"
			+ " FOREIGN KEY ( " + COLUNA_MAIN_SURVEY_ID + " )  REFERENCES "
			+ MainSurveysDAO.TABELA_MAIN_SURVEYS + " (" + MainSurveysDAO.COLUNA_ID
			+ " ) ON DELETE CASCADE ON UPDATE CASCADE);";

	public long createSurveyReply(SurveyReplies surveyReply) {

		ContentValues values = ofSurveyRepliesForContentValues(surveyReply);
		return mDb.insert(TABELA_SURVEY_REPLIES, null, values);
	}

	public static ContentValues ofSurveyRepliesForContentValues(SurveyReplies surveyReply) {
		ContentValues values = new ContentValues();

		values.put(COLUNA_CREATED_AT, surveyReply.getCreated_at().getTime());
		values.put(COLUNA_MAIN_SURVEY_ID, surveyReply.getMain_survey_id());
		values.put(COLUNA_SYNC, surveyReply.getSync() ? SYNC_TRUE : SYNC_FALSE);

		return values;
	}

	public SurveyReplies getSurveyReply(long idSurveyReply) {

		Cursor c = consultSurveyReply(idSurveyReply);
		SurveyReplies surveyReply = ofCursorForSurveyReply(c);
		c.close();

		return surveyReply;
	}

	public static SurveyReplies ofCursorForSurveyReply(Cursor c) {
		if (c == null || c.getCount() < 1) {
			return null;
		}
		SurveyReplies surveyReply = new SurveyReplies();
		surveyReply.setId(c.getLong(c.getColumnIndex(COLUNA_ID)));
		surveyReply.setCreated_at(new Date(c.getLong(c.getColumnIndex(COLUNA_CREATED_AT))));
		surveyReply.setMain_survey_id(c.getLong(c.getColumnIndex(COLUNA_MAIN_SURVEY_ID)));
		surveyReply.setSync((c.getInt(c.getColumnIndex(COLUNA_SYNC)) == SYNC_TRUE));

		return surveyReply;
	}

	public boolean updateSurveyReply(SurveyReplies surveyReply) {

		ContentValues values = ofSurveyRepliesForContentValues(surveyReply);
		return mDb.update(TABELA_SURVEY_REPLIES, values, COLUNA_ID + "=?",
				new String[] { String.valueOf(surveyReply.getId()) }) > 0;
	}

	public boolean deleteSurveyReply(long idSurveyReply) {

		return mDb.delete(TABELA_SURVEY_REPLIES, COLUNA_ID + "=?",
				new String[] { String.valueOf(idSurveyReply) }) > 0;
	}

	public Date getCreatedAt(long idSurveyReply) {

		Cursor c = consultSurveyReply(idSurveyReply);

		Long mili = c.getLong(c.getColumnIndex(COLUNA_CREATED_AT));
		c.close();

		return new Date(mili);
	}

	
	// Retorna TUDO que estiver na tabela Emprestimos, assim como o método V1,
	// mas é bem mais simples.
	public Cursor consultAllSurveyReplies() {

		return mDb
				.query(TABELA_SURVEY_REPLIES, null, null, null, null, null, null);
	}

	public Cursor consultSurveyReply(long idSurveyReply) throws SQLException {

		Cursor mCursor =

		mDb.query(true, TABELA_SURVEY_REPLIES, null, COLUNA_ID + "=?",
				new String[] { String.valueOf(idSurveyReply) }, null, null,
				null, null);
		if (mCursor != null) {
			mCursor.moveToFirst();
		}
		return mCursor;

	}

	
	public List<SurveyReplies> getListSyncSurveyReplies(long idMainSurvey, int sync) {

		Cursor c = getSyncSurveyReplies(idMainSurvey, sync);
		List<SurveyReplies> listSurveyReplies = new ArrayList<SurveyReplies>();
		while(!c.isAfterLast()) {
			SurveyReplies surveyReply = ofCursorForSurveyReply(c);
			listSurveyReplies.add(surveyReply);
		c.moveToNext();
		}
			
		c.close();

		return listSurveyReplies;
	}
	
	public List<SurveyReplies> getListSurveyReplies(long idMainSurvey) {

		Cursor c = consultSurveyRepliesByMainSurvey(idMainSurvey);
		List<SurveyReplies> listSurveyReplies = new ArrayList<SurveyReplies>();
		while(!c.isAfterLast()) {
			SurveyReplies surveyReply = ofCursorForSurveyReply(c);
			listSurveyReplies.add(surveyReply);
		c.moveToNext();
		}
			
		c.close();

		return listSurveyReplies;
	}
	
	public Cursor consultSurveyRepliesByMainSurvey(long idMainSurvey)
			throws SQLException {

		Cursor mCursor =

		mDb.query(true, TABELA_SURVEY_REPLIES, null, COLUNA_MAIN_SURVEY_ID + "=?",
				new String[] { String.valueOf(idMainSurvey) }, null, null, COLUNA_CREATED_AT + " DESC",
				null);
		if (mCursor != null) {
			mCursor.moveToFirst();
		}
		return mCursor;

	}
	
	public Cursor getSyncSurveyReplies(long idMainSurvey, int sync){
		Cursor mCursor =

				mDb.query(true, TABELA_SURVEY_REPLIES, null, COLUNA_MAIN_SURVEY_ID + "=? " + "and " + COLUNA_SYNC + "=" + sync ,
						new String[] { String.valueOf(idMainSurvey) }, null, null, COLUNA_CREATED_AT + " DESC",
						null);
				if (mCursor != null) {
					mCursor.moveToFirst();
				}
				return mCursor;
	}

	public long getNumberOfSurveyReplies() {
		return DatabaseUtils.queryNumEntries(mDb, TABELA_SURVEY_REPLIES);
	}

}
