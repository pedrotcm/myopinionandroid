package br.com.myopinion.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.com.myopinion.model.MainSurveys;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.SQLException;

public class MainSurveysDAO extends BasicDAO {

	public MainSurveysDAO(Context ctx) {
		super(ctx);
	}

	public static final String TABELA_MAIN_SURVEYS = "MAIN_SURVEYS";

	public static final String COLUNA_ID = "_id";
	public static final String COLUNA_DESCRIPTION = "description";
	public static final String COLUNA_CREATED_AT = "created_at";
	public static final String COLUNA_VALIDATE = "validate";
	public static final String COLUNA_SURVEY_TYPE = "survey_type";
	public static final String COLUNA_INTERVAL = "interval";
	public static final String COLUNA_LONGITUDE = "longitude";
	public static final String COLUNA_LATITUDE = "latitude";
	public static final String COLUNA_RADIUS = "radius";
	public static final String COLUNA_DESCRIPTION_COMPLETE = "descriptionComplete";
	public static final String COLUNA_GETCOORDINATES = "getCoordinates";


	public static final int GETCOORDINATES_FALSE = 0;
	public static final int GETCOORDINATES_TRUE = 1;
	
	public static final String MAIN_SURVEY_CREATE_TABLE = "CREATE TABLE "
			+ TABELA_MAIN_SURVEYS + "  (" + COLUNA_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
			+ COLUNA_DESCRIPTION + " TEXT NOT NULL,"
			+ COLUNA_CREATED_AT + " INTEGER NOT NULL,"
			+ COLUNA_VALIDATE + " INTEGER NOT NULL,"
			+ COLUNA_SURVEY_TYPE + " TEXT NOT NULL,"
			+ COLUNA_INTERVAL + " INTEGER,"
			+ COLUNA_LATITUDE + " REAL,"
			+ COLUNA_LONGITUDE + " REAL,"
			+ COLUNA_RADIUS + " REAL,"	
			+ COLUNA_DESCRIPTION_COMPLETE + " TEXT,"
			+ COLUNA_GETCOORDINATES + " INTEGER"
			+ ");";

	public long createMainSurvey(MainSurveys mainSurvey) {

		ContentValues values = ofMainSurveyForContentValues(mainSurvey);
		return mDb.insert(TABELA_MAIN_SURVEYS, null, values);
	}

	public static ContentValues ofMainSurveyForContentValues(MainSurveys mainSurvey) {
		ContentValues values = new ContentValues();

		values.put(COLUNA_DESCRIPTION, mainSurvey.getDescription());
		values.put(COLUNA_CREATED_AT, mainSurvey.getCreated_at().getTime());
		values.put(COLUNA_VALIDATE, mainSurvey.getValidate().getTime());
		values.put(COLUNA_SURVEY_TYPE, mainSurvey.getSurveyType());
		values.put(COLUNA_INTERVAL, mainSurvey.getInterval());
		values.put(COLUNA_LATITUDE, mainSurvey.getLatitude());
		values.put(COLUNA_LONGITUDE, mainSurvey.getLongitude());
		values.put(COLUNA_RADIUS, mainSurvey.getRadius());
		values.put(COLUNA_DESCRIPTION_COMPLETE, mainSurvey.getDescriptionComplete());
		values.put(COLUNA_GETCOORDINATES, mainSurvey.getGetCoordinates() ? GETCOORDINATES_TRUE : GETCOORDINATES_FALSE);
		
		return values;
	}

	public MainSurveys getMainSurvey(long idMainSurvey) {

		Cursor c = consultMainSurvey(idMainSurvey);
		MainSurveys mainSurvey = ofCursorForMainSurvey(c);
		c.close();

		return mainSurvey;
	}

	public static MainSurveys ofCursorForMainSurvey(Cursor c) {
		if (c == null || c.getCount() < 1) {
			return null;
		}
		MainSurveys mainSurvey = new MainSurveys();
		mainSurvey.setId(c.getLong(c.getColumnIndex(COLUNA_ID)));
		mainSurvey.setDescription(c.getString(c.getColumnIndex(COLUNA_DESCRIPTION)));
		mainSurvey.setSurveyType(c.getString(c.getColumnIndex(COLUNA_SURVEY_TYPE)));
		mainSurvey.setCreated_at(new Date(c.getLong(c.getColumnIndex(COLUNA_CREATED_AT))));
		mainSurvey.setValidate(new Date(c.getLong(c.getColumnIndex(COLUNA_VALIDATE))));
		mainSurvey.setInterval(c.getInt(c.getColumnIndex(COLUNA_INTERVAL)));
		mainSurvey.setLatitude(c.getDouble(c.getColumnIndex(COLUNA_LATITUDE)));
		mainSurvey.setLongitude(c.getDouble(c.getColumnIndex(COLUNA_LONGITUDE)));
		mainSurvey.setRadius(c.getDouble(c.getColumnIndex(COLUNA_RADIUS)));
		mainSurvey.setDescriptionComplete(c.getString(c.getColumnIndex(COLUNA_DESCRIPTION_COMPLETE)));
		mainSurvey.setGetCoordinates((c.getInt(c.getColumnIndex(COLUNA_GETCOORDINATES)) == GETCOORDINATES_TRUE));


		return mainSurvey;
	}

	public boolean updateMainSurvey(MainSurveys mainSurvey) {

		ContentValues values = ofMainSurveyForContentValues(mainSurvey);
		return mDb.update(TABELA_MAIN_SURVEYS, values, COLUNA_ID + "=?",
				new String[] { String.valueOf(mainSurvey.getId()) }) > 0;
	}

	public boolean deleteMainSurvey(long idMainSurvey) {

		return mDb.delete(TABELA_MAIN_SURVEYS, COLUNA_ID + "=?",
				new String[] { String.valueOf(idMainSurvey) }) > 0;
	}
	
	
	//exemplo de pegar uma coluna
	public String getSurveyType(long idMainSurvey) {

		Cursor c = consultMainSurvey(idMainSurvey);

		String surveyType = c.getString(c.getColumnIndex(COLUNA_SURVEY_TYPE));
		c.close();

		return surveyType;
	}
	
	// Retorna TUDO que estiver na tabela
	public Cursor consultAllMainSurveys() {

		return mDb
				.query(TABELA_MAIN_SURVEYS, null, null, null, null, null, null);
	}

	public Cursor consultMainSurvey(long idMainSurvey) throws SQLException {

		Cursor mCursor =

		mDb.query(true, TABELA_MAIN_SURVEYS, null, COLUNA_ID + "=?",
				new String[] { String.valueOf(idMainSurvey) }, null, null,
				null, null);
		if (mCursor != null) {
			mCursor.moveToFirst();
		}
		return mCursor;

	}

	public Cursor consultMainSurveyBySurveyType(String surveyType)
			throws SQLException {

		Cursor mCursor =

		mDb.query(true, TABELA_MAIN_SURVEYS, null, COLUNA_SURVEY_TYPE + "=?",
				new String[] { surveyType }, null, null, COLUNA_ID + " DESC",
				null);
		if (mCursor != null) {
			mCursor.moveToFirst();
		}
		return mCursor;

	}

	//numero de MainSurveys
	public long getNumberOfMainSurvey() {
		return DatabaseUtils.queryNumEntries(mDb, TABELA_MAIN_SURVEYS);
	}

}
