package br.com.myopinion.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.SQLException;
import br.com.myopinion.model.AvailableReplies;
import br.com.myopinion.model.OtherQuestion;
import br.com.myopinion.model.OthersReplies;

public class OtherQuestionDAO extends BasicDAO {

	public OtherQuestionDAO(Context ctx) {
		super(ctx);
	}

	public static final String TABELA_OTHER_QUESTION = "OTHER_QUESTION";
	public static final String COLUNA_ID = "_id";
	public static final String COLUNA_TITLE = "title";
	public static final String COLUNA_TEXTTYPE= "text_type";
	public static final String COLUNA_QUESTION_TYPE = "question_type";
	public static final String COLUNA_AVAILABLE_REPLY_ID = "ava_reply_id";

	public static final String OTHER_QUESTION_CREATE_TABLE = "CREATE TABLE "
			+ TABELA_OTHER_QUESTION + "  (" + COLUNA_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
			+ COLUNA_TITLE + " TEXT NOT NULL,"
			+ COLUNA_TEXTTYPE + " TEXT,"
			+ COLUNA_QUESTION_TYPE + " TEXT NOT NULL,"
			+ COLUNA_AVAILABLE_REPLY_ID + " INTEGER NOT NULL,"
			+ " FOREIGN KEY ( " + COLUNA_AVAILABLE_REPLY_ID + " )  REFERENCES "
			+ AvailableRepliesDAO.TABELA_AVAILABLE_REPLIES + " (" + AvailableRepliesDAO.COLUNA_ID
			+ " ) ON DELETE CASCADE ON UPDATE CASCADE);";

	public long createOtherQuestion(OtherQuestion otherQuestion) {

		ContentValues values = ofOtherQuestionForContentValues(otherQuestion);
		return mDb.insert(TABELA_OTHER_QUESTION, null, values);
	}

	public static ContentValues ofOtherQuestionForContentValues(OtherQuestion otherQuestion) {
		ContentValues values = new ContentValues();

		values.put(COLUNA_TITLE, otherQuestion.getTitle());
		values.put(COLUNA_TEXTTYPE, otherQuestion.getTextType());
		values.put(COLUNA_QUESTION_TYPE, otherQuestion.getQuestion_type());
		values.put(COLUNA_AVAILABLE_REPLY_ID, otherQuestion.getAvailable_reply_id());
		return values;
	}

	/*public AvailableReplies getAvaReply(long idAvaReply) {

		Cursor c = consultAvaReply(idAvaReply);
		AvailableReplies avaReply = ofCursorFromAvaReply(c);
		c.close();

		return avaReply;
	}
*/
	public static OtherQuestion ofCursorFromOtherQuestion(Cursor c) {
		if (c == null || c.getCount() < 1) {
			return null;
		}
		OtherQuestion otherQuestion = new OtherQuestion();

		otherQuestion.setId(c.getLong(c.getColumnIndex(COLUNA_ID)));
		otherQuestion.setTitle(c.getString(c.getColumnIndex(COLUNA_TITLE)));
		otherQuestion.setTextType(c.getString(c.getColumnIndex(COLUNA_TEXTTYPE)));
		otherQuestion.setQuestion_type(c.getString(c.getColumnIndex(COLUNA_QUESTION_TYPE)));
		otherQuestion.setAvailable_reply_id(c.getLong(c.getColumnIndex(COLUNA_AVAILABLE_REPLY_ID)));

		return otherQuestion;
	}

	/*public boolean updateAvaReply(AvailableReplies avaReply) {

		ContentValues values = ofAvaReplyForContentValues(avaReply);
		return mDb.update(TABELA_AVAILABLE_REPLIES, values, COLUNA_ID + "=?",
				new String[] { String.valueOf(avaReply.getId()) }) > 0;
	}

	public boolean removeAvaReply(long idAvaReply) {

		return mDb.delete(TABELA_AVAILABLE_REPLIES, COLUNA_ID + "=?",
				new String[] { String.valueOf(idAvaReply) }) > 0;
	}

	public String getReply(long idAvaReply) {

		Cursor c = consultAvaReply(idAvaReply);

		String reply = c.getString(c.getColumnIndex(COLUNA_AVAREPLY));
		c.close();

		return reply;
	}*/
	

	public OtherQuestion getOtherQuestion(long idAvailableReply) {

		Cursor c = consultOtherQuestionByAvailableReply(idAvailableReply);
		OtherQuestion otherQuestion;
		if (c != null) {
			otherQuestion = ofCursorFromOtherQuestion(c);	
		} else
			return null;
		
		return otherQuestion;
	}



	// Retorna TUDO que estiver na tabela
	public Cursor consultAllOtherQuestions() {

		return mDb
				.query(TABELA_OTHER_QUESTION, null, null, null, null, null, null);
	}

	/*public Cursor consultAvaReply(long idAvaReply) throws SQLException {

		Cursor mCursor =

		mDb.query(true, TABELA_AVAILABLE_REPLIES, null, COLUNA_ID + "=?",
				new String[] { String.valueOf(idAvaReply) }, null, null,
				null, null);
		if (mCursor != null) {
			mCursor.moveToFirst();
		}
		return mCursor;

	}*/
	
	public Cursor consultOtherQuestionByAvailableReply(long idAvailableReply) throws SQLException {

		Cursor mCursor =

		mDb.query(true, TABELA_OTHER_QUESTION, null, COLUNA_AVAILABLE_REPLY_ID + "=?",
				new String[] { String.valueOf(idAvailableReply) }, null, null, null,
				null);
		if (mCursor != null) {
			mCursor.moveToFirst();
		}
		return mCursor;

	}

	/*public Cursor consultAvaReplyByQuestion(long idCategoria)
			throws SQLException {

		Cursor mCursor =

		mDb.query(true, TABELA_AVAILABLE_REPLIES, null, COLUNA_QUESTION_ID + "=?",
				new String[] { String.valueOf(idCategoria) }, null, null, null,
				null);
		if (mCursor != null) {
			mCursor.moveToFirst();
		}
		return mCursor;

	}*/

	public long getNumberOfOtherQuestions() {
		return DatabaseUtils.queryNumEntries(mDb, TABELA_OTHER_QUESTION);
	}

}
