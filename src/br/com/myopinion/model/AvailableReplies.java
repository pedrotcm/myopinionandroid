package br.com.myopinion.model;

import java.io.Serializable;

public class AvailableReplies implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8846915326385094411L;
	/**
	 * 
	 */
	private Long id;
    private String avaReply;
    private Long question_id;
    
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getAvaReply() {
		return avaReply;
	}
	public void setAvaReply(String avaReply) {
		this.avaReply = avaReply;
	}
	public long getQuestion_id() {
		return question_id;
	}
	public void setQuestion_id(long question_id) {
		this.question_id = question_id;
	}
    
    
}
