package br.com.myopinion.model;

import java.io.Serializable;
import java.util.Date;

public class MainSurveys implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -482475036007776124L;
	private Long id;
	private String description;
	private Date created_at;
	private Date validate;
	private String surveyType;
	private Integer interval;
	private Double latitude;
	private Double longitude;
	private Double radius;
	private String descriptionComplete;
	private Boolean getCoordinates = false;
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Date getCreated_at() {
		return created_at;
	}
	public void setCreated_at(Date created_at) {
		this.created_at = created_at;
	}
	public Date getValidate() {
		return validate;
	}
	public void setValidate(Date validate) {
		this.validate = validate;
	}
	public String getSurveyType() {
		return surveyType;
	}
	public void setSurveyType(String surveyType) {
		this.surveyType = surveyType;
	}
	public Integer getInterval() {
		return interval;
	}
	public void setInterval(Integer interval) {
		this.interval = interval;
	}
	public Double getLatitude() {
		return latitude;
	}
	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}
	public Double getLongitude() {
		return longitude;
	}
	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}
	public Double getRadius() {
		return radius;
	}
	public void setRadius(Double radius) {
		this.radius = radius;
	}
	public String getDescriptionComplete() {
		return descriptionComplete;
	}
	public void setDescriptionComplete(String descriptionComplete) {
		this.descriptionComplete = descriptionComplete;
	}
	public Boolean getGetCoordinates() {
		return getCoordinates;
	}
	public void setGetCoordinates(Boolean getCoordinates) {
		this.getCoordinates = getCoordinates;
	}
		
}
