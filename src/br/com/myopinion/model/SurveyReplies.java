package br.com.myopinion.model;

import java.io.Serializable;
import java.util.Date;

public class SurveyReplies implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2951696186239378021L;
	private Long id;
    private Date created_at;
    private Long main_survey_id;
    private Boolean sync = false;
    
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Date getCreated_at() {
		return created_at;
	}
	public void setCreated_at(Date created_at) {
		this.created_at = created_at;
	}
	public long getMain_survey_id() {
		return main_survey_id;
	}
	public void setMain_survey_id(long main_survey_id) {
		this.main_survey_id = main_survey_id;
	}
	public Boolean getSync() {
		return sync;
	}
	public void setSync(Boolean sync) {
		this.sync = sync;
	}	
}
