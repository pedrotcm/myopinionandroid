package br.com.myopinion.model;

import java.io.Serializable;

public class Sections implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3248800144327735509L;
	private Long id;
    private Long main_survey_id;
    private String title;
    private Boolean required;
    private String sectionType;
    
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public long getMain_survey_id() {
		return main_survey_id;
	}
	public void setMain_survey_id(Long main_survey_id) {
		this.main_survey_id = main_survey_id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public Boolean getRequired() {
		return required;
	}
	public void setRequired(Boolean required) {
		this.required = required;
	}
	public String getSectionType() {
		return sectionType;
	}
	public void setSectionType(String sectionType) {
		this.sectionType = sectionType;
	}
	
}
