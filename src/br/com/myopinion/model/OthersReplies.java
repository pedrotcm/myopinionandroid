package br.com.myopinion.model;

import java.io.Serializable;

public class OthersReplies implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3752252465339424781L;
    private String otherReply;
    private Long other_question_id;
	public String getOtherReply() {
		return otherReply;
	}
	public void setOtherReply(String otherReply) {
		this.otherReply = otherReply;
	}
	public Long getOther_question_id() {
		return other_question_id;
	}
	public void setOther_question_id(Long other_question_id) {
		this.other_question_id = other_question_id;
	}
    
	
	
}
