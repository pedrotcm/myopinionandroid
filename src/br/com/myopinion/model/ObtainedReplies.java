package br.com.myopinion.model;

import java.io.Serializable;

public class ObtainedReplies implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1808052196631985847L;
	private Long id;
    private String obtReply;
    private boolean other_answer;
    private long question_id;
    private long survey_reply_id;
    
    public ObtainedReplies(){
    	other_answer = false;
    }
    
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getObtReply() {
		return obtReply;
	}
	public void setObtReply(String obtReply) {
		this.obtReply = obtReply;
	}
	public boolean isOther_answer() {
		return other_answer;
	}
	public void setOther_answer(boolean other_answer) {
		this.other_answer = other_answer;
	}
	public long getQuestion_id() {
		return question_id;
	}
	public void setQuestion_id(long question_id) {
		this.question_id = question_id;
	}
	public long getSurvey_reply_id() {
		return survey_reply_id;
	}
	public void setSurvey_reply_id(long survey_reply_id) {
		this.survey_reply_id = survey_reply_id;
	}
    
    
}
