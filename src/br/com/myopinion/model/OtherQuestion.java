package br.com.myopinion.model;

import java.io.Serializable;

public class OtherQuestion implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6734885470940281801L;
	private Long id;
    private String title;
    private String question_type;
    private String textType;
    private Long available_reply_id;
    
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getQuestion_type() {
		return question_type;
	}
	public void setQuestion_type(String question_type) {
		this.question_type = question_type;
	}
	public Long getAvailable_reply_id() {
		return available_reply_id;
	}
	public void setAvailable_reply_id(Long available_reply_id) {
		this.available_reply_id = available_reply_id;
	}
	public String getTextType() {
		return textType;
	}
	public void setTextType(String textType) {
		this.textType = textType;
	}
	
}
