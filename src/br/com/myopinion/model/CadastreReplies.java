package br.com.myopinion.model;

import java.io.Serializable;
import java.util.Date;

public class CadastreReplies implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1073687750972909184L;
	/**
	 * 
	 */
	private Long id;
    private Long cadastre_question_id;
    private Long survey_reply_id;
    
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public long getCadastre_question_id() {
		return cadastre_question_id;
	}
	public void setCadastre_question_id(long cadastre_question_id) {
		this.cadastre_question_id = cadastre_question_id;
	}
	public Long getSurvey_reply_id() {
		return survey_reply_id;
	}
	public void setSurvey_reply_id(Long survey_reply_id) {
		this.survey_reply_id = survey_reply_id;
	}
        
}
