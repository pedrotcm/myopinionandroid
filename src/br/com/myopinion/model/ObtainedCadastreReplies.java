package br.com.myopinion.model;

import java.io.Serializable;

public class ObtainedCadastreReplies implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2917112020458674868L;
	/**
	 * 
	 */
	private Long id;
    private String obtReply;
    private long question_id;
    private long cadastre_reply_id;
    
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getObtReply() {
		return obtReply;
	}
	public void setObtReply(String obtReply) {
		this.obtReply = obtReply;
	}
	public long getQuestion_id() {
		return question_id;
	}
	public void setQuestion_id(long question_id) {
		this.question_id = question_id;
	}
	public long getCadastre_reply_id() {
		return cadastre_reply_id;
	}
	public void setCadastre_reply_id(long cadastre_reply_id) {
		this.cadastre_reply_id = cadastre_reply_id;
	}
    
    
}
