package br.com.myopinion.model;

import java.io.Serializable;

public class Questions implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1750360364869359429L;
	private Long id;
    private Long main_survey_id;
    private String title;
    private Boolean required;
    private Boolean test;
    private String questionType;
    private String textType;
    private Long question_id;
    
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public long getMain_survey_id() {
		return main_survey_id;
	}
	public void setMain_survey_id(Long main_survey_id) {
		this.main_survey_id = main_survey_id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public Boolean getRequired() {
		return required;
	}
	public void setRequired(Boolean required) {
		this.required = required;
	}
	public Boolean getTest() {
		return test;
	}
	public void setTest(Boolean test) {
		this.test = test;
	}
	public String getQuestionType() {
		return questionType;
	}
	public void setQuestionType(String questionType) {
		this.questionType = questionType;
	}
	public String getTextType() {
		return textType;
	}
	public void setTextType(String textType) {
		this.textType = textType;
	}
	public Long getQuestion_id() {
		return question_id;
	}
	public void setQuestion_id(Long question_id) {
		this.question_id = question_id;
	}
    	
}
